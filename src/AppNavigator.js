/**************************************************************
 *  FileName:           AppNavigator.js
 *  Description:        App Custom Wrapper/Navigation Configuration
 *  Copyright:          Binhi-MeDFI © 2017
 *  Original Author:    Jovanni Auxilio
 *  Date Created:       2017-11-07

 *  Modification History:
        Date              By            Description

**************************************************************/
import React from "react";
import { StackNavigator, TabNavigator } from "react-navigation";
import { Easing, Animated } from 'react-native';

import Login from './containers/Login';
import EmployeeDTR from './containers/DTR';
import EmprDashBoard from './containers/DashBoard';
import RootDrawer from './Drawer';
import ChangePassword from './containers/ChangePassword';
import BranchForm from './containers/CompanyProfile/forms/branch';
import CompanyIdForm from './containers/CompanyProfile/forms/companyid';
import PayrollTransaction from './containers/PayrollTransaction';

//TabNavigators
import {AddEmployeeForm} from './TabNavigatorEmployeeForm';
import EmployeeListTabs from './containers/Employees/list/tabs';
import ResolvePending from './containers/Transactions/resolvePending';
import EmployerProfile from './containers/Employer/profile';

/******CHILD NAVIGATORS******/
const BranchFormNav = StackNavigator(
    { BranchForm: {screen: BranchForm} },
    { headerMode: 'screen' }
);

const EmployerProfileNav = StackNavigator(
    { EmployerProfile:{screen: EmployerProfile}},
    { headerMode: 'screen' }
);

const CompanyIdFormNav = StackNavigator(
    { CompanyIdForm: {screen: CompanyIdForm} }
);

const AddEmployeeFormNav = StackNavigator(
    { AddEmployeeForm: {screen: AddEmployeeForm} },
    { headerMode: 'screen' }
);

const EmployeeListTabsNav = StackNavigator(
    { EmployeeListTabs: {screen: EmployeeListTabs} },
    { headerMode: 'screen' }
);

const EmployeeDTRNav = StackNavigator(
    { EmployeeDTR: {screen: EmployeeDTR}},
    { headerMode: 'screen' }
);

const PayrollTransactionNav = StackNavigator(
    { PayrollTransaction: {screen: PayrollTransaction} },
    { headerMode: 'none' }
);

const ResolvePendingNav = StackNavigator(
    { ResolvePending: {screen: ResolvePending} },
    { headerMode: 'none' }
);

/******APP NAVIGATOR******/
const AppNavigator = StackNavigator(
    {
        RootDrawer: { screen: RootDrawer },
        Login: {screen: Login},
        ChangePassword: {screen: ChangePassword},
        BranchFormNav: {screen: BranchFormNav},
        CompanyIdFormNav: {screen: CompanyIdFormNav},
        EmployeeDTR: {screen: EmployeeDTRNav},

        //Employee
        AddEmployeeForm: {screen: AddEmployeeFormNav},
        EmployeeListTabs: {screen: EmployeeListTabsNav},

        //Payroll Summary
        PayrollTransaction: {screen: PayrollTransactionNav},
        ResolvePending:{screen:ResolvePendingNav}
    },

    {
        initialRouteName: 'Login',
        headerMode: 'none',
        mode: 'modal',
        navigationOptions: {
          gesturesEnabled: false,
        },
        transitionConfig: () => ({
            transitionSpec: {
                duration: 500,
                easing: Easing.out(Easing.poly(4)),
                timing: Animated.timing,
            },
            screenInterpolator: sceneProps => {
                const { layout, position, scene } = sceneProps;
                const { index } = scene;
        
                const height = layout.initHeight;
                const translateY = position.interpolate({
                    inputRange: [index - 1, index, index + 1],
                    outputRange: [height, 0, 0],
                });
        
                const opacity = position.interpolate({
                    inputRange: [index - 1, index - 0.1, index],
                    outputRange: [0, 1, 1],
                });
        
                return { opacity/* , transform: [{ translateY }]  */};
            },
        }),
    }
);

export default AppNavigator;
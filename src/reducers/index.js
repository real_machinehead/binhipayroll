import {combineReducers} from 'redux';
import loginReducer from './loginReducer';
import activeCompanyReducer from './activeCompany';
import routeHistoryReducer from './routeHistory';
import activeBranchReducer from './activeBranch';
import dataActionTriggerReducer  from './dataActionTrigger';
import {reducer as companyPoliciesReducer} from './../containers/CompanyPolicies/data/reducer';
import {employees} from './../containers/Employees/data/reducers';
import {companyProfile} from './../containers/CompanyProfile/data/reducer';
import { reducer as dtrReducer } from './../containers/DTR/data/reducer';
import { reducer as transactionsReducer } from './../containers/Transactions/data/reducer';
import { reducer as payrollTransactionReducer } from './../containers/PayrollTransaction/data/reducer';
import { reducer as servicesReducer } from '../services/reducer';
import { reducer as reportsReducer } from '../containers/Reports/data/reducer';
import { reducer as codelistReducer } from '../containers/Codelist/data/reducer';
import { reducer as notificationsReducer } from '../containers/Notifications/data/reducer';
import { reducer as statisticsReducer } from '../containers/DashBoard/statistics/data/reducer';
import { reducer as employeepayslipReducer } from './../containers/Employees/profile/reports/data/reducer';

const rootReducer= combineReducers({
    loginReducer,
    activeCompanyReducer,
    routeHistoryReducer,
    activeBranchReducer,
    dataActionTriggerReducer,
    companyPoliciesReducer,
    employees,
    companyProfile,
    dtr: dtrReducer,
    transactions: transactionsReducer,
    payrolltransaction: payrollTransactionReducer,
    services: servicesReducer,
    reports: reportsReducer,
    codelist: codelistReducer,
    notifications: notificationsReducer,
    statistics: statisticsReducer,
    employeepayslip:employeepayslipReducer,
});

export default rootReducer;
import {store} from '../store';
import { bankInfo } from '../containers/Employees/data/activeProfile/api';

export const getActiveUsername = () => {
	return store.getState().loginReducer.logininfo.resUsername;
};

export const getActiveUserId = () => {
	return store.getState().loginReducer.logininfo.userid;
};

export const getActiveCompany = () => {
	return store.getState().activeCompanyReducer.activecompany.id;
};


export let baseURL = {
	activeCompany: function(){
		return(
		'user/' + getActiveUserId() +
		'/company/' + getActiveCompany()
		)
 	}
}

export let newBaseURL = {
	activeCompany: function(){
		return(
		'company/' + getActiveCompany()
		)
 	}
}

export let notifications = {
	get: function(payload){
		return(
			'reports/notification/' + 
			payload.period + 
			'/?company=' + 
			getActiveCompany() + 
			'&filter=' + 
			payload.filter + 
			'&values=' +
			payload.identifier + '|' +
			payload.employeeid + '|' +
			payload.date + '|' +
			payload.status + '|' +
			payload.showdeleted + '|' +
			'&&page=' + 
			payload.page
 		);
	},
}

export let codelist = {
	employmenttype: function(payload){
		return(
			'codelist/employmentoption'
		);
	},
	paytype: function(payload){
		return(
			'codelist/paytypeoption/?compid='+
			getActiveCompany()
		);
	},
	defaultgovbenefits: function(payload){
		return(
			'codelist/governmentbenefits/?compid=' +
			getActiveCompany()
		);
	},

}

export let company = {
	branch: function(payload){
		console.log('testttttttttttttttttttttttttt',payload);
		return(
			baseURL.activeCompany() +
			'/branch'
		)
	}
}

export let account = {
	timein: function(payload){
		return(
			'timein.php'
		);
	},

	timeout: function(payload){
		return(
			'timeout.php'
		);
	},

	forgotPassword: function(payload){
		return(
			'forgotpassword.php'
		)
	}
}

export let employee = {
	create: function(payload){
		return(
			'maintenance/employer/addemployee'
		)
	},

	draft: {
		get: function(){
			return(
				'maintenance/employee/drafts'
			)
		},

		getProfile: function(payload){
			return(
				'maintenance/employee/form/?employeeid=' +
				payload.employeeid +
				'&companyid=' +
				getActiveCompany()
			)
		},
	},

	defaultData: function(){
		return(
			'maintenance/employee/form/?companyid=' + getActiveCompany()
		)
	},

	list: function(payload){
		return(
			'maintenance/employee/list/?company=' +
			getActiveCompany() +
			(payload.page ? ('&page=' + payload.page) : '')

		)
	},

	search:function(payload){
		return(
			'maintenance/employee/list/?company=' +
			getActiveCompany() +
			(payload.page ? ('&page=' + payload.page) : '') + 
			(payload.lastname ? ('&lastname=' + payload.lastname) : '')
		)
	},
	allInfo: function(payload){
		return(
			'/maintenance/employee/reload/?employee=' +
			payload.employeeid
		)
	},
	
	//Deprecated
	basicInfo: function(payload){
		return(
			baseURL.activeCompany() + 
			'/employee/' + payload + 
			'/personalinfo/basicinfo'
		)
	 },
	 
	personalinfo: {
		add: function(payload){
			return(
				'maintenance/employer/addemployee/?index=' +
				payload.index
			)
		},

		create: function(){
			return(
				baseURL.activeCompany() +
				'/employee/personalinfo'
			)
		},

		basicinfo: {
			update: function(payload){
				return(
					baseURL.activeCompany() +
					'/employee/' + payload.id +
					'/personalinfo/' + 'basicinfo'
				)
			},
		},

		address: {
			update: function(payload){
				return(
					baseURL.activeCompany() +
					'/employee/' + payload.id +
					'/personalinfo/' + 'address'
				)
			}
		},

		family: {
			update: function(payload){
				return(
					baseURL.activeCompany() +
					'/employee/' + payload.id +
					'/personalinfo/' + 'family'
				)
			}
		},
	},

	bankinfo: {
		update: function(payload){
			return(
				baseURL.activeCompany() +
				'/employee/'  + payload.id + 
				'/bankinfo'
			)
		},
		add: function(payload){
			return(
				'maintenance/employer/addemployee/?index=' +
				payload.index
			)
		},
	},

	employmentinfo: {
		workshift: {
			add: function(payload){
				return(
					baseURL.activeCompany() +
					'/employee/'  + payload.employeeId + 
					'/employmentinfo/workshift/'
				)
			},
			update: function(payload){
				return(
					baseURL.activeCompany() +
					'/employee/'  + payload.employeeId + 
					'/employmentinfo/workshift/' + payload.id
				)
			},
			delete: function(payload){
				return(
					baseURL.activeCompany() +
					'/employee/'  + payload.employeeId + 
					'/employmentinfo/workshift/' + payload.id
				)
			}
		},
		benefits: {
			add: function(payload){
				return(
					baseURL.activeCompany() +
					'/employee/'  + payload.employeeId + 
					'/employmentinfo/benefits/'
				)
			},
			delete: function(payload){
				return(
					baseURL.activeCompany() +
					'/employee/'  + payload.employeeId + 
					'/employmentinfo/benefits/' + payload.id
				)
			},
			request: function(payload){
				return(
					'maintenance/employer/addemployee/?index=7'
				)
			}
		},
		
		rank: {
			add: function(payload){
				return(
					baseURL.activeCompany() +
					'/employee/'  + payload.employeeId + 
					'/employmentinfo/ranks/'
				)
			},
			update: function(payload){
				return(
					baseURL.activeCompany() +
					'/employee/'  + payload.employeeId + 
					'/employmentinfo/ranks/' + payload.rank.data.id
				)
			},
			delete: function(payload){
				return(
					baseURL.activeCompany() +
					'/employee/'  + payload.employeeId + 
					'/employmentinfo/ranks/' + payload.id
				)
			},
		},

		/*export let company = {
			branch: function(payload){
				return(
					baseURL.activeCompany() +
					'/branch'
				)
			}
		}*/

		details: {
			add: function(payload){
				return(
					baseURL.activeCompany() +
					'/employee/'  + payload.employeeId + 
					'/employmentinfo/details/'
				)
			},
			update: function(payload){
				return(
					baseURL.activeCompany() +
					'/employee/'  + payload.employeeId + 
					'/employmentinfo/details/' + payload.employmentinfo.details.id
				)
			},
			delete: function(payload){
				return(
					baseURL.activeCompany() +
					'/employee/'  + payload.employeeId + 
					'/employmentinfo/details/' + payload.id
				)
			}
		}

	},
}

export let policy = {
	ranks: {
		get: function(payload){
			return(
				baseURL.activeCompany() +
				'/policies/ranks/'
			)
		},

		create: function(payload){
			return(
				policy.ranks.get()
			)
		},

		remove: function(payload){
			return(
				policy.ranks.get() + 
				payload.data.id
			)
		},

		update: function(payload){
			return(
				policy.ranks.get() + 
				payload.data.id
			)
		},
	},
	branch:{
		get: function(payload){
			return(
				baseURL.activeCompany() +
				'/branch'
			)			
		},
		add: function(payload){
			return(
				'/forms/companyprofile.php'
			)			
		},
		update: function(payload){
			return(
				'/forms/companyprofile.php'
			)			
		},
		remove:function(payload){
			return(
				'/forms/companyprofile.php'
			)			
		}	
		//'forms/companyprofile.php',
	},
	companyid:{
		get: function(payload){
			console.log('logggggggssssssssssssss',payload);
			return(
				'/forms/getcompanyprofile.php'
			)			
		},
		add: function(payload){
			return(
				'/forms/companyprofile.php'
			)			
		},
		update:function(payload){
			return(
				'/forms/updatecompanyprofile.php'
			)			
		},
		remove:function(payload){
			return(
				'/forms/companyprofile.php'
			)			
		}		
		//'forms/companyprofile.php',
	},

	holidays:{
		get: function(payload){
			return(
				'calendar/list/?company='+ getActiveCompany()
			)
		},
		update: function(payload){
			return(
				baseURL.activeCompany() +
				'/holidays/' + 
				payload.id
			)
		},
		remove: function(payload){
			return(
				baseURL.activeCompany() +
				'/holidays/' +
				payload.id 
			)
		},
		add: function(payload){
			return(
				baseURL.activeCompany() +
				'/holidays'
			)
		},
	},
	positions: {
		get: function(payload){
			return(
				baseURL.activeCompany() +
				'/position'
			)
		},
		update: function(payload){
			return(
				baseURL.activeCompany() +
				'/position/' + 
				payload.id
			)
		},
		remove: function(payload){
			return(
				baseURL.activeCompany() +
				'/position/' +
				payload.id 
			)
		},
		add: function(payload){
			return(
				baseURL.activeCompany() +
				'/position'
			)
		},
	},

	payroll: {
		get: function(payload){
			return(
				newBaseURL.activeCompany() +
				'/policy/payroll'
			)
		},

		switchType: function(payload){
			return(
				newBaseURL.activeCompany() +
				'/policy/payroll/switch'
			)
		},

		setPayDay: function(payload){
			return(
				newBaseURL.activeCompany() +
				'/policy/payroll/' + 
				(payload.paytype.value).replace("-", "") +
				'/payday'
			)
		},

		setCutOff: function(payload){
			return(
				newBaseURL.activeCompany() +
				'/policy/payroll/' + 
				(payload.paytype.value).replace("-", "") +
				'/cutoff'
			)
		},

		setFirstPayDate: function(payload){
			return(
				newBaseURL.activeCompany() +
				'/policy/payroll/' + 
				(payload.paytype.value).replace("-", "") +
				'/paydate'
			)
		},

		setIsMovedLastDay: function(payload){
			return(
				newBaseURL.activeCompany() +
				'/policy/payroll/' + 
				(payload.paytype.value).replace("-", "") +
				'/ismovelastday'
			)
		},
	},

	savings: {
		get: function(payload){
			return(
				newBaseURL.activeCompany() +
				'/policy/employeesavings/'
			)
		},

		update: function(payload){
			return(
				newBaseURL.activeCompany() +
				'/policy/employeesavings/'
			)
		}
	}
}

export let reports = {
	dtr: {
		get: function(payload){
			return(
				baseURL.activeCompany() +
				'/employee/'  + payload.employeeid + 
				'/reports/dtr/' + payload.payrollid
			)
		},

		update: function(payload){
			console.log('payloadsssss',payload);
			return(
				baseURL.activeCompany() +
				'/employee/'  + payload.employeeid + 
				'/reports/dtr/'
			)
		},
	},
	

	payrollSchedule: {
		get: function(payload){
			return(
				'payrollschedule/all?filter=COMPANY&'+
				'id=' + getActiveCompany()
			)
		},
	},

	payrollReports: {
		/*get: function(payload){
			return(
				'payrollschedule/all?filter=COMPANY&'+
				'id=' + getActiveCompany()
			)
		},*/

		get: function(payload){
			return(
				'payrolllist/?company=' + getActiveCompany()
			)
		},
	},

	creditInstruction:{
		get: function(payload){
			return(
				'generatecreditinstructionbycompany/?company='+payload.compId+'&paydate='+payload.payDate+'&employer='+payload.employerId
			)
		},
	},

	employeeSummary:{
		get: function(payload){
			return(
				'generatepayrollemployeesummary/?company='+payload.compId+'&email='+payload.email
			)
		},
	},
	
	
	monetaryAdjustment: {
		get: function(payload){
			return(
				'transaction/adjustment/filter/' +
				payload.filter + '/?' + 
				'page= ' + payload.page
			)
		},

		cancel: function(payload){
			return(
				'transaction/adjustment/cancel'
			)
		}
	},

	leaveBalance: {
		get: function(payload){
			return(
				'transaction/leave/leavetypes'
			)
		},
	},

	tardiness: {
		get: function(payload){
			if(payload.filter=='COMPANY'){
				return(
					'validations/tardiness?' +
					'filter=' + payload.filter + 
					'&id=' + payload.compid + 
					'&page=' + payload.page
				);					
			}
			return(
				'validations/tardiness?' +
				'filter=' + payload.filter + 
				'&id=' + payload.empid + 
				'&page=' + payload.page
			);

			/*return(
				'validations/tardiness?' +
				payload.filter + 
				'/?page=' +
				payload.page
			);*/
		},
		confirm: function(payload){
			return(
				'reports/validations/confirm/?' +
				'company=' + payload.compid + 
				'&type=tardiness' +
				'&id=' + 
				payload.id
			)
		},
		update: function(payload){
			return(
				/*'reports/validations/confirm/?' +
				'company=' + payload.compid + 
				'&&type=tardiness' +
				'&&id=' + 
				payload.id*/

				baseURL.activeCompany() +
				'/employee/'  + payload.employeeid + 
				'/reports/dtr/'
			)
		},
	},

	leaves: {
		get: function(payload){
			return(
				'reports/validations/leave/' +
				payload.filter + '/?' + 
				'page= ' + payload.page
			)
		},

		cancel: function(payload){
			return(
				'transaction/leave/cancel'
			)
		},
	},

	undertime: {
		get: function(payload){
			return(
				'reports/validations/undertime/' + 
				payload.filter + 
				'/?page=' +
				payload.page
			)
		},
		confirm: function(payload){
			return(
				'reports/validations/confirm/?' +
				'company=' + payload.compid + 
				'&type=undertime' +
				'&id=' + 
				payload.id
			)
		},
		update: function(payload){
			return(
				/*'reports/validations/confirm/?' +
				'company=' + payload.compid + 
				'&&type=tardiness' +
				'&&id=' + 
				payload.id*/

				baseURL.activeCompany() +
				'/employee/'  + payload.employeeid + 
				'/reports/dtr/'
			)
		},
	},

	overtime: {
		get: function(payload){
			return(
				'reports/validations/overtime/' + 
				payload.filter + 
				'/?page=' +
				payload.page
			)
		},
		confirm: function(payload){
			return(
				'reports/validations/confirm/?' +
				'company=' + payload.compid + 
				'&type=overtime' +
				'&id=' + 
				payload.id
			)
		},
		update: function(payload){
			console.log('payloadssss',payload);
			return(
				/*'reports/validations/confirm/?' +
				'company=' + payload.compid + 
				'&&type=tardiness' +
				'&&id=' + 
				payload.id*/

				baseURL.activeCompany() +
				'/employee/'  + payload.employeeid + 
				'/reports/dtr/'
			)
		},
	},
	
	notimein: {
		get: function(payload){
			return(
				'reports/validations/dtr/notimein/' +
				payload.filter +
				'/?page=' + payload.page
			)
		},
		confirm: function(payload){
			return(
				'reports/validations/confirm/?' +
				'company=' + payload.compid + 
				'&type=timein' +
				'&id=' + 
				payload.id
			)
		},
		update: function(payload){
			console.log('payloadssss',payload);
			return(
				baseURL.activeCompany() +
				'/employee/'  + payload.employeeid + 
				'/reports/dtr/'
			)
		},
	},

	notimeout: {
		get: function(payload){
			return(
				'reports/validations/dtr/notimeout/' +
				payload.filter +
				'/?page=' + payload.page
			)
		},
		confirm: function(payload){
			return(
				'reports/validations/confirm/?' +
				'company=' + payload.compid + 
				'&type=timeout' +
				'&id=' + 
				payload.id
			)
		},
		update: function(payload){
			console.log('payloadssss',payload);
			return(
				baseURL.activeCompany() +
				'/employee/'  + payload.employeeid + 
				'/reports/dtr/'
			)
		},
	},

	payslip: {
		get: function(payload){
			console.log('payloadssss',payload);
			return(
				'payrolltransaction/' + 
				payload.payrollid +
				'/employees/' +
				payload.employeeid
			);
		},
	},
	emailpayslip:{
		get: function(payload){
			console.log('payloadssss',payload);
			return(
				'generatepayslip/?payrollid=' + 
				payload.payrollid +
				'&employee=' +
				payload.id
			);
		},
	},
	dtrmodification:{
		get:function(payload){
			return(
				'reports/validations/dtr/company'
			);
		}
	},
	sss:{
		get:function(payload){
			return(
				'reports/payrollgovbenefits/?page='+payload.page+'&company='+payload.id+'&benefit=sss'
			);
		},
		confirm: function(payload){
			return(
				'reports/validations/confirm/?' +
				'company=' + payload.compid + 
				'&benefits=sss' +
				'&id=' + 
				payload.id
			);
		},
	},

	sssreport:{
		get:function(payload){
			return(
				//'reports/payrollgovbenefits/?page='+payload.page+'&company='+payload.id+'&benefit=sss'
				'reports/employee/benefits/government/?employee='+payload.empid+'&benefit=sss'
			);
		},
		confirm: function(payload){
			return(
				'reports/validations/confirm/?' +
				'company=' + payload.compid + 
				'&benefits=sss' +
				'&id=' + 
				payload.id
			);
		},
	},	
	philhealth:{
		get:function(payload){
			return(
				//'reports/payrollgovbenefits/?page='+payload.page+'&company='+payload.id+'&benefit=philhealth'
				'reports/payrollgovbenefits/?page='+payload.page+'&company='+payload.id+'&benefit=philhealth'
			);
		},
		confirm: function(payload){
			return(
				'reports/validations/confirm/?' +
				'company=' + payload.compid + 
				'&benefits=philhealth' +
				'&id=' + 
				payload.id
			);
		},
	},

	philhealthreport:{
		get:function(payload){
			return(
				//'reports/payrollgovbenefits/?page='+payload.page+'&company='+payload.id+'&benefit=philhealth'
				'reports/employee/benefits/government/?employee='+payload.empid+'&benefit=philhealth'
			);
		},
		confirm: function(payload){
			return(
				'reports/validations/confirm/?' +
				'company=' + payload.compid + 
				'&benefits=philhealth' +
				'&id=' + 
				payload.id
			);
		},
	},	
	pagibig:{
		get:function(payload){
			return(
				'reports/payrollgovbenefits/?page='+payload.page+'&company='+payload.id+'&benefit=pagibig'
			);
		},
		confirm: function(payload){
			return(
				'reports/validations/confirm/?' +
				'company=' + payload.compid + 
				'&benefits=pagibig' +
				'&id=' + 
				payload.id
			);
		},
	},
}

export let transactions = {
	payrollList:{
		get: function(payload){
			return(
				'payrollschedule/currentandprevious?filter=COMPANY&'+
				'id=' + getActiveCompany()
			)
		},

		update: function(payload){
			return(
				policy.ranks.get() + 
				payload.data.id
			)
		}
	},

	payroll:{
		generate: function(payload){
			return(
				'payrolltransaction/' +
				payload
			)
		},

		cancel: function(payload){
			return(
				'payrolltransaction/' +
				payload + 
				'/cancel'
			)
		},

		regenerate: function(payload){
			return(
				'payrolltransaction/' +
				payload + 
				'/regenerate'
			)
		},

		post: function(payload){
			return(
				'payrolltransaction/' +
				payload + 
				'/post'
			)
		},

		switchCloseAllFlag: function (payload){
			return(
				'payrolltransaction/' +
				payload.payrollid +
				'/employees/' + 
				'switchcloseallflag/' + 
				payload.value
			)
		},

		employees:{
			get: function (payload){
				return(
					'payrolltransaction/' +
					payload.payrollid +
					'/employees' +
					'?page=' + payload.page
				)
			},

			regenerate: function (payload){
				return(
					'payrolltransaction/' +
					payload.payrollid +
					'/employees/' + 
					payload.employeeid +
					'/regenerate'
				)
			},

			close: function (payload){
				return(
					'payrolltransaction/' +
					payload.payrollid +
					'/employees/' + 
					payload.employeeid +
					'/close'
				)
			}
		},

		summary:{
			get: function (payload){
				return(
					'payrolltransaction/' +
					payload.payrollid + 
					'/summary'
				)
			},
		},
	
	},

	monetaryAdjustment:{
		create: function (){
			return(
				'transaction/adjustment/create'
			)
		},
	},

	leaveCategory:{
		get: function (){
			return(
				'codelist/leavecategories'
			)
		},
	},

	leaveApplication:{
		create: function (){
			return(
				'transaction/leave/apply'
			)
		},
	},
	dtrModification:{
		get:function(){
			return(baseURL.activeCompany() +
			'/employee/'  + payload.employeeid + 
			'/reports/dtr/' + payload.payrollid)
		},
		currentdtr: function(payload){
			return(
				baseURL.activeCompany() +
				'/employee/'  + payload.employeeid + 
				'/reports/dtr/'
			)
		},
	}
}
export let dashboard = {
	statistics:{
		get:function(){
			return(
				'reports/dashboard/?company='+ getActiveCompany())
		},
	},

	payrollSummary:{
		get:function(payload){
			return(
				'reports/payrollsummary/?company='+payload.compId + '&year=' + payload.year
			)
		},
	},

	payrollYear:{
		get:function(payload){
			return(
				'codelist/payrollyear'
			)
		},
	},
}

export let validations={
	validation:{
		get:function(payload){
			console.log('payloads',payload)
			return('validator/?company=' + payload.id +'&customdate=today');
		},
	},
	emailvalidation:{
		get:function(payload){
			return('generatepayrollsummary/?payrollid='+ payload.payrollid+'&employerid='+payload.id);
		},
	},
}

export let mock = {
	company: {
		branch: 'http://www.mocky.io/v2/5a8bd883320000f92c1ac015'
	},
	employee: {
		list: 'http://www.mocky.io/v2/5a7be5f2300000982828c02a',
		allInfo2: 'http://www.mocky.io/v2/5a8e85142f000056004f26ca',
		allInfo1: 'http://www.mocky.io/v2/5a8e852b2f000048004f26cb',
	},
	policies: {
		ranks: 'http://www.mocky.io/v2/5a7d50b83100002b00cd0811'
	}
}
const React = require('react-native');
const { StyleSheet } = React;

export default {
    container: {
        height: '100%',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },

    txtLabel: {
        fontSize: 13,
        color: '#838383',
        fontFamily: 'Helvetica-Light'
    }
};
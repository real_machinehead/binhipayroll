//Packages
import React, {PureComponent } from 'react';
import {
    View,
    Text,
    TouchableOpacity
} from 'react-native';

//Styles
import styles from './styles';

export default class EmptyList extends PureComponent{
    render(){
        isTouchable = typeof this.props.isTouchable == undefined ? true : !this.props.isTouchable;
        return(
            <TouchableOpacity
                disabled={isTouchable}
                style={styles.container}
                onPress={()=> this.props.onPress()}
            >
                <Text style={styles.txtLabel}> 
                    {this.props.message || 'No Results'}
                </Text>
            </TouchableOpacity>
        )
    }
}

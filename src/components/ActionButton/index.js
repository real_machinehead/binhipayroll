//Packages
import React, { Component, PureComponent } from 'react';
import {
    View,
    TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

//Styles
import styles from './styles';
import background from '../../containers/Employees/addEmployeeForm/personalinfo/background';

export default class SearchBox extends PureComponent{

    //Custom Style
    _checkCustomStyle = () => {
        
    }

    //Icon Props
    _getIconName = () => {
        return this.props.iconname || 'account-plus';
    }

    _getIconSize = () => {
        return this.props.iconSize || 27;
    }

    _getIconColor = () => {
        return '#fff'
    }

    //Component Action OnPress
    _onPress = () => {
        this.props.onPress();
    }

    _getPosition = () => {
        const yOffset = this.props.yOffset || 15;
        const position = this.props.position || 'bottom';
        let positionStyle = {};
        switch (position.toUpperCase()){
            case 'TOP':
                positionStyle.top = yOffset || 15;
                positionStyle.right = 15;
                break;
            case 'BOTTOM':
                positionStyle.bottom = yOffset || 15;
                positionStyle.right = 15;
                break;
            default:
                positionStyle.bottom = yOffset || 15;
                positionStyle.right = 15;
                break;
        }
        return position ? positionStyle : {}
    }

    render(){
        const btnColor = this.props.btnColor || false;
        const size = this.props.size || false;
        const xOffset = this.props.xOffset || false;
        const hideShadows = this.props.hideShadows || false;
        const btnSize = this.props.btnSize || 55;

        return(
            <View
                style={[
                    styles.container, 
                    this._getPosition(),
                    btnSize ? {width: btnSize, height: btnSize} : {},
                    btnColor ? {backgroundColor: btnColor} : {},
                    xOffset ? {right: xOffset} : {},
                    hideShadows ? {elevation: 0} : {},
                ]}>
                <TouchableOpacity 
                    activeOpacity={this.props.activeOpacity || 0.5}
                    onPress={this._onPress}
                    style={{backgroundColor: 'transparent'}}>
                    <View style={styles.contBtn}>
                        <Icon 
                            name={this._getIconName()} 
                            size={this._getIconSize()} 
                            color={this._getIconColor()}
                        />
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

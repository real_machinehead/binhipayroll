const React = require('react-native');
const { StyleSheet } = React;

export default {
    container: {
        width: 55,
        height: 55,
        borderRadius: 100,
        backgroundColor: 'rgba(0, 126, 0, 0.9);',        
        position: 'absolute',
        elevation: 3,
    },

    contBtn: {
        height: '100%',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 25,
        backgroundColor: 'transparent'
    }
}
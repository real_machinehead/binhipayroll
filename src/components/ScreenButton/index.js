import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity
} from 'react-native';

class ScreenButton extends Component {
    render() {
        const title = this.props.title || 'Tap Here';

        return (
            <TouchableOpacity 
                onPress={() => this.props.onPress()}
            >
                <View style={{
                    height: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: 'transparent',

                }}>
                    <Text style={{fontSize: 12, color: '#8E929A'}}>
                        {title}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }
}

export default ScreenButton;
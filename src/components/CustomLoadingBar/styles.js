export default {
    container:{
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
        minHeight: 50,
        maxHeight: 50
    },
}

import React, { PureComponent } from 'react';
import {
    View,
    ActivityIndicator,
} from 'react-native';

//Styles
import styles from './styles';

class CustomLoadingBar extends PureComponent {
    render(){
        return(
            <View style={styles.container}>
                <ActivityIndicator 
                    size={this.props.size || "small"}
                    color={this.props.colo || "#EEB843"}
                /> 
            </View>
        );
    }
}

export default CustomLoadingBar;
import React, { Component, PureComponent } from 'react';
import {
  View,
  Modal,
  Text,
  TouchableNativeFeedback, 
  ScrollView,
  TouchableOpacity,
  FlatList
} from 'react-native';

//Styles
import styles from './styles';

export default class CustomPicker extends PureComponent{

    _keyExtractor = (item, index) => {
        const keyName = this.props.keyName || 'id';
        return(
            item[keyName]
        );
    }

    _renderItem = (data) => {
        const keyName = this.props.keyName || 'id';
        return (
            <View key={data[keyName]} style={styles.modalRules.contentCont}>
                <TouchableNativeFeedback
                    onPress={() => this.props.onSelect(data)}
                    background={TouchableNativeFeedback.SelectableBackground()}>
                    <View style={styles.modalRules.contOption}>
                        <Text style={styles.modalRules.txtBtn}>
                            {data[this.props.objLabelName || 'label']}
                        </Text>
                    </View>
                </TouchableNativeFeedback>
            </View>
        );
    }

    render(){
        const aList = this.props.list || [];

        return(
            <Modal 
                animationType="fade"
                transparent={true}
                visible={this.props.visible || false}
                onRequestClose={() => {}}>
                
                <TouchableOpacity 
                    activeOpacity={1}
                    style={styles.mainContainer}
                    onPress={() => this.props.onClose()}>
                    <TouchableOpacity 
                        activeOpacity={1}
                        style={styles.modalRules.container}
                        onPress={() => {}}>
                        <View style={styles.modalRules.titleCont}>
                            <Text style={styles.modalRules.txtHeader}>
                                {this.props.title || ''}

                            </Text>
                        </View>
                        <FlatList
                            refreshing={false}
                            contentContainerStyle={styles.flatListStyle}
                            ref={(ref) => { this.flatListRef = ref; }}
                            keyExtractor={this._keyExtractor}
                            data={aList}
                            renderItem={({item}) => this._renderItem(item) }
                        />
                        <TouchableOpacity 
                            activeOpacity={0.8}
                            style={styles.modalRules.contFooter}
                            onPress={() => this.props.onClose()}>
                        
                            <Text style={styles.modalRules.txtHeader}>
                                {this.props.closeLabel || 'Cancel'}
                            </Text>
                        </TouchableOpacity>
                    </TouchableOpacity>
                </TouchableOpacity>
            </Modal>

            
        )
    }
}
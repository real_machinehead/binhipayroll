import React, { Component } from 'react';
import {
    View,
    Text,
    FlatList,
    ActivityIndicator,
    Alert,
    ToastAndroid,
} from 'react-native';

//Children Components
import GenericContainer from '../GenericContainer';
import SeeMoreButton from '../SeeMoreButton';
import DefaultStyledHeader from '../StyledHeader';
import CustomLoadingBar from '../CustomLoadingBar';
import CustomBar from '../CustomBar';
import CollapsibleView from '../CollapsibleView';
import EmptyList from '../EmptyList';

//Styles
import styles from './styles';

//Helper
import * as oHelper from '../../helper';

class PaginatedList extends Component {
    constructor(props){
        super(props);
        this.state = {
            page: 1,
            hasReachedEnd: false,
            refreshing: false,
            loadingMore: false,
            loadMoreFailed: false,

            loadingScreen: {
                show: this.props.loadingScreenShow,
                msg: this.props.loadingScreenMsg
            },
            msgBox: {
                show: this.props.msgBoxShow,
                type: this.props.msgBoxType,
                msg: this.props.msgBoxMsg,
                param: this.props.msgBoxParam
            },
        }
    }

    /* shouldComponentUpdate(nextProp, nextState){
        if(
            this.state.loadingMore != nextState.loadingMore ||
            this.state.hasReachedEnd != nextState.hasReachedEnd ||
            this.state.loadMoreFailed != nextState.loadMoreFailed ||
        ){
            this._renderFooter(next.state);
        }
        return true;
    } */

    componentDidMount() {
        this._onMount();
    }

    _onMount = () => {
        this.props.onMount();
    }

    _onRefresh = async() => {
        this.setState({refreshing: true});
        const oRes = await this.props.onRefresh();
        console.log('TTTTTTTT: ' + JSON.stringify(oRes));
        if(oRes.flagno == 1){
            this.setState({
                refreshing: false,
                page: 1,
                hasReachedEnd: false,
            })
            this._showToast('List Successfully Updated');
        }else{
            this.setState({
                refreshing: false
            })
            this._showToast((oRes.message || 'An Unknown Error was encountered') + '. Check your internet connection and try again.');
        }
    }

    _showToast = (value) => {
        ToastAndroid.show(value, ToastAndroid.BOTTOM);
    }

    _onSeeMore = () => {
        this.setState({
            loadMoreFailed: false,
            loadingMore: true,
        }, () => {
            this._loadMoreFromDB();
        });
    }

    _onEndReached = () => {
        this._handleLoadMore();
    }

    _handleLoadMore = () => {
        const {
            loadMoreFailed,
            loadingMore,
            hasReachedEnd,
            refreshing,
        } = this.state;
    
        if (!loadingMore && !hasReachedEnd && !refreshing && !loadMoreFailed) {
            this.setState({
                loadingMore: true,
            },
                () => this._loadMoreFromDB()
            );
        }
    }

    _loadMoreFromDB = async() => {
    
        const data = this.props.data;
        const pageName = this.props.pageName || 'pages';
        const pageOnFirstLevel = this.props.pageOnFirstLevel || false;
        if(data[pageName] != 1){
            const { users, page } = this.state;
            const targetPage = page + 1;
            const oRes = await this.props.onLoadMore(targetPage);
            if(oRes.flagno == 1){
                this.setState({
                    page: targetPage,
                    loadMoreFailed: false,
                    loadingMore: false,
                    refreshing: false,
                    hasReachedEnd: targetPage == 
                        (pageOnFirstLevel ? oRes[pageName] : oRes.data[pageName])
                });
            }else{
                this._displayLoadMoreFailedMsg(res.message);
                this.setState({ loadMoreFailed: true });
            }
        }else{
            this.setState({
                loadingMore: false,
                hasReachedEnd: true 
            });
        }
    };

    _displayLoadMoreFailedMsg = (msg = null) => {
        this.oCompList.displayLoadMoreFailedMsg();
    }

    _keyExtractor = (item, index) => {
        const keyExtractorExt = this.props.keyExtractorExt || null;
        const keyExtractorName = this.props.keyExtractorName || 'id';
        if(this.props.keyExtractorExt){
            return item[keyExtractorName] + '_' + item[keyExtractorExt];
        }else{
            return item[keyExtractorName];
        } 
    }

    _onRenderItem = (item) => {
        this.props.onRenderItem();
    }

    _renderFooter = () => {
        const { page,
            hasReachedEnd,
            refreshing,
            loadingMore,
            loadMoreFailed,
        } = this.state;
        
        console.log('XXXXXXXXXXXXXX_page: ' + page);
        console.log('XXXXXXXXXXXXXX_hasReachedEnd: ' + hasReachedEnd);
        console.log('XXXXXXXXXXXXXX_refreshing: ' + refreshing);
        console.log('XXXXXXXXXXXXXX_loadMoreFailed: ' + loadMoreFailed);

        return (
            hasReachedEnd ?
                <CustomBar title='End of Results' containerStyle={this.props.endOfResultsCustomStyle|| {}}/>
            :
                loadMoreFailed ? 
                    <SeeMoreButton onPress={this._onSeeMore}/>
                :
                    loadingMore ? 
                        <CustomLoadingBar size="small" color="#EEB843" />
                    :
                        null
        );
    }


    displayLoadMoreFailedMsg = (msg = null) => {
        const strMsg = !msg ? 'Please check your internet connection or try loading again in a minute.' : msg;
        Alert.alert(
            "Sorry, We're Having Some Issues",
            strMsg,
            [
              {text: 'OK', onPress: () => {}},
            ],
            { cancelable: false }
        )
    }

    setLoadingScreen = (show, msg) => {
        let oLoadingScreen = {...this.state.loadingScreen};
        oLoadingScreen.show = show;
        oLoadingScreen.msg = msg;
        this.setState({ loadingScreen: oLoadingScreen });
    }

    setMessageBox = (show, type, msg, param) => {
        this.setState({
            msgBox: oHelper.setMsgBox(
                this.state.msgBox,
                show, 
                type,
                msg,
                param
            )
        })
    }

    msgBoxOnClose = (params) => {
        this.setState({
            msgBox: oHelper.clearMsgBox(this.state.msgBox)
        })
    }

    render(){
        const data = this.props.data;
        const aStatus = this.props.status;
        const title = this.props.title || 'UNTITLED';
        const listName = this.props.listName ? this.props.listName : null;
        const onEndReachedThreshold = this.props.onEndReachedThreshold || 0.5;
        const hideHeader = this.props.hideHeader || false;
        console.log('Listname',loadingScreen);
        const {
            page,
            hasReachedEnd,
            refreshing,
            loadingMore,
            loadMoreFailed,
            loadingScreen,
            msgBox,
        } = this.state;

        console.log('messagebox',this.props.loadingScreenShow);
        let oView = null;
        
        if(aStatus[0] == 1){
            //const list=data;
            const list = listName ? data[listName] : data;
            console.log('ListsName',data[listName],data,listName);
            
            try{
                oView = list.length;
            }catch(errr){
                list=[];
            }
            oView = list.length < 1 ? <EmptyList message='No data' /> :
                <FlatList
                    refreshing={this.state.refreshing}
                    onRefresh={this._onRefresh}
                    contentContainerStyle={styles.reportsFlatlist}
                    ref={(ref) => { this.flatListRef = ref; }}
                    extraData={loadingMore || data}
                    keyExtractor={this._keyExtractor}
                    data={list}
                    onEndReached={this._onEndReached}
                    onEndReachedThreshold={onEndReachedThreshold}
                    ListFooterComponent={this._renderFooter}
                    renderItem={({item}) =>
                        this.props.renderItem(item)
                    }
                />
        }

        return( 
            <View style={styles.container}>
                {
                    hideHeader ? 
                        null 
                    :
                        <DefaultStyledHeader title={title} />
                }
                

                <GenericContainer
                    customContainer = {{flex: 1}}
                    msgBoxShow = {msgBox.show}
                    msgBoxType = {msgBox.type}
                    msgBoxMsg = {msgBox.msg}
                    msgBoxOnClose = {this.msgBoxOnClose}
                    msgBoxOnYes = {this._msgBoxOnYes}
                    containerStyle = {[styles.container, this.props.containerStyle || {}]}
                    msgBoxParam = {this.props.msgBoxParam}
                    loadingScreenShow = {this.props.loadingScreenShow}
                    loadingScreenMsg = {this.props.loadingScreenMsg}
                    status={aStatus}
                    title={title}
                    onRefresh={this._onRefresh}>

                    { oView }

                    {
                        !this.props.loadMoreFailed && (this.props.hasFilters || false) ?
                            <CollapsibleView minHeight={30} maxHeight={'100%'} />
                        :
                            null
                    }
                    
                    
                    
                </GenericContainer>
            </View>
        );
    }
}

export default PaginatedList;
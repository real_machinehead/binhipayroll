/**************************************************************
 *  FileName:           digiClock.js
 *  Description:        Date Time Clock
 *  Copyright:          Binhi-MeDFI © 2017
 *  Original Author:    Jovanni Auxilio
 *  Date Created:       2017-11-07

 *  Modification History:
        Date              By            Description

**************************************************************/
import React, { Component } from 'react';
import {
    View,
    Text,
    Button,
} from 'react-native';
import moment from "moment";

import styles from './styles';

export default class DigitalClock extends Component {
    constructor(props){
        super(props);
        this.state = {
            //Date Time for Clock
            curDate: '',
            curDateMDY: '',
            curTime: '',
            curDay: '',
        }
    }

    componentWillMount(){
        this.getCurrentTime();
    }

    componentWillUnmount(){
        clearInterval(this.timer);
    }

    componentDidMount(){
        this.timer = setInterval(() => { 
            this.getCurrentTime();
        }, 1000);
    }

    getTime = () => {
        return this.state.curTime;
    }

    getDate = () => {
        return this.state.curDateMDY;
    }

    getCurrentTime = () => {
        let curWeekday = moment().format("dddd").toUpperCase()
        this.setState({
            curDate: moment().format("LL"),
            curDateMDY:  moment().format("MM/DD/YYYY"),
            curTime: moment().add(8,'hours').format("hh:mm:ss A"),
            /*curTime: moment().format("hh:mm:ss A"),*/
            curDay: this.getDayAbbrev(curWeekday)
        });
    }

    getDayAbbrev = (value) => {
        let strAbbreb = value=='SUNDAY' ? 'SUN':
        value=='MONDAY' ? 'MON':
        value=='TUESDAY'?'TUE':
        value=='WEDNESDAY'?'WED':
        value=='THURSDAY' ? 'THU':
        value=='FRIDAY'?'FRI':
        value=='SATURDAY'?'SAT':'SUN';
        return strAbbreb;
    }

    render(){
        const {
            curDate,
            curDateMDY,
            curTime,
            curDay,
        } = this.state;
        
        return(
            <View>
                <Text style={styles.textDate}>{curDate.toUpperCase() + '  ' + curDay}</Text>
                <Text style={styles.textTime}>{curTime}</Text>
            </View>
        );
    }
}

import React, { PureComponent } from 'react';
import {
  View,
  Text,
  TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import styles from './styles';

class CollapsibleView extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            viewHeight: {height: this.props.minHeight || 0},
            isActive: false,
        };
    }

    onPress = () => {
        
        if(this.state.isActive){
            this.setState({
                isActive: false,
                viewHeight: {height: this.props.minHeight || 0}
            })
        }else{
            this.setState({
                isActive: true,
                viewHeight: {height: '100%'}
            })
        }
    }

  render() {
    const isActive = this.state.isActive;
    const btnLabel = isActive ? 'Hide Filters' : 'Show Filters';
    const btnHeight = this.props.minHeight || 0;
    const btnHeightStyle = btnHeight ? {height: isActive ? btnHeight + 10 : btnHeight} : {height: 0};
    const iconName = isActive ? 'chevron-down' : 'chevron-up'

    return (
        <View style={[styles.container, this.state.viewHeight]}>
            <TouchableOpacity
                activeOpacity={0.8}
                onPress={this.onPress}
            >
                <View style={[styles.contCollapseBtn, btnHeightStyle]}>
                    <View style={styles.contLabel}>
                        <Text style={styles.txtTitle}>
                            {btnLabel}
                        </Text>
                    </View>
                    <View style={styles.contIcon}>
                        <Icon name={iconName} size={30} color={'#505251'}/>
                    </View>
                </View>
            </TouchableOpacity>
            <View>
                <Text>TEST HALO!</Text>
            </View>
        </View>
    );
  }
}

export default CollapsibleView;
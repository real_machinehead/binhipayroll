import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        width: '100%',
        backgroundColor: '#2e3030'
    },

    txtTitle: {
        fontSize: 13,
        color: '#505251',
        fontFamily: 'Open Sans',
    },

    contCollapseBtn: {
        flexDirection: 'row',
        backgroundColor: '#252626'
    },

    contLabel: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    contIcon: {
        paddingRight: 10,
        justifyContent: 'center'
    }

});

export default styles;
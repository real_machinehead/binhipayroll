import React, { Component } from 'react';
import {
    View,
    Text, 
    TouchableNativeFeedback,
    ScrollView,
    RefreshControl,
    TouchableOpacity
} from 'react-native';
import * as PromptScreen from '../ScreenLoadStatus';
import MessageBox from '../MessageBox';
import ActionButton from '../ActionButton';
import FormHeader from '../FormHeader';

import styles from './styles';

export default class GenericContainer extends Component {
    state = {
        refreshing: false
    }

    _onRefresh = () => {
        this.props.onRefresh();
    }

    render(){
            if(this.props.status[0] == 0){
                return <PromptScreen.PromptError 
                            containerStyle={this.props.containerStyle || {}} 
                            title={this.props.title} 
                            onRefresh={this._onRefresh}

                        />;
            }else if(this.props.status[0] == 1){
                return (
                    <View style={[styles.container, this.props.containerStyle ? this.props.containerStyle : {}]}>
                        {
                            this.props.showNextButton || false ?
                                <FormHeader
                                    onNextLabel={this.props.onNextLabel || null}
                                    onNextIconName={this.props.onNextIconName || null}
                                    onNext={()=>this.props.onNext()}
                                />
                            :
                                null
                        }

                        { this.props.children }
                        
                        <MessageBox
                            promptType={this.props.msgBoxType || ''}
                            show={this.props.msgBoxShow || false}
                            onYes={() => this.props.msgBoxOnYes(this.props.msgBoxParam)}
                            onClose={() => this.props.msgBoxOnClose()}
                            onWarningContinue={() => this.props.msgBoxOnContinue(this.props.msgBoxParam)}
                            onForgotPassword={() => this.props.msgBoxOnForgotPassword()}
                            message={this.props.msgBoxMsg || ''}
                        />

                        <PromptScreen.PromptGeneric 
                            show= {this.props.loadingScreenShow || false} 
                            title={this.props.loadingScreenMsg || ''}
                        />

                        
                        
                        
                    </View>
                );
            }else{
                return <PromptScreen.PromptLoading 
                            containerStyle={this.props.containerStyle || {}} 
                            title={'Loading...'}
                        />;
            }
        }  
}
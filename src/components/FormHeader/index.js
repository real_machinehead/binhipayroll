import React, { PureComponent } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Alert
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { withNavigation } from 'react-navigation';

//Styles
import styles from './styles';

class FormHeader extends PureComponent {
    _onClose = () => {
        Alert.alert(
            'Warning',
            'Any unsaved data will be lost. Are you sure you want to exit from employee set-up? ',
            [
                
                {text: 'No', onPress: () => {() => {this.props.navigation.navigate('Employees')}}},
                {text: 'Yes', onPress: () => {this.props.navigation.navigate('Employees')}},
            ],
            { cancelable: false }
          )
        
    }

    render(){
        return(
            <View style={styles.container}>
                <View style={styles.contLeft}>
                    <TouchableOpacity
                        style={styles.btnStyle}
                        onPress={this._onClose}>
                        <View style={styles.contBtn}>
                            <View style={styles.contLabel}>
                                <Text style={styles.txtLabel}>CONTINUE LATER</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.contCenter}>
                </View>
                <View style={styles.contRight}>
                    <TouchableOpacity
                        style={styles.btnStyle}
                        onPress={() => this.props.onNext()}>
                            <View style={styles.contBtn}>
                                <View style={styles.contLabel}>
                                    <Text style={styles.txtLabel}>
                                        {this.props.onNextLabel || 'NEXT'}
                                    </Text>
                                </View>
                                <View style={styles.contIcon}>
                                    <Icon name={this.props.onNextIconName || 'chevron-right'} color='#FFF' 
                                        size={this.props.onNextIconName || false ? 20 : 27} 
                                    />
                                </View>
                            </View>
                    </TouchableOpacity> 
                </View>
            </View>
        );
    }
}

export default withNavigation(FormHeader);
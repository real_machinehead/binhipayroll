import React, { PureComponent } from 'react';
import {
    View,
    Text,
    FlatList,
    ActivityIndicator,
    Alert
} from 'react-native';

//Children Components
import GenericContainer from '../GenericContainer';

//Styles
import styles from './styles';

//Helper
import * as oHelper from '../../helper';

class DefaultStyledHeader extends PureComponent {
    render(){
        const data = this.props.data;
        const aStatus = this.props.status;
        
        return(
            <View style={styles.container}>
                <View style={styles.contTitle}>
                    <Text style={styles.txtTitle}>
                        { this.props.title || 'UNTITLED' }
                    </Text>
                </View>
            </View>
        );
    }
}

export default DefaultStyledHeader;

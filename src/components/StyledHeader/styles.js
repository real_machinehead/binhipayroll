export default {
    container: {
        flex:0,
        backgroundColor: '#818489',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 15,
        minHeight: 25,
        marginBottom: 1,
        elevation: 2
    },

    contTitle: {
        flex: 0,
    },

    txtTitle: {
        fontSize: 15,
        fontWeight: '500',
        fontFamily: 'Helvetica-Light',
        color: '#FFFFFF',
    }


}

const React = require('react-native');
const { StyleSheet } = React;

export default {
    container: {
        height: 35,
        backgroundColor: '#EEB843',
        marginTop: 5,
        elevation: 3
    },

    contLabel: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    txtLabel: {
        fontSize: 13,
        color: '#434646',
        fontFamily: 'Helvetica-Light'
    }
};
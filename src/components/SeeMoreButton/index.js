//Packages
import React, {PureComponent } from 'react';
import {
    View,
    Text,
    TouchableOpacity
} from 'react-native';

//Styles
import styles from './styles';

export default class SeeMoreButton extends PureComponent{
    render(){
        return(
            <TouchableOpacity
                style={styles.container}
                onPress={()=> this.props.onPress()}
            >
                <View style={styles.contLabel}>
                    <Text style={styles.txtLabel}> 
                        See More 
                    </Text>
                </View>
            </TouchableOpacity>
        )
    }
}

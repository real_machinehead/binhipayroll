import {store} from '../../store';

//Company Based Reducers
import * as PolicyBenefits from '../../containers/CompanyPolicies/data/benefits/actions';
import * as PolicyBonus from '../../containers/CompanyPolicies/data/bonus/actions';
import * as PolicyLeaves from '../../containers/CompanyPolicies/data/leaves/actions';
import * as PolicyOvertime from '../../containers/CompanyPolicies/data/overtime/actions';
import * as PolicyPayroll from '../../containers/CompanyPolicies/data/payroll/actions';
import * as PolicyPositions from  '../../containers/CompanyPolicies/data/positions/actions';
import * as PolicyRanks from '../../containers/CompanyPolicies/data/ranks/actions';
import * as PolicySavings from '../../containers/CompanyPolicies/data/savings/actions';
import * as PolicyTardiness from '../../containers/CompanyPolicies/data/tardiness/actions';
import * as PolicyTax from '../../containers/CompanyPolicies/data/tax/actions';
import * as PolicyUndertime from '../../containers/CompanyPolicies/data/undertime/actions';
import * as PolicyWorkshift from  '../../containers/CompanyPolicies/data/workshift/actions';

//Employee Based Reducers
import * as EmployeeList from  '../../containers/Employees/data/list/actions';
import * as ActiveEmployeeProfile from  '../../containers/Employees/data/activeProfile/actions';

export const resetCompanyBasedData = async() => {
    try{
        //Reset Company Based Data
        await store.dispatch(PolicyBenefits.reset());
        await store.dispatch(PolicyBonus.reset());
        await store.dispatch(PolicyLeaves.reset());
        await store.dispatch(PolicyOvertime.reset());
        await store.dispatch(PolicyPayroll.reset());
        await store.dispatch(PolicyPositions.reset());
        await store.dispatch(PolicyRanks.reset());
        await store.dispatch(PolicySavings.reset());
        await store.dispatch(PolicyTardiness.reset());
        await store.dispatch(PolicyTax.reset());
        await store.dispatch(PolicyUndertime.reset());
        await store.dispatch(PolicyWorkshift.reset());

        //Reset Employee Based Data
        await store.dispatch(EmployeeList.reset());
        await store.dispatch(ActiveEmployeeProfile.reset());

        return true;
    }catch(exception){
        console.log('ERROR: ' + exception.message);
        return false;
    }
};

export const resetSession= async() => {
    try{
        await resetCompanyBasedData();
        return true;
    }catch(exception){
        console.log('ERROR: ' + exception.message);
        return false;
    }
};

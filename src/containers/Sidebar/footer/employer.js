import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableNativeFeedback,
    Alert,
    ToastAndroid
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { withNavigation } from 'react-navigation';
import styles from '../styles';
import * as session from '../../../services/session';

import LoadingScreen from '../../../components/LoadingScreen';
import Employer from '../../Employer/profile';

//Services
import * as dataService from '../../../services/dataResetter';
import * as validationActions from '../data/api';
//import * as notimeinApi from '../data/notimein/api';
class SidebarFooterEmployer extends Component{
    constructor(props){
        super(props);
        this.state = {
            splashScreen: {
                show: false,
                message: '',
            },
            _list: [
                {
                    key: "0001",
                    label: "DTR Validation",
                    iconName: 'md-settings',
                    iconSize: 20,
                    iconColor: '#434646',
                    navigateTo: 'EmprDashBoard',
                },
                /* {
                    key: "0002",
                    label: "Sync",
                    iconName: 'md-sync',
                    iconSize: 23,
                    iconColor: '#434646',
                    navigateTo: 'CompanyPolicies',
                    isActive: false
                }, */
                {
                    key: "0003",
                    label: "Logout",
                    iconName: 'md-power',
                    iconSize: 20,
                    iconColor: '#434646',
                    navigateTo: 'Login',
                    isActive: false
                }
            ],
        }
    }

    _onItemPress = (data) => {
        console.log('data: ' + JSON.stringify(data));
        switch(data.key){
            case '0001':
                Alert.alert(
                    'Confirmation',
                    'All dtr validations be update. Are you sure you want to Validate ?',
                    [
                        {text: 'NO', onPress: () => {}},
                        {text: 'YES', onPress: () => {this._validation()}},
                    ],
                    { cancelable: false }
                )
                break;
            case '0002':
                break;
            case '0003':
                Alert.alert(
                    'Confirmation',
                    'Any unsaved data will be lost. Are you sure you want to logout ?',
                    [
                        {text: 'NO', onPress: () => {}},
                        {text: 'YES', onPress: () => {this._logout()}},
                    ],
                    { cancelable: false }
                )
                break;
        }
    }

    _validation = async()=>{
        this._setSplashScreen(true, 'Validating payroll reports...');
        console.log('loggggggggggsssssss',this.props.activecompany.id);

        await validationActions.get({id:this.props.activecompany.id})
        Alert.alert(
            'Success.',
            'DTR Validataion has been Validated',
            [
                {text: 'OK', onPress: () => {}}
            ],
            { cancelable: false }
        )
        this._setSplashScreen(false);
    }
    
    _logout = async() => {
        this._setSplashScreen(true, 'Logging out...');
        await session.revoke()
        .then(async(res)=>{
            if(res.flagno == 1){
                    await this._setSplashScreen(false);
                    this.props.navigation.navigate('Login');
                    ToastAndroid.show('You have been logged out', ToastAndroid.BOTTOM);
            }else{
                Alert.alert(
                    'Logout Failed.',
                    'Unable to logout. Please try again.',
                    [
                        {text: 'OK', onPress: () => {}}
                    ],
                    { cancelable: false }
                )
                this._setSplashScreen(false);
            }
        }).catch((exception)=> {
            console.log(exception);
        });
    }

    _setSplashScreen = (show, message='') => 
        this.setState({ splashScreen: {show: show, message: message }});

    _navigateToProfile = () => {
        this.props.navigation.navigate('EmployerProfile',{profile:this.props.loginInfo});
    }

    _navigateHeaderView = () => {
        //this.props.navigation.navigate('CompanyProfile');
    }


    render(){
        const oSplashScreen = this.state.splashScreen;
        const footerStyles = styles.footer;
        const textStyles = styles.textStyles;
        //console.log('this.props.loginInfo: ' + JSON.stringify(this.props.loginInfo));
        const loginInfo = this.props.loginInfo;
        const loginfomiddlename=loginInfo.middlename.charAt(0);
        if(loginfomiddlename){
            return(
                <View style={footerStyles.container}>
                    <TouchableNativeFeedback
                        onPress={this._navigateToProfile}
                        background={TouchableNativeFeedback.SelectableBackground()}>
                        <View style={footerStyles.profile.container}>
                            <View style={footerStyles.profile.icon}>
                                <Icon 
                                    size={35} 
                                    name='md-person'
                                    color='#434646'  />
                            </View>
                            <View style={footerStyles.profile.label}>
                                <Text style={textStyles.profileTitle}>
                                    {loginInfo.firstname + ' ' + loginInfo.middlename.charAt(0) + ' ' + loginInfo.lastname}
                                </Text>
                                <Text style={textStyles.profileLabel}>
                                    My Profile
                                </Text>
                            </View>
                        </View>
                    </TouchableNativeFeedback>
                    <View style={footerStyles.bottom}>
                        {
                            this.state._list.map((data,index) => 
                                <TouchableNativeFeedback
                                    key={data.key}
                                    onPress={() => {this._onItemPress(data)}}
                                    background={TouchableNativeFeedback.SelectableBackground()}>
                                    <View style={footerStyles.item}>
                                        <View style={footerStyles.icon}>
                                            <Icon 
                                                size={data.iconSize} 
                                                name={data.iconName} 
                                                color={data.iconColor} />
                                        </View>
                                        <View style={footerStyles.label}>
                                            <Text style={textStyles.profileLabel}>
                                                {data.label}
                                            </Text>
                                        </View>
                                    </View>
                                </TouchableNativeFeedback>
                            )  
                        }
                    </View>
                    
                    {
                        oSplashScreen.show ? 
                            <LoadingScreen 
                                show = {oSplashScreen.show}
                                message = {oSplashScreen.message}
                            />
                        :
                            null
                    }
                </View>
            )
        }else{
            return null;
        }
    }
    
}

export default withNavigation(SidebarFooterEmployer)
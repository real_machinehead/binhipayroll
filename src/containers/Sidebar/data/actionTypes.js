export const INITIALIZE = 'report/validation/INITIALIZE';
export const UPDATE = 'reports/validation/UPDATE';
export const STATUS = 'reports/validation/STATUS';
export const RESET = 'reports/validation/RESET';
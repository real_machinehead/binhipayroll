import React, { Component } from 'react';
import {
    View,
    Text,
    Alert,
    Switch,
    ScrollView,
    RefreshControl,
    FlatList
} from 'react-native';
import CustomCard, {SimpleCard} from '../../../components/CustomCards';
import GenericContainer from '../../../components/GenericContainer';
import EmptyList from '../../../components/EmptyList';
import * as PromptScreen from '../../../components/ScreenLoadStatus';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import BranchName from './form';

//Styles
import styles from './styles';

//helper
import * as oHelper from '../../../helper';

//Component Constants
const TITLE = 'List of Holidays';
const DESCRIPTION = 'Add, remove and update Holidays'
const SWITCH_COLOR_ON = '#838383';
const SWITCH_COLOR_THUMB = '#EEB843';
const SWITCH_COLOR_TINT = '#505251';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as branchActions from './../data/branch/actions';

//API
import * as calendarApi from './../data/branch/api';

export class Branchprofile extends Component {
    constructor(props){
        super(props);
        this.state = {
            refreshing: false,
            _showForm: false, 
            disabledMode: false,
            data: {
                isenabled: true,
                datedisplayformat: 'MMM DD YYYY',
                datedisplayformformat: 'MMMM DD, YYYY'
            },

            loadingScreen: {
                show: false,
                msg: 'test'
            },

            msgBox: {
                show: false,
                type: '',
                msg: '',
                param: ''
            },

            activeData: {
                id: '',
                name: '',
                address: '',
                contact: '',
                email: '',
            }
            
        }
    }

    componentDidMount(){
        /*if(this.props.branch.status[0] != 1){
            this._fetchDataFromDB();
        }*/
        //this.props.actions.branch.setRemoteFetchStatus();
    }

    _fetchDataFromDB= () => {
        this.props.actions.branch.getList();
    }

    _setMessageBox = (show, type, msg, param) => {
        this.setState({
            msgBox: oHelper.setMsgBox(
                this.state.msgBox,
                show, 
                type,
                msg,
                param
            )
        })
    }

    _msgBoxOnClose = (params) => {
        this.setState({
            msgBox: oHelper.clearMsgBox(this.state.msgBox)
        })
    }

    _msgBoxOnYes = (param) => {
        console.log('param: ' + param );
        let oData = null;
        switch(param[0].toUpperCase()){
            case 'SWITCHOFFPOLICY':
                oData = {...this.props.branch.data};
                oData.validfrom = null;
                oData.validto = null;
                oData.amount = 0;
                oData.isenabled = param[1];
                this._updateEmpSavPol(oData);
                break;

            case 'CLOSEPOLICYFORM':
                this._setEmpSavPolForm(false);
                this._setMessageBox(false);
                break;

            case 'UPDATEPOLICY':
                this._setMessageBox(false);
                oData = {...param[1]};
                oData.isenabled = true;
                this._updateEmpSavPol(oData);
                break;

            default:
                this._setMessageBox(false);
                break;
        }
    }

    _onPress = () => {
        let oLoadingScreen = {...this.state.loadingScreen};
        oLoadingScreen.show = true;
        oLoadingScreen.msg = 'HAHAHHAhAHA';;
        
        this.setState({
            loadingScreen: oLoadingScreen
        });
    }

    _keyExtractor = (item, index) => item.id;

    _renderItem = (oItem) => {
        console.log('oItems',oItem)
        const menu = 
            oItem.status == 'ACTIVE' ? 
                [
                    { onSelect: () => this._updatePosition(oItem), label: 'Update' },
                    { onSelect: () => this._deletePosition(oItem), label: 'Delete' }
                ]
            :
                []

        const backgroundColor =
            oItem.status == 'ACTIVE' ? 
                {backgroundColor: '#26A65B'}
            : 
                {backgroundColor: '#E74C3C'}
                
        const oTitle = 
            <View style={styles.title.contContent}>
                <View style={styles.title.contIcon}>
                    <View style={styles.title.iconPlaceholder}>
                        <Icon 
                            name='worker'
                            size={30} 
                            color='#f4f4f4'
                        />
                    </View>
                </View>
                <View style={styles.title.contLabel}>
                    <Text style={styles.title.txtLabel}>
                        { oItem.code }
                    </Text>
                </View>
            </View>

        return(
            <View>
                <View style={{
                    marginBottom: 10,
                    marginLeft: 30,
                    marginRight: 30,
                }}>
                    <SimpleCard 
                        data={[
                            ['Holiday Name', oItem.name], 
                            ['Date', oHelper.convertDateToString(oItem.date, 'MMMM DD, YYYY')],
                            ['type',oItem.type]]
                        }
                        customTitleStyle={backgroundColor}
                        oTitle={oTitle}
                        menu={menu}
                    />
                </View>
                
            </View>
        );
    }

    _addNewCalendar = () => {
        this.setState({
            _showForm: true,
            activeData: {id: '', name: '', name:'',date:'',type:''}
        })
    }

    _updateCalendar = (oData) => {
        this.setState({
            _showForm: true,
            activeData: oData
        })
    }

    _deleteCalendar = (oData) => {
        Alert.alert(
            'Warning',
            'Are you sure you want to delete ' + oData.name + ' ?',
            [
                {text: 'No', onPress: () => {}},
                {text: 'Yes', onPress: () => this._deleteToDB(oData)},
            ],
            { cancelable: false }
        ) 
    }

    _closeForm = () => {
        this.setState({
            _showForm: false
        })
    }

    _onSubmitForm = (oData) => {
        if(oHelper.isStringEmptyOrSpace(oData.id)){
            this._addToDB(oData);
        }else{
            Alert.alert(
                'Confirm Action',
                'Are you sure you want to update ' + oData.name + ' ?',
                [
                    {text: 'No', onPress: () => {}},
                    {text: 'Yes', onPress: () => this._updateToDB(oData)},
                ],
                { cancelable: false }
            ) 
        }
    }

    _addToDB = async(oData) => {
        this._setLoadingScreen(true, 'Saving New Position. Please wait...');
        await calendarApi.add(oData)
            .then((response) => response.json())
            .then(async(res) => {
                console.log('RES: ' + JSON.stringify(res));
                if(res.flagno == 1){
                    oData.id = res.id;
                    oData.dateadded = await oHelper.convertDateToString(new Date(), 'YYYY-MM-DD');
                    oData.status = 'ACTIVE';
                    await this.props.actions.branch.add(oData);
                    this._closeForm();
                }
                this._setMessageBox(true, res.flagno==1 ? 'success' : 'error-ok', res.message);
            })
            .catch((exception) => {
                this._setMessageBox(true, 'error-ok', exception.message);
            });
        this._setLoadingScreen(false);
    } 

    _updateToDB = async(oData) => {
        this._setLoadingScreen(true, 'Updating Position. Please wait...');
        await calendarApi.update(oData)
            .then((response) => response.json())
            .then((res) => {
                console.log('RES: ' + JSON.stringify(res));
                if(res.flagno == 1){
                    this.props.actions.branch.update(oData)
                    this._closeForm();
                }
                this._setMessageBox(true, res.flagno==1 ? 'success' : 'error-ok', res.message);
            })
            .catch((exception) => {
                /* this._setMessageBox(true, 'error-ok', 'Failed to update ' + TITLE); */
                this._setMessageBox(true, 'error-ok', exception.message);
            });
        this._setLoadingScreen(false);
    } 

    _deleteToDB = async(oData) => {
        this._setLoadingScreen(true, 'Deleting Position. Please wait...');
        await calendarApi.remove(oData)
            .then((response) => response.json())
            .then((res) => {
                console.log('RES: ' + JSON.stringify(res));
                if(res.flagno == 1){
                    oData.status = 'DELETED';
                    this.props.actions.branch.update(oData);
                    this._closeForm();
                }
                this._setMessageBox(true, res.flagno==1 ? 'success' : 'error-ok', res.message);
            })
            .catch((exception) => {
                /* this._setMessageBox(true, 'error-ok', 'Failed to update ' + TITLE); */
                this._setMessageBox(true, 'error-ok', exception.message);
            });
        this._setLoadingScreen(false);
    } 

    render(){
        
        //const oData = this.props.branch.data;
        console.log('branchform',this.props.data);
        return(
            <View>
                <BranchName 
                    onCancel={this._closeForm}
                    onSubmit={this._onSubmitForm}
                    visible={true}
                    title='HOLIDAYS'
                    data={this.state.activeData}
                />
            </View>

        );
    }
    
}

function mapStateToProps (state) {
    console.log('data length',state);
    return {
        branch: state.branch
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
           branch: bindActionCreators(branchActions, dispatch)
        }
    }
}
  
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Branchprofile)
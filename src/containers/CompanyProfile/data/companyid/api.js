import { fetchApi, mockFetch } from '../../../../services/api';
import * as blackOps from '../../../../global/blackOps';
import * as endPoints from '../../../../global/endpoints';


/* const endPoints = {
	create: 'forms/benefits/bonus.php',
	get: 'forms/benefits/bonus.php',
	toggleSwitch: 'forms/benefits/bonus.php',
	remove: 'forms/benefits/bonus.php',
}; */

//export const get = payload => fetchApi(endPoints.policy.holidays.get(payload), payload, 'get');

//export const getList = payload => fetchApi(endPoints.policy.holidays.getList(payload), payload, 'get');
/*export const update = payload => fetchApi(endPoints.policy.holidays.update(payload), payload, 'put');
export const remove = payload => fetchApi(endPoints.policy.holidays.remove(payload), payload, 'delete');
export const add = payload => fetchApi(endPoints.policy.holidays.add(payload), payload, 'post');*/

export const get = payload => fetchApi(endPoints.company.branch(payload), payload, 'get');
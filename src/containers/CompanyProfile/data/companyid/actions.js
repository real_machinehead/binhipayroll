import * as api from './api';
import * as actionTypes from './actionTypes';
import { CONSTANTS } from '../../../../constants';

/******************** All Employee Info ********************/
export const clear = () =>({
	type: actionTypes.CLEAR
})

export const updateAll = payload =>({
	type: actionTypes.UPDATEALL,
	payload,
})

export const updateElement = payload =>({
	type: actionTypes.UPDATEELEMENT,
	payload,
})

export const setActiveElement = payload =>({
	type: actionTypes.SETACTIVELEMENT,
	payload,
})

export const setRemoteFetchStatus = payload =>({
	type: actionTypes.SETREMOTEFETCHSTATUS,
	payload,
})

export const getList = payload =>
	dispatch => {
		let objRes = {};
		dispatch(setRemoteFetchStatus(CONSTANTS.STATUS.LOADING));

		api.get(payload)
		.then((response) => response.json())
		.then((res) => {
			console.log('RES_getList: ' + JSON.stringify(res));
			if(res.flagno == 1){
				console.log('insert herer',res.holidays);
				dispatch(updateAll(res.holidays));

			}
			objRes = {...res}
			//dispatch(updateAll(response));
			//objRes = {...res}
		})
		.then(() => {
			dispatch(setRemoteFetchStatus([
				objRes.flagno || 0, 
				objRes.message || CONSTANTS.ERROR.SERVER
			]));
		})
		.catch((exception) => {
			dispatch(setRemoteFetchStatus([
				0, 
				exception.message + '.'
			]));
		});

		/*api.get(payload)
		.then((response) => {
			//console.log('RES_getList: ' + JSON.stringify(res));
			//res=JSON.stringify(res);
			console.log('RES_getList: ' + JSON.stringify(response));
			//console.log('Payloads',payload);
			if(response.flagno == 1){
				console.log('insert herer');
				dispatch(updateAll(response));
			}
			objRes = {...response}
		})
		.then(() => {
			dispatch(setRemoteFetchStatus([
				objRes.flagno || 0, 
				objRes.message || CONSTANTS.ERROR.SERVER
			]));
		})
		.catch((exception) => {
			dispatch(setRemoteFetchStatus([
				0, 
				exception.message + '.'
			]));
		});*/
	}

export const create = payload =>
	async dispatch => {
		let oRes = null;

		await api.create(payload)
		.then((res) => {
			//Action Here
			console.log('RES_create: ' + JSON.stringify(res));
			oRes = {...res}
		})
		.catch((exception) => {
			oRes =  {flagno: 0, message: exception.message};
		});

		return oRes;
	}

export const update = payload =>
	async dispatch => {
		let oRes = null;

		await api.update(payload)
		.then((res) => {
			oRes = {...res}
		})
		.catch((exception) => {
			oRes =  {flagno: 0, message: exception.message};
		});

		return oRes;
	}

export const remove = payload =>
	async dispatch => {
		let oRes = null;

		await api.remove(payload)
			.then((res) => {
				oRes = {...res}
			})
			.catch((exception) => {
				oRes =  {flagno: 0, message: exception.message};
			});

		return oRes;
	}

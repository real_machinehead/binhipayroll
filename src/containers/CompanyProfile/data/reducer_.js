import { combineReducers } from 'redux';
import { reducer as branch } from './branch/reducer';
import { reducer as companyid } from './companyid/reducer';


export const companyProfile = combineReducers({
	branch: branch,
	company: companyid
});
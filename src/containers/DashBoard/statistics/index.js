//RN Packages andd Modules
import React, { Component } from 'react';
import {
    View
} from 'react-native';

//Styles
import styles from './styles';

//Children Components
import StatsHeader from './header';
import StatsDetails from './details';
import * as statsActions from './data/statistics/actions';
import * as oHelper from '../../../helper';
import { bindActionCreators } from 'redux';
import * as statsApi from './data/statistics/api';
import { connect } from 'react-redux';

class DashboardStatistics extends Component {
    constructor(props){
        super(props);
        this.state = {
            _oData: {
                datetimeinfo: {
                    title: 'TODAY',
                    description: 'AS OF MAR 3, 2018\nTUE, 12:00:00 PM'
                },

                companyinfo: {
                    label: 'COMPANY',
                    value: this.props.activecompany.name
                },

                employees:{
                    label: 'ACTIVE EMPLOYEES',
                    value: 0
                },

                workingdays: {
                    label: '2018 WORKING DAYS',
                    value: 307
                },

                today: [
                    {
                        id: '0001',
                        label: 'CLOCKED IN',
                        value: 0,
                        icon: 'human-greeting',
                        iconcolor: '#f9f9f9',
                        contlabelbg: '#009e0f',
                        contstatsbg: ['#5cb85c', '#00C851'],
                        statsborderColor: '#009e0f',
                        textstats: '#f9f9f9'
                    },
                    {
                        id: '0002',
                        label: 'ABSENCES',
                        value: 0,
                        icon: 'timer-off',
                        iconcolor: '#f9f9f9',
                        contlabelbg: '#cc0000',
                        contstatsbg: ['#d9534f', '#ef6662'],
                        statsborderColor: '#cc0000',
                        textstats: '#f9f9f9'
                    },
                    {
                        id: '0003',
                        label: 'LATES',
                        value: 0,
                        icon: 'clock-alert',
                        iconcolor: '#f9f9f9',
                        contlabelbg: '#bf9000',
                        contstatsbg: ['#ffbb33', '#ffbb33'],
                        statsborderColor: '#bf9000',
                        textstats: '#f9f9f9'
                    },
                    {
                        id: '0004',
                        label: 'ON LEAVE',
                        value: 0,
                        icon: 'alarm-check',
                        iconcolor: '#f9f9f9',
                        contlabelbg: '#085394',
                        contstatsbg: ['#428bca', '#5bc0de'],
                        statsborderColor: '#085394',
                        textstats: '#f9f9f9'
                    }
                ]
            }
        }
    }

    /*_modifydtr=(item,_showForm)=>{
        console.log('itemssssssssssss',item,_showForm);

        this.setState({
            _showForm:_showForm,
            //activedata:item.views
            activedata:{
                id: item.notimeinid,
                name: item.views[0][1],
                date: item.views[1][1],
                //timeout:item.views[2][1],
                timein:item.views[3][1],
                timeout:item.views[4][1],
                status:item.views[5][1],
                employeeid:item.employeeid
            }
        })
        if(_showForm){        
            console.log('show Form',_showForm);

        }
            
    }*/
    
    componentWillReceiveProps(nextProps) {
        console.log('lagggggggssss',nextProps);
        this.setState({
                _oData: {
                    datetimeinfo: {
                        title: 'TODAY',
                        description: 'AS OF MAR 3, 2018\nTUE, 12:00:00 PM'
                    },
    
                    companyinfo: {
                        label: 'COMPANY',
                        value: this.props.activecompany.name
                    },
    
                    employees:{
                        label: 'ACTIVE EMPLOYEES',
                        value: nextProps.statistics.data.employeestat.statistics
                    },
    
                    workingdays: {
                        label: '2018 WORKING DAYS',
                        value: 307
                    },
    
                    today: [
                        {
                            id: '0001',
                            label: 'CLOCKED IN',
                            value: nextProps.statistics.data.clockin.length,
                            icon: 'human-greeting',
                            iconcolor: '#f9f9f9',
                            contlabelbg: '#009e0f',
                            contstatsbg: ['#5cb85c', '#00C851'],
                            statsborderColor: '#009e0f',
                            textstats: '#f9f9f9'
                        },
                        {
                            id: '0002',
                            label: 'ABSENCES',
                            value: nextProps.statistics.data.absent.length,
                            icon: 'timer-off',
                            iconcolor: '#f9f9f9',
                            contlabelbg: '#cc0000',
                            contstatsbg: ['#d9534f', '#ef6662'],
                            statsborderColor: '#cc0000',
                            textstats: '#f9f9f9'
                        },
                        {
                            id: '0003',
                            label: 'LATES',
                            value: nextProps.statistics.data.late.length,
                            icon: 'clock-alert',
                            iconcolor: '#f9f9f9',
                            contlabelbg: '#bf9000',
                            contstatsbg: ['#ffbb33', '#ffbb33'],
                            statsborderColor: '#bf9000',
                            textstats: '#f9f9f9'
                        },
                        {
                            id: '0004',
                            label: 'ON LEAVE',
                            value: nextProps.statistics.data.leave.length,
                            icon: 'alarm-check',
                            iconcolor: '#f9f9f9',
                            contlabelbg: '#085394',
                            contstatsbg: ['#428bca', '#5bc0de'],
                            statsborderColor: '#085394',
                            textstats: '#f9f9f9'
                        }
                    ]
                }
            })  
    }

    _gettodaynotification=(data,status)=>{
        
        if(data){


            console.log('datassss1234567',this.props.statistics.data);
        }
    }
    _generatePayload = (page) => ({
        compid: this.props.activecompany,

    })

    componentDidMount(){
        if(this.props.statistics.status[0] != 1){
            this._fetchDataFromDB();

        }
    }

    _fetchDataFromDB= () => {
        this.props.actions.statistics.get(
            this._generatePayload(1))

    }

    render(){
        const {status, data} = this.props.statistics;
        const activeCompany = this.props.activecompany;
        //console.log('datassss',data);
        return(
            <View style={styles.container}>
                <StatsHeader data={this.state._oData}/>
                <StatsDetails data={this.state._oData}/>               
            </View>
        )
    }
}

function mapStateToProps (state) {
    return {
        statistics: state.statistics.statistics,
        activecompany: state.activeCompanyReducer.activecompany,
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            statistics: bindActionCreators(statsActions, dispatch),
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DashboardStatistics)


//export default DashboardStatistics;
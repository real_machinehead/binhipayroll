
import React, { Component } from 'react';
import { ScrollView, View, Text } from 'react-native';
import {VictoryChart, VictoryGroup, VictoryLine, VictoryScatter, VictoryTooltip} from 'victory-native';

//Styles
import styles from './styles';

class StatsPayroll extends Component {
    render(){
        const payrollStyles = styles.payrollStatsStyles;
        return(
            <View style={styles.container}>
                <VictoryChart width={700} height={200} domainPadding={{x: [-10, 0]}}>
                    <VictoryGroup
                        data={[
                            {month: "Jan", totalexpenses: 100000},
                            {month: "Feb", totalexpenses: 100000},
                            {month: "Mar", totalexpenses: 100000},
                            {month: "Apr", totalexpenses: 100000},
                            {month: "May", totalexpenses: 100000},
                            {month: "Jun", totalexpenses: 100000},
                            {month: "Jul", totalexpenses: 100000},
                            {month: "Aug", totalexpenses: 100000},
                            {month: "Sep", totalexpenses: 100000},
                            {month: "Oct", totalexpenses: 100000},
                            {month: "Nov", totalexpenses: 100000},
                            {month: "Dec", totalexpenses: 100000}
                        ]}
                        x="month"
                        y="totalexpenses"
                        labelComponent={<VictoryTooltip/>}
                    >
                        <VictoryLine />
                        <VictoryScatter/>
                    </VictoryGroup>
                </VictoryChart>
            </View>
        )
    }
}

export default StatsPayroll;
const React = require('react-native');
const { StyleSheet } = React;

export default {
    container: {
        flex: 1,
        marginRight: 15,
        marginLeft: 15,
        marginTop: 5,
        backgroundColor: '#FFFFFF',
        elevation: 5,
        borderRadius: 7,
    },

    contTitle: {
        flex: 1,
        height: 50,
        //alignItems: 'flex-start',
        //justifyContent: 'flex-end',
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 30,
    },

    contGraph: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 20,
        paddingBottom: 30,
    },

    txtTitle: {
        fontSize: 16,
        fontWeight: '500',
        color: '#434646',
        fontFamily: 'Helvetica-Light'
    },

    dropbutton:{
        flex:1,
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        paddingLeft:30,
    },
    
    picker:{
        flex:1,
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        paddingLeft:30,
        width:150,
        borderRadius: 4,
        borderWidth: 0.5,
        borderColor: '#d6d7da',
    }
}
   
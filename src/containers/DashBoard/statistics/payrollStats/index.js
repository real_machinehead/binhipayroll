
import React, { Component } from 'react';
import { ScrollView, View, Text,Picker } from 'react-native';
import {VictoryChart, VictoryAxis, VictoryBar, VictoryTheme, VictoryZoomContainer, VictoryTooltip, VictoryLabel} from 'victory-native';

/*import CustomCard, 
{
    Description,
    PropTitle,
    PropLevel1, 
    PropLevel2
}
from '../../../../components/CustomCards';*/
import { withNavigation } from 'react-navigation';
import CustomCard, 
{
    Description,
    PropTitle,
    PropLevel1, 
    PropLevel2
}
from './customCards';
import GenericContainer from '../../../../components/GenericContainer';
import * as payrollstatsActions from '../data/payrollstatistics/actions';
import * as payrollStatsApi from '../data/payrollstatistics/api';
//Styles
import styles from './styles';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
class StatsPayroll extends Component {

    constructor(props){
        super(props);
        this.state = {
            year:'',
            loadingScreen: {
                show: false,
                msg: 'test'
            },
            msgBox: {
                show: false,
                type: '',
                msg: '',
                param: ''
            },
            month:{
                jan:'',
                feb:'',
                mar:'',
                apr:'',
                may:'',
                jun:'',
                jul:'',
                aug:'',
                sep:'',
                oct:'',
                nov:'',
                dec:'',
            },
        }
    }


    componentDidMount(){
        this._getDataFromDB();
    }

    _getDataFromDB = () => {
        /*this.props.actions.payrollList.get(); 
        this.setState({visible:true});    
       console.log('logggggggggggggssssssss',this.props);*/
        //this.props.actions.statistics.get(); 
        this.props.actions.payrollStatistics.getPayrollYear()
    }

    _setLoadingScreen = (show, msg) => {
        let oLoadingScreen = {...this.state.loadingScreen};
        oLoadingScreen.show = show;
        oLoadingScreen.msg = msg;
        this.setState({ loadingScreen: oLoadingScreen });
    }

    _getPayrollsummary=async(oItem)=>{
   

        const compId=this.props.companyId;
        const year= oItem;
        console.log('oItemssssssss',year);
        this._setLoadingScreen(true, 'Resolving Undertime Validation. Please wait...');
        await payrollStatsApi.get({compId:compId,year:year})
            .then((response) => response.json())
            .then((res) => {
                let summarydata={...this.state.month}

                summarydata.jan=res.data.month[1]
                summarydata.feb =res.data.month[2]
                summarydata.mar=res.data.month[3]
                summarydata.apr=res.data.month[4]
                summarydata.may=res.data.month[5]
                summarydata.jun=res.data.month[6]
                summarydata.jul=res.data.month[7]
                summarydata.aug=res.data.month[8]
                summarydata.sep=res.data.month[9]
                summarydata.oct=res.data.month[10]
                summarydata.nov=res.data.month[11]
                summarydata.dec=res.data.month[12]

                this.setState({
                    year:oItem,
                    month:summarydata
                });  
            })
            .catch((exception) => {
                this._setMessageBox(true, 'error-ok', exception.message);
            });

          
        this._setLoadingScreen(false);


    }
    render(){
        const payrollStyles = styles.payrollStatsStyles;
        const {data,status}=this.props.payrollStatistics;
        const TITLE='';
            return(
                <GenericContainer
                    customContainer = {{flex: 1}}
                    msgBoxShow = {this.state.msgBox.show}
                    msgBoxType = {this.state.msgBox.type}
                    msgBoxMsg = {this.state.msgBox.msg}
                    msgBoxOnClose = {this._msgBoxOnClose}
                    msgBoxOnYes = {this._msgBoxOnYes}
                    containerStyle = {[styles.container, this.props.containerStyle || {}]}
                    msgBoxParam = {this.state.msgBox.param}
                    loadingScreenShow = {this.state.loadingScreen.show}
                    loadingScreenMsg = {this.state.loadingScreen.msg}
                    status={status}
                    title={TITLE}
                    onRefresh={this._fetchDataFromDB}>   

                <View style={styles.container}>
                    <View style={styles.contTitle}>
                            <Text style={styles.txtTitle}>Payroll Total Expenses Summary</Text>
                    </View>
                    <View style={styles.dropbutton}>
                        <PropLevel1 
                            name='Year'
                            content={
                                    <Picker
                                        mode='dropdown'
                                        style={styles.dropbutton}
                                        selectedValue={this.state.year}
                                        onValueChange={(itemValue, itemIndex) => {this._getPayrollsummary(itemValue)}}>
                                            {
                                                status[0]==1 ?
                                                    data.map((data, index) => (
                                                        <Picker.Item  key={index} label={data} value={data} />
                                                    ))
                                                :
                                                    <Picker.Item label={''} value={''} />
                                            }
                                    </Picker>
                                }
                                                
                                hideBorder={false}
                                contentStyle={{
                                width: 130,
                                justifyContent: 'flex-start',
                            }}
                        />
                    </View>
                    <View style={styles.contGraph}>
                        <VictoryChart 
                            width={600} 
                            height={250} 
                            theme={VictoryTheme.material}
                            domainPadding={10}
                            tickLabelComponent={<VictoryLabel angle={45} />}
                        >
                        <VictoryAxis
                            style={{ axis: { stroke: '#505251' },
                            axisLabel: { fontSize: 16 },
                            ticks: { stroke: '#000' },
                            tickLabels: { fontSize: 10, padding: 1,  verticalAnchor: 'middle', textAnchor:'end' },
                            grid: { stroke: '#B3E5FC', strokeWidth: 0.25 }
                            }} dependentAxis
                        />
                        <VictoryAxis
                            style={{ axis: { stroke: '#505251' },
                            axisLabel: { fontSize: 16 },
                            ticks: { stroke: '#000' },
                            }}
                        />

                        <VictoryBar
                            labelComponent={<VictoryTooltip />}
                            style={{ data: { fill: "#c43a31" } }}
                            data={[    
                                {month: "Jan", totalexpenses: this.state.month.jan, fill: '#505251', label: "HALLO!"},
                                {month: "Feb", totalexpenses: this.state.month.feb, fill: '#505251', label: "HALLO!"},
                                {month: "Mar", totalexpenses: this.state.month.mar, fill: '#505251', label: "HALLO!"},
                                {month: "Apr", totalexpenses: this.state.month.apr, fill: '#505251', label: "HALLO!"},
                                {month: "May", totalexpenses: this.state.month.may, fill: '#505251', label: "HALLO!"},
                                {month: "Jun", totalexpenses: this.state.month.jun, fill: '#505251', label: "HALLO!"},
                                {month: "Jul", totalexpenses: this.state.month.jul, fill: '#505251', label: "HALLO!"},
                                {month: "Aug", totalexpenses: this.state.month.aug, fill: '#505251', label: "HALLO!"},
                                {month: "Sep", totalexpenses: this.state.month.sep, fill: '#505251', label: "HALLO!"},
                                {month: "Oct", totalexpenses: this.state.month.oct, fill: '#505251', label: "HALLO!"},
                                {month: "Nov", totalexpenses: this.state.month.nov, fill: '#505251', label: "HALLO!"},
                                {month: "Dec", totalexpenses: this.state.month.dec, fill: '#505251', label: "HALLO!"},
                            ]}
                            
                            x="month"
                            y="totalexpenses"
                        />
                        </VictoryChart>
                    </View>
                </View>
            </GenericContainer>
            )  
    }
}

function mapStateToProps (state) {
    //console.log('statessssssssssssss',state.statistics);
    return {
        payrollStatistics: state.statistics.payrollstats,
        companyId:state.activeCompanyReducer.activecompany.id,
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            payrollStatistics: bindActionCreators(payrollstatsActions, dispatch),
        }
    }
}
  
export default  withNavigation(connect(
    mapStateToProps,
    mapDispatchToProps
)(StatsPayroll))


//export default StatsPayroll;
const React = require('react-native');
const { StyleSheet } = React;

export default {
    container: {
        flex:1,
        flexDirection: 'column',
        backgroundColor: '#d3d3d3'
    },

    headerStyles: {
        container: {
            flexDirection: 'row',
            height: 78,
            borderBottomWidth: 0.7,
            borderColor: '#D1D4D6',
            backgroundColor: '#ECF0F1',
            elevation: 3
        },

        content: {
            flex: 1,
            borderRightWidth: 0.7,
            borderColor: '#D1D4D6',
            padding: 5,
            flexDirection: 'row',
            backgroundColor: '#FFF'
        },

        left: {
            paddingLeft: 20,
            justifyContent: 'center',
            alignItems: 'flex-start'
        },
        
        right: {
            flex: 1,
            paddingLeft: 15,
            justifyContent: 'center',
            alignItems: 'flex-start'
        }
    },

    detailsStyles: {
        container: {
            flex: 1,
            flexDirection: 'column',
        },

        lowerBlock: {
            flex: 1,
            flexDirection: 'column',
        }
    },

    todayStyles: {
        container: {
            paddingTop: 10,
            paddingLeft: 15,
            paddingRight: 15,
            backgroundColor: 'transparent',
            borderRadius: 0,
        },

        flatListStyle: {
            paddingBottom: 15
        }  
    },

    todayItemStyles: {
        container: {
            height: 80,
            width: 190,
            margin: 7,
            flexDirection: 'row',
            borderRadius: 7,
            elevation: 5,
        },

        upperBlock: {
            flex: 1,
            margin: 10,
            flexDirection: 'row',
            borderRadius: 7,
            elevation: 7,
        },

        statsDiv: {
            flex: 1,
            justifyContent: 'center',
            alignItems: 'flex-start',
            paddingRight: 15,
            paddingLeft: 25,
            /* backgroundColor: 'red' */
        },

        iconDiv: {
            flex: 0.65,
            justifyContent: 'center',
            alignItems: 'flex-end',
        },


        lowerBlock: {
            height: 25,
            backgroundColor: 'green',
            justifyContent: 'center',
            alignItems: 'center'
        }
    },

    todayHeaderStyles: {
        container: {
            height: 45,
            flexDirection: 'row'
        },

        left: {
            paddingLeft: 5,
            justifyContent: 'center',
            alignItems: 'flex-start'
        },

        right: {
            flex: 1,
            paddingLeft: 10,
            justifyContent: 'center',
            alignItems: 'flex-start'
        }
    },

    payrollStatsStyles: {
        container:{
            flex: 1,
            marginTop: 10,
            padding: 30,
            paddingTop: 15,
            backgroundColor: '#FFFFFF',
            borderRadius: 0,
            elevation: 3
        }
    },

    textStyles: {
        companyName: {
            fontSize: 15,
            color: '#434646',
            fontFamily: 'Helvetica-Bold'
        },

        headerLabel: {
            fontSize: 13,
            color: '#838383',
            fontStyle: 'italic',
            fontFamily: 'Helvetica-Bold'
        },

        headerValue: {
            fontSize: 16,
            color: '#434646',
            fontFamily: 'Helvetica-Bold'
        },

        groupTitle: {
            fontSize: 23,
            fontWeight: '500',
            color: '#434646',
            fontFamily: 'Helvetica-Bold'
        },

        groupDescription: {
            fontSize: 10,
            color: '#434646',
            fontFamily: 'Helvetica-Light'
        },

        cardLabel: {
            fontSize: 11,
            fontWeight: '500',
            color: '#FFF',
            fontFamily: 'Helvetica-Light'
        },

        cardStatsValue: {
            fontSize: 15,
            fontWeight: '500',
            color: '#FFFFFF',
            fontFamily: 'Helvetica-Light'
        },
        
        cardStatsLabel: {
            fontSize: 12,
            color: '#FFFFFF',
            fontFamily: 'Helvetica-Light'
        }
    },
    
}
   
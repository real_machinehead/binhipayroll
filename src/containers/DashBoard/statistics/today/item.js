
import React, { PureComponent } from 'react';
import { FlatList, View, Text } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import LinearGradient from 'react-native-linear-gradient';

//Styles
import styles from '../styles';

class StatsTodayItem extends PureComponent {
    _getIconColor = (oItem) => {
        if(oItem.iconcolor){
            return String(oItem.iconcolor);
        }
        else{
            return String('#434646');
        }
    }

    _getTextStatStyle = (oItem) => {
        if(oItem.textstats){
            return ({
                color: String(oItem.textstats)
            })
        }
        else{
            return ({
                color: '#434646'
            })
        }
    }

    render(){
        const itemStyles = styles.todayItemStyles;
        const textStyles = styles.textStyles;
        const oItem = this.props.item;
        return(
            <LinearGradient 
                colors={oItem.contstatsbg}
                style={itemStyles.container}>
                <View style={itemStyles.iconDiv}>
                    <View style={{padding: 10, borderRadius: 50, backgroundColor: 'rgba(0,0,0,0.2)'}}>
                        <Icon name={oItem.icon} size={20} color={this._getIconColor(oItem)}/>
                    </View>
                </View>
                <View style={itemStyles.statsDiv}>
                    <Text style={textStyles.cardStatsValue}>
                        {oItem.value}
                    </Text>
                    <Text style={textStyles.cardStatsLabel}>
                        {oItem.label}
                    </Text>
                </View>
            </LinearGradient>
        )
    }
}

export default StatsTodayItem;
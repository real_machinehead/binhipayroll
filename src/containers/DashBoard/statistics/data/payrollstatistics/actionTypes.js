export const INITIALIZE = 'dashboard/payrollstats/INITIALIZE';
export const UPDATE = 'dashboard/payrollstats/UPDATE';
export const STATUS = 'dashboard/payrollstats/STATUS';
export const RESET = 'dashboard/payrollstats/RESET';
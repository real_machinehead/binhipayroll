import { combineReducers } from 'redux';
import { reducer as statisticsReducer } from './statistics/reducer';
import { reducer as payrollstatsReducer } from './payrollstatistics/reducer';
export const reducer = combineReducers({
	statistics: statisticsReducer,
	payrollstats: payrollstatsReducer,
});
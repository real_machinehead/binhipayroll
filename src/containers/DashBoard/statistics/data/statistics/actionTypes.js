export const INITIALIZE = 'dashboard/statistics/INITIALIZE';
export const UPDATE = 'dashboard/statistics/UPDATE';
export const STATUS = 'dashboard/statistics/STATUS';
export const RESET = 'dashboard/statistics/RESET';
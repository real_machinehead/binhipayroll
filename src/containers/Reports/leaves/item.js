import React, { Component, PureComponent } from 'react';
import {
    View,
    Text
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

//Styles
import { SimpleCard } from '../../../components/CustomCards';
import styles from '../styles';

const ICONNAME_PAID= 'currency-usd';
const ICONNAME_UNPAID = 'currency-usd-off';

export default class LeavesReportItem extends PureComponent{
    _onCancel = () => {
        this.props.onCancel(this.props.item);
    }

    _onApprove = () => {
        alert('I AM APPROVED');
    }

    render(){
        const itemStyles = styles.reportItemStyles;
        const item = this.props.item;
        const isUnpaid = item.leavetypeid == -1 ? true : false;
        const backgroundColor = 
        isUnpaid ? 
                {backgroundColor: 'rgba(193, 66, 66, 0.94)'}
            :
                {backgroundColor: 'rgba(21, 123, 19, 0.86)'}
                
        const menu = 
            item.statusid == null ||
            item.statusid == 1 ?
                [{ onSelect: this._onCancel, label: 'Cancel' }]
            :
                []

        const oTitle = 
            <View style={itemStyles.title.contContent}>
                <View style={itemStyles.title.contIcon}>
                    <View style={itemStyles.title.iconPlaceholder}>
                        <Icon 
                            name={
                                isUnpaid ? ICONNAME_UNPAID : ICONNAME_PAID
                            } 
                            size={30} 
                            color='#f4f4f4'
                        />
                    </View>
                </View>
                <View style={itemStyles.title.contLabel}>
                    <Text style={itemStyles.title.txtLabel}>
                        { item.leavetype }
                    </Text>
                    <Text style={itemStyles.title.txtDescription}>
                        { item.datefrom == item.dateto ? item.datefrom : (item.datefrom + ' - ' + item.dateto) }
                    </Text>
                    <Text style={itemStyles.title.txtDescription}>
                        { item.statusname }
                    </Text>
                </View>
            </View>

        return(
            <SimpleCard
                data={this.props.item.views}
                customTitleStyle={[itemStyles.title.container, backgroundColor]}
                oTitle={ oTitle }
                menu={menu}
            />
        )
    }
}
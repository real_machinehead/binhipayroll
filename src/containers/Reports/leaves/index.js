import React, { Component, PureComponent } from 'react';
import {Alert} from 'react-native';

//Children Components
import PaginatedList from '../../../components/PaginatedList';
import LeavesReportItem from './item';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as leavesActions from '../data/leaves/actions';

//Styles
import styles from '../styles';

//Constants
const TITLE = 'EMPLOYEEE LEAVES REPORT';

//helper
import * as oHelper from '../../../helper';

export class LeavesReport extends Component{

    _generatePayload = (page) => ({
        compid: this.props.activecompany.id,
        page: page,
        //filter: 'COMPANY',
        filter: this.props.filter || 'COMPANY',
        empid: this.props.empid,
    })

    _onMount = () => 
        this.props.actions.leaves.get(
            this._generatePayload(1)
        );

    _onRefresh = async() => 
        await this.props.actions.leaves.refresh(
            this._generatePayload(1)
        );

    _onLoadMore = async(page) => 
        await this.props.actions.leaves.loadMore(
            this._generatePayload(page)
        );
    
    //Local Functions
    _renderItem = item => 
        <LeavesReportItem
            item={item} 
            onCancel={this._onCancelTransaction}
        />;
    
    _onCancelTransaction = async(oItem) => {
        Alert.alert(
            "Confirm Action",
            "Cancel action is irreversible. Are you sure you want to cancel selected transaction ?",
            [
              {text: 'NO', onPress: () => {}},
              {text: 'YES', onPress: () => {this._cancelToDB(oItem)}},
            ],
            { cancelable: false }
        )
    }

    _cancelToDB = async(oItem) => {
        this.oCompList.setLoadingScreen(true, 'Cancelling a Transaction. Please wait...');
        const oRes = await this.props.actions.leaves.cancel(oItem);
        this.oCompList.setMessageBox(true, oRes.flagno==1 ? 'success' : 'error-ok', oRes.message);
        this.oCompList.setLoadingScreen(false);
    }

    render(){
        const {status, data} = this.props.leaves;
        const hideHeader = this.props.hideHeader || false;
        //console.log('leaveslog',data);
        return(
            <PaginatedList
                hideHeader={hideHeader} 
                title={TITLE}
                status={status}
                data={data}
                onMount={this._onMount}
                onRefresh={this._onRefresh}
                onLoadMore={this._onLoadMore}
                listName='resultLists'
                keyExtractorName='leaveid'
                renderItem={this._renderItem}
                ref={(ref) => { this.oCompList = ref; }}
            />
        );
    }
}

function mapStateToProps (state) {
    return {
        leaves: state.reports.leaves,
        activecompany: state.activeCompanyReducer.activecompany
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            leaves: bindActionCreators(leavesActions, dispatch),
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LeavesReport)
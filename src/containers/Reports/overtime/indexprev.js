
import React, { Component, PureComponent } from 'react';
import {Alert} from 'react-native';

//Children Components
import PaginatedList from '../../../components/PaginatedList';
import OvertimeReportItem from './item';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as overtimeReportActions from '../data/overtime/actions';

//Styles
import styles from '../styles';

//Constants
const TITLE = 'OVERTIME REPORT';

//helper
import * as oHelper from '../../../helper';

export class OvertimeReport extends Component{
    _generatePayload = (page) => ({
        compid: this.props.activecompany.id,
        page: page,
        //filter: 'COMPANY',
        filter: this.props.filter || 'COMPANY',
        empid: this.props.empid,
    })

    _onMount = () => 
        this.props.actions.overtimeReport.get(
            this._generatePayload(1)
        );

    _onRefresh = async() => 
        await this.props.actions.overtimeReport.refresh(
            this._generatePayload(1)
        );

    _onLoadMore = async(page) => 
        await this.props.actions.overtimeReport.loadMore(
            this._generatePayload(page)
        );
    
    //Local Functions
    _renderItem = item => 
        <OvertimeReportItem
            item={item} 
            onConfirm={this._onConfirm}
        />;
    
    _onConfirm = async(oItem) => {
        Alert.alert(
            "Confirm Action",
            "Are you sure you want to cofirm selected Overtime Occurence ?",
            [
                {text: 'NO', onPress: () => {}},
                {text: 'YES', onPress: () => {this._confirmToDB(oItem)}},
            ],
            { cancelable: false }
        )
    }

    _confirmToDB = async(oItem) => {
        let oInput = oHelper.copyObject(oItem);
        oInput.compid = this.props.activecompany.id;
        oInput.id = oItem.tardinessid;
        this.oCompList.setLoadingScreen(true, 'Confirming Overtime Occurence. Please wait...');
        const oRes = await this.props.actions.overtimeReport.confirm(oInput);
        this.oCompList.setMessageBox(true, oRes.flagno==1 ? 'success' : 'error-ok', oRes.message);
        this.oCompList.setLoadingScreen(false);
    }

    render(){
        const {status, data} = this.props.overtimeReport;
        const hideHeader = this.props.hideHeader || false;
        return(
            <PaginatedList
                hideHeader={hideHeader} 
                title={TITLE}
                status={status}
                data={data}
                onMount={this._onMount}
                onRefresh={this._onRefresh}
                onLoadMore={this._onLoadMore}
                listName='resultLists'
                renderItem={this._renderItem}
                ref={(ref) => { this.oCompList = ref; }}
                keyExtractorName='tardinessid'
            />
        );
    }
}

function mapStateToProps (state) {
    return {
        overtimeReport: state.reports.overtime,
        activecompany: state.activeCompanyReducer.activecompany
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            overtimeReport: bindActionCreators(overtimeReportActions, dispatch),
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(OvertimeReport)

import React, { Component, PureComponent } from 'react';
import {Alert} from 'react-native';

//Children Components
import PaginatedList from '../../../components/PaginatedList';
import DtrModificationReportItem from './items';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as dtrModificationReportActions from '../data/dtrmodification/actions';

//Styles
import styles from '../styles';

//Constants
const TITLE = 'DTR Modification';

//helper
import * as oHelper from '../../../helper';

export class dtrModificationReport extends Component{

    _generatePayload = (page) => ({
        compid: this.props.activecompany.id,
        page: page,
        filter: 'COMPANY',
    })

    _onMount = () => 
        this.props.actions.dtrmodification.get(
            this._generatePayload(1)
        );

    _onRefresh = async() => 
        await this.props.actions.dtrmodification.refresh(
            this._generatePayload(1)
        );

    _onLoadMore = async(page) => 
        await this.props.actions.dtrmodification.loadMore(
            this._generatePayload(page)
        );
    
    //Local Functions
    _renderItem = item => 
        <DtrModificationReportItem
            item={item} 
            onConfirm={this._onConfirm}
        />;

    render(){
        const {status, data} = this.props.dtrmodification;
        const hideHeader = this.props.hideHeader || false;
        //console.log('stats',this.props);
        return(
            <PaginatedList
                hideHeader={hideHeader} 
                title={TITLE}
                status={status}
                data={data}
                onMount={this._onMount}
                onRefresh={this._onRefresh}
                onLoadMore={this._onLoadMore}
                listName='resultLists'
                renderItem={this._renderItem}
                ref={(ref) => { this.oCompList = ref; }}
                keyExtractorName='payrollid'
            />
        );
    }
}

function mapStateToProps (state) {
    return {
        dtrmodification: state.reports.dtrmodification,
        activecompany: state.activeCompanyReducer.activecompany
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            dtrmodification: bindActionCreators(dtrModificationReportActions, dispatch),
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(dtrModificationReport)
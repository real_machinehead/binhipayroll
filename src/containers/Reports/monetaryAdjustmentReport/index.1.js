import React, { Component, PureComponent } from 'react';
import {
    View,
    Text,
    FlatList
} from 'react-native';

//Children Components
import ReportContent from '../reportContent';
import GenericContainer from '../../../components/GenericContainer';
import MonetaryAdjustmentReportItem from './item';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as monetaryAdjustmentActions from '../data/monetaryAdjustment/actions';
import * as monetaryAdjustmentApi from '../data/monetaryAdjustment/api';

//Styles
import styles from '../styles';

//Constants
const TITLE = 'SPECIAL DEDUCTIONS AND ALLOWANCES';

//helper
import * as oHelper from '../../../helper';

export class MonetaryAdjustmentReport extends Component{
    constructor(props){
        super(props);
        this.state = {
            refreshing: false,
            loadingScreen: {
                show: false,
                msg: 'test'
            },
            msgBox: {
                show: false,
                type: '',
                msg: '',
                param: ''
            },
            
        }
    }

    componentDidMount() {
        this._fetchDataFromDB({ companyid: this.props.activecompany.id });
    }

    _fetchDataFromDB = (oCriteria) => {
        this.props.actions.monetaryAdjustment.get(oCriteria);
    }

    _onCancelTransaction = async(oItem) => {
        /* alert(JSON.stringify(oItem)); */
        this._setLoadingScreen(true, 'Cancelling a Transaction. Please wait...');
        await monetaryAdjustmentApi.cancel(oItem)
            .then((response) => response.json())
            .then((res) => {
                console.log('RES_onCancelTransaction: ' + JSON.stringify(res));
                if(res.flagno == 1){
                    this._updateDataStore(res.data[0]);
                }
                this._setMessageBox(true, res.flagno==1 ? 'success' : 'error-ok', res.message);
            })
            .catch((exception) => {
                /* this._setMessageBox(true, 'error-ok', 'Failed to update ' + TITLE); */
                this._setMessageBox(true, 'error-ok', exception.message);
            });

        this._setLoadingScreen(false);
    }

    _updateDataStore = async(oNewData) => {
        const oCurData = await oHelper.copyObject(this.props.monetaryAdjustment.data);
        const aCurList = await oHelper.copyObject(oCurData.resultLists);
        const newResultLists = await oHelper.updateElementById(aCurList,oNewData);
        oCurData.resultLists = newResultLists;
        this.props.actions.monetaryAdjustment.update(oCurData);
    }

    _keyExtractor = (item, index) => item.id;

    _setLoadingScreen = (show, msg) => {
        let oLoadingScreen = {...this.state.loadingScreen};
        oLoadingScreen.show = show;
        oLoadingScreen.msg = msg;
        this.setState({ loadingScreen: oLoadingScreen });
    }

    _setMessageBox = (show, type, msg, param) => {
        this.setState({
            msgBox: oHelper.setMsgBox(
                this.state.msgBox,
                show, 
                type,
                msg,
                param
            )
        })
    }

    msgBoxOnClose = (params) => {
        this.setState({
            msgBox: oHelper.clearMsgBox(this.state.msgBox)
        })
    }

    setLoadingScreen = (show, msg) => {
        let oLoadingScreen = {...this.state.loadingScreen};
        oLoadingScreen.show = show;
        oLoadingScreen.msg = msg;
        this.setState({ loadingScreen: oLoadingScreen });
    }

    setMessageBox = (show, type, msg, param) => {
        this.setState({
            msgBox: oHelper.setMsgBox(
                this.state.msgBox,
                show, 
                type,
                msg,
                param
            )
        })
    }

    render(){
        console.log('XXXXXXXXXXXXXXXXXXXXXXXXXXthis.props.monetaryAdjustment: ' + JSON.stringify(this.props.monetaryAdjustment));
        const aStatus = this.props.monetaryAdjustment.status;
        const oData = this.props.monetaryAdjustment.data;
        let oBody = null;
        let aFilters = [];

        if(aStatus[0] == 1){
            oBody =
                <FlatList
                    refreshing={this.state.refreshing}
                    onRefresh={this._fetchDataFromDB}
                    contentContainerStyle={styles.reportsFlatlist}
                    ref={(ref) => { this.flatListRef = ref; }}
                    extraData={this.state._activeItem}
                    keyExtractor={this._keyExtractor}
                    data={oData.resultLists}
                    renderItem={({item}) =>
                        <MonetaryAdjustmentReportItem
                            item={item} 
                            onCancel={this._onCancelTransaction}/>
                    }
                />
            aFilters = [];
        }

        return(
            <GenericContainer
                msgBoxShow = {this.state.msgBox.show}
                msgBoxType = {this.state.msgBox.type}
                msgBoxMsg = {this.state.msgBox.msg}
                msgBoxOnClose = {this.msgBoxOnClose}
                msgBoxOnYes = {this._msgBoxOnYes}
                containerStyle = {styles.container}
                msgBoxParam = {this.state.msgBox.param}
                loadingScreenShow = {this.state.loadingScreen.show}
                loadingScreenMsg = {this.state.loadingScreen.msg}
                status={aStatus}
                title={TITLE}
                onRefresh={this._fetchDataFromDB}>

                <ReportContent
                    hideHeader={this.props.hideHeader || false}
                    title={TITLE}
                    body={oBody}
                    filters={aFilters}
                />

            </GenericContainer>
        )
    }
}

function mapStateToProps (state) {
    return {
        monetaryAdjustment: state.reports.monetaryAdjustment,
        activecompany: state.activeCompanyReducer.activecompany
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            monetaryAdjustment: bindActionCreators(monetaryAdjustmentActions, dispatch),
        }
    }
}
  
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MonetaryAdjustmentReport)
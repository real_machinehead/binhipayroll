import React, { Component, PureComponent } from 'react';
import {
    View,
    Text
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

//Styles
import { SimpleCard } from '../../../components/CustomCards';
import styles from '../styles';
import background from '../../Employees/addEmployeeForm/personalinfo/background';

import * as oHelper from '../../../helper';

const ICONNAME_ALLOWANCE = 'account-plus';
const ICONNAME_DEDUCTION = 'account-minus';

export default class MonetaryAdjustmentReportItem extends PureComponent{

    _onCancel = () => {
        this.props.onCancel(this.props.item);
    }

    _onApprove = () => {
        alert('I AM APPROVED');
    }

    render(){
        console.log('I WAS IN RENDER!!!!!!!!!');
        const itemStyles = styles.reportItemStyles;
        const item = this.props.item;
        const isAllowance = item.category.toUpperCase() == 'ALLOWANCE' ? true : false;
        const backgroundColor = 
            isAllowance ? 
                {backgroundColor: 'rgba(21, 123, 19, 0.86)'}
            :
                {backgroundColor: 'rgba(193, 66, 66, 0.94)'}
        const menu = 
            item.enablecancel == null ||
            item.enablecancel == 1 ?
                [{ onSelect: this._onCancel, label: 'Cancel' }]
            :
                []
        const oTitle = 
            <View style={itemStyles.title.contContent}>
                <View style={itemStyles.title.contIcon}>
                    <View style={itemStyles.title.iconPlaceholder}>
                        <Icon 
                            name={
                                isAllowance ? ICONNAME_ALLOWANCE : ICONNAME_DEDUCTION
                            } 
                            size={30} 
                            color='#f4f4f4'
                        />
                    </View>
                </View>
                <View style={itemStyles.title.contLabel}>
                    <Text style={itemStyles.title.txtLabel}>
                        { item.category }
                    </Text>
                    <Text style={itemStyles.title.txtDescription}>
                        { 
                            oHelper.convertDateToString(
                                oHelper.convertStringToDate(item.paydate),
                                'MMM DD, YYYY'
                            )
                        }
                    </Text>
                    <Text style={itemStyles.title.txtDescription}>
                        { item.statusdescription }
                    </Text>
                </View>
            </View>

        return(
            <SimpleCard
                data={this.props.item.views}
                customTitleStyle={[itemStyles.title.container, backgroundColor]}
                oTitle={ oTitle }
                menu={menu}
            />
        )
    }
}
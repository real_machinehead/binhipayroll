import React, { Component, PureComponent } from 'react';
import {Alert} from 'react-native';

//Children Components
import PaginatedList from '../../../components/PaginatedList';
import MonetaryAdjustmentReportItem from './item';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as monetaryAdjustmentActions from '../data/monetaryAdjustment/actions';
import * as monetaryAdjustmentApi from '../data/monetaryAdjustment/api';

//Styles
import styles from '../styles';

//Constants
const TITLE = 'SPECIAL DEDUCTIONS AND ALLOWANCES REPORT';

//helper
import * as oHelper from '../../../helper';

export class MonetaryAdjustmentReport extends Component{
    _generatePayload = (page) => ({
        companyid: this.props.activecompany.id,
        page: page,
        filter: 'COMPANY',
    })

    _onMount = () => 
        this.props.actions.monetaryAdjustment.get(
            this._generatePayload(1)
        );

    _onRefresh = async() => 
        await this.props.actions.monetaryAdjustment.refresh(
            this._generatePayload(1)
        );

    _onLoadMore = async(page) => 
        await this.props.actions.monetaryAdjustment.loadMore(
            this._generatePayload(page)
        );
    
    //Local Functions
    _renderItem = item => 
        <MonetaryAdjustmentReportItem
            item={item} 
            onCancel={this._onCancelTransaction}
        />;
    
    _onCancelTransaction = async(oItem) => {
        Alert.alert(
            "Confirm Action",
            "Cancel action is irreversible. Are you sure you want to cancel selected transaction ?",
            [
              {text: 'NO', onPress: () => {}},
              {text: 'YES', onPress: () => {this._cancelToDB(oItem)}},
            ],
            { cancelable: false }
        )
    }

    _cancelToDB = async(oItem) => {
        this.oCompList.setLoadingScreen(true, 'Cancelling a Transaction. Please wait...');
        const oRes = await this.props.actions.monetaryAdjustment.cancel(oItem);
        this.oCompList.setMessageBox(true, oRes.flagno==1 ? 'success' : 'error-ok', oRes.message);
        this.oCompList.setLoadingScreen(false);
    }

    render(){
        const {status, data} = this.props.monetaryAdjustment;
        const hideHeader = this.props.hideHeader || false;

        return(
            <PaginatedList
                hideHeader={hideHeader} 
                title={TITLE}
                status={status}
                data={data}
                onMount={this._onMount}
                onRefresh={this._onRefresh}
                onLoadMore={this._onLoadMore}
                listName='resultLists'
                renderItem={this._renderItem}
                ref={(ref) => { this.oCompList = ref; }}
            />
        );
    }
}

function mapStateToProps (state) {
    return {
        monetaryAdjustment: state.reports.monetaryAdjustment,
        activecompany: state.activeCompanyReducer.activecompany
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            monetaryAdjustment: bindActionCreators(monetaryAdjustmentActions, dispatch),
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MonetaryAdjustmentReport)
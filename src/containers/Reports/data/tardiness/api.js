import { fetchApi, mockFetch } from '../../../../services/api';
import * as endPoints from'../../../../global/endpoints';
import * as blackOps from '../../../../global/blackOps';

export let get = payload => fetchApi(endPoints.reports.tardiness.get(payload), payload, 'get');
export let confirm = payload => fetchApi(endPoints.reports.tardiness.confirm(payload), payload, 'post');
export let update = payload => fetchApi(endPoints.reports.tardiness.update(payload), payload, 'post');

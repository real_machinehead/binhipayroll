import * as actionTypes from './actionTypes';
import { combineReducers } from 'redux';
import  { CONSTANTS } from '../../../../constants';
import * as oHelper from '../../../../helper';

const initialState = null;
const initialStatus = CONSTANTS.STATUS.LOADING;

export const data = (state = initialState, action) => {
	let oState = oHelper.copyObject(state);
	//console.log('Action Type',action,'sss oStateUpdate',oState,);
	switch (action.type) {
		case actionTypes.INITIALIZE:
			return action.payload;
			break;

		case actionTypes.UPDATE:
			//console.log('sss oStateUpdates',oState);
			oState.resultsList = oHelper.arrayMerge(oState.resultsList, action.payload, 'id');
			//oState.resultLists = oHelper.arrayMerge(oState.resultLists, action.payload, 'leaveid');
			//oState.data = oHelper.arrayMerge(oState.data, action.payload, 'id');
			return oState;
			break;

		default:
			return state;
	}
};

const status = (state = initialStatus, action) => {
	console.log('sss oStateS',action);
	switch (action.type) {
		case actionTypes.STATUS:
			console.log('sss oStateS',action);
			return action.payload;
			break;

		default:
			return state;
	}
};

export const reducer = combineReducers({
	data: data,
	status: status
});
import * as actionTypes from './actionTypes';
import { combineReducers } from 'redux';
import  { CONSTANTS } from '../../../../constants';
import * as oHelper from '../../../../helper';

const initialState = null;
const initialStatus = CONSTANTS.STATUS.LOADING;

export const data = (state = initialState, action) => {
	let oState = oHelper.copyObject(state);
	//console.log('sss oStateUpdate',action);
	console.log('payroll summaries',oState);
	switch (action.type) {
		case actionTypes.INITIALIZE:
			return action.payload;
			break;

		case actionTypes.UPDATE:
			//console.log('payroll summaries',action);
			//oState.data = oHelper.arrayMerge(oState.data, action.payload, 'payrollid');
			//oState.payrollsummary = oHelper.arrayMerge(oState.payrollsummary, action.payload, 'payrollid');
			//return oState;
			return action.payload;
			break;

		default:
			return state;
	}
};

const status = (state = initialStatus, action) => {
	//console.log('sss oStateS',action);
	switch (action.type) {
		case actionTypes.STATUS:
			console.log('sss oStateS',action);
			return action.payload;
			break;

		default:
			return state;
	}
};

export const reducer = combineReducers({
	data: data,
	status: status
});
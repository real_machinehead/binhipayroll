export const INITIALIZE = 'payrollsummary/INITIALIZE';
export const UPDATE = 'payrollsummary/UPDATE';
export const STATUS = 'payrollsummary/STATUS';
export const RESET = 'payrollsummary/RESET';
import { fetchApi, mockFetch } from '../../../../services/api';
import * as endPoints from'../../../../global/endpoints';
import * as blackOps from '../../../../global/blackOps';

//export let get = payload => fetchApi(endPoints.reports.payrollSchedule.get(payload), payload, 'get');
export let get = payload => fetchApi(endPoints.reports.payrollReports.get(payload), payload, 'get');
//export let getpayslip = payload => fetchApi(endPoints.reports.payslip.get(payload), payload, 'get');
export let getpayslip = payload => fetchApi(endPoints.transactions.payroll.summary.get(payload), payload, 'get');
export let email = payload => fetchApi(endPoints.validations.emailvalidation.get(payload),payload, 'get');
//export let get = payload => fetchApi(endPoints.reports.payslip.get(payload), payload, 'get');
export const INITIALIZE = 'payrollreport/INITIALIZE';
export const UPDATE = 'payrollreport/UPDATE';
export const STATUS = 'payrollreport/STATUS';
export const RESET = 'payrollreport/RESET';
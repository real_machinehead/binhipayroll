export const INITIALIZE = 'reports/dtrModification/INITIALIZE';
export const UPDATE = 'reports/dtrModification/UPDATE';
export const STATUS = 'reports/dtrModification/STATUS';
export const RESET = 'reports/dtrModification/RESET';
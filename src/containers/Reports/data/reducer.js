import { combineReducers } from 'redux';
import { reducer as payrollScheduleReducer } from './payrollSchedule/reducer';
import { reducer as monetaryAdjustmentReducer } from './monetaryAdjustment/reducer';
import { reducer as leaveBalanceReducer } from './leaveBalance/reducer';
import { reducer as tardinessReportReducer } from './tardiness/reducer';
import { reducer as payslipReducer } from './payslip/reducer';
import { reducer as undertimeReducer } from './undertime/reducer';
import { reducer as overtimeReducer } from './overtime/reducer';
import { reducer as leavesReducer } from './leaves/reducer';
import { reducer as notimeinReducer } from './notimein/reducer';
import { reducer as notimeoutReducer } from './notimeout/reducer';
import { reducer as dtrmodificationReducer } from './dtrmodification/reducer';
import { reducer as sssReportReducer } from './sss/reducer';
import { reducer as philhealthReducer } from './philhealth/reducer';
import { reducer as pagibigReducer } from './pagibig/reducer';
import { reducer as sssEmpReportReducer} from '../../Employees/profile/reports/data/sss/reducer';
import { reducer as philhealthEmpReportReducer} from '../../Employees/profile/reports/data/philhealth/reducer';
import { reducer as payrollreportReducer} from './payrollSchedule/reducer';
import { reducer as payrollreportsReducer} from './payrollreport/reducer';
import { reducer as payrollsummaryReducer} from './payrollsummary/reducer';
//import { reducer as pagibigEmpReportReducer} from '../../Employees/profile/reports/data/pagibig/reducer';
export const reducer = combineReducers({
	payrollSchedule: payrollScheduleReducer,
	monetaryAdjustment: monetaryAdjustmentReducer,
	leaveBalance: leaveBalanceReducer,
	tardiness: tardinessReportReducer,
	payslip: payslipReducer,
	undertime: undertimeReducer,
	sss:sssReportReducer,
	sssreport:sssEmpReportReducer,
	philhealth:philhealthReducer,
	philhealthreport:philhealthEmpReportReducer,
	pagibig:pagibigReducer,
	//pagibig:pagibigempReportReducer,
	overtime: overtimeReducer,
	leaves: leavesReducer,
	notimein: notimeinReducer,
	notimeout: notimeoutReducer,
	dtrmodification:dtrmodificationReducer,
	payrollreport:payrollreportReducer,
	payrollreports:payrollreportsReducer,
	payrollsummary:payrollsummaryReducer
});
export const INITIALIZE = 'reports/undertime/INITIALIZE';
export const UPDATE = 'reports/undertime/UPDATE';
export const STATUS = 'reports/undertime/STATUS';
export const RESET = 'reports/undertime/RESET';
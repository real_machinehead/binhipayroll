import { fetchApi, mockFetch } from '../../../../services/api';
import * as endPoints from'../../../../global/endpoints';
import * as blackOps from '../../../../global/blackOps';

export let get = payload => fetchApi(endPoints.reports.philhealth.get(payload), payload, 'post');
export let confirm = payload => fetchApi(endPoints.reports.philhealth.confirm(payload), payload, 'post');
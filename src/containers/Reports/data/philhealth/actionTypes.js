export const INITIALIZE = 'reports/philhealth/INITIALIZE';
export const UPDATE = 'reports/philhealth/UPDATE';
export const STATUS = 'reports/philhealth/STATUS';
export const RESET = 'reports/philhealth/RESET';
export const INITIALIZE = 'reports/monetaryAdjustment/INITIALIZE';
export const UPDATE = 'reports/monetaryAdjustment/UPDATE';
export const STATUS = 'reports/monetaryAdjustment/STATUS';
export const RESET = 'reports/monetaryAdjustment/RESET';
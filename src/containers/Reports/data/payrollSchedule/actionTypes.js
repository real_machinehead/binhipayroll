export const INITIALIZE = 'payrollSchedule/INITIALIZE';
export const UPDATE = 'payrollSchedule/UPDATE';
export const STATUS = 'payrollSchedule/STATUS';
export const RESET = 'payrollSchedule/RESET';
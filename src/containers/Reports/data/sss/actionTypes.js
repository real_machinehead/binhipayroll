export const INITIALIZE = 'reports/sss/INITIALIZE';
export const UPDATE = 'reports/sss/UPDATE';
export const STATUS = 'reports/sss/STATUS';
export const RESET = 'reports/sss/RESET';
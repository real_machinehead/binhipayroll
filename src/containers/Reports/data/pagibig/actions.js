import * as api from './api';
import * as actionTypes from './actionTypes';
import  { CONSTANTS } from '../../../../constants';

export const init = payload => ({
	type: actionTypes.INITIALIZE,
	payload
});

export const reset = () => 
	dispatch => {
		dispatch(updateStatus(CONSTANTS.STATUS.LOADING));
		dispatch(init(null));
	}

export const update = payload => ({
	type: actionTypes.UPDATE,
	payload
});

export const updateStatus = payload => ({
	type: actionTypes.STATUS,
	payload,
});

export const get = payload => 
	dispatch => {
		let objRes = {};
		dispatch(updateStatus(CONSTANTS.STATUS.LOADING));
		dispatch(reset());
		api.get(payload)
		.then((response) => response.json())
		.then((res) => {
			console.log('RES_SSS_get: ' + JSON.stringify(res.data));
			dispatch(init(res.data));
			objRes = {...res}
		})
		.then(() => {
			
			dispatch(updateStatus([
				objRes.flagno || 0, 
				objRes.message || CONSTANTS.ERROR.SERVER
			]));
		})
		.catch((exception) => {
			dispatch(updateStatus([
				0,
				exception.message + '.'
			]));
			console.log('exception: ' + exception.message);
		});
	}

export const loadMore = payload =>
	async dispatch => {
		let oRes = null;

		await api.get(payload)
		.then((response) => response.json())
		.then((res) => {
			//Action Here
			console.log('RES_loadMore: ' + JSON.stringify(res));
			if(res.flagno == 1){
				dispatch(update(res.data.resultLists));
			}

			oRes = {...res}
		})
		.catch((exception) => {
			oRes =  {flagno: 0, message: exception.message};
		});

		return oRes;
	}

export const refresh = payload =>
	async dispatch => {
		let oRes = null;

		await api.get(payload)
		.then((response) => response.json())
		.then((res) => {
			//Action Here
			console.log('RES_refresh: ' + JSON.stringify(res));
			if(res.flagno == 1){	
				dispatch(init(res.data));
			}

			oRes = {...res}
		})
		.catch((exception) => {
			oRes =  {flagno: 0, message: exception.message};
		});

		return oRes;
	}

export const confirm = payload =>
	async dispatch => {
		let oRes = null;

		await api.confirm(payload)
		.then((response) => response.json())
		.then((res) => {
			//Action Here
			console.log('RES_confirm: ' + JSON.stringify(res));
			if(res.flagno == 1){	
				dispatch(update(res.data.resultLists));
			}

			oRes = {...res}
		})
		.catch((exception) => {
			oRes =  {flagno: 0, message: exception.message};
		});

		return oRes;
	}


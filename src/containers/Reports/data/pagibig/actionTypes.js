export const INITIALIZE = 'reports/pagibig/INITIALIZE';
export const UPDATE = 'reports/pagibig/UPDATE';
export const STATUS = 'reports/pagibig/STATUS';
export const RESET = 'reports/pagibig/RESET';
export const INITIALIZE = 'reports/notimeout/INITIALIZE';
export const UPDATE = 'reports/notimeout/UPDATE';
export const STATUS = 'reports/notimeout/STATUS';
export const RESET = 'reports/notimeout/RESET';
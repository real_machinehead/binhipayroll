import React, { Component } from 'react';
import {
    View,
    Text,
    Button
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { withNavigation } from 'react-navigation';

//Children Components
import EmployeeSummaryForm from './form';
import * as PromptScreen from '../../../components/ScreenLoadStatus';
import MessageBox from '../../../components/MessageBox';

//Constants
const TITLE = 'GENERATE EMPLOYEE SUMMARY';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as employeeSummaryApi from '../data/employeeSummary/api';
import * as employeeSummaryActions from '../data/employeeSummary/actions';
//helper
import * as oHelper from '../../../helper';

export class EmployeeSummary extends Component {
    constructor(props){
        super(props);
        this.state = {
            calculating: false,
            calculatingMsg: '',
            refreshing: false,
            loadingScreen: {
                show: false,
                msg: 'test'
            },
            visible:false,
            msgBox: {
                show: false,
                type: '',
                msg: '',
                param: ''
            },
        }
    }

    _onFormSubmit = async(oData) => {

        this._setLoadingScreen(true, 'Sending...');
        
        console.log('loggggggggggssssssssssss',this.props);

        const compId=this.props.companyid;
        const email=oData.email;
        await employeeSummaryApi.get({compId:compId,email:email})
        .then((response) => response.json())
        .then((res) => {
            console.log('datasssssss',res);
            this._setMessageBox(true, res.flagno==1 ? 'success' : 'error-ok', res.message);
        })
        .catch((exception) => {          
            this._setMessageBox(true, 'error-ok', exception.message);
        });
        this._setLoadingScreen(false);

        console.log('datassssssss',oData);

    }

    _setCalculationStatus = (value, oData) => {
        this.setState({
            calculating: value,
            calculatingMsg: 'Calculating Payroll. Please wait...' + 
                '\nPayroll Date: ' + oHelper.convertDateToString(oData.payrollDate, 'MMMM DD, YYYY')+
                '\nPayroll Period: ' + oHelper.convertDateToString(oData.periodFrom, 'MMM DD, YYYY')+
                ' - ' + oHelper.convertDateToString(oData.periodTo, 'MMM DD, YYYY')
        });
    }

    componentDidMount(){
        this._getDataFromDB();
    }

    _getDataFromDB = () => {
        this.setState({visible:true});
    }

    _hideForm = () => {
        this.props.hideForm(true);
    }

    //Loading Screen and MsgBox Functions
    _setMessageBox = (show, type, msg, param) => {
        this.setState({
            msgBox: oHelper.setMsgBox(
                this.state.msgBox,
                show, 
                type,
                msg,
                param
            )
        })
    }

    _setLoadingScreen = (show, msg) => {
        this.setState({ 
            loadingScreen: oHelper.setLoadingScreen(show, msg)
        });
    }

    _msgBoxOnClose = (params) => {
        this.setState({
            msgBox: oHelper.clearMsgBox(this.state.msgBox)
        })
        if(this.state.allowCloseForm){
            this._hideForm();
        }
    }

    _onCancel=()=>{
        console.log('cancel here');
        this.setState({visible:false})
    }

    render(){
        const payrollListData = this.props.payrollList.data;
        const payrollListStatus = this.props.payrollList.status;
        //console.log('trackingnnnn',payrollListData,payrollListStatus);
        //console.log('trackingnnnn',this.props);
        const visible=this.state.visible;
        return(
            <View>
            {
                this.state.visible ? 
                <EmployeeSummaryForm
                        data={[]}
                        cl_status={[]}
                        title={TITLE}
                        visible={this.state.visible}
                        onCancel={this._onCancel}
                        onSubmit={this._onFormSubmit}/>
                    : 
                        null
            }
                <MessageBox
                    promptType={this.state.msgBox.type || ''}
                    show={this.state.msgBox.show || false}
                    onClose={this._msgBoxOnClose}
                    message={this.state.msgBox.msg || ''}
                />

                <PromptScreen.PromptGeneric 
                    show= {this.state.loadingScreen.show || false} 
                    title={this.state.loadingScreen.msg || ''}
                />

            </View>
        );
    }
}

function mapStateToProps (state) {
    console.log('statessssssssssssss',state.activeCompanyReducer.activecompany);
    return {
        payrollList: state.reports.payrollSchedule,
        entityid:state.loginReducer.logininfo.entityid,
        companyid:state.activeCompanyReducer.activecompany.id
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            payrollList: bindActionCreators(employeeSummaryActions, dispatch)
            //payrollGeneration: bindActionCreators(payrollTransactionActions, dispatch)
        }
    }
}
  
export default  withNavigation(connect(
    mapStateToProps,
    mapDispatchToProps
)(EmployeeSummary))
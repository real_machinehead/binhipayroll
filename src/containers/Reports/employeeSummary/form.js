import React, { Component } from 'react';
import {
  View,
  Modal,
  Text,
  ScrollView, 
  Keyboard,
  TouchableOpacity
} from 'react-native';
import t from 'tcomb-form-native'; // 0.6.9
import moment from "moment";

//Styles
import styles from './styles';
import stylesheet from '../../../global/globalFormStyle';

//Form Template 
import { customPickerTemplate } from '../../../global/tcomb-custom-select-android';
import { customDatePickerTemplate } from '../../../global/tcomb-custom-datepicker-android';

//Custom Components
//import FormModal from '../../../components/FormModal';
import FormModal from './formModal';
//Helpers
import * as oHelper from '../../../helper';

//Constants
import {CONSTANTS} from '../../../constants';

const Form = t.form.Form;

export default class PayrollGenerationForm extends Component{
    constructor(props){
        super(props);
        this.state = {
            _bHasMounted: false,
            _list: this.props.data,
            _payrollEnums: oHelper.generateDateEnums(this.props.data, 'id', 'payrollDate', 'MMMM DD, YYYY'),
            _oActiveData: null,
            _oFormData: {
                email: ''
            },
            _options: {}
        }
    }
    
    _onChange = (value) => {
        console.log('valuesssssssssssss: ' + JSON.stringify(value));
        let oFormData = {...this.state._oFormData};
        let oActiveData = {...this.state._oActiveData};
        
        oFormData.email=value.email;

        this.setState({
            _oFormData: oFormData,
            _oActiveData: oFormData
        })
    }

    componentDidMount(){
        this.setState({_bHasMounted: true});
    }

    _onCancel = () => {
        Keyboard.dismiss();
        this.props.onCancel();
    }
    
    _onSubmit = () => {
        Keyboard.dismiss();
        let oFormData = this.refs.form_employee_summary.getValue();
        if(oFormData){
            this.props.onSubmit(this.state._oActiveData);
        }
    }

    render(){
        const formStyles = styles.formStyles;
        const OPTIONS = {
            fields: {
                email:{ 
                    label: 'EMAIL ADDRESS',
                    error: CONSTANTS.ERROR.FORM,
                    editable: true
                },
            },
            stylesheet: stylesheet
        };

        const FORMTYPE = t.struct({
            email:t.String,
            //payrollid: t.enums(this.state._payrollEnums),
            //periodfrom: oHelper.isStringEmpty(this.state._oFormData.payrollid) ? t.maybe(t.Date) : t.Date,
            //periodto: oHelper.isStringEmpty(this.state._oFormData.payrollid) ? t.maybe(t.Date) : t.Date,
            //status: oHelper.isStringEmpty(this.state._oFormData.payrollid) ? t.maybe(t.String) : t.String,
        });
        
        const iStatusId = this.state._oFormData.statusid;
        console.log('loggggggggssssss',this.props);
        return(
            <FormModal 
                containerStyle={formStyles.container}
                //disabledSave = {iStatusId == '' || iStatusId == 5}
                visible={this.props.visible}
                onCancel={this._onCancel}
                onOK={this._onSubmit}
                title={this.props.title}

                submitLabel={'SEND TO EMAIL'}
                >
                
                <View style={formStyles.contForm}>
                    {
                        this.state._bHasMounted ?
                            <ScrollView>
                                <View style={formStyles.formContent}>
                                    <Form 
                                        ref='form_employee_summary'
                                        type={FORMTYPE}
                                        value={this.state._oFormData}
                                        options={OPTIONS}
                                        onChange={this._onChange}
                                    />
                                </View>
                            </ScrollView>
                        :
                            null
                    }
                    
                </View>
            </FormModal>
        )
    }
}

import React, { Component, PureComponent } from 'react';
import {Alert} from 'react-native';

//Children Components
import PaginatedList from '../../../components/PaginatedList';
import NoTimeOutReportItem from './item';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as notimeoutReportActions from '../data/notimeout/actions';

//Styles
import styles from '../styles';

//Constants
const TITLE = 'NO TIME-OUT REPORT';

//helper
import * as oHelper from '../../../helper';

export class NoTimeOutReport extends Component{
    _generatePayload = (page) => ({
        compid: this.props.activecompany.id,
        page: page,
        filter: 'COMPANY',
    })

    _onMount = () => 
        this.props.actions.notimeoutReport.get(
            this._generatePayload(1)
        );

    _onRefresh = async() => 
        await this.props.actions.notimeoutReport.refresh(
            this._generatePayload(1)
        );

    _onLoadMore = async(page) => 
        await this.props.actions.notimeoutReport.loadMore(
            this._generatePayload(page)
        );
    
    //Local Functions
    _renderItem = item => 
        <NoTimeOutReportItem
            item={item} 
            onConfirm={this._onConfirm}
        />;

    _onConfirm = async(oItem) => {
        Alert.alert(
            "Confirm Action",
            "Are you sure you want to cofirm selected No time in Occurence ?",
            [
                {text: 'NO', onPress: () => {}},
                {text: 'YES', onPress: () => {this._confirmToDB(oItem)}},
            ],
            { cancelable: false }
        )
    }

    _confirmToDB = async(oItem) => {
        let oInput = oHelper.copyObject(oItem);
        oInput.compid = this.props.activecompany.id;
        oInput.id = oItem.notimeoutid;
        this.oCompList.setLoadingScreen(true, 'Confirming No Time Out Occurence. Please wait...');
        const oRes = await this.props.actions.notimeoutReport.confirm(oInput);
        this.oCompList.setMessageBox(true, oRes.flagno==1 ? 'success' : 'error-ok', oRes.message);
        this.oCompList.setLoadingScreen(false);
    }

    render(){
        const {status, data} = this.props.notimeoutReport;
        const hideHeader = this.props.hideHeader || false;
        return(
            <PaginatedList
                hideHeader={hideHeader} 
                title={TITLE}
                status={status}
                data={data}
                onMount={this._onMount}
                onRefresh={this._onRefresh}
                onLoadMore={this._onLoadMore}
                listName='resultLists'
                renderItem={this._renderItem}
                ref={(ref) => { this.oCompList = ref; }}
                keyExtractorName='notimeoutid'
            />
        );
    }
}

function mapStateToProps (state) {
    return {
        notimeoutReport: state.reports.notimeout,
        activecompany: state.activeCompanyReducer.activecompany
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            notimeoutReport: bindActionCreators(notimeoutReportActions, dispatch),
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NoTimeOutReport)
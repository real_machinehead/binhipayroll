import React, { Component } from 'react';
import {
  View,
  ScrollView,
  Keyboard
} from 'react-native';
import t from 'tcomb-form-native'; // 0.6.9
import moment from "moment";

//Form Template 
import { customPickerTemplate } from '../../../global/tcomb-custom-select-android';
import { customDatePickerTemplate } from '../../../global/tcomb-custom-datepicker-android';

//Styles
import styles from './styles';
import stylesheet from '../../../global/globalFormStyle';
import * as CustomForm from '../../../components/CustomForm';

//Custom Components
import FormModal from '../../../components/FormModal';

//Helper
import * as oHelper from '../../../helper';

const Form = t.form.Form;

export default class tardinessReport extends Component {
    constructor(props){
        super(props);
        this.state={
            disabledSave: false,
            didMount: false,
            data: {
                id: this.props.data.id,
                name: this.props.data.name,
                date: this.props.data.date,
                //timein: this.props.data.date+' '+this.props.data.timein,
                timein:this.props.data.timein,
                lateduration:'',
                penaltytype: this.props.data.penaltytype,
                paydate: this.props.data.paydate,
                status: this.props.data.status,
                employeeid:this.props.data.employeeid
            }
        }
    }

    componentDidMount(){
        this.setState({didMount: true});
    }

    _onDataChange = (value) => {
        let oData = oHelper.copyObject(this.state.data);
        //oData.code = value.code;
    
        oData.timein = value.timein;
        //oData.timein = oHelper.convertDateToString(value.timein, 'hh:mm:ss A')
        if(value.timein)
        this.setState({ data: oData });
    }

    _onSubmit = () => {
        Keyboard.dismiss();
        let oData = this.refs.form_tardiness.getValue();
        console.log('datassssssss',oData);
        if(oData){
            this.props.onSubmit(this.state.data)
        }
    }

    render() {
        const ENTITY = t.struct({
            name: t.String,
            date: t.String,
            timein:t.Date,
            paydate:t.String,
            //id:t.Integer,
            //lateduration:t.Integer,
            //status:t.String

            //oldtime: t.maybe(t.Date),
            //newtime: this.state._oTimeInfo.notimeentry ? t.maybe(t.Date) : t.Date,
        })
        
        const OPTIONS = {
            fields: {
                name:{ 
                    label: 'NAME' ,
                    returnKeyType: 'next',
                    onSubmitEditing: (event) => {this.refs.form_tardiness.getComponent('name').refs.input.focus()},
                    editable: false,
                    //disabled: true,
                },
                date:{ 
                    label: 'DATE' ,
                    returnKeyType: 'next',
                    editable: false,
                    //disabled: true,
                },
                timein:{ 
                    template: customDatePickerTemplate,
                    label: 'Time-In',
                    mode:'time',
                    disabled: false,
                    config:{
                        dialogMode:'spinner',
                        format: (strDate) => oHelper.convertDateToString(strDate, 'hh:mm:ss A')
                    },
                    //error: '*Something went wrong'
                },
                /*lateduration:{ 
                    label: 'Late Duration' ,
                    returnKeyType: 'next',
                    editable: false,
                    //disabled: true,
                },
                paydate:{ 
                    label: 'PAYROLL DATE' ,
                    returnKeyType: 'next',
                    editable: false,
                    disabled: true,
                },
                /*date: {
                    template: customDatePickerTemplate,
                    label: this.state.data.isEndSet ? 'VALID FROM' : 'EFFECTIVE DATE',
                    mode:'date',
                    config:{
                        format:  (strDate) => oHelper.convertDateToString(strDate, this.props.data.displaydateformformat)
                    },
                    error: '*Required field'
                },*/
            },
            stylesheet: stylesheet
        }
        console.log('tardiness ssssss',this.props);
        return (
            <FormModal 
                containerStyle={styles.formStyles.container}
                visible={this.props.visible}
                onCancel={this.props.onCancel}
                onOK={this._onSubmit}
                disabledSave={this.state.disabledSave}
                title={this.props.title}
                submitLabel='SAVE'>
                
                <View style={styles.formStyles.contForm}>
                    {
                        this.state.didMount ?
                            <ScrollView>
                                <View style={styles.formStyles.formContent}>
                                    <Form 
                                        ref='form_tardiness'
                                        type={ENTITY}
                                        value={this.state.data}
                                        options={OPTIONS}
                                        onChange={this._onDataChange}
                                    />
                                </View>
                            </ScrollView>
                        :
                            null
                    }
                    
                </View>
            </FormModal>
        );
    }
}

import React, { Component, PureComponent } from 'react';
import {
    View,
    Text,
    FlatList,
    ActivityIndicator,
    Alert
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

//Children Components
import ReportContent from '../reportContent';
import GenericContainer from '../../../components/GenericContainer';
import TardinessReportItem from './item';
import { SimpleCard } from '../../../components/CustomCards';
import SeeMoreButton from '../../../components/SeeMoreButton';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as tardinessReportActions from '../data/tardiness/actions';
import * as tardinessReportApi from '../data/tardiness/api';

//Styles
import styles from '../styles';

//Constants
const TITLE = 'TARDINESS OCCURENCES REPORT';

//helper
import * as oHelper from '../../../helper';

export class TardinessReport extends Component{
    constructor(props){
        super(props);
        this.state = {
            currentPage: 1,
            refreshing: false,
            loadingMore: false,
            loadingMoreFailed: false,
            loadingScreen: {
                show: false,
                msg: 'test'
            },
            msgBox: {
                show: false,
                type: '',
                msg: '',
                param: ''
            },
            
        }
    }

    componentDidMount() {
        this._fetchDataFromDB({
            filter: 'COMPANY', 
            id: this.props.activecompany.id,
            page: this.state.currentPage
        });
    }

    _fetchDataFromDB = (oCriteria) => {
        this.props.actions.tardinessReport.get(oCriteria);
    }

    //Flat List Related Functions
    _onPullRefresh = () => {
        this.setState({currentPage: 1});
        this._fetchDataFromDB({
            filter: 'COMPANY', 
            id: this.props.activecompany.id,
            page: 1
        });
    }

    _seeMore = () => {
        this.setState({
            loadingMoreFailed: false
        },
            () => {
                this._loadMoreFromDB();
            }
        )
    }

    _keyExtractor = (item, index) => item.id;

    _renderItem = (item) => {
        const ICONNAME_PENDING = 'alert-circle-outline';
        const ICONNAME_RESOLVED = 'check-circle-outline';
        const itemStyles = styles.reportItemStyles;
        const backgroundColor = 
            item.statusid == 4 ? 
                {backgroundColor: 'rgba(21, 123, 19, 0.86)'}
            :
                {backgroundColor: 'rgba(193, 66, 66, 0.94)'}

        const oTitle = 
            <View style={itemStyles.title.contContent}>
                <View style={itemStyles.title.contIcon}>
                    <View style={itemStyles.title.iconPlaceholder}>
                        <Icon 
                            name={
                                item.statusid == 4 ? ICONNAME_RESOLVED : ICONNAME_PENDING
                            } 
                            size={30} 
                            color='#f4f4f4'
                        />
                    </View>
                </View>
                <View style={itemStyles.title.contLabel}>
                    <Text style={itemStyles.title.txtLabel}>
                        {item.lateduration + ' ' + item.latedurationunit}
                    </Text>
                    <Text style={itemStyles.title.txtDescription}>
                        { oHelper.convertDateToString(item.date, 'MMMM DD, YYYY') }
                    </Text>
                    <Text style={itemStyles.title.txtDescription}>
                        { item.statusname }
                    </Text>
                </View>
            </View>


        return (
           
            <SimpleCard
            
                data={item.views}
                customTitleStyle={[itemStyles.title.container, backgroundColor]}
                oTitle={oTitle}
                menu={[]}
            />
        )
    }

    _renderFooter = () => {
        return (
            this.state.loadingMoreFailed ? 
                <SeeMoreButton onPress={this._seeMore}/>
            :
                this.state.loadingMore ? 
                    <ActivityIndicator size="small" color="#EEB843" /> 
                : 
                    null
        );
    }

    //Load More Related Functions
    _updateDataStore = async(res) => {
        let oCurData = oHelper.copyObject(this.props.tardinessReport.data);
        let aResList = res.data.list;
        let mergedList = await oHelper.arrayMerge(oCurData.list, aResList, 'id');
        oCurData.list = mergedList;
        this.props.actions.tardinessReport.update(oCurData);
    }

    _loadMoreFromDB = () => {
        if(!this.state.loadingMore && !this.state.loadingMoreFailed){
            const nextpagenumber =  this.state.currentPage + 1;

            if(nextpagenumber <= this.props.tardinessReport.data.totalpages){
                this.setState({ loadingMore: true, loadingMoreFailed: false });
                const oCriteria = {
                    filter: 'COMPANY', 
                    id: this.props.activecompany.id,
                    page: nextpagenumber
                };

                tardinessReportApi.get(oCriteria)
                    .then((response) => response.json())
                    .then((res) => {
                        if(res.flagno == 1){
                            this._updateDataStore(res);
                            this.setState({currentPage: nextpagenumber})
                        }else{
                            this._displayMoreFailedMsg(res.message);
                            this.setState({ loadingMoreFailed: true });
                        }
                        this.setState({loadingMore: false});
                    })
                    .catch((exception) => {
                        this._displayMoreFailedMsg(exception.message);
                        this.setState({loadingMore: false});
                        this.setState({ loadingMoreFailed: true });
                    });
            }
        }
    };

    _displayMoreFailedMsg = (msg = null) => {
        const strMsg = msg ? 'Please check your internet connection or try loading again in a minute.' : msg;
        Alert.alert(
            "Sorry, We're Having Some Issues",
            strMsg,
            [
              {text: 'OK', onPress: () => {}},
            ],
            { cancelable: false }
        )
    }

    //Children Components Related Functions
    _setLoadingScreen = (show, msg) => {
        let oLoadingScreen = {...this.state.loadingScreen};
        oLoadingScreen.show = show;
        oLoadingScreen.msg = msg;
        this.setState({ loadingScreen: oLoadingScreen });
    }

    _setMessageBox = (show, type, msg, param) => {
        this.setState({
            msgBox: oHelper.setMsgBox(
                this.state.msgBox,
                show, 
                type,
                msg,
                param
            )
        })
    }

    _msgBoxOnClose = (params) => {
        this.setState({
            msgBox: oHelper.clearMsgBox(this.state.msgBox)
        })
    }

    render(){
        const aStatus = this.props.tardinessReport.status;
        const oData = this.props.tardinessReport.data;
        let oBody = null;
        let aFilters = [];

        if(aStatus[0] == 1 || oData){
            oBody =
                <FlatList
                    refreshing={this.state.refreshing || aStatus[0] == 2}
                    onRefresh={this._onPullRefresh}
                    contentContainerStyle={styles.reportsFlatlist}
                    ref={(ref) => { this.flatListRef = ref; }}
                    extraData={this.state.loadingMore}
                    keyExtractor={this._keyExtractor}
                    data={oData.list}
                    onEndReached={this._loadMoreFromDB}
                    onEndReachedThreshold={0.5}
                    ListFooterComponent={this._renderFooter}
                    renderItem={({item}) => this._renderItem(item) }
                />
            aFilters = [];
        }

        return(
            <GenericContainer
                msgBoxShow = {this.state.msgBox.show}
                msgBoxType = {this.state.msgBox.type}
                msgBoxMsg = {this.state.msgBox.msg}
                msgBoxOnClose = {this._msgBoxOnClose}
                msgBoxOnYes = {this._msgBoxOnYes}
                containerStyle = {styles.container}
                msgBoxParam = {this.state.msgBox.param}
                loadingScreenShow = {this.state.loadingScreen.show}
                loadingScreenMsg = {this.state.loadingScreen.msg}
                status={oData ? [1,''] : aStatus}
                title={TITLE}
                onRefresh={this._fetchDataFromDB}>

                <ReportContent
                    hideHeader={this.props.hideHeader || false}
                    title={TITLE}
                    body={oBody}
                    filters={aFilters}
                />
            </GenericContainer>
        )
    }
}

function mapStateToProps (state) {
    return {
        tardinessReport: state.reports.tardiness,
        activecompany: state.activeCompanyReducer.activecompany
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            tardinessReport: bindActionCreators(tardinessReportActions, dispatch),
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(TardinessReport)
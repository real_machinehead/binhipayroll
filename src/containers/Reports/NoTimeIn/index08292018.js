
import React, { Component, PureComponent } from 'react';
import {
    View,
    Text,
    FlatList,
    ActivityIndicator,
    Alert
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

//Children Components
//import PaginatedList from '../../../components/PaginatedList';
import NotimeinReportItem from './item';
import CustomCard, {SimpleCard} from '../../../components/CustomCards';
import GenericContainer from '../../../components/GenericContainer';
import PaginatedList from '../../../components/PaginatedList';
//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as notimeinReportActions from '../data/notimein/actions';
import NotimeinreportForm from './forms';
//Styles
import styles from './styles';

//Constants
const TITLE = 'NO TIME IN REPORT';
const DESCRIPTION='';
//helper
import * as oHelper from '../../../helper';
import * as notimeinApi from '../data/notimein/api';

export class NoTimeInReport extends Component{

    constructor(props){
        super(props);
        this.state = {
            currentPage: 1,
            _showForm: false,
            loadingMore: false,
            loadingMoreFailed: false,
            refreshing: false,
            loadingScreen: {
                show: false,
                msg: 'test'
            },
            msgBox: {
                show: false,
                type: '',
                msg: '',
                param: ''
            },
            activedata:{
                id: '',
                name: '',
                date: '',
                timein: '',
                timeout: '',
                status: '',
                employeeid:'',                
            },
        }
    }

    componentDidMount(){
        if(this.props.notimeinReport.status[0] != 1){
            this._fetchDataFromDB();
        }
    }

    _fetchDataFromDB= () => {
        this.props.actions.notimeinReport.get(
            this._generatePayload(1))
    }

    _generatePayload = (page) => ({
        compid: this.props.activecompany.id,
        page: page,
        //filter: 'COMPANY',
        filter: this.props.filter || 'COMPANY',
        empid: this.props.id,
    })

    _onMount = () => 
        this.props.actions.notimeinReport.get(
            this._generatePayload(1)
    );

    _onRefresh = async() => 
        await this.props.actions.notimeinReport.refresh(
            this._generatePayload(1)
    );

    _onLoadMore = async(page) => 
        await this.props.actions.notimeinReport.loadMore(
            this._generatePayload(page)
        );


   _setLoadingScreen = (show, msg) => {
        let oLoadingScreen = {...this.state.loadingScreen};
        oLoadingScreen.show = show;
        oLoadingScreen.msg = msg;
        this.setState({ loadingScreen: oLoadingScreen });
    }

    /*_renderItem = (oItem) => {
       
            return(

              );
    }*/

    _renderItem = item =>
        <NotimeinReportItem
            item={item} 
            onConfirm={this._onConfirm}
        />;
        
    _onConfirm = async(oItem) => {
  
        Alert.alert(
            "Confirm Action",
            "Are you sure you want to confirm No Time in Occurence ?",
            [
                {text: 'NO', onPress: () => {}},
                {text: 'YES', onPress: () => {this._confirmToDB(oItem)}},
            ],
            { cancelable: false }
        )
    }
    
    _keyExtractor = (item, index) => item.notimeinid;


    _setMessageBox = (show, type, msg, param) => {
        this.setState({
            msgBox: oHelper.setMsgBox(
                this.state.msgBox,
                show, 
                type,
                msg,
                param
            )
        })
    }

    _onSubmitForm = (oData) => {
        console.log('dataaaaaa',oData)
        if(oHelper.isStringEmptyOrSpace(oData.id)){
            this._addToDB(oData);
        }else{
            Alert.alert(
                'Confirm Action',
                'Are you sure you want to update ' + oData.name + ' ?',
                [
                    {text: 'No', onPress: () => {}},
                    {text: 'Yes', onPress: () => this._updateToDB(oData)},
                ],
                { cancelable: false }
            ) 
        }
    }

    _msgBoxOnClose = (params) => {
        this.setState({
            msgBox: oHelper.clearMsgBox(this.state.msgBox)
        })
    }


    /*_confirmToDB = async(oItem) => {
        this.oCompList.setLoadingScreen(true, 'Confirming Overtime Occurence. Please wait...');
        let oInput = oHelper.copyObject(oItem);
        oInput.compid = this.props.activecompany.id;
        oInput.id = oItem.overtimeid;
        const oRes = await this.props.actions.overtime.confirm(oInput);
        this.oCompList.setMessageBox(true, oRes.flagno==1 ? 'success' : 'error-ok', oRes.message);
        this.oCompList.setLoadingScreen(false);
    }*/


    _confirmToDB = async(oItem) => {
        
        
        let oInput = oHelper.copyObject(oItem);
        oInput.compid = this.props.activecompany.id;
 
        oInput.id = oItem.notimeinid;
        
        this._setLoadingScreen(true, 'Resolving No time in Validation. Please wait...');
        await notimeinApi.confirm(oInput)
            .then((response) => response.json())
            .then((res) => {
                console.log('RES: ' + JSON.stringify(res));
                if(res.flagno == 1){
                    oItem.statusid = 4;
                    this.props.actions.notimein.confirm(oInput);
                    this._closeForm();
                }
                this._setMessageBox(true, res.flagno==1 ? 'success' : 'error-ok', res.message);
                this._fetchDataFromDB();    
            })
            .catch((exception) => {
                this._setMessageBox(true, 'error-ok', exception.message);
            });
        this._setLoadingScreen(false);


    }

    _updateToDB = async(oData) => {
        console.log('ConfirmItems',oData);
        //oData.timeout=oHelper.convertDateToString(oData.timeout, 'hh:mm:ss A')
        let oInput = oHelper.copyObject(oData);
        oInput.compid = this.props.activecompany.id;
        oInput.id = oData.id;
        oInput.code="1010";
        oInput.statusid=4;
        oInput.employeeid=oData.employeeid;
        oInput.employeename=oData.name;
        //oInput.newtime=oData.timeout;
        oInput.newtime=oHelper.convertDateToString(oData.timeout, 'hh:mm:ss A');
        oInput.statusid=4;
        //console.log('UpdateDbbbb',oInput);
        this._setLoadingScreen(true, 'Updating No time in Validation. Please wait...');
        await notimeinApi.update(oInput)
            .then((response) => response.json())
            .then((res) => {
                console.log('RES: ' + JSON.stringify(res));
                if(res.flagno == 1){
                    this.props.actions.notimein.updates(oInput)
                    this.props.actions.notimein.confirm(oInput);
                    this._closeForm();
                }
                this._setMessageBox(true, res.flagno==1 ? 'success' : 'error-ok', res.message);
                this._fetchDataFromDB();    
            })
            .catch((exception) => {
                this._setMessageBox(true, 'error-ok', exception.message);
            });
        this._setLoadingScreen(false);
    
    } 


    _modifydtr=(item,_showForm)=>{
        console.log('itemssssssssssss',item,_showForm);

        this.setState({
            _showForm:_showForm,
            //activedata:item.views
            activedata:{
                id: item.notimeinid,
                name: item.views[0][1],
                date: item.views[1][1],
                //timeout:item.views[2][1],
                timein:item.views[3][1],
                timeout:item.views[4][1],
                status:item.views[5][1],
                employeeid:item.employeeid
            }
        })
        if(_showForm){        
            console.log('show Form',_showForm);

        }
            
    }

    _closeForm = () => {
        this.setState({
            _showForm: false
        })
    }

    render(){
        const {status, data} = this.props.notimeinReport;
        const hideHeader = this.props.hideHeader || false;
        const activedata = this.state.activedata;
        console.log('datasssss11',this.props.notimeinReport.status);

        return(

            <GenericContainer
                    customContainer = {{flex: 1}}
                    msgBoxShow = {this.state.msgBox.show}
                    msgBoxType = {this.state.msgBox.type}
                    msgBoxMsg = {this.state.msgBox.msg}
                    msgBoxOnClose = {this._msgBoxOnClose}
                    msgBoxOnYes = {this._msgBoxOnYes}
                    containerStyle = {[styles.container, this.props.containerStyle || {}]}
                    msgBoxParam = {this.state.msgBox.param}
                    loadingScreenShow = {this.state.loadingScreen.show}
                    loadingScreenMsg = {this.state.loadingScreen.msg}
                    status={status}
                    title={TITLE}
                    onRefresh={this._fetchDataFromDB}>         
                {

                    data ?

                        <CustomCard 
                            contentContainerStyle={styles.container}
                            clearMargin
                            title={TITLE} 
                            oType='text'
                            description = {DESCRIPTION}
                        >
    
                            {
                                data.length < 1 ?
                                    <EmptyList message='No Time In  Record' />
                                :

                                    <PaginatedList
                                        hideHeader={hideHeader} 
                                        title={TITLE}
                                        status={status}
                                        data={data}
                                        onMount={this._onMount}
                                        onRefresh={this._onRefresh}
                                        onLoadMore={this._onLoadMore}
                                        listName='resultLists'
                                        renderItem={this._renderItem}
                                        pageName='pages'
                                        //renderItem={({item}) => this._renderItem(data.resultLists) }
                                        ref={(ref) => { this.oCompList = ref; }}
                                        keyExtractorName='notimeinid'
                                        //keyExtractor={this._keyExtractor}
                                    />


                                    /*<PaginatedList
                                        hideHeader={hideHeader} 
                                        title={TITLE}
                                        status={status}
                                        data={data}
                                        onMount={this._onMount}
                                        onRefresh={this._onRefresh}
                                        onLoadMore={this._onLoadMore}
                                        listName='resultsList'
                                        pageName='total_pages'
                                        renderItem={this._renderItem}
                                        ref={(ref) => { this.oCompList = ref; }}
                                        keyExtractorName='id'
                                    />*/
                                    /*<FlatList
                                        refreshing={this.state.refreshing}
                                        onRefresh={this._fetchDataFromDB}
                                        contentContainerStyle={styles.reportsFlatlist}
                                        ref={(ref) => { this.flatListRef = ref; }}
                                        extraData={data}
                                        data={data.resultLists}
                                        keyExtractor={this._keyExtractor}
                                        renderItem={({item}) => this._renderItem(item) }
                                    />*/
                            }
                            
                        </CustomCard> 
                    :
                        null
                /*<PaginatedList
                        hideHeader={hideHeader} 
                        title={TITLE}
                        status={status}
                        data={data}
                        onMount={this._onMount}
                        onRefresh={this._onRefresh}
                        onLoadMore={this._onLoadMore}
                        listName='list'
                        renderItem={this._renderItem}
                        ref={(ref) => { this.oCompList = ref; }}
                        pageName="totalpages"
                />*/
                }   
                {
                    this.state._showForm ?
                        <NotimeinreportForm 
                            onCancel={this._closeForm}
                            onSubmit={this._onSubmitForm}
                            visible={this.state._showForm}
                            title='MODIFY DTR'
                            data={this.state.activedata}
                        />   
                    :
                        null
                }
            </GenericContainer>

        );
    }
}

function mapStateToProps (state) {
    return {
        notimeinReport: state.reports.notimein,
        activecompany: state.activeCompanyReducer.activecompany,
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            notimeinReport: bindActionCreators(notimeinReportActions, dispatch),
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NoTimeInReport)

import React, { Component, PureComponent } from 'react';
import {Alert} from 'react-native';

//Children Components
import PaginatedList from '../../../components/PaginatedList';
import NoTimeInReportItem from './item';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as notimeinReportActions from '../data/notimein/actions';

//Styles
import styles from '../styles';

//Constants
const TITLE = 'NO TIME-IN REPORT';

//helper
import * as oHelper from '../../../helper';

export class NoTimeInReport extends Component{
    _generatePayload = (page) => ({
        compid: this.props.activecompany.id,
        page: page,
        filter: 'COMPANY',
    })

    _onMount = () => 
        this.props.actions.notimeinReport.get(
            this._generatePayload(1)
        );

    _onRefresh = async() => 
        await this.props.actions.notimeinReport.refresh(
            this._generatePayload(1)
        );

    _onLoadMore = async(page) => 
        await this.props.actions.notimeinReport.loadMore(
            this._generatePayload(page)
        );
    
    //Local Functions
    _renderItem = item => 
        <NoTimeInReportItem
            item={item} 
            onConfirm={this._onConfirm}
        />;

    _onConfirm = async(oItem) => {
        Alert.alert(
            "Confirm Action",
            "Are you sure you want to cofirm selected No time in Occurence ?",
            [
                {text: 'NO', onPress: () => {}},
                {text: 'YES', onPress: () => {this._confirmToDB(oItem)}},
            ],
            { cancelable: false }
        )
    }

    _confirmToDB = async(oItem) => {
        let oInput = oHelper.copyObject(oItem);
        oInput.compid = this.props.activecompany.id;
        oInput.id = oItem.notimeinid;
        this.oCompList.setLoadingScreen(true, 'Confirming Overtime Occurence. Please wait...');
        const oRes = await this.props.actions.notimeinReport.confirm(oInput);
        this.oCompList.setMessageBox(true, oRes.flagno==1 ? 'success' : 'error-ok', oRes.message);
        this.oCompList.setLoadingScreen(false);
    }

    render(){
        const {status, data} = this.props.notimeinReport;
        const hideHeader = this.props.hideHeader || false;
        return(
            <PaginatedList
                hideHeader={hideHeader} 
                title={TITLE}
                status={status}
                data={data}
                onMount={this._onMount}
                onRefresh={this._onRefresh}
                onLoadMore={this._onLoadMore}
                listName='resultLists'
                renderItem={this._renderItem}
                ref={(ref) => { this.oCompList = ref; }}
                keyExtractorName='notimeinid'
            />
        );
    }
}

function mapStateToProps (state) {
    return {
        notimeinReport: state.reports.notimein,
        activecompany: state.activeCompanyReducer.activecompany
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            notimeinReport: bindActionCreators(notimeinReportActions, dispatch),
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NoTimeInReport)
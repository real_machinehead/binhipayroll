import React, { Component } from 'react';
import {
  View,
  ScrollView,
  Keyboard
} from 'react-native';
import t from 'tcomb-form-native'; // 0.6.9
import moment from "moment";

//Form Template 
import { customPickerTemplate } from '../../../global/tcomb-custom-select-android';
import { customDatePickerTemplate } from '../../../global/tcomb-custom-datepicker-android';

//Styles
import styles from './styles';
import stylesheet from '../../../global/globalFormStyle';
import * as CustomForm from '../../../components/CustomForm';

//Custom Components
import FormModal from '../../../components/FormModal';

//Helper
import * as oHelper from '../../../helper';

const Form = t.form.Form;

export default class undertimeReport extends Component {
    constructor(props){
        super(props);
        this.state={
            disabledSave: false,
            didMount: false,
            data: {
                id: this.props.data.id,
                name: this.props.data.name,
                date: this.props.data.date,
                //timein: this.props.data.date+' '+this.props.data.timein,
                timeout:this.props.data.timeout,
                employeeid:this.props.data.employeeid,
                undertimeduration:this.props.data.undertimeduration,
                paydate: this.props.data.paydate,
                status: this.props.data.status
            }
        }
    }

    componentDidMount(){
        this.setState({didMount: true});
    }

    _onDataChange = (value) => {
        let oData = oHelper.copyObject(this.state.data);
        //oData.code = value.code;
    
        oData.timeout = value.timeout;
        //oData.timeout = oHelper.convertDateToString(value.timeout, 'hh:mm:ss A')
        if(value.timeout)
        this.setState({ data: oData });
    }

    _onSubmit = () => {
        Keyboard.dismiss();
        let oData = this.refs.form_undertime.getValue();
        console.log('datassssssss',this.state.data);
        if(oData){
            this.props.onSubmit(this.state.data)
        }
    }

    render() {
        const ENTITY = t.struct({
            name: t.String,
            date: t.String,
            timeout:t.Date,
            paydate:t.String,

        })
        
        const OPTIONS = {
            fields: {
                name:{ 
                    label: 'NAME' ,
                    returnKeyType: 'next',
                    onSubmitEditing: (event) => {this.refs.form_undertime.getComponent('name').refs.input.focus()},
                    editable: false,
                    //disabled: true,
                },
                date:{ 
                    label: 'DATE' ,
                    returnKeyType: 'next',
                    editable: false,
                    //disabled: true,
                },
                timeout:{ 
                    template: customDatePickerTemplate,
                    label: 'Time-Out',
                    mode:'time',
                    disabled: false,
                    config:{
                        dialogMode:'spinner',
                        format: (strDate) => oHelper.convertDateToString(strDate, 'hh:mm:ss A')
                    },
                    //error: '*Something went wrong'
                },
            },
            stylesheet: stylesheet
        }
        console.log('tardiness ssssss',this.props);
        return (
            <FormModal 
                containerStyle={styles.formStyles.container}
                visible={this.props.visible}
                onCancel={this.props.onCancel}
                onOK={this._onSubmit}
                disabledSave={this.state.disabledSave}
                title={this.props.title}
                submitLabel='SAVE'>
                
                <View style={styles.formStyles.contForm}>
                    {
                        this.state.didMount ?
                            <ScrollView>
                                <View style={styles.formStyles.formContent}>
                                    <Form 
                                        ref='form_undertime'
                                        type={ENTITY}
                                        value={this.state.data}
                                        options={OPTIONS}
                                        onChange={this._onDataChange}
                                    />
                                </View>
                            </ScrollView>
                        :
                            null
                    }
                    
                </View>
            </FormModal>
        );
    }
}


import React, { Component, PureComponent } from 'react';
import {Alert} from 'react-native';

//Children Components
import PaginatedList from '../../../components/PaginatedList';
import UndertimeReportItem from './item';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as undertimeActions from '../data/undertime/actions';

//Styles
import styles from '../styles';

//Constants
const TITLE = 'UNDERTIME OCCURENCES REPORT';

//helper
import * as oHelper from '../../../helper';

export class UndertimeReport extends Component{
    _generatePayload = (page) => ({
        compid: this.props.activecompany.id,
        page: page,
        //filter: 'COMPANY',
        filter: this.props.filter || 'COMPANY',
        empid: this.props.empid,
    })

    _onMount = () => 
        this.props.actions.undertime.get(
            this._generatePayload(1)
        );

    _onRefresh = async() => 
        await this.props.actions.undertime.refresh(
            this._generatePayload(1)
        );

    _onLoadMore = async(page) => 
        await this.props.actions.undertime.loadMore(
            this._generatePayload(page)
        );
    
    //Local Functions
    _renderItem = item => 
        <UndertimeReportItem
            item={item} 
            onConfirm={this._onConfirm}
        />;
    
    _onConfirm = async(oItem) => {
        Alert.alert(
            "Confirm Action",
            "Are you sure you want to cofirm selected Undertime Occurence ?",
            [
                {text: 'NO', onPress: () => {}},
                {text: 'YES', onPress: () => {this._confirmToDB(oItem)}},
            ],
            { cancelable: false }
        )
    }

    _confirmToDB = async(oItem) => {
        let oInput = oHelper.copyObject(oItem);
        oInput.compid = this.props.activecompany.id;
        oInput.id = oItem.tardinessid;
        this.oCompList.setLoadingScreen(true, 'Confirming Undertime Occurence. Please wait...');
        const oRes = await this.props.actions.undertime.confirm(oInput);
        this.oCompList.setMessageBox(true, oRes.flagno==1 ? 'success' : 'error-ok', oRes.message);
        this.oCompList.setLoadingScreen(false);
    }
    

    render(){
        const {status, data} = this.props.undertime;
        const hideHeader = this.props.hideHeader || false;
        return(
            <PaginatedList
                hideHeader={hideHeader} 
                title={TITLE}
                status={status}
                data={data}
                onMount={this._onMount}
                onRefresh={this._onRefresh}
                onLoadMore={this._onLoadMore}
                listName='resultLists'
                renderItem={this._renderItem}
                ref={(ref) => { this.oCompList = ref; }}
                keyExtractorName='tardinessid'
            />
        );
    }
}

function mapStateToProps (state) {
    return {
        undertime: state.reports.undertime,
        activecompany: state.activeCompanyReducer.activecompany
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            undertime: bindActionCreators(undertimeActions, dispatch),
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UndertimeReport)
import React, { Component } from 'react';
import {
    View,
    Text,
    Button,
    ScrollView,
    TouchableOpacity,
    ActivityIndicator
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as payslipActions from '../data/payrollsummary/actions';
import * as emalApi from '../data/payrollsummary/api';
//Children Components
import CustomPicker from '../../../components/CustomPicker';
import GenericContainer from '../../../components/GenericContainer';
import * as oHelper from '../../../helper';
//Styles
import styles from './stylesbody';
export class EmployeePayslipForm extends Component {
    constructor(props){
        super(props);
        this.state = {
            _bShowPicker: false,
            _bHasError: false,
            _bDidMount: false,
            _payrollid:'',
            _employeeid:'',
            refreshing: false,
            loadingScreen: {
                show: false,
                msg: 'test'
            },
            msgBox: {
                show: false,
                type: '',
                msg: '',
                param: ''
            },
        }
    }

    componentDidMount(){
        this._getDataFromDB();
    }

    _getDataFromDB = () => {
        //const activeRule = this.props.payslip.activeRule;
        //this.props.actions.payslip.get(activeRule);
        //this.props.actions.payslip.getpayslip(activeRule);
        const activePayroll = this.props.activedata;
        this.props.actions.payslip.getpayslip(activePayroll)
        //const activeRule = {payrollid:111000,employeeid: 471}
        console.log('Activepayrolllllllssss',this.props.payslip);
       
    }

    _onPeriodChange = () => {
        this.setState({
            _bShowPicker: true
        })
    }

    _onSelect = () => {
        this._hidePicker();
    }
    
    _hidePicker = () => {
        this.setState({
            _bShowPicker: false
        })
    }

    _setLoadingScreen = (show, msg) => {
        let oLoadingScreen = {...this.state.loadingScreen};
        oLoadingScreen.show = show;
        oLoadingScreen.msg = msg;
        this.setState({ loadingScreen: oLoadingScreen });
    }

    _setMessageBox = (show, type, msg, param) => {
        this.setState({
            msgBox: oHelper.setMsgBox(
                this.state.msgBox,
                show, 
                type,
                msg,
                param
            )
        })
    }
    
    _msgBoxOnClose = (params) => {
        this.setState({
            msgBox: oHelper.clearMsgBox(this.state.msgBox)
        })
    }

    _sentToEmail=async()=>{
       
        const payrollid=this.props.activedata.payrollid
        const employeerid = this.props.loginfo.entityid

        this._setLoadingScreen(true, 'Sending...')

        await emalApi.email({id:employeerid,payrollid:payrollid})
        .then((response) => response.json())
        .then((res) => {
            console.log('datasssssss',res);
            this._setMessageBox(true, res.flagno==1 ? 'success' : 'error-ok', res.message);
        })
        .catch((exception) => {          
            this._setMessageBox(true, 'error-ok', exception.message);
        });
        this._setLoadingScreen(false);
    }

    render(){
        //const headerStyles = styles.header;
        const headerStyles = styles.summaryStyles.header;
        const navigatorStyles = styles.navigator;
        //const contentStyles = styles.content;
        const contentStyles = styles.summaryStyles;
        const titleStyles = styles.title;
        const bodyStyles = styles.body;
        const systemStyles = styles.system;
        const textStyles = styles.textStyles;

        const aStatus = this.props.payslip.status;
        const oAllData = this.props.payslip.data;
        //const aStatus = [1,'success'];
  
        console.log('datasssss11111',this.props.loginfo.companyname[0].name);
        let oView = null;
        if(aStatus[0] == 1 || oAllData){
            oView = 
            
                <View style={styles.payrollSummary.container}>
                    <View tyle={styles.payrollSummary.headertitle}>
                        <View>
                            <Text style={textStyles.companyname}>{this.props.loginfo.companyname[0].name}</Text>
                            <Text style={textStyles.address}></Text>
                        </View>
                            <View>
                                <View style={styles.payrollSummary.summarydetails}>
                                    <Text style={textStyles.companyname}>Details</Text>
                                </View>
                                <View style={styles.payrollSummary.summarydetailsbody}>
                                    <ScrollView>    
                                        <View style={styles.payrollSummary.summarylistdetails}>
                                            <View>
                                                <Icon2 style={textStyles.companyname}
                                                name='check' 
                                                color='#434646'/>
                                            </View>
                                             <View style={styles.payrollSummary.summarylistdescription}>
                                                <Text style={styles.payrollSummary.summarylistdescriptionfont}>{oAllData[0][0]}</Text>
                                            </View>
                                            <View>
                                                <Text style={styles.payrollSummary.summarylistdescriptionfont}>{oAllData[0][1]}</Text>
                                            </View>
                                        </View>
                                        <View style={styles.payrollSummary.summarylistdetails}>
                                            <View>
                                                <Icon2 style={textStyles.companyname}
                                                name='check' 
                                                color='#434646'/>
                                            </View>
                                            <View style={styles.payrollSummary.summarylistdescription}>
                                                <Text style={styles.payrollSummary.summarylistdescriptionfont}>{oAllData[1][0]}</Text>
                                            </View>
                                            <View>
                                                <Text style={styles.payrollSummary.summarylistdescriptionfont}>{oAllData[1][1]}</Text>
                                            </View>
                                        </View>

                                        <View style={styles.payrollSummary.summarylistdetails}>
                                            <View>
                                                <Icon2 style={textStyles.companyname}
                                                name='check' 
                                                color='#434646'/>
                                            </View>
                                            <View style={styles.payrollSummary.summarylistdescription}>
                                                <Text style={styles.payrollSummary.summarylistdescriptionfont}>{oAllData[2][0]}</Text>
                                            </View>
                                            <View>
                                                <Text style={styles.payrollSummary.summarylistdescriptionfont}>{oAllData[2][1]}</Text>
                                            </View>
                                        </View>

                                        <View style={styles.payrollSummary.summarylistdetails}>
                                            <View>
                                                <Icon2 style={textStyles.companyname}
                                                name='check' 
                                                color='#434646'/>
                                            </View>
                                            <View style={styles.payrollSummary.summarylistdescription}>
                                                <Text style={styles.payrollSummary.summarylistdescriptionfont}>{oAllData[3][0]}</Text>
                                            </View>
                                            <View>
                                                <Text style={styles.payrollSummary.summarylistdescriptionfont}>{oAllData[3][1]}</Text>
                                            </View>
                                        </View>
                                        <View style={styles.payrollSummary.summarylistdetails}>
                                            <View>
                                                <Icon2 style={textStyles.companyname}
                                                name='check' 
                                                color='#434646'/>
                                            </View>
                                            <View style={styles.payrollSummary.summarylistdescription}>
                                                <Text style={styles.payrollSummary.summarylistdescriptionfont}>{oAllData[4][0]}</Text>
                                            </View>
                                            <View>
                                                <Text style={styles.payrollSummary.summarylistdescriptionfont}>{oAllData[4][1]}</Text>
                                            </View>
                                        </View>
                                        <View style={styles.payrollSummary.summarylistdetails}>
                                            <View>
                                                <Icon2 style={textStyles.companyname}
                                                name='check' 
                                                color='#434646'/>
                                            </View>
                                            <View style={styles.payrollSummary.summarylistdescription}>
                                                <Text style={styles.payrollSummary.summarylistdescriptionfont}>{oAllData[5][0]}</Text>
                                            </View>
                                            <View>
                                                <Text style={styles.payrollSummary.summarylistdescriptionfont}>{oAllData[5][1]}</Text>
                                            </View>
                                        </View>
                                            <View style={styles.payrollSummary.summarylistdetails}>
                                                <View>
                                                    <Icon2 style={textStyles.companyname}
                                                    name='check' 
                                                    color='#434646'/>
                                                </View>
                                                <View style={styles.payrollSummary.summarylistdescription}>
                                                    <Text style={styles.payrollSummary.summarylistdescriptionfont}>{oAllData[6][0]}</Text>
                                                </View>
                                                <View>
                                                    <Text style={styles.payrollSummary.summarylistdescriptionfont}>{oAllData[6][1]}</Text>
                                                </View>
                                            </View>
                                            <View style={styles.payrollSummary.summarylistdetails}>
                                                <View>
                                                    <Icon2 style={textStyles.companyname}
                                                    name='check' 
                                                    color='#434646'/>
                                                </View>
                                                <View style={styles.payrollSummary.summarylistdescription}>
                                                    <Text style={styles.payrollSummary.summarylistdescriptionfont}>{oAllData[7][0]}</Text>
                                                </View>
                                                <View>
                                                    <Text style={styles.payrollSummary.summarylistdescriptionfont}>{oAllData[7][1]}</Text>
                                                </View>
                                            </View>
                                            <View style={styles.payrollSummary.summarylistdetails}>
                                                <View>
                                                    <Icon2 style={textStyles.companyname}
                                                    name='check' 
                                                    color='#434646'/>
                                                </View>
                                                <View style={styles.payrollSummary.summarylistdescription}>
                                                    <Text style={styles.payrollSummary.summarylistdescriptionfont}>{oAllData[8][0]}</Text>
                                                </View>
                                                <View>
                                                    <Text style={styles.payrollSummary.summarylistdescriptionfont}>{oAllData[8][1]}</Text>
                                                </View>
                                                </View>
                                                <View style={styles.payrollSummary.summarylistdetails}>
                                                    <View>
                                                        <Icon2 style={textStyles.companyname}
                                                        name='check' 
                                                        color='#434646'/>
                                                    </View>
                                                    <View style={styles.payrollSummary.summarylistdescription}>
                                                        <Text style={styles.payrollSummary.summarylistdescriptionfont}>{oAllData[9][0]}</Text>
                                                    </View>
                                                    <View>
                                                        <Text style={styles.payrollSummary.summarylistdescriptionfont}>{oAllData[9][1]}</Text>
                                                     </View>
                                                </View>
                                                <View style={styles.payrollSummary.summarylistdetails}>
                                                    <View>
                                                        <Icon2 style={textStyles.companyname}
                                                        name='check' 
                                                        color='#434646'/>
                                                    </View>
                                                    <View style={styles.payrollSummary.summarylistdescription}>
                                                        <Text style={styles.payrollSummary.summarylistdescriptionfont}>{oAllData[10][0]}</Text>
                                                    </View>
                                                    <View>
                                                        <Text style={styles.payrollSummary.summarylistdescriptionfont}>{oAllData[10][1]}</Text>
                                                    </View>
                                                </View>
                                                <View style={styles.payrollSummary.summarylistdetails}>
                                                    <View>
                                                        <Icon2 style={textStyles.companyname}
                                                        name='check' 
                                                        color='#434646'/>
                                                    </View>
                                                    <View style={styles.payrollSummary.summarylistdescription}>
                                                        <Text style={styles.payrollSummary.summarylistdescriptionfont}>{oAllData[11][0]}</Text>
                                                    </View>
                                                    <View>
                                                        <Text style={styles.payrollSummary.summarylistdescriptionfont}>{oAllData[11][1]}</Text>
                                                    </View>
                                                </View>
                                                <View style={styles.payrollSummary.summarylistdetails}>
                                                    <View>
                                                        <Icon2 style={textStyles.companyname}
                                                        name='check' 
                                                        color='#434646'/>
                                                    </View>
                                                    <View style={styles.payrollSummary.summarylistdescription}>
                                                        <Text style={styles.payrollSummary.summarylistdescriptionfont}>{oAllData[12][0]}</Text>
                                                    </View>
                                                    <View>
                                                        <Text style={styles.payrollSummary.summarylistdescriptionfont}>{oAllData[12][1]}</Text>
                                                    </View>
                                                </View>
                                                <View style={styles.payrollSummary.summarylistdetails}>
                                                    <View>
                                                        <Icon2 style={textStyles.companyname}
                                                        name='check' 
                                                        color='#434646'/>
                                                    </View>
                                                    <View style={styles.payrollSummary.summarylistdescription}>
                                                        <Text style={styles.payrollSummary.summarylistdescriptionfont}>{oAllData[13][0]}</Text>
                                                    </View>
                                                    <View>
                                                        <Text style={styles.payrollSummary.summarylistdescriptionfont}>{oAllData[13][1]}</Text>            
                                                    </View>                               
                                                </View>
                                                <View style={styles.payrollSummary.summarylistdetails}>
                                                    <View>
                                                        <Icon2 style={textStyles.companyname}
                                                        name='check' 
                                                        color='#434646'/>
                                                    </View>
                                                    <View style={styles.payrollSummary.summarylistdescription}>
                                                        <Text style={styles.payrollSummary.summarylistdescriptionfont}>{oAllData[14][0]}</Text>
                                                    </View>
                                                    <View>
                                                        <Text style={styles.payrollSummary.summarylistdescriptionfont}>{oAllData[14][1]}</Text>      
                                                    </View>                                             
                                                </View>
                                                    <View style={styles.payrollSummary.summarylistdetails}>
                                                        <View>
                                                            <Icon2 style={textStyles.companyname}
                                                            name='check' 
                                                            color='#434646'/>
                                                        </View>
                                                        <View style={styles.payrollSummary.summarylistdescription}>
                                                            <Text style={styles.payrollSummary.summarylistdescriptionfont}>{oAllData[15][0]}</Text>
                                                        </View>
                                                        <View>
                                                            <Text style={styles.payrollSummary.summarylistdescriptionfont}>{oAllData[15][1]}</Text>   
                                                        </View>
                                                </View>
                                            </ScrollView>
                                        </View>
                                        <View style={styles.payrollSummary.buttonsummary}>
                                            <View>
                                                <TouchableOpacity onPress={this._sentToEmail}>
                                                    <Text style={textStyles.companyname}>Send to Email</Text>          
                                                </TouchableOpacity>                                       
                                            </View>
                                        </View>
                                    </View>           
                        </View>
                </View>     
        }

        return(
            <GenericContainer

                loadingScreenShow = {this.state.loadingScreen.show}
                loadingScreenMsg = {this.state.loadingScreen.msg}
                msgBoxShow = {this.state.msgBox.show}
                msgBoxType = {this.state.msgBox.type}
                msgBoxMsg = {this.state.msgBox.msg}
                msgBoxOnClose = {this._msgBoxOnClose}
                msgBoxOnYes = {this._msgBoxOnYes}

                status={aStatus}
                title={ 'PAYSLIP' }
                onRefresh={this._getDataFromDB}>

                { oView }

            </GenericContainer>
        );
    }
}

function mapStateToProps (state) {
    console.log('stattesssssssss',state.loginReducer.logininfo);
    return {
        payslip: state.reports.payrollsummary,
        loginfo:state.loginReducer.logininfo
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            payslip: bindActionCreators(payslipActions, dispatch)
        }
    }
}
  
export default(connect(
    mapStateToProps,
    mapDispatchToProps
)(EmployeePayslipForm))

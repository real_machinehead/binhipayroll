
import React, { Component, PureComponent } from 'react';
import {
    View,
    Text,
    FlatList,
    ActivityIndicator,
    Alert
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

//Children Components
import PaginatedList from '../../../components/PaginatedList';
import Payrollreportitem from './item';
import { SimpleCard } from '../../../components/CustomCards';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as payrollreportActions from '../data/payrollSchedule/actions';

//Styles
import styles from '../styles';

//Constants
const TITLE = 'PAYROLL REPORT';

//helper
import * as oHelper from '../../../helper';

export class Payrollschedulereport extends Component{
    
    _generatePayload = (page) => ({
        id: this.props.activecompany.id,
        page: page,
        filter: this.props.filter || 'COMPANY',
        empid: this.props.empid,
    });

    _onMount = () => 
        this.props.actions.payrollreport.get(
            this._generatePayload(1)
        );

    _onRefresh = async() => 
        await this.props.actions.payrollreport.refresh(
            this._generatePayload(1)
        );

    _onLoadMore = async(page) => 
        await this.props.actions.payrollreport.loadMore(
            this._generatePayload(page)
        );

    //Local Functions
    _renderItem = item =>
        <Payrollreportitem
            item={item} 
            onConfirm={this._onConfirm}
        />;


    _onConfirm = async(oItem) => {
        Alert.alert(
            "Confirm Action",
            "Are you sure you want to confirm Tardiness Occurence ?",
            [
                {text: 'NO', onPress: () => {}},
                {text: 'YES', onPress: () => {this._confirmToDB(oItem)}},
            ],
            { cancelable: false }
        )
    }

    _confirmToDB = async(oItem) => {
        this.oCompList.setLoadingScreen(true, 'Confirming Tardiness Occurence. Please wait...');
        let oInput = oHelper.copyObject(oItem);
        oInput.compid = this.props.activecompany.id;
        const oRes = await this.props.actions.payrollreport.confirm(oInput);
        this.oCompList.setMessageBox(true, oRes.flagno==1 ? 'success' : 'error-ok', oRes.message);
        this.oCompList.setLoadingScreen(false);
    }

    render(){
        
        const {status, data} = this.props.payrollreport;
        const hideHeader = this.props.hideHeader || false;
        console.log('loaddata',data);
        return(
            <PaginatedList
                hideHeader={hideHeader} 
                title={TITLE}
                status={status}
                data={data}
                onMount={this._onMount}
                onRefresh={this._onRefresh}
                onLoadMore={this._onLoadMore}
                listName='resultsList'
                pageName='total_pages'
                renderItem={this._renderItem}
                ref={(ref) => { this.oCompList = ref; }}
                keyExtractorName='id'
            />
        );
    }
}

function mapStateToProps (state) {
    return {
        payrollreport: state.reports.payrollreport,
        activecompany: state.activeCompanyReducer.activecompany
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            payrollreport: bindActionCreators(payrollreportActions, dispatch),
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Payrollschedulereport)
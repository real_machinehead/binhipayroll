
import React, { Component, PureComponent } from 'react';
import {
    View,
    Text,
    FlatList,
    ActivityIndicator,
    Alert
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import FormModal from '../../../components/FormModal';
//Children Components
//import PaginatedList from '../../../components/PaginatedList';
import PayslipReportItem from './item';
import CustomCard, {SimpleCard} from '../../../components/CustomCards';
import GenericContainer from '../../../components/GenericContainer';
//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as payrollReportActions from '../data/payrollreport/actions';
import payrollreportForm from './forms';
//Styles
import styles from './styles';
import stylesbody from './stylesbody';
import PayrollTransactionEmployeesBody from './body';
//Constants
const TITLE = 'PAYROLL REPORT';
const DESCRIPTION='';
//helper
import * as oHelper from '../../../helper';
import * as payrollApi from '../data/payrollreport/api';
import PayrollSummaryForm from './payrollSummaryForm';

export class Payrollreport extends Component{

    constructor(props){
        super(props);
        this.state = {
            currentPage: 1,
            _bShowPayslip: false,
            _showForm: false,
            loadingMore: false,
            loadingMoreFailed: false,
            refreshing: false,
            loadingScreen: {
                show: false,
                msg: 'test'
            },
            msgBox: {
                show: false,
                type: '',
                msg: '',
                param: ''
            },
            activedata:{
                id: '',
                name: '',
                date: '',
                timeout: '',
                overtimeduration: '',
                paydate: '',
                status: '',
                employeeid:'',  
                payrollid:'',              
            },
        }
    }

    componentDidMount(){
        if(this.props.payrollreports.status[0] != 1){
            this._fetchDataFromDB();
        }
    }

    _fetchDataFromDB= () => {
        this.props.actions.payrollreports.get(
            this._generatePayload(1))
    }

    _generatePayload = (page) => ({
        compid: this.props.activecompany.id,
        page: page,
        //filter: 'COMPANY',
        filter: this.props.filter || 'COMPANY',
        empid: this.props.id,
    })

    _onMount = () => 
        this.props.actions.payrollreports.get(
            this._generatePayload(1)
    );

    _onRefresh = async() => 
        await this.props.actions.payrollreports.refresh(
            this._generatePayload(1)
    );

    _onLoadMore = async(page) => 
        await this.props.actions.payrollreports.loadMore(
            this._generatePayload(page)
        );


   _setLoadingScreen = (show, msg) => {
        let oLoadingScreen = {...this.state.loadingScreen};
        oLoadingScreen.show = show;
        oLoadingScreen.msg = msg;
        this.setState({ loadingScreen: oLoadingScreen });
    }

    _renderItem = (oItem) => {
        const ICONNAME_PENDING = 'alert-circle-outline';
        const ICONNAME_RESOLVED = 'check-circle-outline';
        const itemStyles = styles.reportItemStyles;
        const item = oItem.views;
        console.log('oitemmsssss',oItem);
            const menu = 
                    [
                        { onSelect:() => this._viewpayslip(oItem), label: 'View Payroll Summary' },
                        /*{ onSelect: () => this._modifydtr(oItem,true), label: 'Modify DTR' }*/
                    ]
    
            const backgroundColor = 
                /*oItem.statusid == 4 ? 
                        {backgroundColor: 'rgba(21, 123, 19, 0.86)'}
                    :
                        {backgroundColor: 'rgba(193, 66, 66, 0.94)'}*/
                {backgroundColor: 'rgba(21, 123, 19, 0.86)'}

            const oTitle = 
                    <View style={itemStyles.title.contContent}>
                        <View style={itemStyles.title.contIcon}>
                            <View style={itemStyles.title.iconPlaceholder}>
                                <Icon 
                                    name={
                                        item.statusid == 4 ? ICONNAME_RESOLVED : ICONNAME_PENDING
                                    } 
                                    size={30} 
                                    color='#f4f4f4'
                                />
                            </View>
                        </View>
                        <View style={itemStyles.title.contLabel}>
                            <Text style={itemStyles.title.txtLabel}>
                                {/*item.lateduration + ' ' + item.latedurationunit*/item[1][1]}
                            </Text>
                            <Text style={itemStyles.title.txtDescription}>
                                { oHelper.convertDateToString(item.date, 'MMMM DD, YYYY') }
                            </Text>
                            <Text style={itemStyles.title.txtDescription}>
                                { /*item.statusname */ item[2][1]}
                            </Text>
                        </View>
                    </View>
    
            return(
                <SimpleCard 
                    //data={oItem.views[0]}
                /*customTitleStyle={[itemStyles.title.container, backgroundColor]}
                oTitle={oTitle}
                menu={menu}*/
                    //data={oItem.views}
                            data={[
                                ['Payroll Id',oItem.payrollid],
                                ['Payroll Period', oItem.views[0][1]], 
                                ['Period Details', oItem.views[1][1]], 
                                ['Status',oItem.views[2][1]]
                            ]
                        }
                    customTitleStyle={[itemStyles.title.container, backgroundColor]}
                    oTitle={oTitle}
                    menu={menu}
                />
              );
    }
    
    _onConfirm = async(oItem) => {
  
        Alert.alert(
            "Confirm Action",
            "Are you sure you want to confirm Overtime Occurence ?",
            [
                {text: 'NO', onPress: () => {}},
                {text: 'YES', onPress: () => {this._confirmToDB(oItem)}},
            ],
            { cancelable: false }
        )
    }
    
    _keyExtractor = (item, index) => item. payrollid;


    _setMessageBox = (show, type, msg, param) => {
        this.setState({
            msgBox: oHelper.setMsgBox(
                this.state.msgBox,
                show, 
                type,
                msg,
                param
            )
        })
    }

    _onSubmitForm = (oData) => {
        console.log('dataaaaaa',oData)
        if(oHelper.isStringEmptyOrSpace(oData.id)){
            this._addToDB(oData);
        }else{
            Alert.alert(
                'Confirm Action',
                'Are you sure you want to update ' + oData.name + ' ?',
                [
                    {text: 'No', onPress: () => {}},
                    {text: 'Yes', onPress: () => this._updateToDB(oData)},
                ],
                { cancelable: false }
            ) 
        }
    }

    _msgBoxOnClose = (params) => {
        this.setState({
            msgBox: oHelper.clearMsgBox(this.state.msgBox)
        })
    }

    
    /*_confirmToDB = async(oItem) => {
        this.oCompList.setLoadingScreen(true, 'Confirming Overtime Occurence. Please wait...');
        let oInput = oHelper.copyObject(oItem);
        oInput.compid = this.props.activecompany.id;
        oInput.id = oItem.overtimeid;
        const oRes = await this.props.actions.overtime.confirm(oInput);
        this.oCompList.setMessageBox(true, oRes.flagno==1 ? 'success' : 'error-ok', oRes.message);
        this.oCompList.setLoadingScreen(false);
    }*/


    _confirmToDB = async(oItem) => {
        
        
        let oInput = oHelper.copyObject(oItem);
        oInput.compid = this.props.activecompany.id;
 
        oInput.id = oItem.payrollid;
        
        this._setLoadingScreen(true, 'Resolving Overtime Validation. Please wait...');
        await payrollApi.confirm(oInput)
            .then((response) => response.json())
            .then((res) => {
                console.log('RES: ' + JSON.stringify(res));
                if(res.flagno == 1){
                    oItem.statusid = 4;
                    this.props.actions.payrollreports.confirm(oInput);
                    this._closeForm();
                }
                this._setMessageBox(true, res.flagno==1 ? 'success' : 'error-ok', res.message);
                this._fetchDataFromDB();    
            })
            .catch((exception) => {
                this._setMessageBox(true, 'error-ok', exception.message);
            });
        this._setLoadingScreen(false);


    }

    _onCancel = () => {
        this.setState({
            _bShowPayslip: false
        })
    }

    _showPaySlip = () => {
        this.setState({ _bShowPayslip: true })
    }

    _viewpayslip=(item,_showForm)=>{
        console.log('itemsssssssss',item);
        this.setState({ _bShowPayslip: true,activedata:{payrollid:item.payrollid} });

    }
    _onCancel = () => {
        this.setState({
            _bShowPayslip: false,
            _bShowDTR: false,
            _activeItem: { name: '' },
            _bShowMonetaryAdjustmentForm: false
        })
    }
    _closeForm = () => {
        this.setState({
            _showForm: false
        })
    }


    render(){
        const {status, data} = this.props.payrollreports;
        const hideHeader = this.props.hideHeader || false;
        const activedata = this.state.activedata;
        console.log('datasssss11',data);

        const fromStyles = stylesbody.form;
        const summaryStyles = styles.summaryStyles;
        //console.log('itemmmmmms',oItem);
        const oPaySlip = (
            <FormModal 
                cancelLabel='Close'
                viewOnly={true}
                containerStyle={fromStyles.container}
                visible={this.state._bShowPayslip}
                onCancel={this._onCancel}
                onOK={this._onSubmit}
                title='PAYROLL SUMMARY'>      
                <View style={styles.container}>
                    <PayrollSummaryForm hideHeader={true} activedata={activedata} />
                </View>
                
            </FormModal>
       
        ); 


        return(

            <GenericContainer
                    customContainer = {{flex: 1}}
                    msgBoxShow = {this.state.msgBox.show}
                    msgBoxType = {this.state.msgBox.type}
                    msgBoxMsg = {this.state.msgBox.msg}
                    msgBoxOnClose = {this._msgBoxOnClose}
                    msgBoxOnYes = {this._msgBoxOnYes}
                    containerStyle = {[styles.container, this.props.containerStyle || {}]}
                    msgBoxParam = {this.state.msgBox.param}
                    loadingScreenShow = {this.state.loadingScreen.show}
                    loadingScreenMsg = {this.state.loadingScreen.msg}
                    status={status}
                    title={TITLE}
                    onRefresh={this._fetchDataFromDB}>  



                {

                    data ?

                        <CustomCard 
                            contentContainerStyle={styles.container}
                            clearMargin
                            title={TITLE} 
                            oType='text'
                            description = {DESCRIPTION}
                        >
    
                            {
                                data.length < 1 ?
                                    <EmptyList message='No Payroll  Record' />
                                :
                                    <FlatList
                                        refreshing={this.state.refreshing}
                                        onRefresh={this._fetchDataFromDB}
                                        contentContainerStyle={styles.reportsFlatlist}
                                        ref={(ref) => { this.flatListRef = ref; }}
                                        extraData={data}
                                        data={data}
                                        keyExtractor={this._keyExtractor}
                                        renderItem={({item}) => this._renderItem(item) }
                                    />
                            }
                            
                        </CustomCard> 
                    :
                        null
                /*<PaginatedList
                        hideHeader={hideHeader} 
                        title={TITLE}
                        status={status}
                        data={data}
                        onMount={this._onMount}
                        onRefresh={this._onRefresh}
                        onLoadMore={this._onLoadMore}
                        listName='list'
                        renderItem={this._renderItem}
                        ref={(ref) => { this.oCompList = ref; }}
                        pageName="totalpages"
                />*/
                }   
                {
                    this.state._showForm ?
                        <PayrollreportForm 
                            onCancel={this._closeForm}
                            onSubmit={this._onSubmitForm}
                            visible={this.state._showForm}
                            title='MODIFY DTR'
                            data={this.state.activedata}
                        />   
                    :
                        null
                }

                { this.state._bShowPayslip ? oPaySlip : null }
            </GenericContainer>

        );
    }
}

function mapStateToProps (state) {
    return {
        payrollreports: state.reports.payrollreports,
        activecompany: state.activeCompanyReducer.activecompany,
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            payrollreports: bindActionCreators(payrollReportActions, dispatch),
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Payrollreport)
import React, { Component } from 'react';
import {
    View,
    Text,
    Button,
    ScrollView,
    TouchableOpacity,
    ActivityIndicator
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as payslipActions from '../data/payrollsummary/actions';

//Children Components
import CustomPicker from '../../../components/CustomPicker';
import GenericContainer from '../../../components/GenericContainer';

//Styles
import styles from './stylesbody';
export class EmployeePayslipForm extends Component {
    constructor(props){
        super(props);
        this.state = {
            _bShowPicker: false,
            _bHasError: false,
            _bDidMount: false,
            _payrollid:'',
            _employeeid:''
        }
    }

    componentDidMount(){
        this._getDataFromDB();
    }

    _getDataFromDB = () => {
        //const activeRule = this.props.payslip.activeRule;
        //this.props.actions.payslip.get(activeRule);
        //this.props.actions.payslip.getpayslip(activeRule);
        const activePayroll = this.props.activedata;
        this.props.actions.payslip.getpayslip(activePayroll)
        //const activeRule = {payrollid:111000,employeeid: 471}
        console.log('Activepayrolllllllssss',this.props.payslip);
       
    }

    _onPeriodChange = () => {
        this.setState({
            _bShowPicker: true
        })
    }

    _onSelect = () => {
        this._hidePicker();
    }
    
    _hidePicker = () => {
        this.setState({
            _bShowPicker: false
        })
    }

    render(){
        //const headerStyles = styles.header;
        const headerStyles = styles.summaryStyles.header;
        const navigatorStyles = styles.navigator;
        //const contentStyles = styles.content;
        const contentStyles = styles.summaryStyles;
        const titleStyles = styles.title;
        const bodyStyles = styles.body;
        const systemStyles = styles.system;
        const textStyles = styles.textStyles;

        const aStatus = this.props.payslip.status;
        const oAllData = this.props.payslip.data;
        
        console.log('datasssss11111',oAllData);
        let oView = null;
        if(aStatus[0] == 1 || oAllData){
            oView = 
                <View style={styles.summaryStyles.container}>
                    <View style={styles.container}>
                        <View style={styles.summaryStyles.header.title}>
                            <View style={headerStyles.generalInfoCont}>
                                <View style={headerStyles.iconCont}>
                                    <Icon 
                                        size={40} 
                                        name='md-home' 
                                        color='#434646'/>
                                </View>
                                <View style={styles.summaryStyles.header.title}>
                                    <View>
                                        <Text style={textStyles.companyname}>JCA Realty Corporation</Text>
                                        <Text style={textStyles.address}>#80 Yacapin Sts., Cagayan de Oro City, Misamis Oriental 9000</Text>  
                                    </View> 
                                </View>


                                {/*<View style={styles.summaryStyles.header.title}>
                                    <View>
                                    <Text style={textStyles.name}>JCA Realty Corporation</Text>
                                    </View>
                                    <View>
                                        <Text style={textStyles.name}>#80 Yacapin Sts., Cagayan de Oro City, Misamis Oriental 9000</Text>
                                    </View>
                                </View>*/}
                            </View>
                        </View>

                        {/*<View style={styles.summaryStyles.body.container}>
                            {
                                oAllData.map((aParamsList, indexParamsList) => 
                                    <View key={indexParamsList} style={headerStyles.paramsList}>
                                        {
                                            aParamsList.map((aParamsArgs, indexParamsArgs) => 
                                                <View key={indexParamsArgs} style={styles.summaryStyles.body.summarydetails}>
                                                    <Text style={textStyles.label}>testssssss</Text>
                                                </View>
                                                /*<View key={indexParamsArgs} style={headerStyles.param}>
                                                    <View style={headerStyles.label}>
                                                        <Text style={textStyles.label}>{aParamsArgs[0]}</Text>
                                                    </View>
                                                    <View style={headerStyles.value}>
                                                        <Text style={textStyles.value}>{aParamsArgs[1]}</Text>
                                                    </View>
                                                </View>*/
                                            /*)
                                        }
                                    </View>
                                )
                            }
                        </View>*/}
                    </View>
                {/*<View style={contentStyles.container}>
                    {
                        oAllData.data.map((oData, indexData) => 
                            <View key={indexData} style={contentStyles.placeholder}>
                                <View style={titleStyles.container}>
                                    <Text style={textStyles.detailsHeader}>{oData.title}</Text>
                                </View>
                                <View style={bodyStyles.container}>
                                    <ScrollView>
                                        <View style={bodyStyles.paramsList}>
                                            {
                                                oData.list.map((aList, indexList) => {
                                                    switch(aList[0].toLowerCase()){
                                                        case 'systembreak':
                                                            return <View key={indexList} style={systemStyles.break}/>;
                                                            break;
                                                        case 'systemdiv':
                                                            return <View key={indexList} style={systemStyles.div}/>;
                                                            break;
                                                        case 'systemfooter':
                                                            return null;
                                                            break;
                                                        default:
                                                            return(
                                                                    <View key={indexData} style={bodyStyles.params}>
                                                                        {
                                                                            aList.map((strParam, indexParams) =>
                                                                                <View 
                                                                                    key={indexParams} 
                                                                                    style={[bodyStyles.paramsArg, 
                                                                                        indexParams==0 ? bodyStyles.paramsLeftMost :
                                                                                        indexParams==(aList.length-1) ? bodyStyles.paramsRightMost :
                                                                                        bodyStyles.paramsCenter]
                                                                                    }
                                                                                >

                                                                                    <Text style={textStyles.details}>{strParam}</Text>
                                                                                </View>
                                                                            )
                                                                        }
                                                                    </View>
                                                            );
                                                    }
                                                })
                                            }
                                            {}
                                        </View>
                                    </ScrollView>
                                    {
                                        oData.list.map((aList, indexList) => {
                                            if(aList[0].toLowerCase() === 'systemfooter'){
                                                return(
                                                    <View key={indexList} 
                                                        style={systemStyles.footer.container}>

                                                        <View style={systemStyles.footer.title}>
                                                            <Text style={textStyles.footerTitle}>
                                                                { aList[1] }
                                                            </Text>
                                                        </View>

                                                        <View style={systemStyles.footer.value}>
                                                            <Text style={textStyles.footerValue}>
                                                                { aList[2] }
                                                            </Text>
                                                        </View>
                                                        
                                                    </View>
                                                )
                                            }
                                            else{
                                                return null;
                                            }
                                        })
                                    }
                                </View>
                            </View>
                        )
                    }
                </View>*/}

            </View>
         
        }

        return(
            <GenericContainer
                status={aStatus}
                title={ 'PAYSLIP' }
                onRefresh={this._getDataFromDB}>

                { oView }

            </GenericContainer>
        );
    }
}

function mapStateToProps (state) {
    //console.log('stattesssssssss',state.reports.payrollsummary)
    return {
        payslip: state.reports.payrollsummary
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            payslip: bindActionCreators(payslipActions, dispatch)
        }
    }
}
  
export default(connect(
    mapStateToProps,
    mapDispatchToProps
)(EmployeePayslipForm))

import React, { Component } from 'react';
import {
    View,
    Text,
} from 'react-native';

import GenericContainer from '../../components/GenericContainer';
import LoadingScreen from '../../components/LoadingScreen';
import LoginForm from './form';


import { connect } from 'react-redux';
import {
    SetLoginInfo,
    SetActiveCompany
} from '../../actions';
import * as loginServiceApi from './api';

import * as session from '../../services/session';
import * as dataService from '../../services/dataResetter';
import * as oHelper from '../../helper';

class Login extends Component {
    static navigationOptions = {
        header : null,
    }

    constructor(props){
        super(props);
        this.state = {
            aStatus: [2, 'Please wait...'],
            username: 'ccronaldo',
            password: '12345',
            name: '',
            disabledBtn: false,
            loadingScreen: {
                show: false,
                msg: 'test'
            },
            msgBox: {
                show: false,
                type: '',
                msg: '',
                param: ''
            },
            showSplashScreen: false,
        };
    }

    componentDidMount(){
        this.setState({
            aStatus: [1, 'Did Mount.']
        },  
            () => {
                this._initializeSession()
            }
        );
    }

    _initializeSession = () => {
        dataService.resetSession();
        session.clearSession();
    }

    onSubmit = async(oData) => {
        const {username, password} = this.state;
        oData.deviceinfo = await oHelper.getDeviceInfo();

        switch(oData.requestType.toUpperCase()){
            case 'LOGIN':
                this.setState({showSplashScreen: true});
                await session.authenticate(username, password, oData)
                    .then(async(res)=>{
                        console.log('RES: ' + JSON.stringify(res));
                        if(res.flagno == 1){
                            const oRes = res;
                            oRes.navigation = this.props.navigation;
                            oRes.username = username;
                            await session.onRequestSuccess(res);
                            await this.props.updateLoginInfo(res);
                            
                            
                            switch(res.usertype.toUpperCase()){
                                case 'EMPLOYEE':
                                    await this.props.updateActiveCompany(res.companyname[0]);
                                    this.props.navigation.navigate('EmployeeProfile', this.props.logininfo);
                                    break;
                                case 'OWNER':
                                    const oActiveComp = await oHelper.getElementByPropValue(res.companyname, 'default', 1);
                                    await this.props.updateActiveCompany(oActiveComp);
                                     this.props.navigation.navigate('EmprDashBoard'); 
                                    /* this.props.navigation.navigate('CompanyPolicies'); */
                                    /* this.props.navigation.navigate('Employees'); */
                                    /* this.props.navigation.navigate('Transactions'); */
                                    /* this.props.navigation.navigate('PayrollTransaction'); */
                                    /* this.props.navigation.navigate('Transactions'); */
                                    /*this.props.navigation.navigate('Reports');*/
                                    /* this.props.navigation.navigate('AddEmployeeForm') */
                                    /* this.props.navigation.navigate('EmploymentInfo') */
                                    /* this.props.navigation.navigate('EmployeeWorkShift') */
                                    /* this.props.navigation.navigate('EmployeeBenefits') */
                                    /* this.props.navigation.navigate('RankBasedRules'); */
                                    /* this.props.navigation.navigate('RankBasedRules'); */
                                    /* this.props.navigation.navigate('PersonalInfo'); */
                                    break;
                                case 'ADMIN':
                                    break;
                                default:
                                    this._setMessageBox(
                                        true, 
                                        'error-ok', 
                                        'Unable to Login. Failed to identify user type. ' +
                                        'Please try again or contact BINHI-MeDFI.'
                                    );
                                    break;
                            }

                        }else if(res.flagno == 3){
                            await session.onRequestSuccess(res);
                            this.setState({name: res.firstname});
                            this._setMessageBox(
                                true, 
                                'error-password', 
                                res.message
                            );

                        }else if(res.flagno == 5){
                            await session.onRequestSuccess(res);
                            this.setState({name: res.firstname});
                            this._setMessageBox(
                                true, 
                                'warning', 
                                res.message,
                                'CHANGEPASSWORD'
                            );
                        }else{
                            this._setMessageBox(true, 'error-ok', res.message);
                        }
                    }).catch((exception)=> {
                        this._setMessageBox(
                            true, 
                            'error-ok', 
                            'Unable to Login. ' +
                            'Please check your internet connection or contact BINHI-MeDFI. ' + 
                            exception.message
                        );
                    });
                    this.setState({showSplashScreen: false});
                break;
            case 'TIMEIN':
                oData.username = username;
                oData.password = password;
                oData.transtype = 'timein';
                this._setLoadingScreen(true, 'Saving Time-in Information. Please wait...');
                await loginServiceApi.timein(oData)
                    .then((response)=> response.json())
                    .then((res)=>{
                        console.log('RES: ' + JSON.stringify(res));
                        if(res.flagno == 1){
                            this._setMessageBox(true, 'success', res.message);
                        }else{
                            this._setMessageBox(true, 'error-ok', res.message);
                        }
                    }).catch((exception)=> {
                        this._setMessageBox(
                            true, 
                            'error-ok', 
                            'Unable to Clock-in. ' +
                            'Please check your internet connection or contact BINHI-MeDFI. ' + 
                            exception.message
                        );
                    });
                this._setLoadingScreen(false, 'Saving Time-in Information. Please wait...');
                break;
            case 'TIMEOUT':
                oData.username = username;
                oData.password = password;
                oData.transtype = 'timein';
                this._setLoadingScreen(true, 'Saving Time-out Information. Please wait...');
                await loginServiceApi.timeout(oData)
                    .then((response)=> response.json())
                    .then((res)=>{
                        console.log('RES: ' + JSON.stringify(res));
                        if(res.flagno == 1){
                            this._setMessageBox(true, 'success', res.message);
                        }else{
                            this._setMessageBox(true, 'error-ok', res.message);
                        }
                    }).catch((exception)=> {
                        this._setMessageBox(
                            true, 
                            'error-ok', 
                            'Unable to Clock-out. ' +
                            'Please check your internet connection or contact BINHI-MeDFI. ' + 
                            exception.message
                        );
                    });
                this._setLoadingScreen(false, 'Saving Time-in Information. Please wait...');
                break;
            default: 
                break;
        }
    }

    _onForgotPassword = async() => {
        this._setMessageBox(
            true,
            'warning',
            'This action will reset your password. Are you sure you want to proceed?',
            'FORGOTPASSWORD'
        );
    }

    _invokeForgotPassword = async() => {
        const {username, password} = this.state;
        this._setLoadingScreen(true, 'Requesting to reset your password. Please wait...');
        await loginServiceApi.forgotPassword({username: username})
            .then((response)=> response.json())
            .then((res)=>{
                if(res.flagno == 1){
                    this._setMessageBox(true, 'success', res.message);
                }else{
                    this._setMessageBox(true, 'error-ok', res.message);
                }
            }).catch((exception)=> {
                this._setMessageBox(
                    true, 
                    'error-ok', 
                    'Unable to Clock-in. ' +
                    'Please check your internet connection or contact BINHI-MeDFI. ' + 
                    exception.message
                );
            });
        this._setLoadingScreen(false, 'Saving Time-in Information. Please wait...');
    }

    setUsername = (value) => {
        console.log('setUsername: ' + value);
        this.setState({
            username: value
        },
            () => {
                this.checkBtnState();
            }
        );
    }

    setPassword = (value) => {
        console.log('setPassword: ' + value);
        this.setState({
            password: value
        },
            () => {
                this.checkBtnState();
            }
        );
    }

    checkBtnState = () => {
        const { username, password, disabledBtn } = this.state;

        if(oHelper.isStringEmptyOrSpace(username) || oHelper.isStringEmptyOrSpace(password)){
            if(!disabledBtn){
                this.setState({disabledBtn: true});
            }
        }else{
            if(disabledBtn){
                this.setState({disabledBtn: false});
            }
        }
    }

    _setMessageBox = (show, type, msg, param) => {
        this.setState({
            msgBox: oHelper.setMsgBox(
                this.state.msgBox,
                show, 
                type,
                msg,
                param
            )
        })
    }

    _setLoadingScreen = (show, msg) => {
        let oLoadingScreen = {...this.state.loadingScreen};
        oLoadingScreen.show = show;
        oLoadingScreen.msg = msg;
        this.setState({ loadingScreen: oLoadingScreen });
    }

    _msgBoxOnClose = (params) => {
        this.setState({
            msgBox: oHelper.clearMsgBox(this.state.msgBox)
        })
    }

    _msgBoxOnContinue = () => {
        this._msgBoxOnClose();
        switch(this.state.msgBox.param.toUpperCase()){
            case 'CHANGEPASSWORD':
                this.props.navigation.navigate('ChangePassword', {
                    username: this.state.username,
                    password: this.state.password,
                    firstname: this.state.name,
                });
                break;
            case 'FORGOTPASSWORD':
                this._invokeForgotPassword();
                break;
            default:
                //NO ACTION
                break;
        }
    }
    
    render(){
        const { 
            username,
            password,
            disabledBtn,
            showSplashScreen
        } = this.state;

        if(showSplashScreen){
            return <LoadingScreen show = {showSplashScreen}/>
        }
        else{
            return(
                <GenericContainer
                    msgBoxShow = {this.state.msgBox.show}
                    msgBoxType = {this.state.msgBox.type}
                    msgBoxMsg = {this.state.msgBox.msg}
                    msgBoxOnClose = {this._msgBoxOnClose}
                    msgBoxOnYes = {this._msgBoxOnYes}
                    msgBoxOnContinue = {this._msgBoxOnContinue}
                    msgBoxOnForgotPassword = {this._onForgotPassword}
                    msgBoxParam = {this.state.msgBox.param}
                    loadingScreenShow = {this.state.loadingScreen.show}
                    loadingScreenMsg = {this.state.loadingScreen.msg}
                    status={this.state.aStatus}>

                    <LoginForm 
                        username={username}
                        password={password}
                        disabledBtn={disabledBtn}
                        onUpdateUsername={this.setUsername}
                        onUpdatePassword={this.setPassword}
                        onSubmit={this.onSubmit}
                    />

                </GenericContainer>
            );
        }
    }
}

function mapStateToProps (state) {
    return {
        logininfo: state.loginReducer.logininfo
    }
}
  
function mapDispatchToProps (dispatch) {
    return {
        updateLoginInfo: (logininfo) => {
            dispatch(SetLoginInfo(logininfo))
        },

        updateActiveCompany: (activecompany) => {
            dispatch(SetActiveCompany(activecompany))
        }
  }
}
  
export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Login)
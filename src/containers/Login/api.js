import { fetchApi, mockFetch } from '../../services/api';
import * as endPoints from '../../global/endpoints';
import * as blackOps from '../../global/blackOps';

/* const endPoints = {
    timein: 'timein.php',
    timeout: 'timeout.php'
} */

export let timein = payload => fetchApi(endPoints.account.timein(payload), payload, 'POST');
export let timeout = payload => fetchApi(endPoints.account.timeout(payload), payload, 'POST');
export let forgotPassword = payload => fetchApi(endPoints.account.forgotPassword(payload), payload, 'POST');
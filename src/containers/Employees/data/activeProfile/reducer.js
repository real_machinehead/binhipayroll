import { combineReducers } from 'redux';
import * as actionTypes from './actionTypes';
import { CONSTANTS } from '../../../../constants';
import * as oHelper from '../../../../helper';

const initialStatus = CONSTANTS.STATUS.LOADING;
const initialState = {};
const initialSummary = false;

export const data = (state = initialState, action) => {
	let oState = JSON.parse(JSON.stringify(state));
	switch (action.type) {
		case actionTypes.ALLINFO.UPDATE.ID:
			oState.id = action.payload;
			return {
				...oState
			}
			break;

		case actionTypes.ALLINFO.UPDATE.DATA:
			oState.id = action.payload.employee.id;
			oState.personalinfo = action.payload.employee.personalinfo;
			oState.bankinfo = action.payload.employee.bankinfo;
			oState.employmentinfo = action.payload.employee.employmentinfo;
			oState.summary = action.payload.employee.summary;
			return {...oState};
			break;
		
		case actionTypes.PERSONALINFO.UPDATE.DATA:
			oState.personalinfo = action.payload;

			return {
				...oState
			}
			break;

		case actionTypes.BASICINFO.INIT.DATA:
			oState.personalinfo.basicinfo = action.payload.employee.basicinfo;
			oState.personalinfo.contactinfo = action.payload.employee.contactinfo;
			oState.personalinfo.ids = action.payload.employee.ids;
			oState.personalinfo.address = action.payload.employee.address;
			oState.personalinfo.family = action.payload.employee.family;
			return {
				...oState
			}
			break;

		case actionTypes.BASICINFO.UPDATE.DATA:
			oState.personalinfo.basicinfo.firstname = action.payload.firstname;
			oState.personalinfo.basicinfo.middlename = action.payload.middlename;
			oState.personalinfo.basicinfo.lastname = action.payload.lastname;
			oState.personalinfo.basicinfo.nickname = action.payload.nickname;
			oState.personalinfo.basicinfo.birthdate.value = action.payload.birthdate;
			oState.personalinfo.basicinfo.gender.value = action.payload.gender;
			oState.personalinfo.basicinfo.civilstatus.value = action.payload.civilstatus;
			
			return {
				...oState
			}
			break;

		case actionTypes.CONTACTINFO.UPDATE.DATA:
			oState.personalinfo.contactinfo.mobile = action.payload.mobile;
			oState.personalinfo.contactinfo.telephone = action.payload.telephone;
			oState.personalinfo.contactinfo.email = action.payload.email;
			return {
				...oState
			}
			break;
		
		case actionTypes.IDS.UPDATE.DATA:
			oState.personalinfo.ids.tin.value = action.payload.tin;
			oState.personalinfo.ids.sss.value = action.payload.sss;
			oState.personalinfo.ids.philhealth.value = action.payload.philhealth;
			oState.personalinfo.ids.pagibig.value = action.payload.pagibig;
			return {
				...oState
			}
			break;
		
		case actionTypes.ADDRESS.UPDATE.DATA:
			oState.personalinfo.address = action.payload.address;
			return {
				...oState
			}
			break;
		
		case actionTypes.DEPENDENTS.UPDATE.DATA:
			
			//Spouse
			oState.personalinfo.family.spouse.name = action.payload.spouse.name;
			oState.personalinfo.family.spouse.birthdate.value = 
				action.payload.spouse.birthdate ? 
					oHelper.convertDateToString(action.payload.spouse.birthdate, 'YYYY-MM-DD')
					:
					null;
			oState.personalinfo.family.spouse.work.jobtitle = action.payload.spouse.jobtitle;
			oState.personalinfo.family.spouse.work.company = action.payload.spouse.company;

			//Dependents
			oState.personalinfo.family.dependents.data = [...action.payload.dependents];

			return {
				...oState
			}
			break;

		case actionTypes.BANKINFO.UPDATE.DATA:
			oState.bankinfo.bankname = action.payload.bankname;
			oState.bankinfo.accountnumber = action.payload.accountnumber;
			return {
				...oState
			}
			break;

		case actionTypes.EMPLOYEEWORKSHIFT.UPDATE.DATA:
			oState.employmentinfo.workshift.data = action.payload;
			
			return {
				...oState
			}
			break;

		case actionTypes.EMPLOYEEBENEFITS.GOVERNMENT.UPDATE.DATA:
			oState.employmentinfo.benefits.government.data = action.payload;
			
			return {
				...oState
			}
			break;
		
		case actionTypes.EMPLOYEEBENEFITS.COMPANY.UPDATE.DATA:
			oState.employmentinfo.benefits.company.data = action.payload;
			
			return {
				...oState
			}
			break;

		case actionTypes.EMPLOYEERANK.UPDATE.DATA:
			oState.employmentinfo.ranks.data = action.payload;
			
			return {
				...oState
			}
			break;

		case actionTypes.EMPLOYMENTDETAILS.UPDATE.DATA:
			oState.employmentinfo.details.data = action.payload;
			
			return {
				...oState
			}
			break;

		case actionTypes.ALLINFO.REMOVE.ACTIVEDATA:
			return initialState;
			break;

		case actionTypes.RESETALL:
			return initialState;
			break;

		default:
			return state;
	}
};

export const status = (state = initialStatus, action) => {
	switch (action.type) {
		case actionTypes.ALLINFO.UPDATE.STATUS:
			return action.payload;
			break;

		default:
			return state;
	}
};

export const hidesummary = (state = initialSummary, action) => {
	switch (action.type) {
		case actionTypes.HIDESUMMARY:
			return typeof action.payload == 'boolean' ? action.payload : !state;
			break;

		default:
			return state;
	}
};

export const viewMode = (state = false, action) => {
	switch (action.type) {
		case actionTypes.VIEWMODE:
			return action.payload;
			break;

		default:
			return state;
	} 
}

export const activeProfile = combineReducers({
    data,
	status,
	hidesummary,
	viewMode
}); 
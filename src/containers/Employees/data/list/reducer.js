import * as actionTypes from './actionTypes';
import { combineReducers } from 'redux';
import {CONSTANTS} from '../../../../constants';
import * as oHelper from '../../../../helper';

const initialState = null;
const initialStatusState =CONSTANTS.STATUS.LOADING;

export const data = (state = initialState, action) => {
	let oState = oHelper.copyObject(state);
	switch (action.type) {
		case actionTypes.UPDATE:
			return action.payload;
			break;

		case actionTypes.ADD:
			oState.data.push(action.payload);
			oState.data.sort((a,b) => a.name > b.name)
			return oState;
			break;

		default:
			return state;
	}
};

export const status = (state = initialStatusState, action) => {
	switch (action.type) {
		case actionTypes.STATUS:
			console.log('STATUS: ' + action.payload);
			return action.payload;
			break;

		default:
			return state;
	}
}

export const list = combineReducers({
	data,
    status
}); 

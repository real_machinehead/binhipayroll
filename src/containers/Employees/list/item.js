//Packages
import React, { Component, PureComponent } from 'react';
import {
    View,
    Text,
    Button,
    FlatList,
    TouchableNativeFeedback,
    TextInput
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import LinearGradient from 'react-native-linear-gradient';
import ActionButton from 'react-native-action-button';
import { withNavigation } from 'react-navigation';

//Styles Properties
import styles from './styles';

//Custom Components
import Header2 from '../../Headers/header2';

//Constants
import { CONSTANTS } from '../../../constants/index';
const btnActive = 'rgba(255, 255, 255, 0.3);'
const btnInactive = 'transparent';

//helper
import * as oHelper from '../../../helper';

class EmployeeListItem extends Component{
    shouldComponentUpdate(nextProps, nextState){
        if(
            (this.props.activeKey === this.props.item[this.props.keyName || 'key' ]) ||
            (nextProps.activeKey === this.props.item[this.props.keyName || 'key' ])
        ){
            return true;
        }
        else{
            return false;
        }
    }

    render(){
        
        const item = this.props.item;
        const isDraft = this.props.isDraft || false;
        const basicInfo = isDraft ? null : item.employee.personalinfo.basicinfo;
        const employmentdetails = isDraft ? null : item.employee.employmentinfo.details;
        const curDetails = isDraft ? null : employmentdetails.data.find(x => x.current == 'true');
        if(basicInfo){
            return(
                <TouchableNativeFeedback 
                    onPress={() => this.props.onPress(item)}
                    onLongPress={() => {
                        alert('ALERT TEST!')
                    }}
                    background={TouchableNativeFeedback.SelectableBackground()}>
                    <View style={[styles.btnCont, this.props.activeKey == item[this.props.keyName || 'key'] ? {backgroundColor: 'rgba(255, 153, 36, 0.1);'} : {}]}>
                        <View style={styles.iconCont}>
                            <View style={styles.iconPlaceholder}>
                                <Text style={styles.txtFirstLetter}>
                                    {isDraft ? item.fullname.charAt(0).toUpperCase() : basicInfo.lastname.charAt(0).toUpperCase()}
                                </Text>
                            </View>
                        </View>
                        <View style={styles.labelCont}>
                            <Text style={styles.txtLabelTitle}>
                                {isDraft ? item.fullname : basicInfo.lastname + ', ' + basicInfo.firstname + ' ' + basicInfo.middlename.charAt(0)}
                            </Text>
                            {
                                isDraft || false ? 
                                    null 
                                :
                                    <View style={{flexDirection: 'column'}}>
                                        <Text style={styles.txtLabel}>
                                            {curDetails ? curDetails.position.value || 'Unavailable' : 'Unavailable'}
                                        </Text>
                                        <Text style={styles.txtLabel}>
                                            {curDetails ? curDetails.branch.value || 'Unavailable' : 'Unavailable'}
                                        </Text>
                                    </View>
                            }
                        </View>
                    </View>
                </TouchableNativeFeedback>
            )
        }else{
            return null;
        }
    }
}

export default EmployeeListItem;
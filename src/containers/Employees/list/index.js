//Packages
import React, { Component, PureComponent } from 'react';
import { View, Text,TextInput,ActivityIndicator } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { withNavigation } from 'react-navigation';

import ActionButton from '../../../components/ActionButton';
import GenericContainer from '../../../components/GenericContainer';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as activeProfileActions from '../data/activeProfile/actions';
import * as employeeApi from '../data/activeProfile/api';
//helper
import * as oHelper from '../../../helper';

//Styles
import styles from '../styles';

//tabs
import EmployeeListTabs from './tabs';
import EmployeeGenericList from './genericList';
import * as employeeListActions from '../data/list/actions';

class EmployeeList extends Component {
    constructor(props){
        super(props);
        this.state={
            _status: [1, 'Success'],
            loadingScreen: {
                show: false,
                msg: 'test'
            },

            msgBox: {
                show: false,
                type: '',
                msg: '',
                param: ''
            },
        }
    }

    componentDidMount = () => {
        this.props.actions.activeProfile.hideSummary(false);
        this._onSearchEmployee('');
    }
    
    _addNewEmployee = async() => {
        this._setLoadingScreen(true, 'Initializing Employee Form, please wait...');
        await employeeApi.getDefaultData()
            .then((response) => response.json())
            .then(async(res) => {
                if(res.flagno == 1){
                    let employee = res.data;
                    employee.id = '';
                    await this.props.actions.activeProfile.updateAllInfo({employee});
                    this.props.navigation.navigate('AddEmployeeForm');
                }else{
                    this._setMessageBox(true, 'error-ok', res.message);
                }
            })
            .catch((exception) => {
                this._setMessageBox(true, 'error-ok', exception.message);
            });
        this._setLoadingScreen(false, 'Initializing Employee Form, please wait...');
    }

    _setMessageBox = (show, type, msg, param) => {
        this.setState({
            msgBox: oHelper.setMsgBox(
                this.state.msgBox,
                show, 
                type,
                msg,
                param
            )
        })
    }

    _setLoadingScreen = (show, msg) => {
        let oLoadingScreen = {...this.state.loadingScreen};
        oLoadingScreen.show = show;
        oLoadingScreen.msg = msg;
        this.setState({ loadingScreen: oLoadingScreen });
    }

    _msgBoxOnClose = (params) => {
        this.setState({
            msgBox: oHelper.clearMsgBox(this.state.msgBox)
        })
    }

    _onSearchEmployee=async(text)=>{
        //const empdata=this.props.employeelist.data;
        //this.props.actions.employeelist.get({page: 1});

        console.log('laggggggss',text);
        if(text){
            this.props.actions.employeelist.getemployee({page: 1,lastname:text,company:106});
        }else{
            this.props.actions.employeelist.get({page: 1});
        }
    }
    render(){
        return(
            <GenericContainer
                containerStyle={styles.leftCont}
                msgBoxShow = {this.state.msgBox.show}
                msgBoxType = {this.state.msgBox.type}
                msgBoxMsg = {this.state.msgBox.msg}
                msgBoxOnClose = {this._msgBoxOnClose}
                msgBoxOnYes = {this._msgBoxOnYes}
                msgBoxParam = {this.state.msgBox.param}
                loadingScreenShow = {this.state.loadingScreen.show}
                loadingScreenMsg = {this.state.loadingScreen.msg}
                status={[1,'Success']}
                title={'Employee List'}

                onRefresh={this._onSearchEmployee}>
                <LinearGradient 
                    colors={['#818489', '#3f4144', '#202626']}
                    style={styles.leftCont}>
                    <LinearGradient 
                        colors={['#818489', '#3f4144', '#202626']}
                        style={styles.contTitle}>

                        
                        <View style={styles.contTitleName}>
                            <Text style={styles.txtTitle}>
                                {this.props.activecompany.name.toUpperCase()}
                            </Text>
                        </View>

                    </LinearGradient>

                    <View style={styles.iconContainer}>
                        <Icon 
                                name='account-search' 
                                size={30} 
                                color='#f4f4f4'
                            />
                        <TextInput
                                    style={{height: 40,width:'80%', borderColor: 'white', borderWidth: 0,color:'white'}}
                                    onChangeText={(text) => this._onSearchEmployee(text)}
                                    //placeholder="SEARCH EMPLOYEE"
                                    value={this.state.text}
                                    placeholderTextColor="white" 
                            
                        />
                    </View>
                    <EmployeeListTabs />
                    {
                        this.props.viewOnly || false ?
                            null
                        :
                            <ActionButton onPress={this._addNewEmployee}/>
                    }
                </LinearGradient>
            </GenericContainer>
        )
    }
}

function mapStateToProps (state) {
    return {
        activecompany: state.activeCompanyReducer.activecompany,
        employeelist:state.employees.list,
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            activeProfile: bindActionCreators(activeProfileActions, dispatch),
            employeelist: bindActionCreators(employeeListActions, dispatch)
        }
    }
}

export default withNavigation(connect(
    mapStateToProps,
    mapDispatchToProps
)(EmployeeList))
//RN Packages andd Modules
import React, { Component } from 'react';
import {
    View, 
    Text
} from 'react-native';

//Styles
import styles from './styles';

//Children Components
import PaginatedListContainer from '../PaginatedListContainer';
import EmployeeListItem from './item';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as employeeListActions from '../data/list/actions';
import * as employeeListApi from '../data/list/api';
import * as activeProfileActions from '../data/activeProfile/actions';

//helper
import * as oHelper from '../../../helper';

const TITLE = 'EMPLOYEE LIST';

class EmployeeGenericList extends Component {
    constructor(props){
        super(props);
        this.state = {
            page: 1,
            hasReachedEnd: false,
            refreshing: false,
            loadingMore: false,
            loadMoreFailed: false,

            _activeItem: null,
            filters: {},
        }
    }

    _onMount = () => {
        if(this.props.employeelist.data){
            const aList = this.props.employeelist.data.data;
            if(aList.length > 0){
                this._setActiveItem(aList[0]);
            }
        }else{
           this._initData()
        }
    }

    _initData = () => {
        this.setState({ 
            page: 1 
        },
            () => {
                this.props.actions.employeelist.get({page: 1});
            }
        );
        
    }
    
    _renderItem = item => {
        return (
            <EmployeeListItem 
                item={item} 
                onPress={this._setActiveItem}
                activeKey={this.state._activeItem}
            />
        );
    }

    _displayLoadMoreFailedMsg = (msg = null) => {
        this.oCompList.displayLoadMoreFailedMsg();
    }

    _handleLoadMore = () => {
        const {
            loadMoreFailed,
            loadingMore,
            hasReachedEnd,
            refreshing,
        } = this.state;
    
        if (!loadingMore && !hasReachedEnd && !refreshing && !loadMoreFailed) {
            this.setState({
                loadingMore: true,
            }, () => {
                this._loadMoreFromDB();
            });
        }
    }

    _setActiveItem = async(value) => {
        this.setState({ _activeItem: value.key });
        let employee = value.employee;
        employee.id = value.key;
        this.props.actions.activeProfile.updateAllInfo({employee});
    }

    _updateDataStore = async(aList) => {
        let oCurData = await oHelper.copyObject(this.props.employeelist.data);
        let mergedList = await oHelper.arrayMerge(oCurData.data, aList, 'key');
        oCurData.data = mergedList;
        this.props.actions.employeelist.update(oCurData);
    }

    _loadMoreFromDB = () => {
        const { filters, page } = this.state;
        const targetPage = page + 1;
        const totalPages = this.props.employeelist.data.total_pages;
        filters.page = targetPage;
        
        if(targetPage<=totalPages){
            employeeListApi.get(filters)
            .then((response) => response.json())
            .then(async(res) => {
                console.log('RES : ' + JSON.stringify(res));
                if(res.flagno == 1){
                    
                    await this._updateDataStore(res.data);
                    this.setState({
                        page: targetPage,
                        loadMoreFailed: false,
                        loadingMore: false,
                        refreshing: false,
                        hasReachedEnd: targetPage >= res.total_pages,
                    });
                }else{
                    this._displayLoadMoreFailedMsg(res.message);
                    this.setState({ loadMoreFailed: true });
                }
            })
            .catch((exception) => {
                this.setState({
                    loadingMore: false,
                    loadMoreFailed: true,
                    refreshing: false,
                });
                this._displayLoadMoreFailedMsg();
                console.log('exception.message: ' + exception.message);
            });
        }else{
            this.setState({
                page: page,
                loadMoreFailed: false,
                loadingMore: false,
                refreshing: false,
                hasReachedEnd: true,
            });
        }
       
    };

    _onSeeMore = () => {
        this.setState({
            loadMoreFailed: false,
            loadingMore: true,
        }, () => {
            this._loadMoreFromDB();
        });
    }

    render(){
        const { 
            page,
            hasReachedEnd,
            refreshing,
            loadingMore,
            loadMoreFailed,
        } = this.state;

        /* const aStatus = [1, '']; */
        const listStyles = styles.listStyles;
        const oData = this.props.employeelist;
        const aStatus = oData.status;
        const aList = aStatus[0] == 1 && oData.data ? oData.data.data : [];

        return(
            <PaginatedListContainer 
                containerStyle = {{backgroundColor: 'transparent'}}
                title={TITLE}
                hideHeader={true}
                status={aStatus}
                extraData={aList}
                data={aList}
                onMount={this._onMount}
                onRefresh={this._initData}
                renderItem={this._renderItem}
                onReachEnd={this._handleLoadMore}
                onSeeMore={this._onSeeMore}
                keyExtractorName='key'

                page={page}
                hasReachedEnd={hasReachedEnd}
                refreshing={refreshing}
                loadingMore={loadingMore}
                loadMoreFailed={loadMoreFailed}
                ref={(ref) => { this.oCompList = ref; }}

                endOfResultsCustomStyle={{height: 20, backgroundColor: '#2e3030'}}
            />
        )
    }
}


function mapStateToProps (state) {
    return {
        activeProfile: state.employees.activeProfile,
        employeelist: state.employees.list
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            employeelist: bindActionCreators(employeeListActions, dispatch),
            activeProfile: bindActionCreators(activeProfileActions, dispatch)
        }
    }
}
  
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EmployeeGenericList);
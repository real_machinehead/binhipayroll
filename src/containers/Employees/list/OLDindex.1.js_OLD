//Packages
import React, { Component, PureComponent } from 'react';
import {
    View,
    Text,
    Button,
    FlatList,
    TouchableNativeFeedback,
    TextInput
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import LinearGradient from 'react-native-linear-gradient';
import ActionButton from 'react-native-action-button';
import { withNavigation } from 'react-navigation';

//Styles Properties
import styles from './styles';

//Custom Components
import Header2 from '../../Headers/header2';
import SearchBox from '../../../components/SearchBox';
import * as PromptScreen from '../../../components/ScreenLoadStatus';
import GenericContainer from '../../../components/GenericContainer';
import CustomPicker from '../../../components/CustomPicker';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as employeeListActions from '../data/list/actions';
import * as employeeActions from '../data/activeProfile/actions';
import * as allProfilesActions from '../data/allProfiles/actions';
import * as employeeApi from '../data/activeProfile/api';
import * as draftlistActions from '../data/draft/actions';
import * as draftlistApi from '../data/draft/api';

//Constants
import { CONSTANTS } from '../../../constants/index';
const btnActive = 'rgba(255, 255, 255, 0.3);'
const btnInactive = 'transparent';

//helper
import * as oHelper from '../../../helper';

export class EmployeeListCard extends Component{
    shouldComponentUpdate(nextProps, nextState){
        if(
            (this.props.activeKey === this.props.item.key) ||
            (nextProps.activeKey === this.props.item.key)
        ){
            /* console.log('this.props.item.name: ' + this.props.item.name); */
            return true;
        }
        else{
            return false;
        }
    }

    render(){
        let item = this.props.item;
        return(
            <TouchableNativeFeedback 
                onPress={() => this.props.itemPressed(item)}
                onLongPress={() => {
                    alert('ALERT TEST!')
                }}
                background={TouchableNativeFeedback.SelectableBackground()}>
                <View style={[styles.btnCont, this.props.activeKey == item.key ? {backgroundColor: 'rgba(255, 153, 36, 0.1);'} : {}]}>
                    <View style={styles.iconCont}>
                        <View style={styles.iconPlaceholder}>
                            <Text style={styles.txtFirstLetter}>{item.name.charAt(0)}</Text>
                        </View>
                    </View>
                    <View style={styles.labelCont}>
                        <Text style={styles.txtLabelTitle}>{item.name}</Text>
                        <Text style={styles.txtLabel}>{item.position || 'Not Available'}</Text>
                        <Text style={styles.txtLabel}>{item.branch || 'Not Available'}</Text>
                    </View>
                </View>
            </TouchableNativeFeedback>
        )
    }
}

export class EmployeeList extends Component {
        //List of Children
    constructor(props){
        super(props);
        this.state = {
            _status: CONSTANTS.STATUS.SUCCESS,
            _promptShow: false,
            _promptMsg: '',
            _refreshing: false,
            _activeKey: '',
            _list: [],
            showDraftList: false,
            loadingScreen: {
                show: false,
                msg: 'test'
            },

            msgBox: {
                show: false,
                type: '',
                msg: '',
                param: ''
            },
        }
    }

    componentDidMount = () => {
        if(
            this.props.employees.list.status[0] == 1 &&
            this.props.employees.list.data.length > 0
        ){
            this._setActiveChild(this.props.employees.list.data[0]);
        }
    }

    _doNothing = () => {

    }

    _addNewEmployee = async() => {
        this._setLoadingScreen(true, 'Initializing Employee Form, please wait...');
        await employeeApi.getDefaultData()
            .then((response) => response.json())
            .then(async(res) => {
                if(res.flagno == 1){
                    let employee = res.data;
                    employee.id = '';
                    await this.props.actions.employee.updateAllInfo({employee});
                    this.props.logininfo.navigation.navigate('AddEmployeeForm');
                }else{
                    this._setMessageBox(true, 'error-ok', res.message);
                }
            })
            .catch((exception) => {
                this._setMessageBox(true, 'error-ok', exception.message);
            });
        this._setLoadingScreen(false, 'Initializing Employee Form, please wait...');
    }

    _setActiveChild = (oItem) => {
        let curKey  = this.state._activeKey;
        if(curKey != oItem.key){
            this.setState({ _activeKey: oItem.key });
            this.props.activeProfileStatus(CONSTANTS.STATUS.LOADING);
            requestAnimationFrame(() => {
                this._checkAndGetEmployeeProfile(oItem)
            })
        }
    }

    _checkAndGetEmployeeProfile = async(oItem) => {
        let bIndex = false;
        await this.props.actions.employee.updateActiveID(oItem.key);
        bIndex = await this._checkIfNewProfile(oItem.key);
        if(bIndex < 0){
            /* console.log('GETTING FROMSERVER'); */
            this.props.actions.employee.getAllInfo(oItem.key);
        }
        else{
            /* console.log('EXISTING. UPDATING FROM VIEW.'); */
            this.props.actions.employee.updateAllInfo({employee:this.props.employees.allProfiles.data[bIndex]});
        }
        this.props.activeProfileStatus(CONSTANTS.STATUS.SUCCESS);
        
    }

    _checkIfNewProfile = async(key) => {
        let newArray = this.props.employees.allProfiles.data.slice();
        iData = newArray.findIndex(obj => obj.id == key);
        if(iData >= 0){
            return iData;
        }
        else{
            return -1;
        }
    }

    _refreshList = () => {
        this.setState({_list: [], _activeKey: ''});
        this.props.actions.allProfiles.clearAll();
        this.props.actions.employeelist.empty();
        this.props.actions.employeelist.get();
    }

    _showDraftList = () => {
        this.setState({ showDraftList: true })
    }

    _hideDraftList = () => {
        this.setState({ showDraftList: false })
    }

    _onSelectDraft = async(value) => {
        this._setLoadingScreen(true, 'Fetching Draft Employee Information. Please wait...');
        await draftlistApi.getProfile({ employeeid: value.empid })
            .then((response) => response.json())
            .then(async(res) => {
                console.log('RES: ' + JSON.stringify(res));
                if(res.flagno == 1){
                    let employee = res.data;
                    employee.id = res.empid;
                    await this.props.actions.employee.updateAllInfo({employee});
                    let strRouteName = '';
                    switch(value.index_start){
                        case 1:
                            strRouteName = 'EmployeeBasicInfo';
                            break;
                        case 2:
                            strRouteName = 'EmployeeAddress';
                            break;
                        case 3:
                            strRouteName = 'EmployeeDependents';
                            break;
                        case 4:
                            strRouteName = 'EmployeeBankAccount';
                            break;
                        case 5:
                            strRouteName = 'EmployeeDetails';
                            break;
                        case 6:
                            strRouteName = 'EmployeeWorkShift';
                            break;
                        case 7:
                            strRouteName = 'EmployeeBenefits';
                            break;
                        case 8:
                            strRouteName = 'RankBasedRules';
                            break;
                        default:
                            strRouteName = '';
                            break;
                    }
                    await this._hideDraftList();
                    this.props.navigation.navigate(strRouteName);
                }else{
                    this._setMessageBox(true, 'error-ok', res.message);
                }
                
            })
            .catch((exception) => {
                this._setMessageBox(true, 'error-ok', exception.message);
            });
        this._setLoadingScreen(false);
    }

    _hideLoadingPrompt = () => {
        this.setState({
            _promptShow: false
        })
    }

    _showLoadingPrompt = (msg) => {
        this.setState({
            _promptMsg: msg,
            _promptShow: true
        })
    }

    _setMessageBox = (show, type, msg, param) => {
        this.setState({
            msgBox: oHelper.setMsgBox(
                this.state.msgBox,
                show, 
                type,
                msg,
                param
            )
        })
    }

    _setLoadingScreen = (show, msg) => {
        let oLoadingScreen = {...this.state.loadingScreen};
        oLoadingScreen.show = show;
        oLoadingScreen.msg = msg;
        this.setState({ loadingScreen: oLoadingScreen });
    }

    _msgBoxOnClose = (params) => {
        this.setState({
            msgBox: oHelper.clearMsgBox(this.state.msgBox)
        })
    }
    
    
    render(){
        /* console.log('this.props.employees.allProfiles: ' + JSON.stringify(this.props.employees.allProfiles)); */
        /* console.log('this.props.employees.list.data: ' + JSON.stringify(this.props.employees.list.data)); */
        console.log('Rendering EmployeeList...')
        let pStatus = [...this.props.employees.list.status]
        let pProgress = pStatus[0];
        let pMessage = pStatus[1];

        if(pProgress==0){
            return (
                <PromptScreen.PromptError title={TITLE} onRefresh={()=>this.props.triggerRefresh(true)}/>
            );
        }

        else if(pProgress==1){
            const oListHeader = (
                <View style={styles.contSearch}>
                    <View style={styles.iconFilter}>
                        <Icon name='filter' size={20} color='#fff' />
                    </View>
                    
                    <SearchBox value={''} 
                        onChangeText={this._doNothing} 
                        onSubmit={this._doNothing}/>

                </View>
            )
            return(
                <GenericContainer
                    containerStyle={styles.leftCont}
                    msgBoxShow = {this.state.msgBox.show}
                    msgBoxType = {this.state.msgBox.type}
                    msgBoxMsg = {this.state.msgBox.msg}
                    msgBoxOnClose = {this._msgBoxOnClose}
                    msgBoxOnYes = {this._msgBoxOnYes}
                    msgBoxParam = {this.state.msgBox.param}
                    loadingScreenShow = {this.state.loadingScreen.show}
                    loadingScreenMsg = {this.state.loadingScreen.msg}
                    status={[1,'Success']}
                    title={'Employee List'}
                    onRefresh={this._fetchDataFromDB}>
                    <LinearGradient 
                        colors={['#818489', '#3f4144', '#202626']}
                        style={styles.leftCont}>
                        
                        <LinearGradient 
                            colors={['#818489', '#3f4144', '#202626']}
                            style={styles.contTitle}>

                            
                            <View style={styles.contTitleName}>
                                <Text style={styles.txtTitle}>
                                    {this.props.activecompany.name.toUpperCase()}
                                </Text>
                            </View>
                        </LinearGradient>

                        <View style={styles.optionsCont}>
                            <FlatList
                                /* getItemLayout={this._getItemLayout} */
                                initialNumToRender={50}
                                refreshing={this.state._refreshing}
                                onRefresh={() => {this._refreshList()}}
                                /* ListHeaderComponent={oListHeader} */
                                ref={(ref) => { this.flatListRef = ref; }}
                                data={this.props.employees.list.data}
                                renderItem={({item}) =>
                                    <EmployeeListCard
                                        activeKey={this.state._activeKey}
                                        item={item} 
                                        itemPressed={(pressedItem) => this._setActiveChild(pressedItem)}/>
                                }
                            />
                            {
                                this.props.viewOnly || false ?
                                    null
                                :
                                    <ActionButton 
                                        offsetX={20}
                                        offsetY={20}
                                        size={50}
                                        activeOpacity={0.8}
                                        bgColor='rgba(0,0,0,0.8)'
                                        buttonColor='rgba(0, 126, 0, 0.9);'
                                        spacing={10}
                                    >
                                        <ActionButton.Item size={45} buttonColor='#3498db' title="DRAFTS" onPress={this._showDraftList}>
                                            <Text style={{position: 'absolute', fontWeight: '900', color: '#035a96', fontSize: 12, right: 10, top: 5}}>{this.props.draftlist.data.length}</Text>
                                            <Icon name='account-edit' color='#fff' size={22} style={styles.actionButtonIcon} />
                                        </ActionButton.Item>
                                        <ActionButton.Item size={45} buttonColor="#EEB843" title="ADD NEW EMPLOYEE" onPress={this._addNewEmployee}>
                                            <Icon name='account-plus' color='#fff' size={22} style={styles.actionButtonIcon} />
                                        </ActionButton.Item>
                                    </ActionButton>
                            }
                        </View>
                        
                        <CustomPicker 
                            keyName='empid'
                            closeLabel='Close'
                            list={this.props.draftlist.data}
                            emptyprompt = 'Error: No data found'
                            title='DRAFT EMPLOYEES'
                            objLabelName={'fullname'}
                            onSelect={this._onSelectDraft}
                            visible={this.state.showDraftList}
                            onClose={this._hideDraftList}/>
        
                        <PromptScreen.PromptGeneric 
                            show= {this.state._promptShow} 
                            title={this.state._promptMsg}/>

                    </LinearGradient>
                </GenericContainer>
            )
        }

        else{
            return (
                <View style={styles.container}>
                    <PromptScreen.PromptLoading title={pMessage}/>
                </View>
            );
        }
    }
}

function mapStateToProps (state) {
    return {
        logininfo: state.loginReducer.logininfo,
        activecompany: state.activeCompanyReducer.activecompany,
        employees: state.employees,
        draftlist: state.employees.draft
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            employeelist: bindActionCreators(employeeListActions, dispatch),
            employee: bindActionCreators(employeeActions, dispatch),
            allProfiles: bindActionCreators(allProfilesActions, dispatch),
            draftlist: bindActionCreators(draftlistActions, dispatch),
        },
    }
}
  
export default withNavigation(connect(
    mapStateToProps,
    mapDispatchToProps
)(EmployeeList))
  
export default withNavigation(connect(
    mapStateToProps,
    mapDispatchToProps
)(EmployeeList))
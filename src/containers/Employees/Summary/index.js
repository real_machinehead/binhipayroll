//Packages
import React, { Component } from 'react';
import {
    View,
    Text,
    Button,
    FlatList,
    TouchableNativeFeedback,
    TextInput,
    ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import LinearGradient from 'react-native-linear-gradient';

//Styles Properties
import styles from './styles';

//Custom Components
import * as PromptScreen from '../../../components/ScreenLoadStatus';
import CustomCard, 
{
    Description,
    PropTitle,
    PropLevel1, 
    PropLevel2
}
from '../../../components/CustomCards';

//Helper
import * as oHelper from '../../../helper';

//Redux
import { connect } from 'react-redux';
import * as employeeActions from '../data/activeProfile/actions';
import { bindActionCreators } from 'redux';
import { CONSTANTS } from '../../../constants/index';

//constants
const btnActive = 'rgba(255, 255, 255, 0.3);'
const btnInactive = 'transparent';
const TITLE = 'Employee Profile Summary'
export class Summary extends Component {

    _triggerRefresh = () => {
        this.props.actions.employee.getAllInfo(this.props.employees.activeProfile.data.id);
    }

    _viewProfile = () => {
        const navigation = this.props.logininfo.navigation;
        navigation.navigate('EmployeeProfile')
    }

    render(){
        const activeProfile = this.props.employees.activeProfile.data;
        const hidesummary = this.props.employees.activeProfile.hidesummary;
        let pStatus = Object.keys(activeProfile).length === 0 && activeProfile.constructor === Object ? CONSTANTS.STATUS.LOADING : CONSTANTS.STATUS.SUCCESS;
        let pProgress = pStatus[0];
        let pMessage = pStatus[1];

        if(pProgress==0){
            return (
                <PromptScreen.PromptError title={TITLE} onRefresh={()=>this._triggerRefresh()}/>
            );
        }

        else if(pProgress==1){
            const summary = activeProfile.summary || {header: [], body: []};
            const header = summary.header;
            const body = summary.body;

            return(
                <View style={styles.rightCont}>
                    {
                        hidesummary ?
                            <View style={styles.contHideSummary}>
                                <Text style={styles.txtHideSummary}>Press a draft employee to continue the set-up</Text>
                            </View>
                        :
                            <View style={styles.contCard}>
                                <CustomCard title={TITLE} oType='Text' clearMargin>
                                    <ScrollView contentContainerStyle={{padding: 30, paddingTop: 0}}>
                                        <View>
                                            <PropTitle name='General Information'/>

                                            {
                                                header.map((data, index) =>
                                                    <PropLevel2 
                                                        key={data[0] + index}
                                                        name={data[0]}
                                                        content={
                                                            <Text style={styles.txtDefault}>
                                                                {data[1]}
                                                            </Text>
                                                        }
                                                        contentStyle={{width: 500}}
                                                        placeHolderStyle={{width: 800, height: 25}}
                                                        hideBorder={true}
                                                    />
                                                )
                                            }

                                            <PropTitle name='Employee Information'/>

                                            {
                                                body.map((data, index) =>
                                                    <PropLevel2 
                                                        key={data[0] + index}
                                                        name={data[0]}
                                                        content={
                                                            <Text style={styles.txtDefault}>
                                                                {data[1]}
                                                            </Text>
                                                        }
                                                        contentStyle={{width: 2000}}
                                                        placeHolderStyle={{width: 1000, height: 60}}
                                                        hideBorder={true}
                                                    />
                                                )
                                            }


                                        </View>
                                    </ScrollView>
                                </CustomCard>
                            </View>
                    }

                    {
                        hidesummary ? null :
                            <TouchableNativeFeedback 
                            onPress={this._viewProfile}
                            background={TouchableNativeFeedback.SelectableBackground()}>
        
                            <View style={styles.contFooter}>
                                <Text style={styles.txtLabel}>View Complete Profile</Text>
                            </View>
        
                        </TouchableNativeFeedback>
                    }
                            
                </View>
                
            );
        }

        else{
            return (
                <View style={styles.container}>
                    <PromptScreen.PromptLoading title={pMessage}/>
                </View>
            );
        }
    }
}

function mapStateToProps (state) {
    return {
        logininfo: state.loginReducer.logininfo,
        activecompany: state.activeCompanyReducer.activecompany,
        employees: state.employees
    }
}
  
function mapDispatchToProps (dispatch) {
    return {
        actions: {
            employee: bindActionCreators(employeeActions, dispatch),
        },
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Summary)
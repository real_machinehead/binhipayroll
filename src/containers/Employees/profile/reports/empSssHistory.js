//Packages
import React, { Component } from 'react';
//Children Components
import SssReport from './empsssreport';
//Redux
import { connect } from 'react-redux';
//Constants
const FILTER = 'EMPLOYEE';

export class EmpSssHistory extends Component {
    render(){
        const activeProfile = this.props.employees.activeProfile.data;
        return(
            <SssReport
                filter = {FILTER}
                empid = {activeProfile.id}
            />
        );
    }
}

function mapStateToProps (state) {
    return {
        employees: state.employees
    }
}

export default connect(
    mapStateToProps,
)(EmpSssHistory)
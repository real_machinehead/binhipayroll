/**************************************************************
 *  FileName:           styles.js
 *  Description:        Company Policy Children Styles
 *  Copyright:          Binhi-MeDFI © 2017
 *  Original Author:    Jovanni Auxilio
 *  Date Created:       2017-11-07

 *  Modification History:
        Date              By            Description

**************************************************************/
const React = require('react-native');
const { StyleSheet } = React;

export default {
    container: {
        flex: 1,
        backgroundColor: '#e8e8e8',
    },

    textStyles: {
        title: {
            fontSize: 15,
            color: '#333333',
            fontWeight: '500'
        },

        name: {
            fontSize: 15,
            color: '#434646',
            fontWeight: '500'
        },

        description: {
            fontSize: 13,
            color: '#505251'
        },

        summary: {
            fontSize: 13.5,
            color: '#434646'
        },

        summaryBold: {
            fontSize: 13.5,
            color: '#434646',
            fontWeight: '800',
        },

        footerLabels: {
            fontSize: 12,
            color: '#FFF'
        },

        groupTitle: {
            fontSize: 14,
            color: '#434646',
            fontWeight: '500',
        },

        groupLabel: {
            fontSize: 14,
            color: '#FFFFFF'
        },

        btnText: {
            fontSize: 12,
            color: '#FFFFFF'
        },

        btnText: {
            fontSize: 12,
            color: '#FFFFFF'
        },

        btnTextError: {
            fontSize: 12,
            color: '#ff4d4d'
        }
    },
    level2Styles: {
        txt: {
            fontFamily: 'Helvetica-Bold',
            fontSize: 14,
            color: '#434646',
            paddingLeft: 15,
            paddingRight: 15,
            height: '100%',
            textAlignVertical: 'center'
        },

        button: {
            margin: 3,
            paddingTop: 5,
            paddingBottom: 5,
            paddingLeft: 15,
            paddingRight: 15,
            borderRadius: 10,
            backgroundColor: '#e5e5e5',
            color: '#505251',
            elevation: 3,
            textAlign: 'center'
        },

        cont: {
            width: 200
        },

        placeHolder: {
            marginTop: 15
        }
    },

    specialNoteStyle: {
        container: {
            paddingTop: 40,
            paddingLeft: 50,
            paddingRight: 80
        },

        txt: {
            fontFamily: 'Helvetica-Light',
            fontSize: 14,
            color: '#838383',
            letterSpacing: 5
        }
    },
    reportItemStyles: {
        title: {

            container: {
                flex: 1,
                backgroundColor: 'rgba(21, 123, 19, 0.86)'
            },

            contContent: {
                flex: 1,
            },

            contIcon: {
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                paddingTop: 10
            },

            contLabel: {
                flex: 0.7,
                justifyContent: 'flex-start',
                alignItems: 'center',
                paddingLeft: 15,
                paddingRight: 15,
                paddingBottom: 15
            },

            iconPlaceholder: {
                /* elevation: 15, */
                width: 60,
                height: 60,
                backgroundColor: 'rgba(22, 18, 20, 0.3)',
                borderRadius: 100,
                justifyContent: 'center',
                alignItems: 'center',
                alignSelf: 'center'
            },

            txtLabel: {
                fontSize: 14,
                color: '#f4f4f4',
                fontWeight: '500',
                fontFamily: 'Helvetica-Light'
            },

            txtDescription: {
                fontSize: 12,
                color: '#f4f4f4',
                fontStyle: 'italic',
                fontFamily: 'Helvetica-Light'
            }
        },
        
    },
    listStyles: {
        container: {
            flex: 1,
            flexDirection: 'column',
            elevation: 3
        },

        header: {
            container: {
                height: 55,
                flexDirection: 'row',
                backgroundColor: '#fcfcfc',
                elevation: 5
            },

            center: {
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'row'
            },

            icon: {
                justifyContent: 'center',
                alignItems: 'center'
            },

            title: {
                paddingLeft: 10,
                justifyContent: 'center',
                alignItems: 'center'
            }
        },


        body: {
            container: {
                flex: 1,
                flexDirection: 'column',
                backgroundColor: 'transparent'
            },

            listComponent: {
                paddingBottom: 10,
                paddingTop: 7.5
            },

            placeholder: {
                marginTop: 7.5,
                marginRight: 15,
                marginLeft: 15,
                marginBottom: 7.5,
                elevation: 3,
                paddingTop: 15,
                paddingBottom: 20,
                paddingLeft: 20,
                paddingRight: 10,
                borderLeftWidth: 20,
                borderColor: 'yellow',
                backgroundColor: '#FFFFFF'
            },

            cardHeader: {
                container: {
                    flex: 1,
                    flexDirection: 'row',
                    backgroundColor: 'transparent'
                },

                left: {
                    flex: 1,
                    backgroundColor: 'transparent',
                    flexDirection: 'row'
                },

                right: {
                    backgroundColor: 'transparent',
                    alignItems: 'flex-end'
                },

                icon: {
                    justifyContent: 'center',
                    alignItems: 'center'
                },

                title: {
                    flexDirection: 'column',
                    justifyContent: 'center',
                    alignItems: 'flex-start'
                }
            },

            cardBody: {
                container: {
                    flex: 1,
                    backgroundColor: 'transparent',
                    flexDirection: 'column',
                    paddingTop: 10,
                    paddingLeft: 5,
                    paddingRight: 13
                },

                content: {
                    flex: 1,
                    backgroundColor: 'transparent',
                    flexDirection: 'row'
                },
                
                label: {
                    flex: 1,
                    backgroundColor: 'transparent'
                },

                value:{
                    backgroundColor: 'transparent'
                }
            }
        },

        footer: {
            container: {
                height: 35,
                flexDirection: 'row',
                backgroundColor: 'rgba(0, 0, 0, 0.5)'
            },
            left: {
                flex: 1,
                justifyContent: 'center',
                alignItems: 'flex-start',
                paddingLeft: 15
            },
            right: {
                paddingRight: 15,
                flexDirection: 'row'

            },
            label: {
                justifyContent: 'center',
                paddingRight: 5
            },
            box:{
                justifyContent: 'center',
            }
        }
    },    
    summaryStyles: {
        container: {
            flex: 1,
            flexDirection: 'column'
        },

        header: {
            container: {
                height: 55,
                flexDirection: 'row',
                backgroundColor: '#fcfcfc',
                elevation: 5,
                borderRightWidth: 0.7,
                borderColor: '#D1D4D6'
            },

            center: {
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'row'
            },

            icon: {
                justifyContent: 'center',
                alignItems: 'center'
            },

            title: {
                paddingLeft: 10,
                justifyContent: 'center',
                alignItems: 'center'
            }
        },

        body: {
            container: {
                flex: 1,
                flexDirection: 'column',
                backgroundColor: 'transparent',
                borderBottomWidth: 1,
                borderColor: '#D1D4D6',
            },

            content: {
                flex: 1,
                backgroundColor: '#FFFFFF',
                flexDirection: 'column',
            },
            upperBlock: {
                elevation: 1,
                padding: 40,
                paddingBottom: 0,
                flexDirection: 'column'
            },

            title: {
                marginBottom: 10
            },

            details: {
                flexDirection: 'row'
            },

            lowerBlock: {
                elevation: 1,
                padding: 30,
                paddingTop: 0,
                flexDirection: 'column'
            },

            cardBody: {
                content: {
                    /* flex: 1, */
                    paddingTop: 5,
                    backgroundColor: 'transparent',
                    flexDirection: 'row'
                },
                
                label: {
                    flex: 1,
                    backgroundColor: 'transparent',
                    paddingLeft: 25
                },

                value:{
                    backgroundColor: 'transparent',
                    flex: 1, 
                    alignItems: 'flex-start'
                }
            },

            horizontalScrollPlaceHolder: {
                backgroundColor: 'red'
            },

            scrollViewPlaceholder: {

            }
        },

        footer: {
            container: {
                flexDirection: 'column',
                backgroundColor: 'transparent'
            },
            content: {
                backgroundColor: '#ECF0F1',
                paddingBottom: 15,
                paddingTop: 10,
                minHeight: 50,
            },
            
            upperBlock: {
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'row',
                backgroundColor: 'transparent'
            },

            btn: {
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'green',
                borderRadius: 5,
                elevation: 3,
                margin: 7,
                width: 150,
                height: 27
            },

            btnPost: {
                backgroundColor: 'green',
            },

            btnCancel: {
                backgroundColor: 'rgba(193, 66, 66, 0.94)',
            },

            btnRegenerate: {
                backgroundColor: '#EEB843',
            },

            btnLater: {
                backgroundColor: '#505251',
            },

            lowerBlock: {
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'row',
                backgroundColor: 'transparent'
            }
        }
    },
    formStyles: {
        container: {
            paddingTop: 30,
            paddingBottom: 30,
            justifyContent: 'center',
            alignItems: 'center',
            width: '100%',
            maxWidth: 400,
            minWidth: 300
        },

        contForm: {
            flex: 1
        },

        formContent: {
            flex: 1,
            paddingTop: 35,
            paddingLeft: 40,
            paddingRight: 40,
            paddingBottom: 25
        }
    },

    reportsFlatlist: {
        paddingBottom: 15,
        paddingTop: 15
    },
    payrollSummary:{
        headertitle:{
            justifyContent: 'center',
            alignItems: 'center',
            //flexDirection: 'column'
        },
        body:{
            flex: 2
        },
        container:{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            paddingTop:10,
        },
        headercontainer:{
            flex: 1,
            flexDirection: 'column'
        },
        body2:{
            flex:10
        },
        summarydetails:{
            paddingTop:10,
        },
        summarydetailsbody:{
           height:'68%',
           width:'100%',
           paddingTop:10,     
        },
        summarylistdetails:{
            flexDirection: 'row',           
        },
        summarylistdescription:{
            flexDirection: 'row',  
            width:'40%',
        },
        summarylistdescriptionfont:{
            paddingLeft:35,
            paddingTop:10,
            fontSize: 15,
            color: '#434646',         
        },
        buttonsummary:{
            justifyContent: 'center',
            alignItems: 'center',
            paddingTop:10,
        }
    },
    title: {

        container: {
            padding: 10,
            /* backgroundColor:'yellow', */
            justifyContent: 'center',
            alignItems: 'center',
            borderBottomWidth: 0.7,
            borderColor: '#838383'
        },

        contContent: {
            flex: 1,
        },

        contIcon: {
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            paddingTop: 10
        },

        contLabel: {
            flex: 0.7,
            justifyContent: 'flex-start',
            alignItems: 'center',
            paddingLeft: 15,
            paddingRight: 15,
            paddingBottom: 15
        },

        iconPlaceholder: {
            /* elevation: 15, */
            width: 60,
            height: 60,
            backgroundColor: 'rgba(22, 18, 20, 0.3)',
            borderRadius: 100,
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center'
        },

        txtLabel: {
            fontSize: 14,
            color: '#f4f4f4',
            fontWeight: '500',
            fontFamily: 'Helvetica-Light'
        },

        txtDescription: {
            fontSize: 12,
            color: '#f4f4f4',
            fontStyle: 'italic',
            fontFamily: 'Helvetica-Light'
        }
    },
    
    body: {
        container: {
            flex: 1,
            /* backgroundColor: 'green', */
            flexDirection: 'column'
        },

        paramsList: {
            flex: 1,
            padding: 13
        },

        params: {
            /* flex: 1, */
            paddingBottom: 7,
            flexDirection: 'row',
            /* borderWidth: 1, */
            borderColor: '#FFF'
        },

        paramsArg: {
            flex: 1,
            /* borderWidth: 1, */
            justifyContent: 'center',
            /* borderColor: 'blue' */
        },

        paramsLeftMost: {
            flex: 1.5,
            alignItems: 'flex-start'
        },

        paramsRightMost: {
            flex: 1.2,
            alignItems: 'flex-end'
        },

        paramsCenter: {
            alignItems: 'center'
        }
    },
    content: {
        container: {
            flex: 1,
            flexDirection: 'row'
        },

        placeholder: {
            flex: 1,
            flexDirection: 'column',
            borderTopWidth: 0.7,
            borderRightWidth: 0.7,
            borderColor: '#838383'
        }
    },
    form: {
        container: {
            paddingTop: 30,
            paddingBottom: 30,
            justifyContent: 'center',
            alignItems: 'center',
            width: '90%',
        },

        content: {
            flex: 1,
            marginLeft: 15,
            marginRight: 15,
            borderWidth: 0.7,
            borderColor: '#D1D4D6'
        },

        contentFullscreen: {
            flex: 1,
            borderWidth: 0.7,
            borderColor: '#D1D4D6'
        }
        
    },

    header: {
        container: {
            /* backgroundColor: 'orange', */
            flexDirection: 'row'
        },

        left: {
            width: '35%',
            /* backgroundColor:'blue', */
            flexDirection: 'row',
            /* borderRightWidth: 0.7,
            borderColor: '#838383', */
            padding: 10
        },

        right: {
            width: '65%',
            /* backgroundColor:'indigo', */
            flexDirection: 'row',
            justifyContent: 'center',
            padding: 10
        },

        iconCont: {
            justifyContent: 'center',
            alignItems: 'center',
            /* borderWidth: 1, */
            paddingRight: 8
        },

        generalInfoCont: {
            width: '100%',
            flexDirection: 'row',
            /* backgroundColor: 'green' */
        },

        titleCont: {
            flex: -1,
            /* backgroundColor: 'red', */
            flexDirection: 'column',
            justifyContent: 'center'
        },
        
        addressCont: {
        },

        paramsList: {
            flexDirection: 'column',
            /* borderWidth: 1, */
            width: '50%'
        },

        param: {
            flexDirection: 'row',
            /* backgroundColor: 'orange' */
        },

        label: {
            width: '40%',
            /* borderWidth: 1, */
            paddingLeft: 10,
        },

        value: {
            width: '60%',
            /* borderWidth: 1, */
        }
        
    },


    system: {
        break: {
            height: 8,
            /* backgroundColor: 'blue' */
        },

        div: {
            height: 1,
            marginTop: 10,
            marginBottom: 15,
            backgroundColor: '#D1D4D6'
        },

        footer: {
            container:{
                /* position: 'absolute', */
                /* backgroundColor:'red', */
                height: 75,
                /* right: 0,
                left: 0,
                bottom: 0, */
                flexDirection: 'column'
            },
            title: {
                flex: 0.7,
                backgroundColor: '#D1D4D6',
                borderTopWidth: 1,
                borderBottomWidth: 1,
                borderColor: '#505251',
                justifyContent: 'center',
                alignItems: 'center'
            
            },
            value: {
                flex: 1,
                /* backgroundColor: 'orange', */
                justifyContent: 'center',
                alignItems: 'center'
            }
        }
    },

    textStyles: {
        companyName: {
            fontSize: 14,
            fontFamily: 'Helvetica-Bold',
            color: '#434646',
            fontWeight: '500'
        },
        
        address: {
            fontSize: 11,
            fontFamily: 'Helvetica-Light',
            color: '#838383'
        },

        label: {
            fontSize: 12,
            fontFamily: 'Helvetica-Light',
            color: '#434646'
        },

        value: {
            fontSize: 12,
            fontFamily: 'Helvetica-Light',
            color: '#434646'
        },

        details: {
            fontSize: 12,
            fontFamily: 'Helvetica-Light',
            color: '#434646'
        },

        detailsHeader: {
            fontSize: 12,
            fontFamily: 'Helvetica-Light',
            color: '#434646',
            fontWeight: '500'
        },

        footerTitle: {
            fontSize: 13,
            fontFamily: 'Helvetica-Light',
            color: '#434646',
            fontWeight: '500'
        },

        footerValue: {
            fontSize: 14,
            fontFamily: 'Helvetica-Light',
            color: '#434646'
        },

        cardTitle: {
            fontSize: 17,
            fontFamily: 'Helvetica-Bold',
            color: '#434646'
        }
    }
}
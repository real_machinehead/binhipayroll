import React, { Component } from 'react';
import {
    View,
    Text,
    Alert
} from 'react-native';

//Custom Components
import FormModal from '../../../../../../components/FormModal';
import GenericContainer from '../../../../../../components/GenericContainer';

//Styles
import styles from '../styles';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as summaryActions from '../../data/summary/actions';
import * as employeesActions from '../../data/employees/actions';
import * as payrollTransactionApi from '../../data/all/api';
import * as employeesApi from '../../data/employees/api';

//Children components
import PayrollTransactionEmployeesHeader from './header';
import PayrollTransactionEmployeesFooter from './footer';
import PayrollTransactionEmployeesBody from './body';
import EmployeePayslipForm from '../../../../../Reports/payslip/payslipForm';
import EmployeeDTRCalendar from '../../../../../DTR/main';
import MonetaryAdjustment from '../../../../../Transactions/monetaryAdjustment';

//Helper
import * as oHelper from '../../../../../../helper';

export class PayrollTransactionEmployees extends Component {
    constructor(props){
        super(props);
        this.state = {
            _bShowPayslip: false,
            _bShowDTR: false,
            _bShowMonetaryAdjustmentForm: false,
            _activeItem: {
                name: ''
            },
            refreshing: false,
            loadingScreen: {
                show: false,
                msg: 'test'
            },

            msgBox: {
                show: false,
                type: '',
                msg: '',
                param: ''
            },
        }
    }
    
    _showPaySlip = () => {
        this.setState({ _bShowPayslip: true })
    }

    _showDTR = (oItem) => {
        console.log('ZZZZZZZZZZZZZZZZ ITEM: ' + JSON.stringify(oItem));
        this.setState({ 
            _bShowDTR: true,
            _activeItem: oItem
        })
    }

    _showMonetaryAdjustmentForm = (oItem) => {
        this.setState({ 
            _bShowMonetaryAdjustmentForm: true,
            _activeItem: oItem
        })
    }

    _onCloseEmployee = async(oItem) => {
        const oPayrollInfo = this.props.transactionInfo.activerule;
        this._setLoadingScreen(true, 'Closing an employee. Please wait...');
        await employeesApi.close({payrollid: oPayrollInfo.payrollid, employeeid: oItem.id})
            .then((response) => response.json())
            .then((res) => {
                if(res.flagno == 1){
                    this._updateEmpStatusInStore(res.data, oItem.id);
                    this._setMessageBox(true, 'success', res.message, 'CLOSE');
                }else{
                    this._setMessageBox(true, 'error-ok', res.message);
                }
            })
            .catch((exception) => {
                this._setMessageBox(true, 'error-ok', exception.message);
            });

        this._setLoadingScreen(false);
    }

    _updateEmpStatusInStore = (strUpdatedStatus, iEmpId) => {
        let oAllEmpData = oHelper.copyObject(this.props.employees.data);
        oAllEmpData.status = strUpdatedStatus;
        const iCurIndex = oHelper.getElementIndexByPropValue(oAllEmpData.list, 'id', iEmpId);
        oAllEmpData.list[iCurIndex].status = 3;
        this.props.actions.employees.update(oAllEmpData);
    }

    _updateAllEmpStatusInStore = async(strUpdatedStatus) => {
        let oAllEmpData = oHelper.copyObject(this.props.employees.data);
        oAllEmpData.status = strUpdatedStatus;
        await oAllEmpData.list.map((oEmp, index) => {
            oAllEmpData.list[index].status = 3;
        })
        this.props.actions.employees.update(oAllEmpData);
    }

    _updateMarkAllAsClosedInStore = value => {
        let oAllEmpData = oHelper.copyObject(this.props.employees.data);
        oAllEmpData.markallasclosed = value;
        this.props.actions.employees.update(oAllEmpData);
    }

    _updateStatusLabelInStore = (strUpdatedStatus) => {
        let oAllEmpData = oHelper.copyObject(this.props.employees.data);
        oAllEmpData.status = strUpdatedStatus;
        this.props.actions.employees.update(oAllEmpData);
    }

    _onCancel = () => {
        this.setState({
            _bShowPayslip: false,
            _bShowDTR: false,
            _activeItem: { name: '' },
            _bShowMonetaryAdjustmentForm: false
        })
    }

    _onRegenerate = oItem => {
        Alert.alert(
            'Warning',
            'Regenerating an Individual Payroll Transaction will discard the individual calculated payroll ' +
            'and may cause changes in the Payroll Summary. ' + 
            '\n\nAre you sure you want to regenerate payroll for ' + oItem.name + ' ?',
            [
                {text: 'NO', onPress: () => {}},
                {text: 'YES', onPress: () => this._regenerateEmployee(oItem)}
            ],
            { cancelable: false }
        );
    }

    _onCloseAll = (value) => {
        const strMsg = value ? 
            'When "marked all as closed" is switched on, all employee payroll transactions will be marked ' +
            'as closed. When an employee payroll transaction is individually regenerated, it will be marked ' +
            'as closed by default. However, it will be switched off when the entire payroll transaction is regenerated. ' +
            '\n\nAre you sure you want to proceed ?'
        :
            'When "marked all as closed" is switched off, existing employee payroll transaction status will be ' +
            'preserved. When an employee payroll transaction is individually regenerated, it will be marked as open ' +
            'by default. ' +
            '\n\nAre you sure you want to proceed ?'
            
        Alert.alert(
            'Warning',
            strMsg,
            [
                {text: 'NO', onPress: () => {}},
                {text: 'YES', onPress: () => this._switchCloseAll(value)}
            ],
            { cancelable: false }
        );
    }

    _regenerateEmployee = async(oItem) => {
        const oPayrollInfo = this.props.transactionInfo.activerule;
        this._setLoadingScreen(true, "Regenerating payroll transaction for " + oItem.name +  " Please wait...");
        await employeesApi.regenerate({payrollid: oPayrollInfo.payrollid, employeeid: oItem.id})
            .then((response) => response.json())
            .then(async(res) => {
                if(res.flagno == 1){
                    this._updateAfterEmployeeRegeneration(res.data, oItem);
                    this._setMessageBox(true, 'success', res.message, 'CLOSE');
                }else{
                    this._setMessageBox(true, 'error-ok', res.message);
                }
            })
            .catch((exception) => {
                this._setMessageBox(true, 'error-ok', exception.message);
            });

        this._setLoadingScreen(false);
    }

    _updateAfterEmployeeRegeneration = async(oRes, oItem) => {
        let oAllEmpData = oHelper.copyObject(this.props.employees.data);
        oAllEmpData.status = oRes.employees.status;
        const curItemIndex = await oHelper.getElementIndexByPropValue(oAllEmpData.list, 'id', oItem.id);
        oAllEmpData.list[curItemIndex] = oRes.employees.list[0];

        this.props.actions.employees.update(oAllEmpData);

        let oSummaryData = await oHelper.copyObject(this.props.summary.data);
        this.props.actions.summary.update(oRes.summary)
    }

    _switchCloseAll = async(value) => {
        const oPayrollInfo = this.props.transactionInfo.activerule;
        this._setLoadingScreen(true, 'Marking all employee transactions as closed. Please wait...');
        await payrollTransactionApi.switchCloseAllFlag({payrollid: oPayrollInfo.payrollid, value: value})
            .then((response) => response.json())
            .then(async(res) => {
                if(res.flagno == 1){
                    if(value){
                        await this._updateAllEmpStatusInStore(res.data);
                    }else{
                        await this._updateStatusLabelInStore(res.data);
                    }
                    this._updateMarkAllAsClosedInStore(value);
                    this._setMessageBox(true, 'success', res.message, 'CLOSE');
                }else{
                    this._setMessageBox(true, 'error-ok', res.message);
                }
            })
            .catch((exception) => {
                this._setMessageBox(true, 'error-ok', exception.message);
            });

        this._setLoadingScreen(false);
    }

    //Generic Functions
    _setMessageBox = (show, type, msg, param = null) => {
        this.setState({
            msgBox: oHelper.setMsgBox(
                this.state.msgBox,
                show, 
                type,
                msg,
                param
            )
        })
    }

    _setLoadingScreen = (show, msg) => {
        let oLoadingScreen = {...this.state.loadingScreen};
        oLoadingScreen.show = show;
        oLoadingScreen.msg = msg;
        this.setState({ loadingScreen: oLoadingScreen });
    }

    _msgBoxOnClose = () => {
        this.setState({
            msgBox: oHelper.clearMsgBox(this.state.msgBox)
        })
    }

    render(){
        console.log('XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXPT->EMP-INDX')
        console.log('loggggggggggggsssssss',this.props);
        const aStatus = this.props.status;
        const oData = this.props.data;
        let iPayrollStatus = oData ? oData.payrollinfo.status : null;
        const oActiveItem = this.state._activeItem;

    
        const fromStyles = styles.formStyles;
        const summaryStyles = styles.summaryStyles;
        const oPaySlip = (
        <FormModal 
                cancelLabel='Close'
                viewOnly={true}
                containerStyle={fromStyles.container}
                visible={this.state._bShowPayslip}
                onCancel={this._onCancel}
                onOK={this._onSubmit}
                title='EMPLOYEE PAYSLIP'>
                
                <View style={fromStyles.content}>
                    <EmployeePayslipForm hideHeader={true}/>
                </View>
            </FormModal>
        );
        const oDTR = (
            <FormModal 
                cancelLabel='Close'
                viewOnly={true}
                containerStyle={fromStyles.container}
                visible={this.state._bShowDTR}
                onCancel={this._onCancel}
                onOK={this._onSubmit}
                title={'Daily Time Record - ' + this.state._activeItem.name}>

                <View style={fromStyles.contentFullscreen}>
                    <EmployeeDTRCalendar 
                        hideHeader={true}
                        isCalledByActiveRule
                    />
                </View>

            </FormModal>
        );

        /*const oMonetaryAd = (
            <MonetaryAdjustment
                onCancel={this._onCancel}
                onSubmit={this._onCancel}
                visible={this.state._bShowMonetaryAdjustmentForm}
                isExternalCall
                hideForm={this._onCancel}
                employee={{
                    id: oActiveItem.id,
                    name: oActiveItem.name
                }}
                payroll={{
                    id: oData ? oData.payrollinfo.id : null,
                    date: oData ? oData.payrollinfo.payrollDate : null
                }}
            />
        )*/
        
        return(
           /* <View style={summaryStyles.container}>
                <GenericContainer
                    msgBoxShow = {this.state.msgBox.show}
                    msgBoxType = {this.state.msgBox.type}
                    msgBoxMsg = {this.state.msgBox.msg}
                    msgBoxOnClose = {this._msgBoxOnClose}
                    msgBoxParam = {this.state.msgBox.param}
                    loadingScreenShow = {this.state.loadingScreen.show}
                    loadingScreenMsg = {this.state.loadingScreen.msg}
                    status={aStatus}
                    title={ "Payroll Transaction Employees' List" }
                    onRefresh={() => this.props.onRefresh() }>
                    {console.log('XXXXXXXXXXXXODATA: ' + JSON.stringify(oData))}
                    <PayrollTransactionEmployeesHeader data={oData}/>

                    <PayrollTransactionEmployeesBody
                        onRefresh={() => this.props.onRefresh()}
                        status={aStatus}
                        data={oData}
                        showPayslip={this._showPaySlip}
                        showDTR={this._showDTR}
                        showMonetaryAdjustmentForm={this._showMonetaryAdjustmentForm}
                        onCloseEmployee={this._onCloseEmployee}
                        onRegenerate={this._onRegenerate}
                    />

                    {
                        iPayrollStatus == 3 ?
                            null
                        :
                            <PayrollTransactionEmployeesFooter 
                                data={oData}
                                onCloseAll={this._onCloseAll}
                            />
                    }
                    

                    { this.state._bShowPayslip ? oPaySlip : null }

                    { this.state._bShowDTR ? oDTR : null }

                    { this.state._bShowMonetaryAdjustmentForm ? oMonetaryAd : null }
                </GenericContainer>
            </View>*/
            <View style={summaryStyles.container}>
                <PayrollTransactionEmployeesHeader data={oData}/>

                    <PayrollTransactionEmployeesBody
                        onRefresh={() => this.props.onRefresh()}
                        status={aStatus}
                        data={oData}
                        showPayslip={this._showPaySlip}
                        showDTR={this._showDTR}
                        showMonetaryAdjustmentForm={this._showMonetaryAdjustmentForm}
                        onCloseEmployee={this._onCloseEmployee}
                        onRegenerate={this._onRegenerate}
                    />

                    {
                        iPayrollStatus == 3 ?
                            null
                        :
                            <PayrollTransactionEmployeesFooter 
                                data={oData}
                                onCloseAll={this._onCloseAll}
                            />
                    }
                    

                    { this.state._bShowPayslip ? oPaySlip : null }

                    { this.state._bShowDTR ? oDTR : null }

                    { this.state._bShowMonetaryAdjustmentForm ? oMonetaryAd : null }
            </View>
        );
    }
}

function mapStateToProps (state) {
    return {
        summary: state.payrolltransaction.summary,
        employees: state.payrolltransaction.employees,
        transactionInfo: state.payrolltransaction.all
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            summary: bindActionCreators(summaryActions, dispatch),
            employees: bindActionCreators(employeesActions, dispatch),
        }
    }
}
  
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PayrollTransactionEmployees)
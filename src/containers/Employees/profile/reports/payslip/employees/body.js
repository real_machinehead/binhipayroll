import React, { Component } from 'react';
import {
    View,
    Text,
    FlatList,
    ActivityIndicator,
    Alert
} from 'react-native';
import {
    Menu,
    MenuOptions,
    MenuOption,
    MenuTrigger,
  } from 'react-native-popup-menu';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { withNavigation } from 'react-navigation';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as employeesActions from '../../data/employees/actions';
import * as employeesApi from '../../data/employees/api';
import * as payslipActions from '../../../reports/data/payslip/actions';
import * as dtrActions from '../../../reports/data/DTR/actions';

//Children Components
import EmployeePayrollCard from './item';
import SeeMoreButton from '../../../../../../components/SeeMoreButton';

//Styles
import styles from '../styles';

//helper
import * as oHelper from '../../../../../../helper';

export class PayrollTransactionEmployeesBody extends Component {
    constructor(props){
        super(props);
        this.state = {
            currentPage: 1,
            refreshing: false,
            loadingMore: false,
            loadingMoreFailed: false,
        }
    }

    _keyExtractor = (item, index) => item.id;
    _onPullRefresh = () => {
        this.props.onRefresh();
    }

    _displayMoreFailedMsg = (msg = null) => {
        const strMsg = msg ? 'Please check your internet connection or try loading again in a minute.' : msg;
        Alert.alert(
            "Sorry, We're Having Some Issues",
            strMsg,
            [
              {text: 'OK', onPress: () => {}},
            ],
            { cancelable: false }
        )
    }

    _renderFooter = () => {
        return (
            this.state.loadingMoreFailed ? 
                <SeeMoreButton onPress={this._seeMore}/>
            :
                this.state.loadingMore ? 
                    <ActivityIndicator size="small" color="#EEB843" /> 
                : 
                    null
        );
    }

    _loadMoreFromDB = () => {
        if(!this.state.loadingMore && !this.state.loadingMoreFailed){
            const nextpagenumber =  this.state.currentPage + 1;

            if(nextpagenumber <= this.props.data.totalpages){
                this.setState({ loadingMore: true, loadingMoreFailed: false });

                const iPayrollId = this.props.transactionInfo.activerule.payrollid;
                const oCriteria = {
                    payrollid: iPayrollId,
                    page: nextpagenumber
                };

                employeesApi.get(oCriteria)
                    .then((response) => response.json())
                    .then((res) => {
                        if(res.flagno == 1){
                            this._updateDataStore(res);
                            this.setState({currentPage: nextpagenumber})
                        }else{
                            this._displayMoreFailedMsg(res.message);
                            this.setState({ loadingMoreFailed: true });
                        }
                        this.setState({loadingMore: false});
                    })
                    .catch((exception) => {
                        this._displayMoreFailedMsg(exception.message);
                        this.setState({loadingMore: false});
                        this.setState({ loadingMoreFailed: true });
                    });
            }
        }
    };

    _updateDataStore = async(res) => {
        let oCurData = oHelper.copyObject(this.props.employees.data);
        let aResList = res.data.list;
        let mergedList = await oHelper.arrayMerge(oCurData.list, aResList, 'id');
        oCurData.list = mergedList;
        this.props.actions.employees.update(oCurData);
    }

    _showPaySlip = (oItem) => {
        this.props.actions.payslip.setActiveRule({payrollid: oItem.payrollid, employeeid: oItem.id});
        this.props.showPayslip();
    }

    _showDTR = (oItem) => {
        this.props.actions.dtr.setActiveRule({payrollid: oItem.payrollid, employeeid: oItem.id});
        this.props.showDTR(oItem);
    }

    _showMonetaryAdjustmentForm = (oItem) => {
        this.props.showMonetaryAdjustmentForm(oItem);
    }

    _onCloseEmployee = oItem => {
        this.props.onCloseEmployee(oItem);
    }

    _onRegenerate = oItem => {
        this.props.onRegenerate(oItem);
    }

    render(){
        const oData = this.props.data.list;
        const aStatus = this.props.status;
        const bodyStyles = styles.listStyles.body;
        return(
            <View style={bodyStyles.container}>
                <FlatList
                    refreshing={this.state.refreshing || aStatus[0] == 2}
                    onRefresh={this._onPullRefresh}
                    extraData={this.state.loadingMore}
                    contentContainerStyle={bodyStyles.listComponent}
                    ref={(ref) => { this.ref_payrollGenerationEmployeeList = ref; }}
                    data={oData}
                    keyExtractor={this._keyExtractor}
                    onEndReached={this._loadMoreFromDB}
                    onEndReachedThreshold={0.5}
                    ListFooterComponent={this._renderFooter}
                    renderItem={({item}) => 
                        <EmployeePayrollCard 
                            showPayslip={this._showPaySlip} 
                            item={item}
                            showDTR={this._showDTR}
                            showMonetaryAdjustmentForm={this._showMonetaryAdjustmentForm}
                            onCloseEmployee={this._onCloseEmployee}
                            onRegenerate={this._onRegenerate}
                        />
                    }
                />
            </View>
        );
    }
}

function mapStateToProps (state) {
    return {
        summary: state.payrolltransaction.summary,
        employees: state.payrolltransaction.employees,
        transactionInfo: state.payrolltransaction.all
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            employees: bindActionCreators(employeesActions, dispatch),
            payslip: bindActionCreators(payslipActions, dispatch),
            dtr: bindActionCreators(dtrActions, dispatch)
        }
    }
}
  
export default  withNavigation(connect(
    mapStateToProps,
    mapDispatchToProps
)(PayrollTransactionEmployeesBody));
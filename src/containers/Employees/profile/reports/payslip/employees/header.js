import React, { Component } from 'react';
import {
    View,
    Text,
    Button
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

//Styles
import styles from '../styles';

export default class PayrollTransactionEmployeesHeader extends Component {
    render(){
        const headerStyles = styles.listStyles.header;
        const textStyles = styles.textStyles;
        console.log('loggsssss123',this.props)
        return(
            <View style={headerStyles.container}>
                <View style={headerStyles.center}>

                    <View style={headerStyles.icon}>
                        <Icon name='ios-people' size={28} color='#333333'/>
                    </View>
                    <View style={headerStyles.title}>
                        <Text style={textStyles.title}>EMPLOYEE PAYSLIP</Text>
                    </View>

                </View>
            </View>
        );
    }
}
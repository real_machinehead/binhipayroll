import React, { Component } from 'react';
import {
    View,
    Text,
    Button,
    ScrollView,
    TouchableOpacity,
    ActivityIndicator,FlatList
} from 'react-native';
import FormModal from '../../../../../components/FormModal';
//import Icon from 'react-native-vector-icons/Ionicons';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
//Redux
import payrollreportForm from './forms';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as payslipActions from '../data/payslip/actions';
import CustomCard, {SimpleCard} from '../../../../../components/CustomCards';
//Children Components
import CustomPicker from '../../../../../components/CustomPicker';
import GenericContainer from '../../../../../components/GenericContainer';
import * as oHelper from '../../../../../helper';
import PayslipForm from './payslipForm';
//Styles
import styles from './styles';


export class EmployeePayslipForm extends Component {
    constructor(props){
        super(props);
        this.state = {
            _bShowPicker: false,
            _bHasError: false,
            _bDidMount: false,
            _bShowPayslip: false,
            msgBox: {
                show: false,
                type: '',
                msg: '',
                param: ''
            },
            refreshing: false,
            loadingScreen: {
                show: false,
                msg: 'test'
            },
            activedata:{
                id: '',
                name: '',
                date: '',
                timeout: '',
                overtimeduration: '',
                paydate: '',
                status: '',
                employeeid:'',  
                payrollid:'',              
            },
        }
    }

    componentDidMount(){
        this._getDataFromDB();
    }

    _getDataFromDB = () => {
        const activeRule = this.props.payslip.activeRule;
        this.props.actions.payslip.get(activeRule);
    }

    _onPeriodChange = () => {
        this.setState({
            _bShowPicker: true
        })
    }

    _onSelect = () => {
        this._hidePicker();
    }
    
    _hidePicker = () => {
        this.setState({
            _bShowPicker: false
        })
    }

    _renderItem = (oItem) => {
        const ICONNAME_PENDING = 'alert-circle-outline';
        const ICONNAME_RESOLVED = 'check-circle-outline';
        const itemStyles = styles.reportItemStyles;
        const item = oItem.views;
        console.log('oitemmsssss',oItem);
            const menu = 
                    [
                        { onSelect:() => this._viewpayslip(oItem), label: 'View Payslip' },
                        /*{ onSelect: () => this._modifydtr(oItem,true), label: 'Modify DTR' }*/
                    ]
    
            const backgroundColor = 
                        {backgroundColor: 'rgba(21, 123, 19, 0.86)'}
        
            const oTitle = 
                    <View style={itemStyles.title.contContent}>
                        <View style={itemStyles.title.contIcon}>
                            <View style={itemStyles.title.iconPlaceholder}>
                                <Icon 
                                    name={
                                        item.statusid == 4 ? ICONNAME_RESOLVED : ICONNAME_PENDING
                                    } 
                                    size={30} 
                                    color='#f4f4f4'
                                />
                            </View>
                        </View>
                        <View style={itemStyles.title.contLabel}>
                            <Text style={itemStyles.title.txtLabel}>
                                {/*item.lateduration + ' ' + item.latedurationunit*/item[1][1]}
                            </Text>
                            <Text style={itemStyles.title.txtDescription}>
                                { oHelper.convertDateToString(item.date, 'MMMM DD, YYYY') }
                            </Text>
                            <Text style={itemStyles.title.txtDescription}>
                                { /*item.statusname */ item[2][1]}
                            </Text>
                        </View>
                    </View>
    
            return(
                <SimpleCard 
                            data={[
                                ['Payroll Id',oItem.payrollid],
                                ['Payroll Period', oItem.views[0][1]], 
                                ['Period Details', oItem.views[1][1]], 
                                ['Status',oItem.views[2][1]]
                            ]}
                    customTitleStyle={[itemStyles.title.container, backgroundColor]}
                    oTitle={oTitle}
                    menu={menu}
                />
              );
    }

    _keyExtractor = (item, index) => item. payrollid;

    _viewpayslip=(item,_showForm)=>{
        console.log('itemsssssssss',item);
        this.setState({ _bShowPayslip: true,activedata:{payrollid:item.payrollid,employeeid:this.props.employeeid} });

    }

    _closeForm = () => {
        this.setState({
            _showForm: false
        })
    }

    _onCancel = () => {
        this.setState({
            _bShowPayslip: false
        })
    }
    
    render(){
        const headerStyles = styles.header;
        const navigatorStyles = styles.navigator;
        const contentStyles = styles.content;
        const titleStyles = styles.title;
        const bodyStyles = styles.body;
        const systemStyles = styles.system;
        const textStyles = styles.textStyles;

        const status = this.props.payslip.status;
        const data = this.props.payslip.data;
        const TITLE ='PAYSLIP REPORT';
        const DESCRIPTION='';
        console.log('rennnnnnderrrrr',data);

        const fromStyles = styles.form;
        const summaryStyles = styles.summaryStyles;
        const activedata = this.state.activedata;

        const oPaySlip = (
            <FormModal 
                cancelLabel='Close'
                viewOnly={true}
                containerStyle={fromStyles.container}
                visible={this.state._bShowPayslip}
                onCancel={this._onCancel}
                onOK={this._onSubmit}
                title='PAYROLL SUMMARY'>      

                <View style={styles.container}>
                    <PayslipForm hideHeader={true} activedata={activedata} />
                </View>
                
            </FormModal>
       
        ); 

        return(
            <GenericContainer
                customContainer = {{flex: 1}}
                msgBoxShow = {this.state.msgBox.show}
                msgBoxType = {this.state.msgBox.type}
                msgBoxMsg = {this.state.msgBox.msg}
                msgBoxOnClose = {this._msgBoxOnClose}
                msgBoxOnYes = {this._msgBoxOnYes}
                containerStyle = {[styles.container, this.props.containerStyle || {}]}
                msgBoxParam = {this.state.msgBox.param}
                loadingScreenShow = {this.state.loadingScreen.show}
                loadingScreenMsg = {this.state.loadingScreen.msg}
                status={status}
                title={TITLE}
                onRefresh={this._fetchDataFromDB}>  



            {

                data ?

                    <CustomCard 
                        contentContainerStyle={styles.container}
                        clearMargin
                        title={TITLE} 
                        oType='text'
                        description = {DESCRIPTION}
                    >

                        {
                            data.length < 1 ?
                                <EmptyList message='No Payroll  Record' />
                            :
                                <FlatList
                                    refreshing={this.state.refreshing}
                                    onRefresh={this._fetchDataFromDB}
                                    contentContainerStyle={styles.reportsFlatlist}
                                    ref={(ref) => { this.flatListRef = ref; }}
                                    extraData={data}
                                    data={data.resultsList}
                                    keyExtractor={this._keyExtractor}
                                    renderItem={({item}) => this._renderItem(item) }
                                />
                        }
                        
                    </CustomCard> 
                :
                    null
                }   

                { this.state._bShowPayslip ? oPaySlip : null }
            </GenericContainer>
        );
    }
}

function mapStateToProps (state) {
    //console.log('statesssss',state);
    return {
        payslip: state.reports.payslip,
        employeeid: state.employees.activeProfile.data.id
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            payslip: bindActionCreators(payslipActions, dispatch)
        }
    }
}
  
export default(connect(
    mapStateToProps,
    mapDispatchToProps
)(EmployeePayslipForm))

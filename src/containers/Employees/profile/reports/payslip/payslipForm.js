import React, { Component } from 'react';
import {
    View,
    Text,
    Button,
    ScrollView,
    TouchableOpacity,
    ActivityIndicator,FlatList
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
//import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as payslipActions from '../data/employees/actions';
import CustomCard, {SimpleCard} from '../../../../../components/CustomCards';
//Children Components
import CustomPicker from '../../../../../components/CustomPicker';
import GenericContainer from '../../../../../components/GenericContainer';
import * as oHelper from '../../../../../helper';
import PayrollTransactionEmployees from './employees';
import * as emalApi from '../data/employees/api';
//Styles
import styles from './styles';
import PayrollTransactionEmployeesHeader from './employees/header';
import PayrollTransactionEmployeesFooter from './employees/footer';
import PayrollTransactionEmployeesBody from './employees/body';

export class PayslipForms extends Component {
    constructor(props){
        super(props);
        this.state = {
            _bShowPicker: false,
            _bHasError: false,
            _bDidMount: false,
            msgBox: {
                show: false,
                type: '',
                msg: '',
                param: ''
            },
            refreshing: false,
            loadingScreens: {
                show: false,
                msg: 'test'
            },
        }
    }

    componentDidMount(){
        this._getDataFromDB();
    }

    _getDataFromDB = () => {
    
        const activeData = this.props.activedata;
        //console.log('loggsssssssssss',activeData);
        const payrollid = this.props.activedata.payrollid;
        //const employeeid = this.props.payslip.activeRule.employeeid;
        this.props.actions.payslipform.getemppayslip(activeData);
        //console.log('loggggggggggggggg3456',this.props);

    }

    _onPeriodChange = () => {
        this.setState({
            _bShowPicker: true
        })
    }

    _onSelect = () => {
        this._hidePicker();
    }
    
    _hidePicker = () => {
        this.setState({
            _bShowPicker: false
        })
    }
    
    _setonLoadingScreen = (show, msg) => {
        let oLoadingScreen = {...this.state.loadingScreens};
        oLoadingScreen.show = show;
        oLoadingScreen.msg = msg;
        console.log('loadingggg',oLoadingScreen);
        //this.setState({ loadingScreens: oLoadingScreen });
        this.setState({ loadingScreens: {show:show,msg:msg }});
    }

    _setMessageBox = (show, type, msg, param) => {
        this.setState({
            msgBox: oHelper.setMsgBox(
                this.state.msgBox,
                show, 
                type,
                msg,
                param
            )
        })
    }
    
    _msgBoxOnClose = (params) => {
        this.setState({
            msgBox: oHelper.clearMsgBox(this.state.msgBox)
        })
    }


    _sentToEmail=async(email)=>{
    
        const payrollid=this.props.activedata.payrollid
        const employeeid = this.props.activedata.employeeid
        console.log('looogsssssss',payrollid);
        this._setonLoadingScreen(true, 'Sending...')
 
         await emalApi.getEmailPayslip({id:employeeid,payrollid:payrollid})
         .then((response) => response.json())
         .then((res) => {
             console.log('datasssssss',res);
             this._setMessageBox(true, res.flagno==1 ? 'success' : 'error-ok', res.message);
         })
         .catch((exception) => {          
             this._setMessageBox(true, 'error-ok', exception.message);
         });
         this._setonLoadingScreen(false);
    }

    render(){
        const headerStyles = styles.header;
        const navigatorStyles = styles.navigator;
        const contentStyles = styles.content;
        const titleStyles = styles.title;
        const bodyStyles = styles.body;
        const systemStyles = styles.system;
        const textStyles = styles.textStyles;

        const status = this.props.payslipform.status;
        const data = this.props.payslipform.data;
        const TITLE ='PAYSLIP REPORT';
        const DESCRIPTION='';
        //console.log('rennnnnnderrrrr',data);
        let oView = null;

        if(status[0] == 1 || data){
            oView = 
            <View style={styles.container}>
            {
                this.props.hideHeader || false ?
                    null
                :
                    <View style={navigatorStyles.container}>
                        <View style={navigatorStyles.left}>
                            <Text style={textStyles.cardTitle}>Employee Detailed Payslip</Text>
                        </View>
                        <View style={navigatorStyles.right}>
                            <TouchableOpacity
                                activeOpacity={0.6}
                                onPress={this._onPeriodChange}>
                                <Icon2 name='calendar-clock' size={35} color='#434646'/>
                            </TouchableOpacity>
                        </View>
                    </View>
            }

            <View style={headerStyles.container}>
                <View style={headerStyles.left}>
                    <View style={headerStyles.generalInfoCont}>
                        <View style={headerStyles.iconCont}>
                            <Icon 
                                size={40} 
                                name='md-home' 
                                color='#434646'/>
                        </View>
                        <View style={headerStyles.titleCont}>
                            <Text style={textStyles.companyName}>JCA Realty Corporation</Text>
                            <Text style={textStyles.address}>#80 Yacapin Sts., Cagayan de Oro City, Misamis Oriental 9000</Text>
                        </View>
                    </View>
                </View>

                <View style={headerStyles.right}>
                    {
                        data.info.map((aParamsList, indexParamsList) => 
                            <View key={indexParamsList} style={headerStyles.paramsList}>
                                {
                                    aParamsList.map((aParamsArgs, indexParamsArgs) => 
                                        <View key={indexParamsArgs} style={headerStyles.param}>
                                            <View style={headerStyles.label}>
                                                <Text style={textStyles.label}>{aParamsArgs[0]}</Text>
                                            </View>
                                            <View style={headerStyles.value}>
                                                <Text style={textStyles.value}>{aParamsArgs[1]}</Text>
                                            </View>
                                        </View>
                                    )
                                }
                            </View>
                        )
                    }
                </View>
            </View>

            <View style={contentStyles.container}>
                {   
                    data.data.map((oData, indexData) => 
                        <View key={indexData} style={contentStyles.placeholder}>
                            <View style={titleStyles.container}>
                                <Text style={textStyles.detailsHeader}>{oData.title}</Text>
                            </View>
                            <View style={bodyStyles.container}>
                                <ScrollView>
                                    <View style={bodyStyles.paramsList}>
                                        {
                                            oData.list.map((aList, indexList) => {
                                                switch(aList[0].toLowerCase()){
                                                    case 'systembreak':
                                                        return <View key={indexList} style={systemStyles.break}/>;
                                                        break;
                                                    case 'systemdiv':
                                                        return <View key={indexList} style={systemStyles.div}/>;
                                                        break;
                                                    case 'systemfooter':
                                                        return null;
                                                        break;
                                                    default:
                                                        return(
                                                                <View key={indexData} style={bodyStyles.params}>
                                                                    {
                                                                        aList.map((strParam, indexParams) =>
                                                                            <View 
                                                                                key={indexParams} 
                                                                                style={[bodyStyles.paramsArg, 
                                                                                    indexParams==0 ? bodyStyles.paramsLeftMost :
                                                                                    indexParams==(aList.length-1) ? bodyStyles.paramsRightMost :
                                                                                    bodyStyles.paramsCenter]
                                                                                }
                                                                            >

                                                                                <Text style={textStyles.details}>{strParam}</Text>
                                                                            </View>
                                                                        )
                                                                    }
                                                                </View>
                                                        );
                                                }
                                            })
                                        }
                                        {}
                                    </View>
                                </ScrollView>
                                {
                                    oData.list.map((aList, indexList) => {
                                        if(aList[0].toLowerCase() === 'systemfooter'){
                                            return(
                                                <View key={indexList} 
                                                    style={systemStyles.footer.container}>

                                                    <View style={systemStyles.footer.title}>
                                                        <Text style={textStyles.footerTitle}>
                                                            { aList[1] }
                                                        </Text>
                                                    </View>

                                                    <View style={systemStyles.footer.value}>
                                                        <Text style={textStyles.footerValue}>
                                                            { aList[2] }
                                                        </Text>
                                                    </View>
                                                    
                                                </View>
                                            )
                                        }
                                        else{
                                            return null;
                                        }
                                    })
                                }
                            </View>


                        </View>                     
                    )
                }           
            </View>
              
            {
                this.state._bShowPicker ? 
                    <CustomPicker 
                        list={oAllData.payrollperiods}
                        dateformat={oAllData.datedisplayformat}
                        emptyprompt = 'Error: No data found'
                        title='SELECT PAYROLL PERIOD'
                        onSelect={this._onSelect}
                        visible={this.state._bShowPicker}
                        onClose={this._hidePicker}/>
                :
                    null
            }

            <View style={styles.payrollSummary.buttonsummary}>
                <View>
                    <TouchableOpacity onPress={this._sentToEmail}>
                        <Text style={textStyles.companyname}>Send to Email</Text>          
                    </TouchableOpacity>                                       
                </View>
            </View> 

        </View>   
                             
        }
       
        return(
            <GenericContainer
            
                loadingScreenShow = {this.state.loadingScreens.show}
                loadingScreenMsg = {this.state.loadingScreens.msg}
                msgBoxShow = {this.state.msgBox.show}
                msgBoxType = {this.state.msgBox.type}
                msgBoxMsg = {this.state.msgBox.msg}
                msgBoxOnClose = {this._msgBoxOnClose}
                msgBoxOnYes = {this._msgBoxOnYes}
            
                status={status}
                title={ 'PAYSLIP' }>

                {oView}
                
            </GenericContainer>

            /*<PayrollTransactionEmployees 
                data={data}
                status={status}
                //onRefresh={this._getDataFromDB}
            />*/

        );
    }
}

function mapStateToProps (state) {
    console.log('stateeeeesssssssss',state.employeepayslip.employees)
    return {
        payslipform: state.employeepayslip.employees
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            payslipform: bindActionCreators(payslipActions, dispatch)
        }
    }
}
  
export default(connect(
    mapStateToProps,
    mapDispatchToProps
)(PayslipForms))

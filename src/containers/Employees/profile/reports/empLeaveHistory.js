//Packages
import React, { Component } from 'react';
//Children Components
import LeavesReport from '../../../Reports/leaves';
//Redux
import { connect } from 'react-redux';
//Constants
const FILTER = 'EMPLOYEE';

export class EmpLeaveHistory extends Component {
    render(){
        const activeProfile = this.props.employees.activeProfile.data;
        return(
            <LeavesReport
                filter = {FILTER}
                empid = {activeProfile.id}
            />
        );
    }
}

function mapStateToProps (state) {
    return {
        employees: state.employees
    }
}

export default connect(
    mapStateToProps,
)(EmpLeaveHistory)
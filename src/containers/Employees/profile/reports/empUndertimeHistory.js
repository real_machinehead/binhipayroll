//Packages
import React, { Component } from 'react';
//Children Components
import UndertimeReport from '../../../Reports/undertime';
//Redux
import { connect } from 'react-redux';
//Constants
const FILTER = 'EMPLOYEE';

export class EmpUndertimeHistory extends Component {
    render(){
        const activeProfile = this.props.employees.activeProfile.data;
        return(
            <UndertimeReport
                filter = {FILTER}
                empid = {activeProfile.id}
            />
        );
    }
}

function mapStateToProps (state) {
    return {
        employees: state.employees
    }
}

export default connect(
    mapStateToProps,
)(EmpUndertimeHistory)
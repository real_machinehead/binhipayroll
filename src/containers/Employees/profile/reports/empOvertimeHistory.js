//Packages
import React, { Component } from 'react';
//Children Components
import OvertimeReport from '../../../Reports/overtime';
//Redux
import { connect } from 'react-redux';
//Constants
const FILTER = 'EMPLOYEE';

export class EmpOvertimeHistory extends Component {
    render(){
        const activeProfile = this.props.employees.activeProfile.data;
        return(
            <OvertimeReport
                filter = {FILTER}
                empid = {activeProfile.id}
            />
        );
    }
}

function mapStateToProps (state) {
    return {
        employees: state.employees
    }
}

export default connect(
    mapStateToProps,
)(EmpOvertimeHistory)
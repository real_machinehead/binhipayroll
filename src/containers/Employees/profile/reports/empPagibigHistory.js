//Packages
import React, { Component } from 'react';
//Children Components
import PagibigReport from '../../../Reports/pagibig';
//Redux
import { connect } from 'react-redux';
//Constants
const FILTER = 'EMPLOYEE';

export class EmpPagibigHistory extends Component {
    render(){
        const activeProfile = this.props.employees.activeProfile.data;
        return(
            <PagibigReport
                filter = {FILTER}
                empid = {activeProfile.id}
            />
        );
    }
}

function mapStateToProps (state) {
    return {
        employees: state.employees
    }
}

export default connect(
    mapStateToProps,
)(EmpPagibigHistory)
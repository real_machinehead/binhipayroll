//Packages
import React, { Component } from 'react';
//Children Components
//import PhilhealthReport from '../../../Reports/philhealth';
import PhilhealthReport from './empphilhealthreport';
//Redux
import { connect } from 'react-redux';
//Constants
const FILTER = 'EMPLOYEE';

export class EmpPhilhealthHistory extends Component {
    render(){
        const activeProfile = this.props.employees.activeProfile.data;
        return(
            <PhilhealthReport
                filter = {FILTER}
                empid = {activeProfile.id}
            />
        );
    }
}

function mapStateToProps (state) {
    return {
        employees: state.employees
    }
}

export default connect(
    mapStateToProps,
)(EmpPhilhealthHistory)
import React, { Component, PureComponent } from 'react';
import {
    View,
    Text
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

//Styles
import { SimpleCard } from '../../../../../components/CustomCards';
import styles from './styles';

//helper
import * as oHelper from '../../../../../helper';

const ICONNAME_PENDING = 'alert-circle-outline';
const ICONNAME_RESOLVED = 'check-circle-outline';

export default class DtrModificationReportItem extends PureComponent{

    _onConfirm = () => {
        this.props.onConfirm(this.props.item);
    }

    _onApprove = () => {
        alert('I AM APPROVED');
    }

    render(){
        console.log('Items',this.props.item.statusname);
        const item = this.props.item;
        const itemStyles = styles.reportItemStyles;
        const backgroundColor = 
            item.statusid == 4 ? 
                {backgroundColor: 'rgba(21, 123, 19, 0.86)'}
            :
                {backgroundColor: 'rgba(193, 66, 66, 0.94)'}

        const menu = 
            item.statusid == 4 ?
                []
            :
                [/* { onSelect: this._onConfirm, label: 'Confirm' } */]

        const oTitle = 
            <View style={itemStyles.title.contContent}>
                <View style={itemStyles.title.contIcon}>
                    <View style={itemStyles.title.iconPlaceholder}>
                        <Icon 
                            name={
                                item.statusid == 4 ? ICONNAME_RESOLVED : ICONNAME_PENDING
                            } 
                            size={30} 
                            color='#f4f4f4'
                        />
                    </View>
                </View>
                <View style={itemStyles.title.contLabel}>
                    <Text style={itemStyles.title.txtLabel}>
                        {item.overtimeduration + ' hrs'}
                    </Text>
                    <Text style={itemStyles.title.txtDescription}>
                        { oHelper.convertDateToString(item.date, 'MMMM DD, YYYY') }
                    </Text>
                    <Text style={itemStyles.title.txtDescription}>
                        { item.statusname }
                    </Text>
                </View>
            </View>

        return (
            <SimpleCard
                data={item.views}
                customTitleStyle={[itemStyles.title.container, backgroundColor]}
                oTitle={oTitle}
                menu={menu}
            />
        )
    }
}
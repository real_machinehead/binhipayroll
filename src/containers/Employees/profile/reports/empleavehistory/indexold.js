//Packages
import React, { Component } from 'react';
import {
    View,
    Text,
    Button,
    FlatList,
    TouchableNativeFeedback,
    TextInput,
    ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import LinearGradient from 'react-native-linear-gradient';

//Styles Properties
import styles from './styles';

//Custom Components
import * as StatusLoader from '../../../../../components/ScreenLoadStatus'
import CustomCard, 
{
    Description,
    PropTitle,
    PropLevel1, 
    PropLevel2
}
from '../../../../../components/CustomCards';

//Helper
import * as oHelper from '../../../../../helper';

//Redux
import { connect } from 'react-redux';
//import * as employeeActions from '../../../data/activeProfile/actions';
import { bindActionCreators } from 'redux';

import PaginatedList from '../../../../../components/PaginatedList';
import empLeaveHistoryItem from './items';

import * as empLeaveHistoryAction from '../data/empleavehistory/actions';


//Constants
const btnActive = 'rgba(255, 255, 255, 0.3);'
const btnInactive = 'transparent';
const TITLE = 'Employee Leave History'

export class EmpLeaveHistory extends Component {

    _generatePayload = (page) => ({
        compid: this.props.activecompany.id,
        page: page,
        filter: 'COMPANY',
    })

    _onMount = () => 
        this.props.actions.EmpLeaveHistory.get(
            this._generatePayload(1)
        );

    _onRefresh = async() => 
        await this.props.actions.EmpLeaveHistory.refresh(
            this._generatePayload(1)
        );

    _onLoadMore = async(page) => 
        await this.props.actions.EmpLeaveHistory.loadMore(
            this._generatePayload(page)
        );
    
    //Local Functions
    _renderItem = item => 
        <DtrModificationReportItem
            item={item} 
            onConfirm={this._onConfirm}
        />;

    render(){
        const {status, data} = this.props.EmpLeaveHistory;
        const hideHeader = this.props.hideHeader || false;
        //console.log('stats',this.props);
        return(
            <PaginatedList
                hideHeader={hideHeader} 
                title={TITLE}
                status={status}
                data={data}
                onMount={this._onMount}
                onRefresh={this._onRefresh}
                onLoadMore={this._onLoadMore}
                listName='resultLists'
                renderItem={this._renderItem}
                ref={(ref) => { this.oCompList = ref; }}
                keyExtractorName='payrollid'
            />
        );
    }
}

function mapStateToProps (state) {
    return {
        empleavehistory: state.reports.dtrmodification,
        activecompany: state.activeCompanyReducer.activecompany
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            empleavehistory: bindActionCreators(empLeaveHistoryAction, dispatch),
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EmpLeaveHistory)
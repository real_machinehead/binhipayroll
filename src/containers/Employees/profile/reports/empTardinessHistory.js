//Packages
import React, { Component } from 'react';
//Children Components
import TardinessReport from '../../../Reports/tardiness';
//Redux
import { connect } from 'react-redux';
//Constants
const FILTER = 'EMPLOYEE';

export class EmpTardinessHistory extends Component {
    render(){
        const activeProfile = this.props.employees.activeProfile.data;
        return(
            <TardinessReport
                filter = {FILTER}
                empid = {activeProfile.id}
            />
        );
    }
}

function mapStateToProps (state) {
    return {
        employees: state.employees
    }
}

export default connect(
    mapStateToProps,
)(EmpTardinessHistory)
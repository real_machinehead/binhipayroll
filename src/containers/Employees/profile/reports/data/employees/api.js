import { fetchApi, mockFetch } from '../../../../../../services/api';
import * as endPoints from '../../../../../../global/endpoints';
import * as blackOps from '../../../../../../global/blackOps';

export let get = payload => fetchApi(endPoints.transactions.payroll.employees.get(payload), payload, 'get');
export let regenerate = payload => fetchApi(endPoints.transactions.payroll.employees.regenerate(payload), payload, 'post');
export let close = payload => fetchApi(endPoints.transactions.payroll.employees.close(payload), payload, 'post');

export let getPayslip = payload => fetchApi(endPoints.reports.payslip.get(payload), payload, 'get');

export let getEmailPayslip = payload => fetchApi(endPoints.reports.emailpayslip.get(payload), payload, 'get');


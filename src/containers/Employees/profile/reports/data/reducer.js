import { combineReducers } from 'redux';
import { reducer as payrollTransactionEmployeesReducer } from './employees/reducer';
import { reducer as payrollTransactionSummaryReducer } from './summary/reducer';
import { reducer as payrollTransactionAllReducer } from './all/reducer';
import { reducer as PayrollTransactionPayslipReducer } from './payslip/reducer';

export const reducer = combineReducers({
    summary: payrollTransactionSummaryReducer,
    employees: payrollTransactionEmployeesReducer,
    all: payrollTransactionAllReducer,
    payslip:PayrollTransactionPayslipReducer,
});
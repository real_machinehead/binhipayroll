import { fetchApi, mockFetch } from '../../../../../../services/api';
import * as endPoints from'../../../../../../global/endpoints';
import * as blackOps from '../../../../../../global/blackOps';

//export let get = payload => fetchApi(endPoints.reports.payslip.get(payload), payload, 'get');

export let get = payload => fetchApi(endPoints.reports.payrollReports.get(payload), payload, 'get');
export let getPayslip = payload => fetchApi(endPoints.reports.payslip.get(payload), payload, 'get');

export const INITIALIZE = 'reports/pagibigreport/INITIALIZE';
export const UPDATE = 'reports/pagibigreport/UPDATE';
export const STATUS = 'reports/pagibigreport/STATUS';
export const RESET = 'reports/pagibigreport/RESET';
import { fetchApi, mockFetch } from '../../../../../../services/api';
import * as endPoints from'../../../../../../global/endpoints';
import * as blackOps from '../../../../../../global/blackOps';

export let get = payload => fetchApi(endPoints.reports.dtrmodification.get(payload), payload, 'post');
export let cancel = payload => fetchApi(endPoints.reports.dtrmodification.cancel(payload), payload, 'post');
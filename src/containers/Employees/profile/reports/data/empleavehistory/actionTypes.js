export const INITIALIZE = 'profile/dtrModification/INITIALIZE';
export const UPDATE = 'profile/dtrModification/UPDATE';
export const STATUS = 'profile/dtrModification/STATUS';
export const RESET = 'profile/dtrModification/RESET';
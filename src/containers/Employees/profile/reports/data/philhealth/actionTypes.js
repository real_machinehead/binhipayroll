export const INITIALIZE = 'reports/philhealthreport/INITIALIZE';
export const UPDATE = 'reports/philhealthreport/UPDATE';
export const STATUS = 'reports/philhealthreport/STATUS';
export const RESET = 'reports/philhealthreport/RESET';
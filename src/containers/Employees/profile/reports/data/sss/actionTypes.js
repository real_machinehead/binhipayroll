export const INITIALIZE = 'reports/sssreport/INITIALIZE';
export const UPDATE = 'reports/sssreport/UPDATE';
export const STATUS = 'reports/sssreport/STATUS';
export const RESET = 'reports/sssreport/RESET';
//Packages
import React, { Component } from 'react';
import {
    View,
    Text,
    Button,
    FlatList,
    TouchableNativeFeedback,
    TextInput,
    ScrollView,
    TouchableOpacity,
    Alert
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import LinearGradient from 'react-native-linear-gradient';

//Styles Properties
import styles from '../styles';

//Custom Components
import * as StatusLoader from '../../../../components/ScreenLoadStatus'
import CustomCard from '../../../../components/CustomCards';
import FixedCard1 from '../../../../components/FixedCards';
import FormModal from '../../../../components/FormModal';

//Children Components
import EmployeeBankAccount from '../../addEmployeeForm/bankinfo/bankaccount';

//Helper
import * as oHelper from '../../../../helper';

//Redux
import { connect } from 'react-redux';
import * as employeeActions from '../../data/activeProfile/actions';
import { bindActionCreators } from 'redux';

//Constants
const btnActive = 'rgba(255, 255, 255, 0.3);'
const btnInactive = 'transparent';
const TITLE = 'Personal Bank Account Information'

export class EmpBankAccount extends Component {
    constructor(props){
        super(props);
        this.state = {
            _bShowForm: false,
        }
    }

    _editData = () => {
        this.setState({ _bShowForm: true });
    }

    _hideForm = () => {
        this.setState({ _bShowForm: false });
    }

    render(){
        const viewMode = this.props.employees.activeProfile.viewMode;
        const oBankInfo =  this.props.employees.activeProfile.data.bankinfo;
        const navigation = this.props.logininfo.navigation;
        const attribs_BankAccount = 
            [
                {
                    label: 'BANK NAME',
                    value: oBankInfo.bankname || '-'
                },
                {
                    label: 'ACCOUNT NUMBER',
                    value: oBankInfo.accountnumber || '-'
                }
            ]

        return(
            <View style={styles.child.container}>
                <View style={styles.child.contCard}>
                    <CustomCard 
                        clearMargin={true} 
                        title={TITLE} 
                        oType='Button'
                        rightHeader={
                            viewMode ? 
                                null 
                            :
                                <TouchableOpacity
                                    style={styles.child.contBtn}
                                    onPress={this._editData}>
                                    <Icon name='ios-create-outline' size={40} color='#000000'/>
                                </TouchableOpacity>
                        }>
                        <ScrollView>
                            <View style={styles.child.contContent}>

                                <FixedCard1
                                    hideActionIcon={true}
                                    title={oBankInfo.title || 'BANK ACCOUNT INFORMATION'}
                                    attributes={attribs_BankAccount}/>

                            </View>
                        </ScrollView>
                        
                    </CustomCard>
                </View>
                {
                    this.state._bShowForm ? 
                        <EmployeeBankAccount
                            onHideForm={this._hideForm}
                            editMode
                        />
                    :
                        null
                }
            </View>
        );
    }
}

function mapStateToProps (state) {
    return {
        logininfo: state.loginReducer.logininfo,
        employees: state.employees
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            employee: bindActionCreators(employeeActions, dispatch),
        },
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EmpBankAccount)
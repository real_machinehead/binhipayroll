//Packages
import React, { Component, PureComponent } from 'react';
import {
    View,
    Text,
    Button,
    FlatList,
    TouchableNativeFeedback,
    TextInput,
    ScrollView,
    Alert,
    ToastAndroid,
    BackHandler
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import LinearGradient from 'react-native-linear-gradient';
import { NavigationActions } from 'react-navigation';
import * as session from '../../../services/session';

//Styles Properties
import styles from './styles';

//Custom Components
import CustomCard, 
{
    Description,
    PropTitle,
    PropLevel1, 
    PropLevel2
}
from '../../../components/CustomCards';
import Header1 from '../../Headers/header1';
import GenericContainer from '../../../components/GenericContainer';
import * as PromptScreen from '../../../components/ScreenLoadStatus';

//Children Components
import EmpBasicInfo from './personalInfo/empBasicInfo';
import EmpAddressInfo from './personalInfo/empAddressInfo';
import EmpFamily from './personalInfo/empFamily';
import EmpBankAccount from './bankInfo/empBankAccount';
import LoadingScreen from '../../../components/LoadingScreen';
import EmpBenefits from './employmentInfo/empBenefits';
import EmpBonus from './employmentInfo/empBonus';
import EmpDetails from './employmentInfo/empDetails';
import EmpPayroll from './employmentInfo/empPayroll';
import EmpTax from './employmentInfo/empTax';
import EmpRank from './employmentInfo/empRank';
import EmpWorkshift from './employmentInfo/empWorkshift';
/* import EmpLeaves from './employmentInfo/empLeaves';
import EmpOvertime from './employmentInfo/empOvertime'; */
/* import EmpUndertime from './employmentInfo/empUndertime'; */
/* import EmpTardiness from './employmentInfo/empTardiness'; */

import EmpBenefitsReport from './reports/empBenefitsReport';
import EmpDTR from './reports/empDTR';
import EmpLeaveHistory from './reports/empLeaveHistory';
import EmpPaySlip from './reports/empPaySlip';
import EmpTimeDisputes from './reports/empTimeDisputes';
import EmpTardinessHistory from './reports/empTardinessHistory';
import EmpOvertimeHistory from './reports/empOvertimeHistory';
import EmpUndertimeHistory from './reports/empUndertimeHistory';
import EmpSssHistory from './reports/empSssHistory';
import EmpPhilhealthHistory from './reports/empPhilhealthHistory';
import EmpPagibigHistory from './reports/empPagibigHistory';
//import EmpMonetaryAdjustmentHistory from './reports/empMonetaryAdjustmentHistory';

//Helper
import * as oHelper from '../../../helper';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as employeeActions from '../data/activeProfile/actions';

//constants
import { CONSTANTS } from '../../../constants/index';
const btnActive = 'rgba(255, 255, 255, 0.3);'
const btnInactive = 'transparent';
const TITLE = 'Employee Profile Summary'

export class ProfileMenu extends Component{
    shouldComponentUpdate(nextProps, nextState){
        if(
            (this.props.activeItem.key === this.props.item.key) ||
            (nextProps.activeItem.key === this.props.item.key)
        ){
            /* console.log('this.props.item.name: ' + this.props.item.name); */
            return true;
        }
        else{
            return false;
        }
    }

    render(){
        let item = this.props.item;
        return(
            <TouchableNativeFeedback 
                onPress={() => this.props.itemPressed(item)}
                background={TouchableNativeFeedback.SelectableBackground()}>
                <View>
                    {
                        item.type.toUpperCase() == 'CONTENT' ?
                        <View style={[styles.btnCont, this.props.activeItem.key == item.key ? styles.btnContActive : {}]}>
                                <View style={styles.iconCont}>
                                    <View style={styles.iconPlaceholder}>
                                        <Icon name={item.icon} size={16} color='#fff'/>
                                    </View>
                                </View>
                                <View style={styles.labelCont}>
                                    <Text style={styles.txtLabel}>{item.name}</Text>
                                </View>
                            </View>
                        :
                            <View style={styles.titleCont}>
                                <View style={styles.contContentTitle}>
                                    <Text style={styles.txtLabelTitle}>{item.name}</Text>
                                </View>
                            </View>
                    }
                </View>
            </TouchableNativeFeedback>
        )
    }
}

export class Profile extends Component {
    constructor(props){
        super(props);
        this.state = {
            splashScreen: {
                show: false,
                message: '',
            },

            _viewMode: false,
            _status: CONSTANTS.STATUS.LOADING,
            _refreshing: false,
            _list: [
                {
                    key: "0001",
                    name: "PERSONAL INFORMATION",
                    type: "title"

                },
                {
                    key: "0002",
                    name: "Basic & Contact Info",
                    icon: "information-variant",
                    type: "content",
                    oComponent: <EmpBasicInfo/>
                },
                {
                    key: "0003",
                    name: "Address Info",
                    icon: "map-marker",
                    type: "content",
                    oComponent: <EmpAddressInfo/>
                },
                {
                    key: "0004",
                    name: "Family & Dependents",
                    icon: "account-multiple",
                    type: "content",
                    oComponent: <EmpFamily/>
                },
                /* {
                    key: "0005",
                    name: "Work and Education",
                    icon: "briefcase",
                    type: "content",
                    oComponent: <BasicInfo/>
                }, */
                {
                    key: "0006",
                    name: "BANK INFORMATION",
                    type: "title"
                },
                {
                    key: "0007",
                    name: "Bank Account Info",
                    icon: "bank",
                    type: "content",
                    oComponent: <EmpBankAccount/>
                },
                {
                    key: "0008",
                    name: "EMPLOYMENT INFORMATION",
                    type: "title"
                },
                {
                    key: "0009",
                    name: "Employment Details",
                    icon: "file-document",
                    type: "content",
                    oComponent: <EmpDetails/>
                },
                {
                    key: "0010",
                    name: "Work Shift",
                    icon: "calendar-clock",
                    type: "content",
                    oComponent: <EmpWorkshift/>
                },
                {
                    key: "0011",
                    name: "Payroll Schedulesss",
                    icon: "credit-card",
                    type: "content",
                    oComponent: <EmpPayroll/>
                },
                /* {
                    key: "0012",
                    name: "Tardiness Policy",
                    icon: "clock-alert",
                    type: "content",
                    oComponent: <EmpTardiness/>
                }, */
                /* {
                    key: "0013",
                    name: "Undertime Policy",
                    icon: "timelapse",
                    type: "content",
                    oComponent: <EmpUndertime/>
                }, */
                /* {
                    key: "0014",
                    name: "Overtime Policy",
                    icon: "clock-fast",
                    type: "content",
                    oComponent: <EmpOvertime/>
                }, */
                /* {
                    key: "0015",
                    name: "Leaves",
                    icon: "timer-off",
                    type: "content",
                    oComponent: <EmpLeaves/>
                }, */
                {
                    key: "0015",
                    name: "Rank",
                    icon: "account-star",
                    type: "content",
                    oComponent: <EmpRank/>
                },
                {
                    key: "0016",
                    name: "Benefits",
                    icon: "format-list-numbers",
                    type: "content",
                    oComponent: <EmpBenefits/>
                },
                {
                    key: "0017",
                    name: "Tax",
                    icon: "calculator",
                    type: "content",
                    oComponent: <EmpTax/>
                },
                {
                    key: "0018",
                    name: "13th Month Pay",
                    icon: "account-star",
                    type: "content",
                    oComponent: <EmpBonus/>
                },
                {
                    key: "0019",
                    name: "PERSONAL REPORTS",
                    type: "title"
                },
                {
                    key: "0020",
                    name: "Daily Time Record",
                    icon: "timetable",
                    type: "content",
                    oComponent: <EmpDTR/>
                },
                {
                    key: "0021",
                    name: "Leave History",
                    icon: "file-excel",
                    type: "content",
                    oComponent: <EmpLeaveHistory/>
                },
                {
                    key: "0022",
                    name: "Tardiness History",
                    icon: "file-delimited",
                    type: "content",
                    oComponent: <EmpTardinessHistory/>
                },
                {
                    key: "0023",
                    name: "Overtime History",
                    icon: "file-delimited",
                    type: "content",
                    oComponent: <EmpOvertimeHistory/>
                },
                {
                    key: "0024",
                    name: "Under Time History",
                    icon: "file-delimited",
                    type: "content",
                    oComponent: <EmpUndertimeHistory/>
                },               
                /*{
                    key: "0025",
                    name: "Benefits",
                    icon: "file-send",
                    type: "content",
                    oComponent: <EmpBenefitsReport/>
                },*/
                {
                    key: "0025",
                    name: "SSS Contribution",
                    icon: "file-send",
                    type: "content",
                    oComponent: <EmpSssHistory />
                },  
                {
                    key: "0026",
                    name: "Philhealth Contribution",
                    icon: "file-send",
                    type: "content",
                    oComponent: <EmpPhilhealthHistory />
                },  
                {
                    key: "0027",
                    name: "Pag-ibig Contribution",
                    icon: "file-send",
                    type: "content",
                    oComponent: <EmpPagibigHistory />
                },                              
                {
                    key: "0028",
                    name: "Pay Slips",
                    icon: "file-account",
                    type: "content",
                    oComponent: <EmpPaySlip/>
                },
                {
                    key: "9999",
                    name: "Logout",
                    icon: "logout-variant",
                    type: "content",
                    oComponent: null
                }
            ],
            _activeItem: {
                key: "0002",
                name: "Basic & Contact Info",
                icon: "information-variant",
                type: "content",
                oComponent: <EmpBasicInfo/>
            },

        }
    }
    
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }
    

    static navigationOptions = ({navigation}) => {
        const params = navigation.state.params;
        let strLockMode = 'unlocked';
        try{
            if(params.logininfo != undefined){
                switch(params.logininfo.usertype.toUpperCase()){
                    case 'EMPLOYEE':
                        strLockMode = 'locked-closed';
                        bHideDrawerBtn = true;
                        bHideBackBtn = true;
                        return {
                            header : null,
                            drawerLockMode: 'locked-closed'
                        };
                        break;
                    default:
                        return {
                            header : 
                                <Header1 title = 'MY EMPLOYEES' />
                        }
                        break;
                }
            }
        }catch(Exception){
            return {
                header : <Header1 title = 'MY EMPLOYEES' />
            }
        } 
    }

    /* static navigationOptions = ({navigation}) => {
        const params = navigation.state.params;
        let strLockMode = 'unlocked';
        try{
            if(params.logininfo != undefined){
                switch(params.logininfo.usertype.toUpperCase()){
                    default:
                        return {
                            header : 
                                <Header1 title = 'MY EMPLOYEES' />
                        }
                        break;
                }
            }
        }catch(Exception){
            return {
                header : <Header1 title = 'MY EMPLOYEES' />
            }
        } 
    } */

    handleBackPress = () => {
        ToastAndroid.show('Back Button is disabled for employees. Press Logout to Exit', ToastAndroid.BOTTOM);
        return true;
    }

    componentDidMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        this._initComponent();
    }

    _initComponent = async() => {
        if(this.props.logininfo.usertype.toUpperCase() == 'EMPLOYEE'){
            const {setParams} = this.props.navigation;
            await setParams({logininfo: this.props.logininfo});
            this._getDataFromDB();
            this.props.actions.employee.setViewMode(true);
        }else{
            this.props.actions.employee.setViewMode(false);
            this.props.actions.employee.updateAllInfoStatus(['1', 'Success!']);
        }
    }

    _getDataFromDB = () => {
        const aStatus = this.props.activeProfile.status;
        const oLoginInfo = this.props.logininfo;
        const strUserType = oLoginInfo.usertype.toUpperCase();
        console.log('aStatus: ' + JSON.stringify(aStatus));
        console.log('strUserType: ' + strUserType);
        if(aStatus[0] != 1 && strUserType=='EMPLOYEE'){
            const empId = oLoginInfo.entityid;
            this.props.actions.employee.getAllInfo({employeeid: empId});
        }
    }

    _setActiveChild = async(oItem) => {
        
        if(oItem.key == '9999'){
            Alert.alert(
                'Confirmation',
                'Are you sure you want to logout ?',
                [
                    {text: 'NO', onPress: () => {}},
                    {text: 'YES', onPress: () => {this._logout()}},
                ],
                { cancelable: false }
            )
        }else{
            await this._setInitialActiveComponent(oItem)
            requestAnimationFrame(() => {
                let oActiveItem = {...this.state._activeItem};
                oActiveItem.key=oItem.key;
                oActiveItem.name=oItem.name;
                oActiveItem.icon=oItem.Icon;
                oActiveItem.type=oItem.type;
                oActiveItem.oComponent=oItem.oComponent;
                this.setState({ _activeItem: oActiveItem })
            })
        }
        
    }

    _logout = async() => {
        this._setSplashScreen(true, 'Logging out...');
        await session.revoke()
        .then(async(res)=>{
            if(res.flagno == 1){
                    await this._setSplashScreen(false);
                    this.props.navigation.navigate('Login');
                    ToastAndroid.show('You have been logged out successfully', ToastAndroid.BOTTOM);
            }else{
                Alert.alert(
                    'Logout Failed.',
                    'Unable to logout. Please try again.',
                    [
                        {text: 'OK', onPress: () => {}}
                    ],
                    { cancelable: false }
                )
                this._setSplashScreen(false);
            }
        }).catch((exception)=> {
            console.log(exception);
        });
    }

    _setSplashScreen = (show, message='') => 
        this.setState({ splashScreen: {show: show, message: message }});


    _setInitialActiveComponent = (oItem) => {
        let oInitialItem = {...this.state._activeItem};
        oInitialItem.key=oItem.key;
        oInitialItem.oComponent = <PromptScreen.PromptLoading title={''}/>;
        this.setState({ _activeItem: oInitialItem })
    }

/*     _setActiveChild = (oItem) => {
        if(this.state._activeChild != oItem.key){
            this._setButtons(oItem); //immediately trigg
            requestAnimationFrame(() => {
                this._setChildComponent(oItem);
                this.flatListRef.scrollToIndex({animated: true, index: Number(oItem.key)-1});
            })
        }
    } */

    render(){
        const oData = this.props.activeProfile.data;
        const status = this.props.activeProfile.status;
        const aStatus = oData ? status : CONSTANTS.STATUS.LOADING;
        let oProfile = null;
        let oListHeader = null;
        let strName = '';

        if(aStatus[0]==1){
            
            strName = oData.personalinfo == undefined ? '' : oData.personalinfo.basicinfo.lastname + ', ' +
                oData.personalinfo.basicinfo.firstname;
            oListHeader = (
                <View style={styles.contSearch}>
                    <View style={styles.contSearchBox}>
                        <Icon name='magnify' size={22} color='#000' style={styles.iconSearch}/>
                        <TextInput 
                            style={styles.textinputField}
                            placeholder='Search'
                            onChangeText={txtInput => {}}
                            value={''}
                            ref='_ref_emp_search'
                            returnKeyType="search"
                            underlineColorAndroid='transparent'
                        />
                    </View>
                </View>
            )
        }

        const navigation = this.props.logininfo.navigation;
        return(
            <GenericContainer status={aStatus}>
                <View style={styles.container}>
                    <LinearGradient 
                        colors={['#818489', '#3f4144', '#202626']}
                        style={styles.leftCont}>
                        
                        <View style={styles.contTitle}>

                            <View style={styles.contIconProfile}>
                                {/* <View style={{width: 65, height: 65, backgroundColor: 'red', borderWidth: 1, borderColor: '#EEB843', borderRadius: 100, justifyContent: 'center', alignItems: 'center'}}> */}
                                    <Icon name='account-circle' size={67} color='#fff'/>
                                {/* </View> */}
                            </View>

                            <View style={styles.contInfoProfile}>
                                <Text style={styles.txtProfileTitle}>
                                    { strName }
                                </Text>
                                <Text style={styles.txtProfileLabel}>
                                    {this.props.activecompany.name}
                                </Text>
                                <Text style={styles.txtProfileLabel}>
                                    Auditor
                                </Text>
                                <Text style={styles.txtProfileLabel}>
                                    Yacapin Branch
                                </Text>
                            </View>

                        </View>

                        <View style={styles.optionsCont}>
                            <FlatList
                                /* getItemLayout={this._getItemLayout} */
                                ListHeaderComonent={oListHeader}
                                ref={(ref) => { this.flatListRef = ref; }}
                                data={this.state._list}
                                renderItem={({item}) => 
                                    <ProfileMenu
                                        activeItem={this.state._activeItem}
                                        item={item} 
                                        itemPressed={(pressedItem) => this._setActiveChild(pressedItem)}/>
                                }
                            />
                        </View>
                    </LinearGradient>
                        
                    <View style={styles.rightCont}>
                        {
                            this.state._activeItem.oComponent
                        }
                    </View>
                </View>
                {
                    this.state.splashScreen.show ? 
                        <LoadingScreen 
                            show = {this.state.splashScreen.show}
                            message = {this.state.splashScreen.message}
                        />
                    :
                        null
                }
            </GenericContainer>
        );
    }
}


function mapStateToProps (state) {
    return {
        logininfo: state.loginReducer.logininfo,
        activecompany: state.activeCompanyReducer.activecompany,
        employees: state.employees,
        activeProfile: state.employees.activeProfile
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            employee: bindActionCreators(employeeActions, dispatch),
        },
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Profile)
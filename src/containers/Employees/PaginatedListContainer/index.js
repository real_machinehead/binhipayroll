import React, { Component } from 'react';
import {
    View,
    Text,
    FlatList,
    ActivityIndicator,
    Alert
} from 'react-native';

//Children Components
import GenericContainer from '../../../components/GenericContainer';
import SeeMoreButton from '../../../components/SeeMoreButton';
import DefaultStyledHeader from '../../../components/StyledHeader';
import CustomLoadingBar from '../../../components/CustomLoadingBar';
import CustomBar from '../../../components/CustomBar';
import CollapsibleView from '../../../components/CollapsibleView';
import EmptyList from '../../../components/EmptyList';

//Styles
import styles from '../styles';

//Helper
import * as oHelper from '../../../helper';

class PaginatedListContainer extends Component {
    constructor(props){
        super(props);
        this.state = {
            refreshing: false,
            loadingScreen: {
                show: false,
                msg: 'test'
            },
            msgBox: {
                show: false,
                type: '',
                msg: '',
                param: ''
            },
        }
    }

    componentDidMount() {
        this._onMount();
    }

    _onMount = () => {
        this.props.onMount();
    }

    _onPullRefresh = () => {
        this.props.onPullRefresh();
    }

    _onSeeMore = () => {
        this.props.onSeeMore();
    }

    _keyExtractor = (item, index) => {
        const keyExtractorExt = this.props.keyExtractorExt || null;
        const keyExtractorName = this.props.keyExtractorName || 'id';
        if(this.props.keyExtractorExt){
            return item[keyExtractorName] + item[keyExtractorExt];
        }else{
            return item[keyExtractorName];
        } 
    }

    _onRenderItem = (item) => {
        this.props.onRenderItem();
    }

    _renderFooter = () => {
        const {
            hasReachedEnd,
            loadMoreFailed,
            loadingMore,
        } = this.props;

        return (
            this.props.hasReachedEnd ?
                <CustomBar title='End of Results' containerStyle={this.props.endOfResultsCustomStyle || {}}/>
            :
                this.props.loadMoreFailed ? 
                    <SeeMoreButton onPress={this._onSeeMore}/>
                :
                    this.props.loadingMore ? 
                        <CustomLoadingBar size="small" color="#EEB843" /> 
                    : 
                        null
        );
    }

    _onEndReached = () => {
        this.props.onReachEnd();
    }

    displayLoadMoreFailedMsg = (msg = null) => {
        const strMsg = !msg ? 'Please check your internet connection or try loading again in a minute.' : msg;
        Alert.alert(
            "Sorry, We're Having Some Issues",
            strMsg,
            [
              {text: 'OK', onPress: () => {}},
            ],
            { cancelable: false }
        )
    }

    setLoadingScreen = (show, msg) => {
        let oLoadingScreen = {...this.state.loadingScreen};
        oLoadingScreen.show = show;
        oLoadingScreen.msg = msg;
        this.setState({ loadingScreen: oLoadingScreen });
    }

    setMessageBox = (show, type, msg, param) => {
        this.setState({
            msgBox: oHelper.setMsgBox(
                this.state.msgBox,
                show, 
                type,
                msg,
                param
            )
        })
    }

    msgBoxOnClose = (params) => {
        this.setState({
            msgBox: oHelper.clearMsgBox(this.state.msgBox)
        })
    }

    render(){
        const data = this.props.data;
        const aStatus = this.props.status;
        const title = this.props.title || 'UNTITLED';
        const listName = this.props.listName ? this.props.listName : null;
        const onEndReachedThreshold = this.props.onEndReachedThreshold || 0.5;
        const hideHeader = this.props.hideHeader || false;

        const {
            page,
            hasReachedEnd,
            refreshing,
            loadingMore,
            loadMoreFailed,
        } = this.props;

        const {
            loadingScreen,
            msgBox,
        } = this.state;

        let oView = null;
        
        if(aStatus[0] == 1){
            const list = listName ? data[listName] : data;
            oView = list.length < 1 ? <EmptyList message='No data' /> :
            
                <FlatList
                    refreshing={this.state.refreshing}
                    onRefresh={() => this.props.onRefresh()}
                    contentContainerStyle={styles.reportsFlatlist}
                    ref={(ref) => { this.flatListRef = ref; }}
                    extraData={data}
                    keyExtractor={this._keyExtractor}
                    data={list}
                    onEndReached={this._onEndReached}
                    onEndReachedThreshold={onEndReachedThreshold}
                    ListFooterComponent={this._renderFooter}
                    renderItem={({item}) =>
                        this.props.renderItem(item)
                    }
                />
        }

        return(
            <View style={styles.container}>
                {
                    hideHeader ? 
                        null 
                    :
                        <DefaultStyledHeader title={title} />
                }
                <GenericContainer
                    customContai
                    msgBoxShow = {msgBox.show}
                    msgBoxType = {msgBox.type}
                    msgBoxMsg = {msgBox.msg}
                    msgBoxOnClose = {this.msgBoxOnClose}
                    msgBoxOnYes = {this._msgBoxOnYes}
                    containerStyle = {[styles.container, this.props.containerStyle || {}]}
                    msgBoxParam = {msgBox.param}
                    loadingScreenShow = {loadingScreen.show}
                    loadingScreenMsg = {loadingScreen.msg}
                    status={aStatus}
                    title={title}
                    onRefresh={() => this.props.onRefresh()}>

                    { oView }

                    {
                        !this.props.loadMoreFailed && (this.props.hasFilters || false) ?
                            <CollapsibleView minHeight={30} maxHeight={'100%'} />
                        :
                            null
                    }
                    
                </GenericContainer>            
            </View>
            
        );
    }
}

export default PaginatedListContainer;
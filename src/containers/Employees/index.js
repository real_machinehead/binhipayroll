//Packages
import React, { Component } from 'react';
import {
    View,
    Text,
    Button,
    FlatList,
    TouchableNativeFeedback,
    TextInput
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import LinearGradient from 'react-native-linear-gradient';

//Children Components
import EmployeeList from './list';

//Styles Properties
import styles from './styles';

//Custom Components
import Header2 from '../Headers/header2';
import * as PromptScreen from '../../components/ScreenLoadStatus';
import GenericContainer from '../../components/GenericContainer';

//Children Components
import Summary from './Summary';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as employeeListActions from './data/list/actions';
import * as draftlistActions from './data/draft/actions';

import { CONSTANTS } from '../../constants/index';

const TITLE = 'Loading Employee List'
export class Employees extends Component {
    constructor(props){
        super(props);
        this.state={
            _summaryStatus: [2, 'Loading...'],
            _bDidMount: false
        }
    }

    static navigationOptions = {
        header : 
            <Header2 title= 'MY EMPLOYEES'/>
    }

    componentDidMount = () => {
        if(this.props.employeelist.status[0] != 1){
            this._getEmployeeListFromDB();
            this.setState({_bDidMount: true});
        }
        else{
            setTimeout( () => {
                this.setState({_bDidMount: true});
            },100);
        }
    }

    _getEmployeeListFromDB = () => {
        this.props.actions.employeelist.get({page: 1});
    }

    render(){
        const oData = this.props.employeelist.data;
        const oStatus = this.props.employeelist.status;
        const aStatus = (oData && oStatus[0] == 2) || oStatus[0] == 1 ? 
            CONSTANTS.STATUS.SUCCESS : oStatus[0] == 2 ? 
            CONSTANTS.STATUS.LOADING :  CONSTANTS.STATUS.ERROR

        return(
            <GenericContainer 
                status={aStatus}
                title={TITLE}
                onRefresh={this._getEmployeeListFromDB}>
                <View style={styles.container}>
                    <EmployeeList/>
                    <Summary />
                </View>

            </GenericContainer>
        );
    }
}

function mapStateToProps (state) {
    return {
        logininfo: state.loginReducer.logininfo,
        activecompany: state.activeCompanyReducer.activecompany,
        employeelist: state.employees.list,
        draftlist: state.employees.draft
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            employeelist: bindActionCreators(employeeListActions, dispatch),
            draftlist: bindActionCreators(draftlistActions, dispatch),
        },
    }
}
  
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Employees)

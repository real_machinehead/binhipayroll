import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  TouchableHighlight,
  Text,
  DatePickerAndroid,
  Button,
  TextInput,
  Alert
} from 'react-native';
import t from 'tcomb-form-native'; // 0.6.9
import moment from "moment";
import { withNavigation } from 'react-navigation';

//Styles
import styles from './styles';
import stylesheet from '../../../../global/globalFormStyle';

//Form Template 
import { customPickerTemplate } from '../../../../global/tcomb-custom-select-android';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as employeeActions from '../../data/activeProfile/actions';

//API
import * as employeeApi from '../../data/activeProfile/api';

//Custom Component
import {FormCard, PropTitle} from '../../../../components/CustomCards';
import * as CustomForm from '../../../../components/CustomForm';
import GenericContainer from '../../../../components/GenericContainer';
import FormModal from '../../../../components/FormModal';

//helper
import * as oHelper from '../../../../helper';

//Constants
const update_loading_message = 'Updating Employee Address Information. Please wait...';

const Form = t.form.Form;

class AddressForm extends Component {
    constructor(props){
        super(props);
        this.state={
            _oAddress: {
                province: this.props.initialValue.province,
                city: this.props.initialValue.city,
                barangay: this.props.initialValue.barangay,
                street: this.props.initialValue.street
            },
            _oProvinces: {},
            _oCities: {},
            _oBarangay: {},
        }
    }

    componentDidMount = async() => {
        let oProvince = this.state._oAddress.province;
        let oCity = this.state._oAddress.city;
        let oBarangay = this.state._oAddress.barangay;

        this._getProvinces(true);
        if(oCity != ''){
            this._getCities(oProvince, true);

            if(oBarangay != ''){
                this._getBarangays(oProvince, oCity, true);
            }
        }
    }

    _getProvinces = async(bHideLoading) => {
        
        bHideLoading ? null : this.s;

        employeeApi.getProvinces()
        .then((response) => response.json())
        .then((res) => {
            if(res.flagno != 1){
                
            }
            else{
                this._updateList('PROVINCE', res.province);
            }
            bHideLoading ? null : this.props.setLoadingStatus(false, 'Fetching List of Provinces. Please wait...');
        })
        .catch((exception) => {
            console.log('exception: ' + exception.message);
            bHideLoading ? this.props.setLoadingStatus(false) : null;
        });
    }

    _getCities = async(provinceID, bHideLoading) => {
 
        bHideLoading ? null : this.props.setLoadingStatus(true, 'Fetching List of Cities. Please wait...');

        employeeApi.getCities({province: provinceID})
        .then((response) => response.json())
        .then((res) => {
            /* console.log('_getCities: ' + JSON.stringify(res)) */
            if(res.flagno != 1){
                
            }
            else{
                this._updateList('CITY', res.city)
            }
            bHideLoading ? null : this.props.setLoadingStatus(false);
        })
        .catch((exception) => {
            console.log('exception: ' + exception.message)
            bHideLoading ? null : this.props.setLoadingStatus(false);
        });
    }

    _getBarangays = async(provinceID, cityID, bHideLoading) => {

        bHideLoading ? null : this.props.setLoadingStatus(true, 'Fetching List of Barangays. Please wait...');

        employeeApi.getBarangays({province: provinceID, city: cityID})
        .then((response) => response.json())
        .then((res) => {
            /* console.log('_getBarangays: ' + JSON.stringify(res)) */
            if(res.flagno != 1){
                
            }
            else{
                this._updateList('BARANGAY', res.barangay)
            }

            bHideLoading ? null : this.props.setLoadingStatus(false);
        })
        .catch((exception) => {
            console.log('exception: ' + exception.message)
            bHideLoading ? null : this.props.setLoadingStatus(false);
        });
    }

    _updateList = (strType, aList) => {
        let oList = null;
        switch(strType.toUpperCase()){
            case 'PROVINCE':
                oList = {...this.state._oProvinces};
                this._arrayToEnums(aList, oList);
                this.setState({_oProvinces: oList});
                break;

            case 'CITY': 
                oList = {...this.state._oCities};
                this._arrayToEnums(aList, oList);
                this.setState({_oCities: oList});
                break;

            case 'BARANGAY': 
                oList = {...this.state._oBarangay};
                this._arrayToEnums(aList, oList);
                this.setState({_oBarangay: oList});
                break;

            default:
                //Display Error Here
                break;
        }
    }

    _arrayToEnums = async(aList, oList) => {
        await aList.map(data => {
            console.log("data: " + JSON.stringify(data));
            oList[String(data.id)]=data.name;
        })
    }

    _onChangePS = (value) => {
        if((value.province != '') && this.state._oAddress.province !== value.province){
            this._getCities(value.province);
            value.city = ''
            value.barangay = '';
            this.setState({_oCities: {}})
            this.setState({_oBarangay: {}})
        }else if((value.city != '') && (this.state._oAddress.city !== value.city)){
            this._getBarangays(value.province, value.city);
            value.barangay = '';
            this.setState({_oBarangay: {}})
        }else {

        }
        this.setState({
            _oAddress: value
        })
    }

    getValue = () => this.refs.form_address.getValue();

    getAddressNames = () => {
        return {
            provincevalue: this.state._oProvinces[this.state._oAddress.province],
            cityvalue: this.state._oCities[this.state._oAddress.city],
            barangayvalue: this.state._oBarangay[this.state._oAddress.barangay],
        }
    }

    render(){
        /* console.log('this.state._oProvinces:' + JSON.stringify(this.state._oProvinces)); */
        const Province = t.enums(this.state._oProvinces);
        const City = t.enums(this.state._oCities);

        const Barangay = t.enums(this.state._oBarangay);
        
        const EMPLOYEE_PRESENTADD = t.struct({
            province: Province,
            city: City,
            barangay: Barangay,
            street: t.String
        });

        const OPTIONS = {
            fields: {
                province:{ 
                    template: customPickerTemplate,
                    label: 'SELECT PROVINCE' ,
                    error: '*Required field'
                },
                city:{ 
                    template: customPickerTemplate,
                    label: 'SELECT CITY',
                    error: '*Required field'
                },
                barangay:{ 
                    template: customPickerTemplate,
                    label: 'SELECT BARANGAY',
                    error: '*Required field'
                },
                street:{ 
                    label: 'ENTER STREET',
                    returnKeyType: 'done',
                    error: '*Required field'
                }
            },
            stylesheet: stylesheet
        };

        return(
            <Form 
                ref='form_address'
                type={EMPLOYEE_PRESENTADD} 
                value={this.state._oAddress}
                onChange={this._onChangePS}
                options={OPTIONS}/>
        );        
    }
}

export class EmployeeAddress extends Component {
    constructor(props){
        super(props);
        this.state={
            _promptMsg: 'Populating Address List. Please wait...',
            
            //Gereric States
            _promptShow: false,
            _promptMsg: '',
            _msgBoxShow: false,
            _msgBoxType: '',
            _resMsg: '',
            _refreshing: false,
            _disabledMode: true,
            _status: [2, 'Loading...'],
            _hasSentRequest: false,
            _bLastStatus: 1,
            loadingScreen: {
                show: false,
                msg: 'test'
            },
            msgBox: {
                show: false,
                type: '',
                msg: '',
                param: ''
            },

            present: '',
            permanent: '',
        }
    }

    _onTriggerNext = async(formTriggerSave=false) => {
        if(!this.state.loadingScreen.show){
            this._setLoadingScreen(true, 'Saving employee address information. Please wait...');
            let bPermanentAdd = await this.permanent_address.getValue();
            let bPresentAdd = await this.present_address.getValue();
            if (bPresentAdd && bPermanentAdd) {
                let oPermanentNames = this.permanent_address.getAddressNames();
                let oPresentNames = this.present_address.getAddressNames();
                this.setState({
                    present: { ...bPresentAdd, ...oPresentNames },
                    permanent: { ...bPermanentAdd, ...oPermanentNames },
                },
                    async () => {
                        if(formTriggerSave){
                            const oEmployee = await this._initInputParam();
                            this._updateToDB(oEmployee);
                        }
                        else{
                            const oEmployee = await this._initInputParam();
                            this._addToDB(oEmployee);
                        }
                    }
                )  
            }else{
                this._setLoadingScreen(false);
                Alert.alert(
                    'Error',
                    'One of the inputs is invalid. Check the highlighted fields.',
                    [
                    {text: 'Review Form', onPress: () => {}},
                    ],
                    { cancelable: false }
                )
            }
        }
    }

    _initInputParam = async() => {
        const {present, permanent} = this.state;
        let oEmployee = await oHelper.copyObject(this.props.oEmployee);
        let oPersonalInfo = oEmployee.personalinfo;
        let oPresent = oPersonalInfo.address.present;
        let oPermanent = oPersonalInfo.address.permanent;
        
        oPresent.province.id = present.province;
        oPresent.province.value = present.provincevalue;

        oPresent.city.id = present.city;
        oPresent.city.value = present.cityvalue;

        oPresent.barangay.id = present.barangay;
        oPresent.barangay.value = present.barangayvalue;

        oPresent.street.value = present.street;

        oPermanent.province.id = permanent.province;
        oPermanent.province.value = permanent.provincevalue;

        oPermanent.city.id = permanent.city;
        oPermanent.city.value = permanent.cityvalue;

        oPermanent.barangay.id = permanent.barangay;
        oPermanent.barangay.value = permanent.barangayvalue;

        oPermanent.street.value = permanent.street;
    
        oEmployee.id = oEmployee.id,
        oEmployee.compid = this.props.activecompany.id;
        oEmployee.empid = oEmployee.id;
        oEmployee.index = 2;

        return oEmployee;
    }

    _addToDB = async(oData) => {
        const {present, permanent} = this.state;
        let bFlag = false;
        let oRes = null;
        await employeeApi.personalinfo.address.add(oData)
            .then((response) => response.json())
            .then(async(res) => {
                console.log('XXXXXXXXXXXXXXXXXXXXXXXXXXXX');
                console.log('RES: ' + JSON.stringify(res));
                oRes = {...res};
                if(res.flagno==1){
                    await this.props.actions.employee.updateAddress({
                        address: oData.personalinfo.address
                    });
                    this._setMessageBox(true, 'success', res.message, 'ROUTENEXT');
                }else{
                    this._setMessageBox(true, 'error-ok', res.message);
                }
            })
            .catch((exception) => {
                this._setMessageBox(true, 'error-ok', res.message);
            });
        this._setLoadingScreen(false);
    }

    _updateToDB = async(oData) => {
        const {present, permanent} = this.state;
        await employeeApi.personalinfo.address.update(oData)
            .then((response) => response.json())
            .then(async(res) => {
                console.log('XXXXXXXXXXXXXXXXXXXXXXXXXXXX');
                console.log('res: ' + JSON.stringify(res));
                if(res.flagno==1){
                    await this.props.actions.employee.updateAddress({
                        address: oData.personalinfo.address
                    });
                    this._setMessageBox(true, 'success', res.message, 'CLOSEFORM');
                }else{
                    this._setMessageBox(true, 'error-ok', res.message);
                }
            })
            .catch((exception) => {
                this._setMessageBox(true, 'error-ok', exception.message);
            });
        this._setLoadingScreen(false);
    }

    _onFormClose = () => {
        this.setState({
            _bShowCompForm: false,
            _bShowGovForm: false
        })
    }

    _setMessageBox = (show, type, msg, param = '') => {
        this.setState({
            msgBox: oHelper.setMsgBox(
                this.state.msgBox,
                show, 
                type,
                msg,
                param
            )
        })
    }
    
    _setLoadingScreen = (show, msg) => {
        let oLoadingScreen = {...this.state.loadingScreen};
        oLoadingScreen.show = show;
        oLoadingScreen.msg = msg;
        this.setState({ loadingScreen: oLoadingScreen });
    }
    
    _msgBoxOnClose = () => {
        this.setState({
            msgBox: oHelper.clearMsgBox(this.state.msgBox)
        })
    
        const param = this.state.msgBox.param;
        switch(param.toUpperCase()){
            case 'ROUTENEXT':
                this.props.navigation.navigate('EmployeeDependents');
                break;
            case 'CLOSEFORM':
                if(this.props.editMode || false){
                  this.props.onHideForm();
                }
                break;
            default:
                break;
        }
    }

    render() {
        const content =
            <GenericContainer
                containerStyle={{flex: 1}}
                showNextButton = {this.props.editMode || false ? false : true}
                onNext={this._onTriggerNext}
                msgBoxShow = {this.state.msgBox.show}
                msgBoxType = {this.state.msgBox.type}
                msgBoxMsg = {this.state.msgBox.msg}
                msgBoxOnClose = {this._msgBoxOnClose}
                msgBoxOnYes = {this._msgBoxOnYes}
                msgBoxParam = {this.state.msgBox.param}
                loadingScreenShow = {this.state.loadingScreen.show}
                loadingScreenMsg = {this.state.loadingScreen.msg}
                status={[1,'Success']}
                title={'Basic Info'}
                onRefresh={this._fetchDataFromDB}>

                <ScrollView>
                    <View style={styles.container}>
                        <View style={styles.contDivider}>
                            <View style={styles.contFormLeft}>
                                { /********** Present Address **********/ }
                                <View style={styles.contTitle}>
                                    <Text style={styles.txtFormTitle}> PRESENT ADDRESS </Text>
                                </View>
                                <AddressForm 
                                    initialValue={{
                                        province: this.props.oEmployeeAddress.present.province.id,
                                        city: this.props.oEmployeeAddress.present.city.id,
                                        barangay: this.props.oEmployeeAddress.present.barangay.id,
                                        street: this.props.oEmployeeAddress.present.street.value
                                    }}
                                    ref={(oInstance) => { this.present_address = oInstance; }}
                                    setLoadingStatus={this._setLoadingScreen}/>
                            </View>

                            <View style={styles.contFormRight}>
                                { /********** PERMANENT Information **********/ }
                                <View style={styles.contTitle}>
                                    <Text style={styles.txtFormTitle}> PERMANENT ADDRESS </Text>
                                </View>
                                <AddressForm 
                                    initialValue={{
                                        province: this.props.oEmployeeAddress.permanent.province.id,
                                        city: this.props.oEmployeeAddress.permanent.city.id,
                                        barangay: this.props.oEmployeeAddress.permanent.barangay.id,
                                        street: this.props.oEmployeeAddress.permanent.street.value
                                    }}
                                    ref={(oInstance) => { this.permanent_address = oInstance; }}
                                    setLoadingStatus={this._setLoadingScreen}/>
                            </View>
                        </View>
                        {/* <View style={{flex:1, padding: 40}}>
                            <Button
                                onPress={this._onPress}
                                title='Next'
                                color="#3b5998"
                                accessibilityLabel='Next'
                            />
                        </View> */}
                    </View>
                </ScrollView>
            </GenericContainer>

        if(this.props.editMode || false){
            return(
              <FormModal 
                containerStyle={styles.formModal.container}
                title="MODIFY BASIC & CONTACT INFORMATION"
                visible={true}
                onCancel={()=>this.props.onHideForm()}
                onOK={() => this._onTriggerNext(true)}
              >
                {content}
              </FormModal>
            )
          }else{
            return content;
          }
        //This is put into render method to allow direct access to class properties
    }
}

function mapStateToProps (state) {
    return {
        logininfo: state.loginReducer.logininfo,
        activecompany: state.activeCompanyReducer.activecompany,
        oEmployeeAddress: state.employees.activeProfile.data.personalinfo.address,
        oEmployee: state.employees.activeProfile.data,
        formTriggerNext: state.employees.formTriggerNext
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            employee: bindActionCreators(employeeActions, dispatch),
        },
    }
  }
  
export default withNavigation(connect(
    mapStateToProps,
    mapDispatchToProps
)(EmployeeAddress))
import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  TouchableHighlight,
  Text,
  DatePickerAndroid,
  Button,
  TextInput,
  Alert
} from 'react-native';
import t from 'tcomb-form-native'; // 0.6.9
import moment from "moment";
import { withNavigation } from 'react-navigation';

//Styles
import styles from './styles';
import stylesheet from '../../../../global/globalFormStyle';

//Form Template 
import { customPickerTemplate } from '../../../../global/tcomb-custom-select-android';
import { customDatePickerTemplate } from '../../../../global/tcomb-custom-datepicker-android';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as employeeActions from '../../data/activeProfile/actions';

//API
import * as employeeApi from '../../data/activeProfile/api'

//Custom Component
import {FormCard, PropTitle} from '../../../../components/CustomCards';
import * as CustomForm from '../../../../components/CustomForm';
import * as PromptScreen from '../../../../components/ScreenLoadStatus';
import MessageBox from '../../../../components/MessageBox';
import GenericContainer from '../../../../components/GenericContainer';
import FormModal from '../../../../components/FormModal';

//helper
import * as oHelper from '../../../../helper';

//Constants
const update_loading_message = 'Updating Employee Basic Information. Please wait...';




const Form = t.form.Form;

const Gender = t.enums({
  Male: "Male",
  Female: "Female"
});

const CivilStatus = t.enums({
  Single: 'Single',
  Married: 'Married',
  Divorced: 'Divorced',
  Separated: 'Separated',
  Widowed: 'Widowed'
});

const EMPLOYEE_BASICINFO = t.struct({
  firstname: t.String,
  middlename: t.String,
  lastname: t.String,
  nickname: t.maybe(t.String),
  birthdate: t.Date,
  gender: Gender,
  civilstatus: CivilStatus
});

//Form - Employee Government IDs
const EMPLOYEE_GOVID = t.struct({
  tin: t.maybe(t.String),
  sss: t.maybe(t.String),
  philhealth: t.maybe(t.String),
  pagibig: t.maybe(t.String)
});


export class EmployeeBasicInfo extends Component {
  constructor(props){
    super(props);
    this.state={
      _dateFormat: this.props.employeePersonalInfo.basicinfo.birthdate.format || "MMMM DD, YYYY",
      _oBasicInfo: {
        firstname: this.props.employeePersonalInfo.basicinfo.firstname,
        middlename: this.props.employeePersonalInfo.basicinfo.middlename,
        lastname: this.props.employeePersonalInfo.basicinfo.lastname,
        nickname: this.props.employeePersonalInfo.basicinfo.nickname,
        birthdate: this.props.employeePersonalInfo.basicinfo.birthdate.value ? new Date( this.props.employeePersonalInfo.basicinfo.birthdate.value) : null,
        gender: this.props.employeePersonalInfo.basicinfo.gender.value,
        civilstatus: this.props.employeePersonalInfo.basicinfo.civilstatus.value,
      },
      _oContactInfo:{
        mobile: this.props.employeePersonalInfo.contactinfo.mobile,
        telephone: this.props.employeePersonalInfo.contactinfo.telephone,
        email: this.props.employeePersonalInfo.contactinfo.email
      },
      _oGovID:{
        tin: this.props.employeePersonalInfo.ids.tin.value,
        sss: this.props.employeePersonalInfo.ids.sss.value,
        philhealth: this.props.employeePersonalInfo.ids.philhealth.value,
        pagibig: this.props.employeePersonalInfo.ids.pagibig.value,
      },

      //Gereric States
      _refreshing: false,
      _disabledMode: true,
      _hasSentRequest: false,
      _bLastStatus: 1,
      _saveTriggered: false,
      loadingScreen: {
        show: false,
        msg: 'test'
      },
      msgBox: {
          show: false,
          type: '',
          msg: '',
          param: ''
      },
    }
  }

  _onTriggerNext = async(formTriggerSave=false) => {
    if(!this.state.loadingScreen.show){
      this._setLoadingScreen(true, 'Validating information. Please wait...');
      let aMobile = this.mobileList.getValue();
      let aTelephone = this.telephoneList.getValue();
      let aEmail = this.emailList.getValue();

      let oBasicForm = this.refs.basic_form.getValue();
      let oGovForm = this.refs.govid_form.getValue();

      if (oBasicForm && oGovForm) {
        let oContactInfo = {
          mobile: aMobile, 
          telephone: aTelephone, 
          email: aEmail
        }
        
        this.setState({
          _oBasicInfo: {...oBasicForm},
          _oContactInfo: {...oContactInfo},
          _oGovID: {...oGovForm},
        },
          async() => {
            if(formTriggerSave){
              const oEmployee = await this._initInputParam();
              this._updateToDB(oEmployee);
            }
            else{
              const oEmployee = await this._initInputParam();
              this._addToDB(oEmployee);
            }
          }
        );
        
      }
      else{
        this._setLoadingScreen(false);
        Alert.alert(
          'Error',
          'One of the inputs is invalid. Please check the highlighted fields.',
          [
            {text: 'Review Form', onPress: () => {}},
          ],
          { cancelable: false }
        )
      }
    }
  }

  _updateStore = (oBasicForm, oGovForm, oContactInfo) => {
    this.props.actions.employee.updateBasicInfo(oBasicForm);
    this.props.actions.employee.updateIDS(oGovForm);
    this.props.actions.employee.updateContactInfo(oContactInfo);
  }

  _initInputParam = async() => {
    const {_oBasicInfo, _oContactInfo, _oGovID} = this.state;
    let oEmployee = await oHelper.copyObject(this.props.oEmployee);
    let oPersonalInfo = oEmployee.personalinfo;
    let oBasicInfo = oPersonalInfo.basicinfo;
    let oContactInfo = oPersonalInfo.contactinfo;
    let oIds = oPersonalInfo.ids;

    oBasicInfo.firstname = _oBasicInfo.firstname;
    oBasicInfo.middlename = _oBasicInfo.middlename;
    oBasicInfo.lastname = _oBasicInfo.lastname;
    oBasicInfo.nickname = _oBasicInfo.nickname;
    oBasicInfo.birthdate.value = oHelper.convertDateToString(_oBasicInfo.birthdate, 'YYYY-MM-DD');
    oBasicInfo.gender.value = _oBasicInfo.gender;
    oBasicInfo.civilstatus.value = _oBasicInfo.civilstatus;

    oContactInfo.mobile = _oContactInfo.mobile;
    oContactInfo.telephone = _oContactInfo.telephone;
    oContactInfo.email = _oContactInfo.email;

    oIds.tin.value = _oGovID.tin;
    oIds.sss.value = _oGovID.sss;
    oIds.pagibig.value = _oGovID.pagibig;
    oIds.philhealth.value = _oGovID.philhealth;

    oEmployee.id = oEmployee.id,
    oEmployee.compid = this.props.activecompany.id;
    oEmployee.empid = oEmployee.id;
    oEmployee.index = 1;

    return oEmployee;
  }

  _onChangeBasicInfo = (value) => {
    let oBasicInfo = oHelper.copyObject(this.state._oBasicInfo);

    oBasicInfo.firstname = value.firstname;
    oBasicInfo.middlename = value.middlename;
    oBasicInfo.lastname = value.lastname;
    oBasicInfo.nickname = value.nickname;
    oBasicInfo.birthdate = value.birthdate;
    oBasicInfo.gender = value.gender;
    oBasicInfo.civilstatus = value.civilstatus;

    this.setState({_oBasicInfo: oBasicInfo});
  }
  
  _addToDB = async(oData) => {
    const {_oBasicInfo, _oContactInfo, _oGovID} = this.state;
    let bFlag = false;
    let oRes = null;
    this._setLoadingScreen(true, 'Adding New Employee. Please wait...')
    await employeeApi.personalinfo.basicinfo.add(oData)
        .then((response) => response.json())
        .then((res) => {
            if(res.flagno==1){
              this.props.actions.employee.updateActiveID(res.data.employeeid);
              this._updateStore(_oBasicInfo, _oGovID, _oContactInfo);
              this._setMessageBox(true, 'success', res.message, 'ROUTENEXT');
            }else{
              this._setMessageBox(true, 'error-ok', res.message);
            }
        })
        .catch((exception) => {
            this._setMessageBox(true, 'error-ok', exception.message);
        });
    this._setLoadingScreen(false);
    return bFlag;
  }

  _updateToDB = async(oData) => {
    let bFlag = false;
    let oRes = null;
    const {_oBasicInfo, _oContactInfo, _oGovID} = this.state;
    this._setLoadingScreen(true, update_loading_message);
    await employeeApi.personalinfo.basicinfo.update(oData)
        .then((response) => response.json())
        .then((res) => {
            if(res.flagno==1){
              this._updateStore(_oBasicInfo, _oGovID, _oContactInfo);
              this._setMessageBox(true, 'success', res.message, 'CLOSEFORM');
            }else{
              this._setMessageBox(true, 'error-ok', res.message);
            }
        })
        .catch((exception) => {
            this._setMessageBox(true, 'error-ok', exception.message);
        });
    this._setLoadingScreen(false);
    return bFlag;
  }

  _onFormClose = () => {
      this.setState({
          _bShowCompForm: false,
          _bShowGovForm: false
      })
  }

  _setMessageBox = (show, type, msg, param = '') => {
    this.setState({
        msgBox: oHelper.setMsgBox(
            this.state.msgBox,
            show, 
            type,
            msg,
            param
        )
    })
  }

  _setLoadingScreen = (show, msg) => {
      let oLoadingScreen = {...this.state.loadingScreen};
      oLoadingScreen.show = show;
      oLoadingScreen.msg = msg;
      this.setState({ loadingScreen: oLoadingScreen });
  }

  _msgBoxOnClose = () => {
    this.setState({
        msgBox: oHelper.clearMsgBox(this.state.msgBox)
    })

    const param = this.state.msgBox.param;
    switch(param.toUpperCase()){
      case 'ROUTENEXT':
        this.props.navigation.navigate('EmployeeAddress');
        break;
      case 'CLOSEFORM':
        if(this.props.editMode || false){
          this.props.onHideForm();
        }
        break;
      default:
        break;
    }
  }

  render() {
    let myFormatFunction = (format,strDate) => {
      return moment(strDate).format(format);
    }
    
    let oBday = {
        template: customDatePickerTemplate,
        label: 'BIRTHDATE',
        mode:'date',
        config:{
            format: (strDate) => myFormatFunction(this.state._dateFormat, strDate)
        },
        error: '*Select birth date'
    };

    { /********** Basic Information **********/ }
    const OPTIONS_BASICINFO = {
      fields: {
        firstname:{ 
          label: 'FIRST NAME' ,
          returnKeyType: 'next',
          onSubmitEditing: (event) => {this.refs.basic_form.getComponent('middlename').refs.input.focus()},
          error: '*First Name should not be empty'
        },
        middlename:{ 
          label: 'MIDDLE NAME',
          returnKeyType: 'next',
          onSubmitEditing: (event) => {this.refs.basic_form.getComponent('lastname').refs.input.focus()},
          error: '*Middle Name should not be empty'
        },
        lastname:{ 
          label: 'LAST NAME',
          returnKeyType: 'next',
          onSubmitEditing: (event) => {this.refs.basic_form.getComponent('nickname').refs.input.focus()},
          error: '*Last Name should not be empty'
        },
        nickname:{ 
          label: 'NICK NAME (Optional)',
          returnKeyType: 'next'
        },
        gender:{ 
          template: customPickerTemplate,
          label: 'GENDER',
          error: '*Select a gender'
        },
        birthdate: oBday,

        civilstatus:{
          template: customPickerTemplate,
          label: 'CIVIL STATUS',
          error: '*Select Civil Status'
        }
      },
      stylesheet: stylesheet
    };

    { /********** GOV IDS **********/ }
    const OPTIONS_GOVID = {
      fields: {
        tin:{ 
          label: 'TIN' ,
          returnKeyType: 'next',
          onSubmitEditing: (event) => {this.refs.govid_form.getComponent('sss').refs.input.focus()},
          error: '*First Name should not be empty'
        },
        sss:{ 
          label: 'SSS NO',
          returnKeyType: 'next',
          onSubmitEditing: (event) => {this.refs.govid_form.getComponent('philhealth').refs.input.focus()},
          error: '*Middle Name should not be empty'
        },
        philhealth:{ 
          label: 'PHILHEALTH NO',
          returnKeyType: 'next',
          onSubmitEditing: (event) => {this.refs.govid_form.getComponent('pagibig').refs.input.focus()},
          error: '*Last Name should not be empty'
        },
        pagibig:{ 
          label: 'PAGIBIG NO',
          returnKeyType: 'done'
        }
      },
      stylesheet: stylesheet
    };

    const content = 
      <GenericContainer
          containerStyle={{flex: 1}}
          showNextButton = {this.props.editMode || false ? false : true}
          onNext={this._onTriggerNext}
          msgBoxShow = {this.state.msgBox.show}
          msgBoxType = {this.state.msgBox.type}
          msgBoxMsg = {this.state.msgBox.msg}
          msgBoxOnClose = {this._msgBoxOnClose}
          msgBoxOnYes = {this._msgBoxOnYes}
          msgBoxParam = {this.state.msgBox.param}
          loadingScreenShow = {this.state.loadingScreen.show}
          loadingScreenMsg = {this.state.loadingScreen.msg}
          status={[1,'Success']}
          title={'Basic Info'}
          onRefresh={this._fetchDataFromDB}>
          <ScrollView>
            <View style={styles.container}>
              <View style={styles.contDivider}>
                <View style={styles.contFormLeft}>
                  { /********** Basic Information **********/ }
                  <View style={styles.contTitle}>
                    <Text style={styles.txtFormTitle}> BASIC INFORMATION </Text>
                  </View>
                  <Form 
                    ref='basic_form'
                    type={EMPLOYEE_BASICINFO} 
                    value={this.state._oBasicInfo}
                    onChange={this._onChangeBasicInfo}
                    options={OPTIONS_BASICINFO}/>

                  { /********** Basic Information **********/ }
                  <View style={styles.contTitle}>
                    <Text style={styles.txtFormTitle}> GOVERNMENT IDS </Text>
                  </View>
                  <Form 
                    ref='govid_form'
                    type={EMPLOYEE_GOVID} 
                    value={this.state._oGovID}
                    onChange={this._onChangeIds}
                    options={OPTIONS_GOVID}/>
                  
                </View>

                <View style={styles.contFormRight}>
                  { /********** Contact Information **********/ }
                  <View style={styles.contTitle}>
                    <Text style={styles.txtFormTitle}> CONTACT INFORMATION </Text>
                  </View>
                  
                  <CustomForm.DynamicList 
                    ref={(oInstance) => {this.mobileList = oInstance;}}
                    label='MOBILE NO' 
                    value={this.state._oContactInfo.mobile}
                    /* onChange={(value) => this._onChange('MOBILE', value)} */
                    keyboardType='phone-pad'/>

                  <CustomForm.DynamicList 
                    ref={(oInstance) => {this.telephoneList = oInstance;}}
                    label='TELEPHONE NO' 
                    value={this.state._oContactInfo.telephone}
                    keyboardType='phone-pad'/>

                  <CustomForm.DynamicList 
                    ref={(oInstance) => {this.emailList = oInstance;}}
                    label='EMAIL ADDRESS' 
                    value={this.state._oContactInfo.email}
                    keyboardType='email-address'/>
                  
                </View>
              </View>
            </View>
          </ScrollView>
        </GenericContainer>

    if(this.props.editMode || false){
      return(
        <FormModal 
          containerStyle={styles.formModal.container}
          title="MODIFY BASIC & CONTACT INFORMATION"
          visible={true}
          onCancel={()=>this.props.onHideForm()}
          onOK={() => this._onTriggerNext(true)}
        >
          {content}
        </FormModal>
      )
    }else{
      return content;
    }
  }
}

function mapStateToProps (state) {
  return {
      logininfo: state.loginReducer.logininfo,
      activecompany: state.activeCompanyReducer.activecompany,
      employeePersonalInfo: state.employees.activeProfile.data.personalinfo,
      oEmployee: state.employees.activeProfile.data,
      formTriggerNext: state.employees.formTriggerNext
  }
}

function mapDispatchToProps (dispatch) {
  return {
      actions: {
          employee: bindActionCreators(employeeActions, dispatch),
      },
  }
}

export default withNavigation(connect(
  mapStateToProps,
  mapDispatchToProps
)(EmployeeBasicInfo))
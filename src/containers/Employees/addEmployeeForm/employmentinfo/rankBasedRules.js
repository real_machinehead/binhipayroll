import React, { Component } from 'react';
import {
    View,
    Text,
    Picker,
    TouchableOpacity,
    Alert
} from 'react-native';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

//Children Components
import EmployeeRankForm from './forms/employeeRankForm';

//Custom Components
import GenericContainer from '../../../../components/GenericContainer';
import * as PromptScreen from '../../../../components/ScreenLoadStatus';
import EffectiveDatePicker from '../../../../components/EffectiveDatePicker';

//Styles
import styles from './styles';
import Ranks from '../../../CompanyPolicies/ranks';
import MessageBox from '../../../../components/MessageBox';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as ranksActions from '../../../CompanyPolicies/data/ranks/actions';
import * as employeeActions from '../../data/activeProfile/actions';
import * as employeelistActions from '../../data/list/actions';
import * as employeeApi from '../../data/activeProfile/api'


//Helper
import * as oHelper from '../../../../helper';

//Constants 
import {CONSTANTS} from '../../../../constants';
const TITLE = 'Rank Based Rules';
const add_loading_message = 'Saving a New Employee Rank. Please wait.';
const delete_loading_message = 'Deleting an Employee Rank. Please wait...';
const update_loading_message = 'Updating an Employee Rank. Please wait...';

export class RankBasedRules extends Component{
    constructor(props){
        super(props);
        this.state = {
            //Generic States
            _promptShow: false,
            _promptMsg: '',
            _msgBoxShow: false,
            _msgBoxType: '',
            _resMsg: '',
            _refreshing: false,
            _status: [2, 'Loading...'],

            _oActiveData: {
                id: ''
            },
            _tempRankValue: null,
            _bShowRankForm: false,
            _oDefaultData: {
                id: '',
                rankid: '',
                effectivedate: {
                    from: {
                        value: null,
                        format: "MMM DD, YYY"
                    },
                    to: {
                        value: null,
                        format: "MMM DD, YYYY"
                    }
                },
                remarks: ''
            },

            loadingScreen: {
                show: false,
                msg: ''
            },

            msgBox: {
                show: false,
                type: '',
                msg: '',
                param: ''
            },
        }
    }
    
    componentWillUnmount(){
        this.props.actions.ranks.setActiveRule('');
    }

    componentDidMount(){
        this._getRanksPolicyFromDB();
    }

    componentWillReceiveProps(nextProps){
        if(
            (nextProps.ranksPolicy.status[0] == 1) && 
            this.state._status[0] != 1
        ){
            this.setState({ _status: [...nextProps.ranksPolicy.status]});
            if(this.props.oEmpRank.data.length > 0){
                this._setActiveData(this.props.oEmpRank.data[0].id);
            }
        }
    }

    _onTriggerNext = async() => {
        if(!this.state.loadingScreen.show){
            await this._setLoadingScreen(true, 'Creating employee. Please wait...');
            const aData = this.props.oEmpRank.data;
            if(aData.length < 1){
                this._setMessageBox(true, 'error-ok', 'Atleast one (1) rank rule is required.');
            }else{
                await this._createUser();
            }
            await this._setLoadingScreen(false);
        }  
    }

    _createUser = async() => {
        await employeeApi.createUser({
            isdraft: "false",
            empid: this.props.oEmployee.id,
            compid: this.props.activecompany.id
        })
            .then((response) => response.json())
            .then(async(res) => {
                console.log('XXXXXXXXXXXXXXXXXXXXXXXXXXXX');
                console.log('RANKres_createUser: ' + JSON.stringify(res));
                if(res.flagno === 1){

                    let credentials = 'Username: ' + res.credentials.username + '\n' + 'Password: ' + res.credentials.password;
                    this._setMessageBox(true, 'success', res.message + '\n\n' + credentials, 'ROUTELIST');
                    /* await this.props.actions.employeelist.add({
                        key: this.props.oEmployee.data.id,
                        employee: this.props.oEmployee.data
                    }); */
                    res.data.id = res.data.key;
                    await this.props.actions.employeelist.add(res.data);
                }else{
                    this._setMessageBox(true, 'error-ok', res.message);
                }
            })
            .catch((exception) => {
                this._setMessageBox(true, 'error-ok', exception.message);
            });
    }

    _getRanksPolicyFromDB = () => {
        if(this.props.ranksPolicy.status[0] != 1){
            this.props.actions.ranks.get()
        }else{
            if(this.props.oEmpRank.data.length > 0){
                this._setActiveData(this.props.oEmpRank.data[0].id);
            }
        }
    }

    _setActiveData = (id) => {
        let oActiveData = oHelper.getElementByPropValue(this.props.oEmpRank.data, 'id', id);
        let aRanksPolicy = this.props.ranksPolicy.data.data;
        let oActiveRankRule = oHelper.getElementByPropValue(aRanksPolicy, 'id', oActiveData.rankid);

        this.props.actions.ranks.setActiveRule(oActiveRankRule);
        this.setState({_oActiveData: oActiveData});
    }

    _generateRanks = () => {
        let arrRTypes = [...this.props.ranksPolicy.data.data];
        let oRList = {};
        arrRTypes.map((data, index) => {
            oRList[data.name.value + CONSTANTS.SPLITSTRING + data.id] = data.name.value
        })
        return oRList;
    }

    _addNewRank = () => {
        let oCurData = JSON.parse(JSON.stringify(this.state._oDefaultData));
        this.setState({
            _tempRankValue: null,
            _oActiveData: oCurData,
            _bShowRankForm: true 
        })
    }

    _editActiveRank = () => {
        console.log('================================================');
        console.log('XXXXXXthis.state._oActiveData: ' + JSON.stringify(this.state._oActiveData));
        console.log('XXXXXXthis.props.ranksPolicy.data.data: ' + JSON.stringify(this.props.ranksPolicy.data.data));
        console.log('XXXXXXthis.props.ranksPolicy.data.data: ' + JSON.stringify(this.props.ranksPolicy.data.data));
        let oActiveData = oHelper.copyObject(this.state._oActiveData);
        let arrRTypes = [...this.props.ranksPolicy.data.data];
        let oActiveRankRule = oHelper.getElementByPropValue(arrRTypes, 'id', oActiveData.rankid);
        let tempRankValue = oActiveRankRule.name.value + CONSTANTS.SPLITSTRING + oActiveRankRule.id;
        this.setState({ 
            _tempRankValue: tempRankValue,
            _oActiveData: oActiveData,
            _bShowRankForm: true 
        })
    }

    _hideForm = () => {
        this.setState({ _bShowRankForm: false })
    }

    _submitTransaction = async(value) => {
        let oData = JSON.parse(JSON.stringify(this.state._oDefaultData));
        let splitRType = await value.rankid.split(CONSTANTS.SPLITSTRING);
        oData.id = this.state._oActiveData.id;
        oData.rankid = splitRType[1];
        oData.effectivedate.from.value = await (oHelper.convertDateToString(value.effectivedate, 'YYYY-MM-DD'));
        oData.remarks = value.remarks;
        let oInputData = {
            employeeId: this.props.oEmployee.id,
            rank: {
                data: oData
            }
        };
        if( oData.id === ''){
            this._saveNewDataToDB(oInputData);
        }

        else{
            let originalData = {...this.state._oActiveData};
            let newData = {...oData};
            if(
                (newData.rankid==originalData.rankid) &&
                (newData.effectivedate.from.value==originalData.effectivedate.from.value) 
            ){
                Alert.alert(
                    'Identical Data',
                    'Unable to commit changes. Modified data is similar to current data.',
                    [
                        {text: 'OK', onPress: () => {}}
                    ],
                    { cancelable: false }
                )
            }
            else{
                let oInputData = {
                    employeeId: this.props.oEmployee.id,
                    rank: {
                        data: oData
                    }
                }
                Alert.alert(
                    'Warning',
                    'All changes will be saved and will be irreversible. ' + 
                    'Are you sure you want to proceed ?',
                    [
                        {text: 'NO', onPress: () => {}},
                        {text: 'YES', onPress: () => this._updateDataToDB(oInputData)}
                    ],
                    { cancelable: false }
                )
            }
        }
    }

    _saveNewDataToDB = (oData) => {
        this._setLoadingScreen(true, add_loading_message);
        let bFlag = false;
        let oRes = null;

        employeeApi.employmentinfo.rank.add(oData)
        .then((response) => response.json())
        .then((res) => {
            console.log('XXXXXXXXXXXXXXXXXXXXXXXXXXXX');
            console.log('RANKres: ' + JSON.stringify(res));
            oRes = res;
            this._setLoadingScreen(false);
            bFlag = this._evaluateResponse(res);
            if(res.flagno === 1){
                this.props.actions.employee.updateRank(res.employee.employmentinfo.rank.data);
                this._hideForm();
            }
        })
        .catch((exception) => {
            this._setLoadingScreen(false);
            this._setMessageBox(true, 'error-ok', exception.message);
        });
    }

    _updateDataToDB = (oData) => {
        this._setLoadingScreen(true, add_loading_message);
        let bFlag = false;
        let oRes = null;

        employeeApi.employmentinfo.rank.update(oData)
        .then((response) => response.json())
        .then((res) => {
            console.log('XXXXXXXXXXXXXXXXXXXXXXXXXXXX');
            console.log('RANKres: ' + JSON.stringify(res));
            oRes = res;
            this._setLoadingScreen(false);
            bFlag = this._evaluateResponse(res);
            if(res.flagno === 1){
                this.props.actions.employee.updateRank(res.employee.employmentinfo.rank.data);
                this._hideForm();
                this._setActiveData(oData.rank.data.id);
            }
        })
        .catch((exception) => {
            this._setLoadingScreen(false);
            this._setMessageBox(true, 'error-ok', exception.message);
        });
    }

    _requestDelete = () => {
        let oData = JSON.parse(JSON.stringify(this.state._oActiveData));
        oData.employeeId = this.props.oEmployee.id;
        Alert.alert(
            'WARNING',
            'Deleting an assigned employee rank is an irreversible action. ' + 
            'Are you sure you want to proceed ?',
            [
                {text: 'NO', onPress: () => {}},
                {text: 'YES', onPress: () => this._deleteDataFromDB(oData)}
            ],
            { cancelable: false }
        )
    }

    _deleteDataFromDB = (oData) => {
        this._setLoadingScreen(true, delete_loading_message);
        let oRes = null;
        
        employeeApi.employmentinfo.rank.delete(oData)
            .then((response) => response.json())
            .then((res) => {
                console.log('XXXXXXXXXXXXXXXXXXXXXXXXXXXX');
                console.log('res: ' + JSON.stringify(res));
                oRes = res;
                this._setLoadingScreen(false);
                bFlag = this._evaluateResponse(res);
                if(res.flagno === 1){
                    this.props.actions.employee.updateRank(res.employee.employmentinfo.rank.data);
                    this._hideForm();
                }
            })
            .catch((exception) => {
                this._setLoadingScreen(false);
                this._setMessageBox(true, 'error-ok', exception.message);
            });
    }


    //Generic Methods
    _evaluateResponse = (res) => {
        switch (res.flagno){
            case 0:
                this._setMessageBox(true, 'error-ok', res.message);
                return false
                break;
            case 1:
                this._setMessageBox(true, 'success', res.message);
                return true;
                break;
            default:
                this._setMessageBox(true, 'error-ok', CONSTANTS.ERROR.UNKNOWN);
                return false
                break;
        }
    }

    _setMessageBox = (show, type, msg, param = '') => {
        this.setState({
            msgBox: oHelper.setMsgBox(
                this.state.msgBox,
                show, 
                type,
                msg,
                param
            )
        })
    }
    
    _setLoadingScreen = (show, msg) => {
        let oLoadingScreen = {...this.state.loadingScreen};
        oLoadingScreen.show = show;
        oLoadingScreen.msg = msg;
        this.setState({ loadingScreen: oLoadingScreen });
    }
    
    _msgBoxOnClose = () => {
        this.setState({
            msgBox: oHelper.clearMsgBox(this.state.msgBox)
        })
    
        const param = this.state.msgBox.param;
        switch(param.toUpperCase()){
            case 'ROUTELIST':
                this.props.navigation.navigate('Employees');
                break;
            default:
                break;
        }
    }

    render(){
        const aStatus = this.props.ranksPolicy.status;
        return(
            <GenericContainer
                showNextButton = {this.props.editMode || false ? false : true}
                onNext={this._onTriggerNext}
                onNextLabel='DONE'
                onNextIconName='check-circle-outline'
                containerStyle={styles.transparentContainer}
                msgBoxShow = {this.state.msgBox.show}
                msgBoxType = {this.state.msgBox.type}
                msgBoxMsg = {this.state.msgBox.msg}
                msgBoxOnClose = {this._msgBoxOnClose}
                msgBoxOnYes = {this._msgBoxOnYes}
                msgBoxParam = {this.state.msgBox.param}
                loadingScreenShow = {this.state.loadingScreen.show}
                loadingScreenMsg = {this.state.loadingScreen.msg}
                status={aStatus}
                title={TITLE}
                onRefresh={this._fetchDataFromDB}>

                {
                    aStatus[0] == 1 ?
                        <View style={styles.transparentContainer}>
                            { 
                                this.props.oEmpRank.data.length === 0 ?
                                    <TouchableOpacity 
                                        style={styles.emptyDataContainer}
                                        activeOpacity={0.8}
                                        onPress={this._addNewRank}>
                                            <Text>
                                                No Rank is assigned to employee. Tap here to add.
                                            </Text>
                                    </TouchableOpacity>
                                :
                                    <View style={styles.container}>
                                        <EffectiveDatePicker 
                                            selectedValue={this.state._oActiveData.id}
                                            options={this.props.oEmpRank.data}
                                            onChange={this._setActiveData}/>

                                        <View style={styles.workshiftStyles.body.container}>
                                            <View style={styles.workshiftStyles.body.contRule}>
                                                <Ranks hideHeader={true} viewOnly={true}/>
                                            </View>
                                        </View>
                                        {
                                            this.props.viewMode ?
                                                null
                                            :
                                                <ActionButton
                                                    bgColor='rgba(0,0,0,0.8)'
                                                    shadowStyle={{elevation: 30}}
                                                    buttonColor="#EEB843"
                                                    spacing={10}
                                                    icon={<Icon name="account-star" color='#fff' size={25} style={styles.actionButtonIcon} />}>
                                                    <ActionButton.Item size={45} buttonColor='#26A65B' title="ADD A NEW RANK" onPress={this._addNewRank}>
                                                        <Icon name="bell-plus" color='#fff' size={18} style={styles.actionButtonIcon} />
                                                    </ActionButton.Item>
                                                    <ActionButton.Item size={45} buttonColor='#4183D7' title="MODIFY CURRENT RANK" onPress={this._editActiveRank}>
                                                        <Icon name="table-edit" color='#fff' size={18} style={styles.actionButtonIcon} />
                                                    </ActionButton.Item>
                                                </ActionButton>
                                        }
                                        
                                    </View>
                            }
                        </View>
                    :
                            null
                }
                
                {
                    this.state._bShowRankForm ?
                        <EmployeeRankForm
                            rankValue={this.state._tempRankValue}
                            activeData = {this.state._oActiveData}
                            minEffectiveDate={null}
                            onDelete={this._requestDelete}
                            visible={this.state._bShowRankForm}
                            cancelForm={this._hideForm}
                            submitForm={this._submitTransaction}
                            title= {this.state._oActiveData.id ? 'MODIFY EMPLOYEE RANK' : 'ADD NEW EMPLOYEE RANK'}
                            ranksTypes={this._generateRanks()}/>
                    :
                        null
                }
            </GenericContainer>
        )
    }
}

function mapStateToProps (state) {
    return {
        logininfo: state.loginReducer.logininfo,
        activecompany: state.activeCompanyReducer.activecompany,
        ranksPolicy: state.companyPoliciesReducer.ranks,
        oEmpRank: state.employees.activeProfile.data.employmentinfo.ranks,
        oEmployee: state.employees.activeProfile.data,
        formTriggerNext: state.employees.formTriggerNext,
        viewMode: state.employees.activeProfile.viewMode,
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            employee: bindActionCreators(employeeActions, dispatch),
            ranks: bindActionCreators(ranksActions, dispatch),
            employeelist: bindActionCreators(employeelistActions, dispatch)
        },
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RankBasedRules)
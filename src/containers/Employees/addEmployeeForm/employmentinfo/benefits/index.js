import React, { Component } from 'react';
import {
    Alert,
    View,
    Text,
    Switch,
    TouchableOpacity,
    ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { withNavigation } from 'react-navigation';

//Styles
import styles from '../styles';

//Children Components
import EmpGovBenefits from './governmentBenefits';
import EmpCompBenefits from './companyBenefits';

//Helper
import * as oHelper from '../../../../../helper';
import { CONSTANTS } from '../../../../../constants/index';

//Custom Components
import EffectiveDateForm from '../forms/effectiveDateForm';
import CompanyBenefitsForm from '../forms/companyBenefitsForm';
import ActionButton from '../../../../../components/ActionButton';
import GenericContainer from '../../../../../components/GenericContainer';

//Redux
import { connect } from 'react-redux';

class EmployeeBenefits extends Component {
    _triggerRefresh = () => {
        alert('Should have refreshed');
    }

    componentWillReceiveProps(nextProps){
        if(
            (
                (this.props.formTriggerNext.index !== nextProps.formTriggerNext.index) &&
                (nextProps.formTriggerNext.key === this.props.navigation.state.key)
            )
        ){
            this._onTriggerNext();
        }
    }

    _onTriggerNext = async() => {
        Alert.alert(
            'Confirmation',
            'You can still update employee benefits after initial set-up. Are you sure you want to proceed ?.',
            [
                {text: 'CANCEL', onPress: () => {}},
                {text: 'CONTINUE', onPress: () => {this._routeToNext()}}
            ],
            { cancelable: false }
        )
    }

    _routeToNext = () => {
        this.props.navigation.navigate('RankBasedRules');
    }

    render(){
        return(
            <GenericContainer
                status={[1,'Success']}
                title='Employee Benefits'
                containerStyle={{flex: 1}}
                showNextButton = {this.props.editMode || false ? false : true}
                onNext={this._onTriggerNext}
            >
                <View style={styles.container}>
                    <View style={styles.benefitsStyles.container}>
                        <View style={styles.benefitsStyles.contLeft}>
                            <EmpGovBenefits/>
                        </View>
                        <View style={styles.benefitsStyles.contRight}>
                            <EmpCompBenefits/>
                        </View>
                    </View>
                </View>
            </GenericContainer>
        )
    }
}

function mapStateToProps (state) {
    return {
        formTriggerNext: state.employees.formTriggerNext
    }
}
export default withNavigation(connect(
    mapStateToProps
)(EmployeeBenefits))
 
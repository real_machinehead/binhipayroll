import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  TouchableHighlight,
  Text,
  DatePickerAndroid,
  Button,
  TextInput,
  Alert
} from 'react-native';
import t from 'tcomb-form-native'; // 0.6.9
import moment from "moment";
import { withNavigation } from 'react-navigation';

//Styles
import styles from '../personalinfo/styles';
import stylesheet from '../../../../global/globalFormStyle';

//Form Template 
import { customPickerTemplate } from '../../../../global/tcomb-custom-select-android';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as employeeActions from '../../data/activeProfile/actions';

//API
import * as employeeApi from '../../data/activeProfile/api'

//Custom Component
import {FormCard, PropTitle} from '../../../../components/CustomCards';
import * as CustomForm from '../../../../components/CustomForm';
import GenericContainer from '../../../../components/GenericContainer';
import FormModal from '../../../../components/FormModal';

//Helper
import * as oHelper from '../../../../helper';

//CONSTANTS
const add_loading_message = 'Saving New Employee Bank Information. Please wait.';
const update_loading_message = 'Saving Employee Bank Information. Please wait...';

const Form = t.form.Form;

export class EmployeeBankAccount extends Component {
  constructor(props){
    super(props);
    console.log('XXXXXXXXXXXXXXX_oEmployeeBankInfo: ' + JSON.stringify(this.props.oEmployeeBankInfo));
    this.state={
      _oBankAccount: {
          bankname: this.props.oEmployeeBankInfo.bankname || '',
          accountnumber: this.props.oEmployeeBankInfo.accountnumber || '',
      },
      loadingScreen: {
        show: false,
        msg: 'test'
      },
      msgBox: {
          show: false,
          type: '',
          msg: '',
          param: ''
      },
      _refreshing: false,
      _disabledMode: true,
      _status: [2, 'Loading...'],
      _hasSentRequest: false,
    }
  }

  _onChangeData = (value) => {
    let oData = oHelper.copyObject(this.state._oBankAccount);
    oData.bankname = value.bankname || '';
    oData.accountnumber = value.accountnumber || '';
    this.setState({_oBankAccount: oData})
  }

  _onTriggerNext = async(formTriggerSave=false) => {
    if(!this.state.loadingScreen.show){
      console.log('XXXentering_validation');
      await this._setLoadingScreen(true, 'Saving Employee Bank Details. Please wait...');
      const oBankInfo = await this.refs.form_bankinfo.getValue();
      console.log('XXXoBankInfo: ' + JSON.stringify(oBankInfo));
      if (oBankInfo) {
        if(formTriggerSave){
          const oEmployee = await this._initInputParam();
            this._updateToDB(oEmployee);
        }
        else{
            const oEmployee = await this._initInputParam();
            this._addToDB(oEmployee);
        }
      }else{
        
        console.log('oBankInfo is false!');
        this._setLoadingScreen(false);
        Alert.alert(
          'Error',
          'One of the inputs is invalid. Please check the highlighted fields.',
          [
            {text: 'Review Form', onPress: () => {}},
          ],
          { cancelable: false }
        )
      }
    }
  }

  _initInputParam = async() => {
    const {_oBankAccount} = this.state;
    let oEmployee = await oHelper.copyObject(this.props.oEmployee);
    let oBankInfo = oEmployee.bankinfo;

    oBankInfo.bankname = _oBankAccount.bankname;
    oBankInfo.accountnumber = _oBankAccount.accountnumber;

    oEmployee.id = oEmployee.id,
    oEmployee.compid = this.props.activecompany.id;
    oEmployee.empid = oEmployee.id;
    oEmployee.index = 4;

    return oEmployee;
  }

  _addToDB = async(oData) => {
    const {_oBankAccount} = this.state;
    await employeeApi.bankinfo.add(oData)
      .then((response) => response.json())
      .then(async(res) => {
        if(res.flagno==1){
          console.log('RES_addToDB: ' + JSON.stringify(res));
          await this.props.actions.employee.updateBankInfo(_oBankAccount);
          this._setMessageBox(true, 'success', res.message, 'ROUTENEXT');
        }else{
          this._setMessageBox(true, 'error-ok', res.message);
        }
      })
      .catch((exception) => {
        this._setMessageBox(true, 'error-ok', exception.message);
      });
    this._setLoadingScreen(false);
  }

  _updateToDB = async(oData) => {
    const {_oBankAccount} = this.state;
    await employeeApi.bankinfo.update(oData)
      .then((response) => response.json())
      .then(async(res) => {
        if(res.flagno==1){
          await this.props.actions.employee.updateBankInfo(_oBankAccount);
          this._setMessageBox(true, 'success', res.message);
        }else{
          this._setMessageBox(true, 'error-ok', res.message);
        }
      })
      .catch((exception) => {
        this._setMessageBox(true, 'error-ok', exception.message);
      });
    this._setLoadingScreen(false);
  }

  _onFormClose = () => {
    this.setState({
      _bShowCompForm: false,
      _bShowGovForm: false
    })
  }

  _setMessageBox = (show, type, msg, param = '') => {
    this.setState({
        msgBox: oHelper.setMsgBox(
            this.state.msgBox,
            show, 
            type,
            msg,
            param
        )
    })
}

_setLoadingScreen = (show, msg) => {
    let oLoadingScreen = {...this.state.loadingScreen};
    oLoadingScreen.show = show;
    oLoadingScreen.msg = msg;
    this.setState({ loadingScreen: oLoadingScreen });
}

_msgBoxOnClose = () => {
    this.setState({
        msgBox: oHelper.clearMsgBox(this.state.msgBox)
    })

    const param = this.state.msgBox.param;
    switch(param.toUpperCase()){
        case 'ROUTENEXT':
            this.props.navigation.navigate('EmployeeDetails');
            break;
        default:
            break;
    }
}


  render() {
    //This is put into render method to allow direct access to class properties
    EMPLOYEE_BANKINFO = t.struct({
        bankname: oHelper.isStringEmptyOrSpace(this.state._oBankAccount.accountnumber) ? t.maybe(t.String) : t.String,
        accountnumber: oHelper.isStringEmptyOrSpace(this.state._oBankAccount.bankname) ? t.maybe(t.String) : t.String
    })
      
    { /********** OPTIONS **********/ }
    const OPTIONS = {
        fields: {
            bankname:{ 
                label: 'BANK NAME (Optional)' ,
                returnKeyType: 'next',
                onSubmitEditing: (event) => {this.refs.form_bankinfo.getComponent('accountnumber').refs.input.focus()},
                error: '*You have an Account Number input. Enter a Bank Name.'
            },
            accountnumber:{ 
                label: 'ACCOUNT NUMBER (Optional)',
                returnKeyType: 'done',
                error: '*You have an Bank Name input. Enter an Account Number.'
            }
        },
        stylesheet: stylesheet
    };
    const content = 
      <GenericContainer
        containerStyle={styles.genericContainer}
        showNextButton = {this.props.editMode || false ? false : true}
        onNext={this._onTriggerNext}
        msgBoxShow = {this.state.msgBox.show}
        msgBoxType = {this.state.msgBox.type}
        msgBoxMsg = {this.state.msgBox.msg}
        msgBoxOnClose = {this._msgBoxOnClose}
        msgBoxOnYes = {this._msgBoxOnYes}
        msgBoxParam = {this.state.msgBox.param}
        loadingScreenShow = {this.state.loadingScreen.show}
        loadingScreenMsg = {this.state.loadingScreen.msg}
        status={[1,'Success']}
        title={'Basic Info'}
        onRefresh={this._fetchDataFromDB}>
        <ScrollView>
          <View style={styles.container}>
            <View style={styles.contFormBankInfo}>

                <Form 
                  ref='form_bankinfo'
                  type={EMPLOYEE_BANKINFO} 
                  value={this.state._oBankAccount}
                  onChange={this._onChangeData}
                  options={OPTIONS}/>

            </View>

            <View style={{flex:1, marginTop: 100, alignSelf: 'center', width: 400}}>
            </View>

          </View>
        </ScrollView>
      </GenericContainer>
    if(this.props.editMode || false){
      return(
        <FormModal 
          containerStyle={styles.formModal.container}
          title="MODIFY BASIC & CONTACT INFORMATION"
          visible={true}
          onCancel={()=>this.props.onHideForm()}
          onOK={() => this._onTriggerNext(true)}
        >
          {content}
        </FormModal>
      )
    }else{
      return content;
    }
  }
}

function mapStateToProps (state) {
  return {
        logininfo: state.loginReducer.logininfo,
        activecompany: state.activeCompanyReducer.activecompany,
        oEmployeeBankInfo: state.employees.activeProfile.data.bankinfo,
        oEmployee: state.employees.activeProfile.data,
        formTriggerNext: state.employees.formTriggerNext
  }
}

function mapDispatchToProps (dispatch) {
  return {
      actions: {
          employee: bindActionCreators(employeeActions, dispatch),
      },
  }
}

export default withNavigation(connect(
  mapStateToProps,
  mapDispatchToProps
)(EmployeeBankAccount))
import React, { Component } from 'react';
import { StackNavigator, TabNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';
import { View } from 'react-native';

import EmployeeBankAccount from './bankaccount';

const EmployeeBankAccountNav = StackNavigator(
  { EmployeeBankAccount: {screen: EmployeeBankAccount} },
  { headerMode: 'none' }
);

const BankInfo = TabNavigator({
  EmployeeBankAccount: {
    screen: EmployeeBankAccountNav,
    navigationOptions: {
      tabBarLabel: 'BANK ACCOUNT INFORMATION'
    }
  }
},
  {
    animationEnabled: false,
    tabBarPosition: 'top',
    swipeEnabled: true,
    tabBarOptions: {
      showIcon: false,
      showLabel: true,
      scrollEnabled: false,
      activeTintColor: '#d69200',
      inactiveTintColor: '#434646',
      tabStyle: { height: 40},
      labelStyle: {
        fontSize: 12,
        fontWeight: '500'
      },
      style: {
        backgroundColor: '#fff',
        zIndex: 999,
        elevation: 3,
      },
      indicatorStyle: {
        backgroundColor: '#EEB843',
        height: 5
      }
    }
  }
);

export default BankInfo;
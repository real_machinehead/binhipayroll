import { fetchApi, mockFetch } from '../../../services/api';
/* import * as endPoints from '../../../../global/endpoints'; */

const endPoints = {
    changePassword: 'changepassword'
}

export let commit = payload => fetchApi(endPoints.changePassword, payload, 'post');
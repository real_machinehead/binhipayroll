
import React, { Component } from 'react';
import {
    View,
    Text
} from 'react-native';
import {
    Menu,
    MenuOptions,
    MenuOption,
    MenuTrigger,
  } from 'react-native-popup-menu';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

//Styles
import styles from '../styles';

export default class EmployeePayrollCard extends Component {
    
    render(){
        const bodyStyles = styles.listStyles.body;
        const cardHeaderStyles = styles.listStyles.body.cardHeader;
        const cardBodyStyles = styles.listStyles.body.cardBody;
        const textStyles = styles.textStyles;
        const item = this.props.item;
        const isClosed = item.status == 3;
        const statusColor = isClosed ? 'rgba(21, 123, 19, 0.86)' : 'rgba(193, 66, 66, 0.94)';
        return(
            <View style={[bodyStyles.placeholder, {borderColor: statusColor} ]}>
                <View style={cardHeaderStyles.container}>
                    <View style={cardHeaderStyles.left}>
                        <View style={cardHeaderStyles.icon}>
                            <Icon name='account-circle' size={73} color='#434646'/>
                        </View>
                        <View style={cardHeaderStyles.title}>
                            <Text style={textStyles.name}>{item.name}</Text>
                            <Text style={textStyles.description}>{item.position}</Text>
                            <Text style={textStyles.description}>{item.branch}</Text>
                        </View>
                    </View>
                    <View style={cardHeaderStyles.right}>
                        <Menu>
                            <MenuTrigger children={<Icon name='dots-vertical' size={30} color='#434646'/>}/>
                            <MenuOptions>
                                <MenuOption onSelect={() => this.props.showPayslip(item)} text='View Payslip' />
                                <MenuOption onSelect={() => this.props.showDTR(item)} text='View Daily Time Record'/>
                                <MenuOption onSelect={() => this.props.showMonetaryAdjustmentForm(item)} text='Special Deduction/Allowance'/>
                                <MenuOption onSelect={() => this.props.onRegenerate(item)} text='Regenerate'/>
                                {
                                    isClosed ?
                                        null
                                    :
                                        <MenuOption onSelect={() => this.props.onCloseEmployee(item)} text='Mark as Closed' />
                                }
                                
                            </MenuOptions>
                        </Menu>
                    </View>
                </View>
                <View style={cardBodyStyles.container}>
                    {
                        item.summary.map((data, index) => 
                            <View key={index} style={cardBodyStyles.content}>
                                <View style={cardBodyStyles.label}>
                                    <Text style={index === (item.summary.length-1) ? textStyles.summaryBold : textStyles.summary}>
                                        {data[0]}
                                    </Text>
                                </View>
                                <View style={cardBodyStyles.value}>
                                    <Text style={index === (item.summary.length-1) ? textStyles.summaryBold : textStyles.summary}>
                                        {data[1]}
                                    </Text>
                                </View>
                            </View>
                        )
                    }
                </View>
            </View>
        )
    }
}
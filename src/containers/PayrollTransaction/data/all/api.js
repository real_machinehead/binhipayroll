import { fetchApi, mockFetch } from '../../../../services/api';
import * as endPoints from'../../../../global/endpoints';
import * as blackOps from '../../../../global/blackOps';

export let generate = payload => fetchApi(endPoints.transactions.payroll.generate(payload), payload, 'post');
export let cancel = payload => fetchApi(endPoints.transactions.payroll.cancel(payload), payload, 'post');
export let regenerate = payload => fetchApi(endPoints.transactions.payroll.regenerate(payload), payload, 'post');
export let post = payload => fetchApi(endPoints.transactions.payroll.post(payload), payload, 'post');
export let switchCloseAllFlag = payload => fetchApi(endPoints.transactions.payroll.switchCloseAllFlag(payload), payload, 'post');

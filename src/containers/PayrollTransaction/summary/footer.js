import React, { Component } from 'react';
import {
    View,
    Text,
    Button,
    TouchableOpacity
} from 'react-native';

//Styles
import styles from '../styles';

export default class PayrollTransactionSummaryFooter extends Component {
    render(){
        const footerStyles = styles.summaryStyles.footer;
        const textStyles = styles.textStyles;
        const iStatus = this.props.data.payrollinfo.status;
        return(
            <View style={footerStyles.container}>
                <View style={footerStyles.content}>
                    {
                        iStatus == 3 ?
                            <View style={footerStyles.upperBlock}>
                                <TouchableOpacity
                                    activeOpacity={0.8}
                                    onPress={() => this.props.onBack()}>
                                    <View style={[footerStyles.btn, footerStyles.btnLater]}>
                                        <Text style={textStyles.btnText}>
                                            BACK
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                                {/* <TouchableOpacity
                                    activeOpacity={0.8}
                                    onPress={() => this.props.regenerate()}>
                                    <View style={[footerStyles.btn, footerStyles.btnLater]}>
                                        <Text style={textStyles.btnText}>
                                            PRINT
                                        </Text>
                                    </View>
                                </TouchableOpacity> */}
                            </View>
                        :
                            <View>
                                <View style={footerStyles.upperBlock}>
                                    <TouchableOpacity
                                        activeOpacity={0.8}
                                        onPress={() => this.props.cancel()}>
                                        <View style={[footerStyles.btn, footerStyles.btnCancel]}>
                                            <Text style={textStyles.btnText}>
                                                CANCEL
                                            </Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        activeOpacity={0.8}
                                        onPress={() => this.props.regenerate()}>
                                        <View style={[footerStyles.btn, footerStyles.btnRegenerate]}>
                                            <Text style={textStyles.btnTextError}>
                                                REGENERATE
                                            </Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                <View style={footerStyles.lowerBlock}>
                                    <TouchableOpacity
                                        activeOpacity={0.8}
                                        onPress={() => this.props.continueLater()}>
                                        <View style={[footerStyles.btn, footerStyles.btnLater]}>
                                            <Text style={textStyles.btnText}>
                                                CONTINUE LATER
                                            </Text>
                                        </View>
                                    </TouchableOpacity>
            
                                    <TouchableOpacity
                                        activeOpacity={0.8}
                                        onPress={() => this.props.post()}>
                                        <View style={[footerStyles.btn, footerStyles.btnPost]}>
                                            <Text style={textStyles.btnText}>
                                                POST
                                            </Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                    }
                </View>
            </View>
        );
    }
}
import React, { Component } from 'react';
import {
    View,
    Text,
    Button,
    ScrollView,
    RefreshControl
} from 'react-native';
import { PropValCard } from '../../../components/CustomCards';

//Styles
import styles from '../styles';

export default class PayrollTransactionSummaryBody extends Component {
    state = {
        _refreshing: false
    }

    render(){
        const bodyStyles = styles.summaryStyles.body;
        const textStyles = styles.textStyles;
        const oSummary = this.props.data.payrollsummary;
        const cardBodyStyles = styles.summaryStyles.body.cardBody;
        const oDetailsCard = (oData, padding, hasIcon) => (
            <View style={bodyStyles.upperBlock}>
                <View style={bodyStyles.title}>
                    <Text style={textStyles.groupTitle}>
                        {oData.title}
                    </Text>
                </View>
                {
                    oData.data.map((data, index) => 
                        <PropValCard 
                            hasIcon = { hasIcon }
                            data={data} 
                            key={index}
                            customPadding={padding ? padding : {}}
                        />
                    ) 
                }
            </View>
        )

        return(
            <View style={bodyStyles.container}>
                <View style={bodyStyles.content}>
                    <ScrollView
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state._refreshing}
                                onRefresh={() => this.props.onRefresh()}
                            />
                        }
                    >
                        {oDetailsCard(oSummary.payrollinfo, 1, false)}
                        {oDetailsCard(oSummary.amountsummary, 10, true)}
                        <View style={{flex:1, height: 20}} />
                    </ScrollView>
                </View>
            </View>
        );
    }
}
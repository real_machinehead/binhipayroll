import React, { Component } from 'react';
import {
    View,
    Text,
    Button,
    Alert
} from 'react-native';
import { withNavigation } from 'react-navigation';

//Styles
import styles from '../styles';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as summaryActions from '../data/summary/actions';
import * as employeesActions from '../data/employees/actions';
import * as payrollTransactionApi from '../data/all/api';

//Children components
import PayrollTransactionSummaryHeader from './header';
import PayrollTransactionSummaryFooter from './footer';
import PayrollTransactionSummaryBody from './body';
import GenericContainer from '../../../components/GenericContainer';

//Helper
import * as oHelper from '../../../helper';

export class PayrollTransactionSummary extends Component {
    constructor(props){
        super(props);
        this.state = {
            refreshing: false,
            loadingScreen: {
                show: false,
                msg: 'test'
            },

            msgBox: {
                show: false,
                type: '',
                msg: '',
                param: ''
            },
            
        }
    }

    //Alert Confirmations
    _onCancel = () => {
        Alert.alert(
            'Warning',
            'All saved calculations for this payroll will be discarded and will not be saved. ' +
            'You can still generate payroll again later. ' + 
            '\n\nAre you sure you want to CANCEL current payroll transaction ?',
            [
                {text: 'NO', onPress: () => {}},
                {text: 'YES', onPress: () => this._cancelTransaction()}
            ],
            { cancelable: false }
        );
    }

    _onRegenerate = () => {
        Alert.alert(
            'Warning',
            'Regeneration will remove and recalculate all payroll data for current Payroll Schedule. ' +
            '\n\nAre you sure you want to REGENERATE current payroll ? ',
            [
                {text: 'NO', onPress: () => {}},
                {text: 'YES', onPress: () => this._regenarateTransaction()}
            ],
            { cancelable: false }
        );
    }

    _onContinueLater = () => {
        Alert.alert(
            'Confirmation',
            'All saved data for the payroll transaction will be preserved. ' +
            '\nAre you sure you want to CONTINUE LATER ? ',
            [
                {text: 'NO', onPress: () => {}},
                {text: 'YES', onPress: () => this._continueLaterTransaction()}
            ],
            { cancelable: false }
        );
    }

    _onPost = () => {
        Alert.alert(
            'Warning',
            "When a payroll transaction is posted, you can't be able to " +
            "modify any calculated and generated data. Make sure you have reviewed the " +
            "system generated summary and calculation results." +
            '\n\nAre you sure you want to POST this payroll transaction ? ',
            [
                {text: 'NO', onPress: () => {}},
                {text: 'YES', onPress: () => this._reconfirmBeforePost()}
            ],
            { cancelable: false }
        );
    }

    _reconfirmBeforePost = () => {
        Alert.alert(
            'Payroll Posting Confirmation',
            "\nPress POST to finally submit transaction. " +
            "\nPress BACK to suspend payroll posting and return to transaction. ",
            [
                {text: 'BACK', onPress: () => this._suspendedPayrollPosting()},
                {text: 'POST TRANSACTION', onPress: () => this._postTransaction()}
            ],
            { cancelable: false }
        );
    }

    _suspendedPayrollPosting = () => {
        Alert.alert(
            'Payroll Posting',
            "Payroll posting was cancelled. You can still recalculate and post the payroll transaction when ready.",
            [
                {text: 'OK', onPress: () => {}},
            ],
            { cancelable: false }
        );
    }

    //Actions on Confirmation
    _cancelTransaction = async() => {
        const oPayrollInfo = this.props.transactionInfo.activerule;
        this._setLoadingScreen(true, 'Cancelling Payroll Transaction. Please wait...');
        await payrollTransactionApi.cancel(oPayrollInfo.payrollid)
            .then((response) => response.json())
            .then((res) => {
                if(res.flagno == 1){
                    this._setMessageBox(true, 'success', res.message, 'CANCEL');
                }else{
                    this._setMessageBox(true, 'error-ok', res.message);
                }
            })
            .catch((exception) => {
                this._setMessageBox(true, 'error-ok', exception.message);
            });

        this._setLoadingScreen(false);
    }

    _regenarateTransaction = async() => {
        const oPayrollInfo = this.props.transactionInfo.activerule;
        this._setLoadingScreen(true, 'Regenerating Payroll. Please wait...');
        await payrollTransactionApi.regenerate(oPayrollInfo.payrollid)
            .then((response) => response.json())
            .then((res) => {
                if(res.flagno == 1){
                    this._setMessageBox(true, 'success', res.message, 'REGENERATE');
                }else{
                    this._setMessageBox(true, 'error-ok', res.message);
                }
            })
            .catch((exception) => {
                this._setMessageBox(true, 'error-ok', exception.message);
            });

        this._setLoadingScreen(false);
    }

    _continueLaterTransaction = () => {
        this.props.navigation.navigate('Transactions');
    }

    _postTransaction = async() => {
        const oPayrollInfo = this.props.transactionInfo.activerule;
        this._setLoadingScreen(true, 'Posting Payroll Transaction. Please wait...');
        await payrollTransactionApi.post(oPayrollInfo.payrollid)
            .then((response) => response.json())
            .then((res) => {
                if(res.flagno == 1){
                    this._setMessageBox(true, 'success', res.message, 'POST');
                }else{
                    this._setMessageBox(true, 'error-ok', res.message);
                }
            })
            .catch((exception) => {
                this._setMessageBox(true, 'error-ok', exception.message);
            });

        this._setLoadingScreen(false);
    }

    _onBack = () => {
        Alert.alert(
            'Confirm Action',
            "Are you sure you want to exit from Payroll Transaction Record ?",
            [
                {text: 'NO', onPress: () => {}},
                {text: 'OK', onPress: () => {this._back()}},
            ],
            { cancelable: false }
        );
    }

    _back = () => {
        this.props.navigation.navigate('Transactions');
    }

    //Generic Functions
    _setMessageBox = (show, type, msg, param = null) => {
        this.setState({
            msgBox: oHelper.setMsgBox(
                this.state.msgBox,
                show, 
                type,
                msg,
                param
            )
        })
    }

    _setLoadingScreen = (show, msg) => {
        let oLoadingScreen = {...this.state.loadingScreen};
        oLoadingScreen.show = show;
        oLoadingScreen.msg = msg;
        this.setState({ loadingScreen: oLoadingScreen });
    }

    _msgBoxOnClose = async() => {
        const param = this.state.msgBox.param;
        const oPayrollInfo = this.props.transactionInfo.activerule;

        await this.setState({
            msgBox: oHelper.clearMsgBox(this.state.msgBox)
        })

        switch(param ? param.toUpperCase() : ''){
            case 'CANCEL':
                this.props.navigation.navigate('Transactions');
                break;
            case 'REGENERATE':
                this.props.actions.summary.get(oPayrollInfo);
                this.props.actions.employees.get({payrollid: oPayrollInfo.payrollid, page: 1});
                break;
            case 'POST':
                this.props.navigation.navigate('Transactions');
                break;
            default:
                break;
        }
    }

    render(){
        const summaryStyles = styles.summaryStyles;
        const aStatus = this.props.data.status;
        const oData = this.props.data.data;

        return(
            <View style={summaryStyles.container}>
                <GenericContainer
                    msgBoxShow = {this.state.msgBox.show}
                    msgBoxType = {this.state.msgBox.type}
                    msgBoxMsg = {this.state.msgBox.msg}
                    msgBoxOnClose = {this._msgBoxOnClose}
                    msgBoxParam = {this.state.msgBox.param}
                    loadingScreenShow = {this.state.loadingScreen.show}
                    loadingScreenMsg = {this.state.loadingScreen.msg}
                    status={aStatus}
                    title={ 'Payroll Transaction Summary' }
                    onRefresh={() => this.props.onRefresh()}>

                    <PayrollTransactionSummaryHeader data={oData}/>
                    <PayrollTransactionSummaryBody 
                        data={oData}
                        onRefresh={() => this.props.onRefresh()}
                    />
                    <PayrollTransactionSummaryFooter 
                        data={oData}
                        cancel={this._onCancel}
                        regenerate={this._onRegenerate}
                        continueLater={this._onContinueLater}
                        post={this._onPost}
                        onBack={this._onBack}
                    />

                </GenericContainer>
            </View>
        );
    }
}

function mapStateToProps (state) {
    return {
        summary: state.payrolltransaction.summary,
        employees: state.payrolltransaction.employees,
        transactionInfo: state.payrolltransaction.all
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            summary: bindActionCreators(summaryActions, dispatch),
            employees: bindActionCreators(employeesActions, dispatch),
        }
    }
}
  
export default withNavigation(connect(
    mapStateToProps,
    mapDispatchToProps
)(PayrollTransactionSummary))

import { combineReducers } from 'redux';
import { reducer as notificationsPreviousReducer } from './previous/reducer';
import { reducer as notificationsCurrentReducer } from './current/reducer';

export const reducer = combineReducers({
    current: notificationsCurrentReducer,
    previous: notificationsPreviousReducer,
});
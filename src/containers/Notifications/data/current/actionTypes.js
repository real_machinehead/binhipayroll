export const INITIALIZE = 'notifications/currentperiod/INITIALIZE';
export const UPDATE = 'notifications/currentperiod/UPDATE';
export const STATUS = 'notifications/currentperiod/STATUS';
export const RESET = 'notifications/currentperiod/RESET';
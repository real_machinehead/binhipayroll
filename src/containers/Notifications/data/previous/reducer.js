import * as actionTypes from './actionTypes';
import { combineReducers } from 'redux';
import  { CONSTANTS } from '../../../../constants';
import * as oHelper from '../../../../helper';

const initialState = null;
const initialStatus = CONSTANTS.STATUS.LOADING;

export const data = (state = initialState, action) => {
	let oState = oHelper.copyObject(state);
	switch (action.type) {
		case actionTypes.INITIALIZE:
			return action.payload;
			break;

		case actionTypes.UPDATE:
			/* console.log('KKKKKKKKKKKKKKKKK_oState.data: ' + JSON.stringify(oState.data));
			console.log('KKKKKKKKKKKKKKKKK_action.payload: ' + JSON.stringify(action.payload)); */
			oState.data = oHelper.arrayMergeMutipleProp(oState.data, action.payload.data, ['notification_id', 'identifier']);
			return oState;
			break;

		default:
			return state;
	}
};

const status = (state = initialStatus, action) => {
	switch (action.type) {
		case actionTypes.STATUS:
			return action.payload;
			break;
			
		default:
			return state;
	}
};

export const reducer = combineReducers({
	data: data,
	status: status
});
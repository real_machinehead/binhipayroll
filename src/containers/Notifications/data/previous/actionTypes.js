export const INITIALIZE = 'notifications/previousperiod/INITIALIZE';
export const UPDATE = 'notifications/previousperiod/UPDATE';
export const STATUS = 'notifications/previousperiod/STATUS';
export const RESET = 'notifications/previousperiod/RESET';
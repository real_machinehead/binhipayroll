//Packages
import React, { Component, PureComponent } from 'react';
import { View, Text } from 'react-native';

//Styles
import styles from './styles';

//Constatants
const TITLE = 'DASHBOARD';

//Children Components
import NotificationsHeader from './header';

//Tabs
import NotificationsTabs from './tabs'; 

export default class Notifications extends Component {
    constructor(props){
        super(props);
        this.state={
            _status: [1, 'Success']
        }
    }

    render(){
        const listStyles = styles.listStyles;
        return(
            <View style={listStyles.container}>
                <NotificationsHeader/>
                <NotificationsTabs/>
            </View>
        )
    }
}
//RN Packages andd Modules
import React, { Component } from 'react';
import { View, Text } from 'react-native';

import styles from '../styles';

const NotificationsDetails = (props) => {
    const views = props.item.views;
    const textStyles = styles.textStyles;
    return(
        <View>
            {
                views.map((data, index) =>
                    <Text 
                        key={data}
                        style={textStyles.notification.details}>

                        {data[0] + ':  ' + data[1]}

                    </Text>
                )
            }
        </View>
    )
}

export default NotificationsDetails;

//RN Packages andd Modules
import React from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const NotificationsIcon = (props) => {
    let iconName = '';
    let iconColor = '';
    const item = props.item;
    const statusid = item.statusid || -1;

    switch(item.statusid){
        case 5:
            iconName = 'close-circle';
            iconColor = '#e82d27';
            break;

        case 3: 
            iconName = 'check-circle';
            iconColor = '#009602';
            break;

        /* case 'WARNING':
            iconName = 'alert-circle';
            iconColor = '#d8d300';
            break; */

        /* case 'INFO':
            iconName = 'information';
            iconColor = '#04b9d3';
            break; */
            
        default: 
            iconName = 'information';
            iconColor = '#04b9d3';
            break;
    }

    return(
        <Icon name={iconName} size={30} color={iconColor}/>
    );
}

export default NotificationsIcon;
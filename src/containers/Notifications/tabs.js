import React, { Component } from 'react';
import { TabNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';

import NotificationCurrent from './current';
import NotificationPrevious from './previous';

const NotificationsTabs = TabNavigator({
    NotificationCurrent: {
    screen: NotificationCurrent,
    navigationOptions: {
      tabBarLabel: 'Current Period',
    }
  },

  NotificationPrevious: {
    screen: NotificationPrevious,
    navigationOptions: {
      tabBarLabel: 'Previous Period',
    }
  },
},
  {
    animationEnabled: true,
    tabBarPosition: 'top',
    swipeEnabled: true,
    tabBarOptions: {
      showIcon: false,
      showLabel: true,
      scrollEnabled: false,
      activeTintColor: '#d69200',
      inactiveTintColor: '#434646',
      tabStyle: { height: 30},
      labelStyle: {
        fontSize: 12,
        /* fontWeight: '500' */
      },
      style: {
        backgroundColor: '#fff',
        zIndex: 999
      },
      indicatorStyle: {
        backgroundColor: '#EEB843',
        height: 5
      }
    },
    
    lazy: true
  }
);

export default NotificationsTabs;
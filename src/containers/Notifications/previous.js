import React, { Component, PureComponent } from 'react';
import {Alert} from 'react-native';

//Children Components
import PaginatedList from '../../components/PaginatedList';
import NotificationsItem from './common/item';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as notificationspreviousActions from './data/previous/actions';

//Styles
import styles from './styles';

//Constants
const TITLE = 'PREVIOUS PERIOD';

//helper
import * as oHelper from '../../helper';

export class NotificationPrevious extends Component{
    _generatePayload = (page) => ({
        companyid: this.props.activecompany.id,
        period: 'previousperiod',
        filter: '00000',
        identifier: null,
        employeeid: null,
        date: null,
        status: null,
        showdeleted: null,
        page: page,
    })

    _onMount = () => 
        this.props.actions.notificationsprevious.get(
            this._generatePayload(1)
        );

    _onRefresh = async() => 
        await this.props.actions.notificationsprevious.refresh(
            this._generatePayload(1)
        );

    _onLoadMore = async(page) => 
        await this.props.actions.notificationsprevious.loadMore(
            this._generatePayload(page)
        );
    
    //Local Functions
    _renderItem = item => 
        <NotificationsItem
            item={item} 
            onCancel={this._onCancelTransaction}
        />;
    
    _onCancelTransaction = async(oItem) => {
        Alert.alert(
            "Sorry, We're Having Some Issues",
            strMsg,
            [
              {text: 'CANCEL', onPress: () => {}},
              {text: 'OK', onPress: () => {this._cancelToDB()}},
            ],
            { cancelable: false }
        )
    }

    _cancelToDB = async(oItem) => {
        this.oCompList.setLoadingScreen(true, 'Cancelling a Transaction. Please wait...');
        const oRes = await this.props.actions.notificationsprevious.cancel(oItem);
        this.oCompList.setMessageBox(true, oRes.flagno==1 ? 'success' : 'error-ok', oRes.message);
        this.oCompList.setLoadingScreen(false);
    }

    render(){
        const {status, data} = this.props.notificationsprevious;
        const hideHeader = this.props.hideHeader || false;
        const oData = this.props.notificationsprevious;
        const aStatus = oData.status;

        return(
            <PaginatedList
                containerStyle = {{backgroundColor: 'transparent'}}
                title={TITLE}
                hideHeader={true}
                status={aStatus}
                data={data}
                onMount={this._onMount}
                onRefresh={this._onRefresh}
                onLoadMore={this._onLoadMore}
                renderItem={this._renderItem}
                ref={(ref) => { this.oCompList = ref; }}
                keyExtractorName='notification_id'
                keyExtractorExt='identifier'
                pageName='total_pages'
                pageOnFirstLevel
                listName='data'
                hasFilters
            />
        );
    }
}

function mapStateToProps (state) {
    return {
        notificationsprevious: state.notifications.previous,
        activecompany: state.activeCompanyReducer.activecompany
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            notificationsprevious: bindActionCreators(notificationspreviousActions, dispatch),
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NotificationPrevious)
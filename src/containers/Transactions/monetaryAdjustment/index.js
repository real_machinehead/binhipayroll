import React, { Component } from 'react';
import {
    View,
    Text,
    Button
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { withNavigation } from 'react-navigation';

//Children Components
import MonetaryAdjustmentForm from './form';
import * as PromptScreen from '../../../components/ScreenLoadStatus';
import MessageBox from '../../../components/MessageBox';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as payrollListActions from '../../Reports/data/payrollSchedule/actions';
import * as employeeListActions from '../../Employees/data/list/actions';

//API
import * as monetaryAdjustmentApi from '../data/monetaryAdjustment/api';

//helper
import * as oHelper from '../../../helper';

//Constants
const TITLE = 'SPECIAL DEDUCTIONS & ALLOWANCES';

export class MonetaryAdjustment extends Component {
    constructor(props){
        super(props);
        this.state = {
            allowCloseForm: false,
            refreshing: false,
            loadingScreen: {
                show: false,
                msg: 'test'
            },

            msgBox: {
                show: false,
                type: '',
                msg: '',
                param: ''
            },
            oExternalData: null
        }
    }

    //Loading Screen and MsgBox Functions
    _setMessageBox = (show, type, msg, param) => {
        this.setState({
            msgBox: oHelper.setMsgBox(
                this.state.msgBox,
                show, 
                type,
                msg,
                param
            )
        })
    }

    _setLoadingScreen = (show, msg) => {
        this.setState({ 
            loadingScreen: oHelper.setLoadingScreen(show, msg)
        });
    }

    _msgBoxOnClose = (params) => {
        this.setState({
            msgBox: oHelper.clearMsgBox(this.state.msgBox)
        })
        if(this.state.allowCloseForm){
            /* this._hideForm(); */
        }
    }

    //Class based functions
    componentDidMount(){
        if(this.props.isExternalCall || false){
            this._populateExternalData();
        }else{
            this._getUpdatedRequiredDataFromDB();
        }
    }

    _populateExternalData = async() => {
        const aEmp = [{
            key: this.props.employee.id,
            name: this.props.employee.name
        }];

        const aPay = [{
            id: this.props.payroll.id,
            payrollDate: this.props.payroll.date
        }]

        const oCurExternalData = {
            aEmployee: aEmp,
            aPayroll: aPay
        }

        await this.setState({
            oExternalData: oCurExternalData
        })

        this.props

    }

    _getUpdatedRequiredDataFromDB = () => {
        this.props.actions.employeelist.get({page: null});
        this.props.actions.payrollList.get();
    }

    _hideForm = () => {
        this.props.hideForm(true);
    }

    _onFormSubmit = (oData) => {
        let oCurData = {...oData};
        aEmpIdSplit = (oCurData.employeeid).split(oHelper.separatorString);
        aPayrollId = (oCurData.payrollid).split(oHelper.separatorString);
        oCurData.id = '';
        oCurData.payrollid = aPayrollId[1];
        oCurData.employeeid = aEmpIdSplit[1];
        oCurData.enablecancel = 1;
        oCurData.status_id = 5;
        this._createDataToDB(oCurData);
    }

    _createDataToDB = async(oCurData) => {
        this._setLoadingScreen(true, 'Saving Transaction. Please wait...');
        await monetaryAdjustmentApi.create(oCurData)
        .then((response) => response.json())
        .then((res) => {
            this._setMessageBox(
                true, 
                res.flagno == 1 ? 'success' : 'error-ok',
                res.message
            );
            if(res.flagno == 1){
                this.setState({allowCloseForm: true});
            }
        }).catch((exception) => {
            this._setMessageBox(
                true, 
                'error-ok',
                exception.message
            );
        });
        this._setLoadingScreen(false);
    }

    render(){
        const oExternalData = this.state.oExternalData;
        const payrollListStatus = this.props.payrollList.status;
        const aEmpStatus = this.props.employeelist.status;
        if((payrollListStatus[0] != 1 || aEmpStatus[0] !=1) && !oExternalData){
            return(
                <View style={{flex: 1, position: 'absolute'}}>
                    {
                        payrollListStatus[0] == 0 || aEmpStatus[0] == 0 ?
                            <MessageBox
                                promptType={'error-ok'}
                                show={true}
                                onClose={this._hideForm}
                                message={'Unable to fetch Payroll List. Check your internet connection or Please try again.'}
                            />
                        : 
                            <PromptScreen.PromptGeneric 
                                show= {true} 
                                title={'Preparing Employee and Payroll Information... Please wait...'}
                            />
                    }
                </View>
            );
        }else{
            let aEmpList = [];
            let aPayrollList = [];
            let aEmpListEnums = {};
            let aPayrollListEnums = {};
            if(oExternalData){
                aEmpList = oExternalData.aEmployee;
                aEmpListEnums = oHelper.generateEnumsConcatPropAndVal(aEmpList, 'key', 'name'); 
                aPayrollList = oExternalData.aPayroll;
                aPayrollListEnums = oHelper.generateEnumsConcatPropAndValDate(aPayrollList, 'id', 'payrollDate', 'MMMM DD, YYYY');
            }else{
                aEmpList = this.props.employeelist.data.data;
                aEmpListEnums = oHelper.generateEnumsConcatPropAndVal(aEmpList, 'key', 'name');
                payrollListData = this.props.payrollList.data;
                const curPayroll = payrollListData.current;
                const futurePayroll = payrollListData.future;
                const aPayrollList = [...curPayroll, ...futurePayroll];
                aPayrollListEnums = oHelper.generateEnumsConcatPropAndValDate(aPayrollList, 'id', 'payrollDate', 'MMMM DD, YYYY');
            }
            
            console.log('aPayrollListEnums: ' + JSON.stringify(aPayrollListEnums));
            return(
                <View>
                    <MonetaryAdjustmentForm
                        disableBasicOption={oExternalData ? true : false}
                        title={TITLE}
                        data ={{employeeList: aEmpListEnums, payrollList: aPayrollListEnums}}
                        visible={this.props.visible}
                        onCancel={() => this.props.onCancel()}
                        onSubmit={this._onFormSubmit}/>

                    <MessageBox
                        promptType={this.state.msgBox.type || ''}
                        show={this.state.msgBox.show || false}
                        onClose={this._msgBoxOnClose}
                        message={this.state.msgBox.msg || ''}
                    />

                    <PromptScreen.PromptGeneric 
                        show= {this.state.loadingScreen.show || false} 
                        title={this.state.loadingScreen.msg || ''}
                    />
                </View>
            );
        }
    }
}

function mapStateToProps (state) {
    return {
        employeelist: state.employees.list,
        payrollList: state.reports.payrollSchedule
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            employeelist: bindActionCreators(employeeListActions, dispatch),
            payrollList: bindActionCreators(payrollListActions, dispatch),
        }
    }
}
  
export default  withNavigation(connect(
    mapStateToProps,
    mapDispatchToProps
)(MonetaryAdjustment))
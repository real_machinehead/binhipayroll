import * as api from './api';
import * as actionTypes from './actionTypes';
import  { CONSTANTS } from '../../../../constants';

export const init = payload => ({
	type: actionTypes.INITIALIZE,
	payload
});

export const reset = () => 
	dispatch => {
		dispatch(updateStatus(CONSTANTS.STATUS.LOADING));
		dispatch(init(null));
	}

export const update = payload => ({
	type: actionTypes.UPDATE,
	payload,
});

export const get = payload => 
	dispatch => {
		let objRes = {};
		dispatch(updateStatus(CONSTANTS.STATUS.LOADING));
		dispatch(reset());

		api.get(payload)
		.then((response) => response.json())
		.then((res) => {
			console.log('monetarAdjustmentReport: ' + JSON.stringify(res));
			dispatch(init(res.data));
			objRes = {...res}
		})
		.then(() => {
			dispatch(updateStatus([
				objRes.flagno || 0, 
				objRes.message || CONSTANTS.ERROR.SERVER
			]));
		})
		.catch((exception) => {
			dispatch(updateStatus([
				0,
				exception.message + '.'
			]));
			console.log('exception: ' + exception.message);
		});
	}

export const updateStatus = payload => ({
	type: actionTypes.STATUS,
	payload,
});

export const create = payload =>
	async dispatch => {
		let oRes = null;

		await api.create(payload)
		.then((response) => response.json())
		.then((res) => {
			console.log('RES_leaveApplication_create: ' + JSON.stringify(res));
			oRes = {...res}
		})
		.catch((exception) => {
			oRes =  {flagno: 0, message: exception.message};
		});

		return oRes;
	}


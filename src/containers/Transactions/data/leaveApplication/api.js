import { fetchApi, mockFetch } from '../../../../services/api';
import * as endPoints from'../../../../global/endpoints';
import * as blackOps from '../../../../global/blackOps';

export let get = payload => fetchApi(endPoints.transactions.leaveApplication.get(payload), payload, 'get');
export let create = payload => fetchApi(endPoints.transactions.leaveApplication.create(payload), payload, 'post');
export let update = payload => fetchApi(endPoints.transactions.leaveApplication.update(payload), payload, 'post');
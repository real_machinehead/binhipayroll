import { fetchApi, mockFetch } from '../../../../../services/api';
import * as endPoints from '../../../../../global/endpoints';
import * as blackOps from '../../../../../global/blackOps';

export let get = payload => fetchApi(endPoints.reports.notimeout.get(payload), payload, 'post');
export let confirm = payload => fetchApi(endPoints.reports.notimeout.confirm(payload), payload, 'post');
export let update = payload => fetchApi(endPoints.reports.notimeout.update(payload), payload, 'post');
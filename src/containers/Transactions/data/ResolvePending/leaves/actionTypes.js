export const INITIALIZE = 'reports/leaveApplication/INITIALIZE';
export const UPDATE = 'reports/leaveApplication/UPDATE';
export const STATUS = 'reports/leaveApplication/STATUS';
export const RESET = 'reports/leaveApplication/RESET';
import { combineReducers } from 'redux';
import { reducer as tardinessReportReducer } from './tardiness/reducer';
import { reducer as undertimeReducer } from './undertime/reducer';
import { reducer as overtimeReducer } from './overtime/reducer';
import { reducer as leavesReducer } from './leaves/reducer';
import { reducer as notimeinReducer } from './notimein/reducer';
import { reducer as notimeoutReducer } from './notimeout/reducer';

//import { reducer as pagibigEmpReportReducer} from '../../Employees/profile/reports/data/pagibig/reducer';
export const reducer = combineReducers({
	tardiness: tardinessReportReducer,
	undertime: undertimeReducer,
	overtime: overtimeReducer,
	leaves: leavesReducer,
	notimein: notimeinReducer,
	notimeout: notimeoutReducer
});
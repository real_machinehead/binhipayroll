export const INITIALIZE = 'reports/overtime/INITIALIZE';
export const UPDATE = 'reports/overtime/UPDATE';
export const STATUS = 'reports/overtime/STATUS';
export const RESET = 'reports/overtime/RESET';
export const INITIALIZE = 'reports/tardiness/INITIALIZE';
export const UPDATE = 'reports/tardiness/UPDATE';
export const STATUS = 'reports/tardiness/STATUS';
export const RESET = 'reports/tardiness/RESET';
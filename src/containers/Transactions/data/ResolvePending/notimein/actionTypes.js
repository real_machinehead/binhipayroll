export const INITIALIZE = 'reports/notimein/INITIALIZE';
export const UPDATE = 'reports/notimein/UPDATE';
export const STATUS = 'reports/notimein/STATUS';
export const RESET = 'reports/notimein/RESET';
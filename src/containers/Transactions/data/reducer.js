import { combineReducers } from 'redux';
import { reducer as leaveCategoryReducer } from './leaveCategory/reducer';
import { reducer as dtrModificationReducer } from './dtrModification/reducer';
import { reducer as dtrReducer } from './DTR/reducer';
import { reducer as employeeReducer } from './employee/data/reducers';
import { reducer as leaveReducer } from './leaveApplication/reducer';
import { reducer as monetaryAdjustmentReducer } from './monetaryAdjustment/reducer';

export const reducer = combineReducers({
	dtrModifcation: dtrModificationReducer,
	dtr:dtrReducer,
	//employee:employeeReducer,
	leave:leaveReducer,
	leaveCategory:leaveCategoryReducer,
	monetaryAdjustment:monetaryAdjustmentReducer,
});
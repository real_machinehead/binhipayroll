import { fetchApi, mockFetch } from '../../../../../../services/api';
import * as endPoints from '../../../../../../global/endpoints';
import * as blackOps from '../../../../../../global/blackOps';

export let get = payload => fetchApi(endPoints.employee.draft.get(payload), payload, 'post');
export let getProfile = payload => fetchApi(endPoints.employee.draft.getProfile(payload), payload, 'post');
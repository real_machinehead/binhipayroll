import React, { Component } from 'react';
import {
    View,
    Text,
    Button
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

//Children Components
import LeaveApplicationForm from './form';
import * as PromptScreen from '../../../components/ScreenLoadStatus';
import MessageBox from '../../../components/MessageBox';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as employeeListActions from '../../Employees/data/list/actions';
import * as leaveCategoryActions from '../data/leaveCategory/actions';
import * as leaveBalanceApi from '../../Reports/data/leaveBalance/api';
import * as leaveApplicationActions from '../data/leaveApplication/actions';

//helper
import * as oHelper from '../../../helper';

//Constants
const TITLE = 'EMPLOYEE LEAVE APPLICATION';

export class LeaveApplication extends Component {
    constructor(props){
        super(props);
        this.state = {
            allowCloseForm: false,
            refreshing: false,
            loadingScreen: {
                show: false,
                msg: 'test'
            },
            leaveBalance: [],
            msgBox: {
                show: false,
                type: '',
                msg: '',
                param: ''
            },
        }
    }

    componentDidMount(){
        this._getUpdatedRequiredDataFromDB();
    }

    _getUpdatedRequiredDataFromDB = () => {
        this.props.actions.employeelist.get({page: null});
        this.props.actions.leaveCategory.get();
    }

    _hideForm = () => {
        this.props.hideForm();
    }

    _onChangeEmployee = async(oEmpId) => {
        const oActiveCompany = this.props.activecompany;
        this._setLoadingScreen(true, 'Getting Employee Leave Balance. Please wait...');
        let empId = oEmpId.split(oHelper.separatorString)[1];
        await leaveBalanceApi.get({empid: empId, compid: oActiveCompany.id})
            .then((response) => response.json())
            .then((res) => {
                console.log('RES: ' + JSON.stringify(res));
                
                if(res.flagno == 1){
                    this.setState({leaveBalance: res.leavebalancelist});
                }else{
                    this._setMessageBox(
                        true, 
                        res.flagno == 1 ? 'success' : 'error-ok',
                        res.message
                    );
                }
            }).catch((exception) => {
                this._setMessageBox(
                    true, 
                    'error-ok',
                    exception.message
                );
            });
        this._setLoadingScreen(false);
    }

    _onFormSubmit = async(oData) => {
        this._setLoadingScreen(true, 'Saving Leave Transaction. Please wait...');
        let oInput = {};
        oInput.compid = this.props.activecompany.id;
        oInput.empid = (oData.employeeid).split(oHelper.separatorString)[1];
        oInput.categoryid = (oData.category).split(oHelper.separatorString)[1];
        oInput.leavetype = (oData.leavetype).split(oHelper.separatorString)[1];
        oInput.dateinformed = oHelper.convertDateToString(oData.dateinformed, 'YYYY-MM-DD');
        oInput.timeinformed = oHelper.convertDateToString(oData.timeinformed, 'hh:mm:ss');
        oInput.datefrom = oHelper.convertDateToString(oData.datefrom, 'YYYY-MM-DD');
        oInput.dateto = oData.dateto ? oHelper.convertDateToString(oData.dateto, 'YYYY-MM-DD') : oData.dateto;
        oInput.description = oData.reason;
        oInput.remarks = oData.reason;

        const oRes = await this.props.actions.leaveApplication.create(oInput);
        this._setMessageBox(
            true, 
            oRes.flagno == 1 ? 'success' : 'error-ok',
            oRes.message
        );
        this._setLoadingScreen(false);
    }

    _createDataToDB = async(oCurData) => {

    }

    //Loading Screen and MsgBox Functions
    _setMessageBox = (show, type, msg, param) => {
        this.setState({
            msgBox: oHelper.setMsgBox(
                this.state.msgBox,
                show, 
                type,
                msg,
                param
            )
        })
    }

    _setLoadingScreen = (show, msg) => {
        this.setState({ 
            loadingScreen: oHelper.setLoadingScreen(show, msg)
        });
    }

    _msgBoxOnClose = (params) => {
        this.setState({
            msgBox: oHelper.clearMsgBox(this.state.msgBox)
        })
        if(this.state.allowCloseForm){
            this._hideForm();
        }
    }

    render(){
        const aEmpStatus = this.props.employeelist.status;
        const aLeaveCategoryStatus = this.props.leaveCategory.status;

        if(aEmpStatus[0] != 1 || aLeaveCategoryStatus[0] != 1){
            
            return(
                <View style={{flex: 1, position: 'absolute'}}>
                    {
                        aEmpStatus[0] == 0 || aLeaveCategoryStatus[0] == 0 ?
                            <MessageBox
                                promptType={'error-ok'}
                                show={true}
                                onClose={this._hideForm}
                                message={'Unable to fetch Required Data for Leave Application. Check your internet connection or Please try again.'}
                            />
                        : 
                            aEmpStatus[0] == 2 || aLeaveCategoryStatus[0] == 2 ?
                                <PromptScreen.PromptGeneric 
                                    show= {true} 
                                    title={'Preparing Required Data for Leave Application. Please wait...'}
                                />
                            : null
                    }
                </View>
            );
            
        }else{
            const aEmpList = this.props.employeelist.data.data;
            const aEmpListEnums = oHelper.generateEnumsConcatPropAndVal(aEmpList, 'key', 'name');

            const aCategoryList = this.props.leaveCategory.data; 
            const aCategoryEnums = oHelper.generateEnumsConcatPropAndVal(aCategoryList, 'id', 'name');

            const aLeaveBalace = this.state.leaveBalance;
            const aLeaveBalanceEnums = oHelper.generateEnumsConcatPropAndVal(aLeaveBalace, 'id', 'name');
            return(
                <View>               
                    <LeaveApplicationForm 
                        title={TITLE}
                        data ={{
                            employeeList: aEmpListEnums, 
                            category: aCategoryEnums,
                            leaveType: aLeaveBalanceEnums
                            
                        }}
                        onChangeEmployee={this._onChangeEmployee}
                        visible={this.props.visible}
                        onCancel={() => this.props.onCancel()}
                        onSubmit={this._onFormSubmit}/>

                    <MessageBox
                        promptType={this.state.msgBox.type || ''}
                        show={this.state.msgBox.show || false}
                        onClose={this._msgBoxOnClose}
                        message={this.state.msgBox.msg || ''}
                    />

                    <PromptScreen.PromptGeneric 
                        show= {this.state.loadingScreen.show || false} 
                        title={this.state.loadingScreen.msg || ''}
                    />
                </View>
            );
        }
    }
}

function mapStateToProps (state) {
    return {
        activecompany: state.activeCompanyReducer.activecompany,
        employeelist: state.employees.list,
        leaveCategory: state.transactions.leaveCategory
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            employeelist: bindActionCreators(employeeListActions, dispatch),
            leaveCategory: bindActionCreators(leaveCategoryActions, dispatch),
            leaveApplication: bindActionCreators(leaveApplicationActions, dispatch)
        }
    }
}
  
export default  connect(
    mapStateToProps,
    mapDispatchToProps
)(LeaveApplication)
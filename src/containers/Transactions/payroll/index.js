import React, { Component } from 'react';
import {
    View,
    Text,
    Button
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { withNavigation } from 'react-navigation';

//Children Components
import PayrollGenerationForm from './form';
import * as PromptScreen from '../../../components/ScreenLoadStatus';
import MessageBox from '../../../components/MessageBox';

//Constants
const TITLE = 'GENERATE PAYROLL';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as payrollListActions from '../../Reports/data/payrollSchedule/actions';
import * as payrollTransactionApi from '../../PayrollTransaction/data/all/api';
import * as payrollTransactionActions from '../../PayrollTransaction/data/all/actions';

//helper
import * as oHelper from '../../../helper';

export class PayrollGeneration extends Component {
    constructor(props){
        super(props);
        this.state = {
            calculating: false,
            calculatingMsg: '',
            refreshing: false,
            loadingScreen: {
                show: false,
                msg: 'test'
            },

            msgBox: {
                show: false,
                type: '',
                msg: '',
                param: ''
            },
        }
    }

    _onFormSubmit = async(oData) => {
        console.log('XXXXXXXXXXX_onFormSubmit: ' + JSON.stringify(oData));
        /* this.props.onSubmit(); */
        /* this.props.navigation.navigate('PayrollGeneration'); */
        if(oData.status==3 || oData.status==6){
            await this.props.actions.payrollGeneration.updateActiveRule({payrollid: oData.id});
            this.props.onSubmit();
            this.props.navigation.navigate('PayrollTransaction');
        }else{
            this._setCalculationStatus(true, oData);
            this._generatePayroll(oData);
        }
    }

    _generatePayroll = async (oData) => {
        
        await payrollTransactionApi.generate(oData.id)
            .then((response) => response.json())
            .then(async(res) => {
                this._setCalculationStatus(false, oData);
                console.log('RES_GEN_PAYROLL: ' + JSON.stringify(res));
                if(res.flagno == 1){
                    this._hideForm();
                    await this.props.actions.payrollGeneration.updateActiveRule({payrollid: oData.id});
                    this.props.navigation.navigate('PayrollTransaction');
                    /* this.props.actions.payrollGeneration.update(res.data); */
                    /* this._setMessageBox(true, 'success', res.message); */
                }else{
                    this._setMessageBox(true, 'error-ok', res.message);
                }
            })
            .catch((exception) => {
                this._setMessageBox(true, 'error-ok', exception.message);
                this._setCalculationStatus(false, oData); 
                console.log('RES_GEN_PAYROLL: ' + exception.message);
            });
    }

    _setCalculationStatus = (value, oData) => {
        this.setState({
            calculating: value,
            calculatingMsg: 'Calculating Payroll. Please wait...' + 
                '\nPayroll Date: ' + oHelper.convertDateToString(oData.payrollDate, 'MMMM DD, YYYY')+
                '\nPayroll Period: ' + oHelper.convertDateToString(oData.periodFrom, 'MMM DD, YYYY')+
                ' - ' + oHelper.convertDateToString(oData.periodTo, 'MMM DD, YYYY')
        });
    }

    componentDidMount(){
        this._getDataFromDB();
    }

    _getDataFromDB = () => {
        this.props.actions.payrollList.get(); 
    }

    _hideForm = () => {
        this.props.hideForm(true);
    }

    //Loading Screen and MsgBox Functions
    _setMessageBox = (show, type, msg, param) => {
        this.setState({
            msgBox: oHelper.setMsgBox(
                this.state.msgBox,
                show, 
                type,
                msg,
                param
            )
        })
    }

    _setLoadingScreen = (show, msg) => {
        this.setState({ 
            loadingScreen: oHelper.setLoadingScreen(show, msg)
        });
    }

    _msgBoxOnClose = (params) => {
        this.setState({
            msgBox: oHelper.clearMsgBox(this.state.msgBox)
        })
        if(this.state.allowCloseForm){
            this._hideForm();
        }
    }

    render(){
        const payrollListData = this.props.payrollList.data;
        const payrollListStatus = this.props.payrollList.status;
        console.log('trackingnnnn',payrollListData,payrollListStatus);
        if(this.state.calculating){
            return(
                <PromptScreen.PromptGeneric 
                    show= {true} 
                    title={this.state.calculatingMsg}
                />
            );
        }else if(payrollListStatus[0] != 1){
            
            return(
                <View style={{flex: 1, position: 'absolute'}}>
                    {
                        payrollListStatus[0] == 0 ?
                            <MessageBox
                                promptType={'error-ok'}
                                show={true}
                                onClose={this._hideForm}
                                message={'Unable to fetch Payroll List. Check your internet connection or Please try again.'}
                            />
                        : 
                            payrollListStatus[0] == 2 ?
                                <PromptScreen.PromptGeneric 
                                    show= {true} 
                                    title={'Fetching Payroll List... Please wait...'}
                                />
                            : null

                    }
                </View>
            );
        }else{
            const curPayroll = payrollListData.current;
            const cl_status = payrollListData.cl_status;
            const prevPayroll = payrollListData.previous;
            const aPayrollList = [...curPayroll, ...prevPayroll];
    
            if(curPayroll.length == 0){
                return(
                    <MessageBox
                        promptType={'error-ok'}
                        show={true}
                        onClose={this._hideForm}
                        message={'Cannot process request. No Existing Payroll Schedule found. Set Payroll Policy first.'}
                    />
                );
            }
            
            else{
                return(
                    <View>
                        <PayrollGenerationForm
                            data={aPayrollList}
                            cl_status={cl_status}
                            title={TITLE}
                            visible={this.props.visible}
                            onCancel={() => this.props.onCancel()}
                            onSubmit={this._onFormSubmit}/>

                        <MessageBox
                        promptType={this.state.msgBox.type || ''}
                        show={this.state.msgBox.show || false}
                        onClose={this._msgBoxOnClose}
                        message={this.state.msgBox.msg || ''}
                    />

                        <PromptScreen.PromptGeneric 
                            show= {this.state.loadingScreen.show || false} 
                            title={this.state.loadingScreen.msg || ''}
                        />
                    </View>
                );
            }
        }
        
    }
}

function mapStateToProps (state) {
    return {
        payrollList: state.reports.payrollSchedule
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            payrollList: bindActionCreators(payrollListActions, dispatch),
            payrollGeneration: bindActionCreators(payrollTransactionActions, dispatch)
        }
    }
}
  
export default  withNavigation(connect(
    mapStateToProps,
    mapDispatchToProps
)(PayrollGeneration))
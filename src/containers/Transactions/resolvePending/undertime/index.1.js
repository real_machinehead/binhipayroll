import React, { Component, PureComponent } from 'react';
import {
    View,
    Text,
    FlatList,
    Alert
} from 'react-native';

//Children Components
import PaginatedListContainer from '../../../components/PaginatedListContainer';
import UndertimeReportItem from './item';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as undertimeActions from '../data/undertime/actions';
import * as undertimeApi from '../data/undertime/api';

//Styles
import styles from '../styles';

//Constants
const TITLE = 'UNDERTIME OCCURENCES REPORT';

//helper
import * as oHelper from '../../../helper';

export class UndertimeReport extends Component{
    constructor(props){
        super(props);
        this.state = {
            page: 1,
            hasReachedEnd: false,
            refreshing: false,
            loadingMore: false,
            loadMoreFailed: false,
        }
    }

    _loadDataFromDB = () => {
        const companyid =  this.props.activecompany.id;
        this.setState({
            hasReachedEnd: false,
            loadingMore: false,
            loadMoreFailed: false,
            page: 1,
        })
        
        this.props.actions.undertime.get({
            compid: companyid,
            page: 1,
            filter: 'COMPANY',
        });
    }
    
    _onMount = () => {
        this._loadDataFromDB();
    }

    _onSeeMore = () => {
        this.setState({
            loadMoreFailed: false,
            loadingMore: true,
        }, () => {
            this._loadMoreFromDB();
        });
    }

    _handleLoadMore = () => {
        const {
            loadMoreFailed,
            loadingMore,
            hasReachedEnd,
            refreshing,
        } = this.state;
    
        if (!loadingMore && !hasReachedEnd && !refreshing && !loadMoreFailed) {
            this.setState({
                loadingMore: true,
            }, () => {
                this._loadMoreFromDB();
            });
        }
    }

    _updateDataStore = async(aList) => {
        let oCurData = oHelper.copyObject(this.props.undertime.data);
        let mergedList = await oHelper.arrayMerge(oCurData.resultLists, aList, 'id');
        oCurData.resultLists = mergedList;
        this.props.actions.undertime.update(oCurData);
    }

    _loadMoreFromDB = () => {
        const companyid =  this.props.activecompany.id;
        const { users, page } = this.state;
        const targetPage = page + 1;
        const oPayload = {
            compid: companyid,
            page: targetPage,
            filter: 'COMPANY',
        };

        undertimeApi.get(oPayload)
            .then((response) => response.json())
            .then((res) => {
                console.log('RES : ' + JSON.stringify(res));
                if(res.flagno == 1){
                    
                    this._updateDataStore(res.data.resultLists);
                    this.setState({
                        page: targetPage,
                        loadMoreFailed: false,
                        loadingMore: false,
                        refreshing: false,
                        hasReachedEnd: targetPage == res.data.pages,
                    });
                }else{
                    this._displayLoadMoreFailedMsg(res.message);
                    this.setState({ loadMoreFailed: true });
                }
            })
            .catch(() => {
                this.setState({
                    loadingMore: false,
                    loadMoreFailed: true,
                    refreshing: false,
                });
                this._displayLoadMoreFailedMsg();
            });
    };

    _displayLoadMoreFailedMsg = (msg = null) => {
        this.oCompList.displayLoadMoreFailedMsg();
    }
    
    _renderItem = item => 
        <UndertimeReportItem
            item={item} 
            onCancel={this._onCancelTransaction}
        />

    render(){
        const hideHeader = this.props.hideHeader || false;
        const aStatus = this.props.undertime.status;
        const oData = this.props.undertime.data;
        const { page,
            hasReachedEnd,
            refreshing,
            loadingMore,
            loadMoreFailed
        } = this.state;

        return(
            <PaginatedListContainer 
                title={TITLE}
                hideHeader={hideHeader}
                status={aStatus}
                data={oData}
                onMount={this._onMount}
                onRefresh={this._loadDataFromDB}
                listName='resultLists'
                renderItem={this._renderItem}
                onReachEnd={this._handleLoadMore}
                onSeeMore={this._onSeeMore}
                keyExtractorName='tardinessid'

                page={page}
                hasReachedEnd={hasReachedEnd}
                refreshing={refreshing}
                loadingMore={loadingMore}
                loadMoreFailed={loadMoreFailed}
                ref={(ref) => { this.oCompList = ref; }}
            />
        );
    }
}

function mapStateToProps (state) {
    return {
        undertime: state.reports.undertime,
        activecompany: state.activeCompanyReducer.activecompany,
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            undertime: bindActionCreators(undertimeActions, dispatch),
        }
    }
}
  
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UndertimeReport)

import React, { Component, PureComponent } from 'react';
import {
    View,
    Text,
    FlatList,
    ActivityIndicator,
    Alert
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

//Children Components
//import PaginatedList from '../../../components/PaginatedList';
import UndertimeReportItem from './item';
import CustomCard, {SimpleCard} from '../../../../components/CustomCards';
import GenericContainer from '../../../../components/GenericContainer';
//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as undertimeReportActions from '../../data/ResolvePending/undertime/actions';
import UndertimereportForm from './forms';
//Styles
import styles from './styles';

//Constants
const TITLE = 'UNDERTIME REPORT';
const DESCRIPTION='';
//helper
import * as oHelper from '../../../../helper';
import * as undertimeApi from '../../data/ResolvePending/undertime/api';

export class UndertimeReport extends Component{

    constructor(props){
        super(props);
        this.state = {
            currentPage: 1,
            _showForm: false,
            loadingMore: false,
            loadingMoreFailed: false,
            refreshing: false,
            loadingScreen: {
                show: false,
                msg: 'test'
            },
            msgBox: {
                show: false,
                type: '',
                msg: '',
                param: ''
            },
            activedata:{
                id: '',
                name: '',
                date: '',
                timeout: '',
                undertimeduration: '',
                paydate: '',
                status: '',
                employeeid:'',                
            },
        }
    }

    componentDidMount(){
        if(this.props.undertime.status[0] != 1){
            this._fetchDataFromDB();
        }
    }

    _fetchDataFromDB= () => {
        this.props.actions.undertime.get(
            this._generatePayload(1))
    }

    _generatePayload = (page) => ({
        compid: this.props.activecompany.id,
        page: page,
        //filter: 'COMPANY',
        filter: this.props.filter || 'COMPANY',
        empid: this.props.id,
    })

    _onMount = () => 
        this.props.actions.undertime.get(
            this._generatePayload(1)
    );

    _onRefresh = async() => 
        await this.props.actions.undertime.refresh(
            this._generatePayload(1)
    );

    _onLoadMore = async(page) => 
        await this.props.actions.undertime.loadMore(
            this._generatePayload(page)
        );


   _setLoadingScreen = (show, msg) => {
        let oLoadingScreen = {...this.state.loadingScreen};
        oLoadingScreen.show = show;
        oLoadingScreen.msg = msg;
        this.setState({ loadingScreen: oLoadingScreen });
    }

    _renderItem = (oItem) => {
        const ICONNAME_PENDING = 'alert-circle-outline';
        const ICONNAME_RESOLVED = 'check-circle-outline';
        const item = oItem.views;
        const itemStyles = styles.reportItemStyles;
        console.log('itemmmmmms',oItem);
            const menu = 
                oItem.statusid == 5 ? 
                    [
                        { onSelect:() => this._onConfirm(oItem), label: 'Confirm' },
                        { onSelect: () => this._modifydtr(oItem,true), label: 'Modify DTR' }
                    ]
                :
                    []
    
            const backgroundColor = 
                oItem.statusid == 4 ? 
                        {backgroundColor: 'rgba(21, 123, 19, 0.86)'}
                    :
                        {backgroundColor: 'rgba(193, 66, 66, 0.94)'}
        
            const oTitle = 
                    <View style={itemStyles.title.contContent}>
                        <View style={itemStyles.title.contIcon}>
                            <View style={itemStyles.title.iconPlaceholder}>
                                <Icon 
                                    name={
                                        item.statusid == 4 ? ICONNAME_RESOLVED : ICONNAME_PENDING
                                    } 
                                    size={30} 
                                    color='#f4f4f4'
                                />
                            </View>
                        </View>
                        <View style={itemStyles.title.contLabel}>
                            <Text style={itemStyles.title.txtLabel}>
                                {/*item.lateduration + ' ' + item.latedurationunit*/item[3][1]}
                            </Text>
                            <Text style={itemStyles.title.txtDescription}>
                                { oHelper.convertDateToString(item.date, 'MMMM DD, YYYY') }
                            </Text>
                            <Text style={itemStyles.title.txtDescription}>
                                { /*item.statusname */ item[5][1]}
                            </Text>
                        </View>
                    </View>
    
            return(
                <SimpleCard 
                    //data={oItem.views[0]}
                /*customTitleStyle={[itemStyles.title.container, backgroundColor]}
                oTitle={oTitle}
                menu={menu}*/
                    //data={oItem.views}
                            data={[
                                ['Id',oItem.undertimeid],
                                ['Name', oItem.views[0][1]], 
                                ['Date', oItem.views[1][1]], 
                                ['Time-out',oItem.views[2][1]],
                                ['Undertime Duration', oItem.views[3][1]], 
                                ['Penalty Date', oItem.views[4][1]],
                                ['Status',oItem.views[6][1]]
                            ]
                        }
                    customTitleStyle={[itemStyles.title.container, backgroundColor]}
                    oTitle={oTitle}
                    menu={menu}
                />
              );
    }
    
    _onConfirm = async(oItem) => {
  
        Alert.alert(
            "Confirm Action",
            "Are you sure you want to confirm Undertime Occurence ?",
            [
                {text: 'NO', onPress: () => {}},
                {text: 'YES', onPress: () => {this._confirmToDB(oItem)}},
            ],
            { cancelable: false }
        )
    }
    
    _keyExtractor = (item, index) => item. undertimeid;


    _setMessageBox = (show, type, msg, param) => {
        this.setState({
            msgBox: oHelper.setMsgBox(
                this.state.msgBox,
                show, 
                type,
                msg,
                param
            )
        })
    }

    _onSubmitForm = (oData) => {
        console.log('dataaaaaa',oData)
        if(oHelper.isStringEmptyOrSpace(oData.id)){
            this._addToDB(oData);
        }else{
            Alert.alert(
                'Confirm Action',
                'Are you sure you want to update ' + oData.name + ' ?',
                [
                    {text: 'No', onPress: () => {}},
                    {text: 'Yes', onPress: () => this._updateToDB(oData)},
                ],
                { cancelable: false }
            ) 
        }
    }

    _msgBoxOnClose = (params) => {
        this.setState({
            msgBox: oHelper.clearMsgBox(this.state.msgBox)
        })
    }


    /*_confirmToDB = async(oItem) => {
        this.oCompList.setLoadingScreen(true, 'Confirming Overtime Occurence. Please wait...');
        let oInput = oHelper.copyObject(oItem);
        oInput.compid = this.props.activecompany.id;
        oInput.id = oItem.overtimeid;
        const oRes = await this.props.actions.overtime.confirm(oInput);
        this.oCompList.setMessageBox(true, oRes.flagno==1 ? 'success' : 'error-ok', oRes.message);
        this.oCompList.setLoadingScreen(false);
    }*/


    _confirmToDB = async(oItem) => {   
        
        let oInput = oHelper.copyObject(oItem);
        oInput.compid = this.props.activecompany.id;
 
        oInput.id = oItem.undertimeid;
        
        this._setLoadingScreen(true, 'Resolving Undertime Validation. Please wait...');
        await undertimeApi.confirm(oInput)
            .then((response) => response.json())
            .then((res) => {
                console.log('RES: ' + JSON.stringify(res));
                if(res.flagno == 1){
                    oItem.statusid = 4;
                    this.props.actions.undertime.confirm(oInput);
                    this._closeForm();
                }
                this._setMessageBox(true, res.flagno==1 ? 'success' : 'error-ok', res.message);
                this._fetchDataFromDB();    
            })
            .catch((exception) => {
                this._setMessageBox(true, 'error-ok', exception.message);
            });
        this._setLoadingScreen(false);


    }

    _updateToDB = async(oData) => {
        console.log('ConfirmItems',oData);
        //oData.timeout=oHelper.convertDateToString(oData.timeout, 'hh:mm:ss A')
        let oInput = oHelper.copyObject(oData);
        oInput.compid = this.props.activecompany.id;
        oInput.id = oData.id;
        oInput.code="1010";
        oInput.statusid=4;
        oInput.employeeid=oData.employeeid;
        oInput.employeename=oData.name;
        //oInput.newtime=oData.timeout;
        oInput.newtime=oHelper.convertDateToString(oData.timeout, 'hh:mm:ss A');
        oInput.statusid=4;
        //console.log('UpdateDbbbb',oInput);
        this._setLoadingScreen(true, 'Updating Undertime Validation. Please wait...');
        await undertimeApi.update(oInput)
            .then((response) => response.json())
            .then((res) => {
                console.log('RES: ' + JSON.stringify(res));
                if(res.flagno == 1){
                    this.props.actions.undertime.updates(oInput)
                    this.props.actions.undertime.confirm(oInput);
                    this._closeForm();
                }
                this._setMessageBox(true, res.flagno==1 ? 'success' : 'error-ok', res.message);
                this._fetchDataFromDB();    
            })
            .catch((exception) => {
                this._setMessageBox(true, 'error-ok', exception.message);
            });
        this._setLoadingScreen(false);
    
    } 


    _modifydtr=(item,_showForm)=>{
        console.log('itemssssssssssss',item,_showForm);

        this.setState({
            _showForm:_showForm,
            //activedata:item.views
            activedata:{
                id: item.undertimeid,
                name: item.views[0][1],
                date: item.views[1][1],
                //timeout:item.views[2][1],
                undertimeduration:item.views[3][1],
                paydate:item.views[4][1],
                status:item.views[5][1],
                employeeid:item.employeeid
            }
        })
        if(_showForm){        
            console.log('show Form',_showForm);

        }
            
    }

    _closeForm = () => {
        this.setState({
            _showForm: false
        })
    }

    render(){
        const {status, data} = this.props.undertime;
        const hideHeader = this.props.hideHeader || false;
        const activedata = this.state.activedata;
        console.log('datasssss11',this.props.undertime);

        return(

            <GenericContainer
                    customContainer = {{flex: 1}}
                    msgBoxShow = {this.state.msgBox.show}
                    msgBoxType = {this.state.msgBox.type}
                    msgBoxMsg = {this.state.msgBox.msg}
                    msgBoxOnClose = {this._msgBoxOnClose}
                    msgBoxOnYes = {this._msgBoxOnYes}
                    containerStyle = {[styles.container, this.props.containerStyle || {}]}
                    msgBoxParam = {this.state.msgBox.param}
                    loadingScreenShow = {this.state.loadingScreen.show}
                    loadingScreenMsg = {this.state.loadingScreen.msg}
                    status={status}
                    title={TITLE}
                    onRefresh={this._fetchDataFromDB}>         
                {

                    data ?

                        <CustomCard 
                            contentContainerStyle={styles.container}
                            clearMargin
                            title={TITLE} 
                            oType='text'
                            description = {DESCRIPTION}
                        >
    
                            {
                                data.length < 1 ?
                                    <EmptyList message='No Undertime  Record' />
                                :
                                    <FlatList
                                        refreshing={this.state.refreshing}
                                        onRefresh={this._fetchDataFromDB}
                                        contentContainerStyle={styles.reportsFlatlist}
                                        ref={(ref) => { this.flatListRef = ref; }}
                                        extraData={data}
                                        data={data.resultLists}
                                        keyExtractor={this._keyExtractor}
                                        renderItem={({item}) => this._renderItem(item) }
                                    />
                            }
                            
                        </CustomCard> 
                    :
                        null
                /*<PaginatedList
                        hideHeader={hideHeader} 
                        title={TITLE}
                        status={status}
                        data={data}
                        onMount={this._onMount}
                        onRefresh={this._onRefresh}
                        onLoadMore={this._onLoadMore}
                        listName='list'
                        renderItem={this._renderItem}
                        ref={(ref) => { this.oCompList = ref; }}
                        pageName="totalpages"
                />*/
                }   
                {
                    this.state._showForm ?
                        <UndertimereportForm 
                            onCancel={this._closeForm}
                            onSubmit={this._onSubmitForm}
                            visible={this.state._showForm}
                            title='MODIFY DTR'
                            data={this.state.activedata}
                        />   
                    :
                        null
                }
            </GenericContainer>

        );
    }
}

function mapStateToProps (state) {
    return {
        undertime: state.reports.undertime,
        activecompany: state.activeCompanyReducer.activecompany,
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            undertime: bindActionCreators(undertimeReportActions, dispatch),
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UndertimeReport)
import React, { Component, PureComponent } from 'react';
import {
    View,
    Text
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

//Styles
import { SimpleCard } from '../../../../components/CustomCards';
import styles from '../styles';

//helper
import * as oHelper from '../../../../helper';

const ICONNAME_PENDING = 'alert-circle-outline';
const ICONNAME_RESOLVED = 'check-circle-outline';

export default class NoTimeInReportItem extends PureComponent{

    _onConfirm = () => {
        this.props.onConfirm(this.props.item);
    }

    _onApprove = () => {
        alert('I AM APPROVED');
    }

    _modifydtr=()=>{
        this.props.onModify(this.props.item);
    }
    render(){
        const ICONNAME_PENDING = 'alert-circle-outline';
        const ICONNAME_RESOLVED = 'check-circle-outline';
        const oItem = this.props.item;
        const itemStyles = styles.reportItemStyles;
        console.log('itemmmmmms',oItem);
            const menu = 
                oItem.statusid == 5 ? 
                    [
                        { onSelect:() => this._onConfirm(oItem), label: 'Confirm' },
                        { onSelect: () => this._modifydtr(oItem,true), label: 'Modify DTR' }
                    ]
                :
                    []
    
            const backgroundColor = 
                oItem.statusid == 4 ? 
                        {backgroundColor: 'rgba(21, 123, 19, 0.86)'}
                    :
                        {backgroundColor: 'rgba(193, 66, 66, 0.94)'}
        
            const oTitle = 
                    <View style={itemStyles.title.contContent}>
                        <View style={itemStyles.title.contIcon}>
                            <View style={itemStyles.title.iconPlaceholder}>
                                <Icon 
                                    name={
                                        oItem.statusid == 4 ? ICONNAME_RESOLVED : ICONNAME_PENDING
                                    } 
                                    size={30} 
                                    color='#f4f4f4'
                                />
                            </View>
                        </View>
                        <View style={itemStyles.title.contLabel}>
                            <Text style={itemStyles.title.txtLabel}>
                                {/*item.lateduration + ' ' + item.latedurationunit*/oItem.views[3][1]}
                            </Text>
                            <Text style={itemStyles.title.txtDescription}>
                                { oHelper.convertDateToString(oItem.date, 'MMMM DD, YYYY') }
                            </Text>
                            <Text style={itemStyles.title.txtDescription}>
                                { /*item.statusname  oItem[5][1] */}
                            </Text>
                        </View>
                    </View>

        return (
            /*<SimpleCard
                data={oItem.views}
                customTitleStyle={[itemStyles.title.container, backgroundColor]}
                oTitle={oTitle}
                menu={menu}
            />*/
            <SimpleCard 
                    //data={oItem.views[0]}
                //customTitleStyle={[itemStyles.title.container, backgroundColor]}
                oTitle={oTitle}
                menu={menu}
                    //data={oItem.views}
                            data={[
                                ['Id',oItem.notimeinid],
                                ['Name', oItem.views[0][1]], 
                                ['Date', oItem.views[1][1]], 
                                //['Time-in',oItem.views[2][1]],
                                //['Time-out', oItem.views[3][1]], 
                                ['Status',oItem.views[5][1]]
                            ]
                        }
                    customTitleStyle={[itemStyles.title.container, backgroundColor]}
                    oTitle={oTitle}
                    menu={menu}
            />
        )
    }
}
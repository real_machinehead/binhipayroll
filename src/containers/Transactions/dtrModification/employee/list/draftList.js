//RN Packages andd Modules
import React, { Component } from 'react';
import {
    View, 
    Text
} from 'react-native';
import { withNavigation } from 'react-navigation';

//Styles
import styles from './styles';

//Children Components
import GenericContainer from '../../../../../components/GenericContainer';
import PaginatedListContainer from '../../../../../components/PaginatedListContainer';
import EmployeeListItem from './item';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as employeeListActions from '../../../data/employee/data/list/actions';
import * as activeProfileActions from '../../../data/employee/data/activeProfile/actions';
import * as draftlistActions from '../../../data/employee/data/draft/actions';
import * as draftlistApi from '../../../data/employee/data/draft/api'

const TITLE = 'DRAFT EMPLOYEES';

//helper
import * as oHelper from '../../../../../helper';

class EmployeeDraftList extends Component {
    constructor(props){
        super(props);
        this.state = {
            page: 1,
            hasReachedEnd: false,
            refreshing: false,
            loadingMore: false,
            loadMoreFailed: false,

            _activeItem: null,
            filters: {},

            loadingScreen: {
                show: false,
                msg: 'test'
            },

            msgBox: {
                show: false,
                type: '',
                msg: '',
                param: ''
            },
        }
    }

    _onMount = () => {
        this._initData();
    }

    _initData = () => {
        this.props.actions.draftlist.get({
            compid: this.props.activecompany.id,
            isdraft: true
        });
    }

    _renderItem = item => {
        return (
            <EmployeeListItem 
                item={item} 
                isDraft
                keyName='empid'
                onPress={this._setActiveItem}
                activeKey={this.state._activeItem}
            />
        );
    }

    _handleLoadMore = () => {
    }

    _setActiveItem = async(value) => {
        const navigation = this.props.logininfo.navigation;
        this._setLoadingScreen(true, 'Fetching Draft Employee Information. Please wait...');
        await draftlistApi.getProfile({ employeeid: value.empid })
            .then((response) => response.json())
            .then(async(res) => {
                console.log('RES: ' + JSON.stringify(res));
                if(res.flagno == 1){
                    console.log('YYYYYYYYYYYYYYYYY_value: ' + JSON.stringify(value));
                    let employee = res.data;
                    employee.id = employee.empid;
                    employee.isdraft = true;
                    await this.props.actions.activeProfile.updateAllInfo({employee});
                    let strRouteName = '';
                    switch(value.index_start){
                        case 1:
                            strRouteName = 'EmployeeBasicInfo';
                            break;
                        case 2:
                            strRouteName = 'EmployeeAddress';
                            break;
                        case 3:
                            strRouteName = 'EmployeeDependents';
                            break;
                        case 4:
                            strRouteName = 'EmployeeBankAccount';
                            break;
                        case 5:
                            strRouteName = 'EmployeeDetails';
                            break;
                        case 6:
                            strRouteName = 'EmployeeWorkShift';
                            break;
                        case 7:
                            strRouteName = 'EmployeeBenefits';
                            break;
                        case 8:
                            strRouteName = 'RankBasedRules';
                            break;
                        default:
                            strRouteName = '';
                            break;
                    }
                    navigation.navigate(strRouteName);
                }else{
                    this._setMessageBox(true, 'error-ok', res.message);
                }
            })
            .catch((exception) => {
                this._setMessageBox(true, 'error-ok', exception.message);
            });
        this._setLoadingScreen(false);
    }

    _onSeeMore = () => {
    }

    _setMessageBox = (show, type, msg, param) => {
        this.setState({
            msgBox: oHelper.setMsgBox(
                this.state.msgBox,
                show, 
                type,
                msg,
                param
            )
        })
    }

    _setLoadingScreen = (show, msg) => {
        let oLoadingScreen = {...this.state.loadingScreen};
        oLoadingScreen.show = show;
        oLoadingScreen.msg = msg;
        this.setState({ loadingScreen: oLoadingScreen });
    }

    _msgBoxOnClose = (params) => {
        this.setState({
            msgBox: oHelper.clearMsgBox(this.state.msgBox)
        })
    }

    render(){
        const { 
            page,
            hasReachedEnd,
            refreshing,
            loadingMore,
            loadMoreFailed,
        } = this.state;

        /* const aStatus = [1, '']; */
        const listStyles = styles.listStyles;
        const oData = this.props.draftlist;
        const aStatus = oData.status;
        const aList = aStatus[0] == 1 && oData ? oData.data : [];
        
        return(
            <GenericContainer
                containerStyle={{backgroundColor: 'transparent'}}
                msgBoxShow = {this.state.msgBox.show}
                msgBoxType = {this.state.msgBox.type}
                msgBoxMsg = {this.state.msgBox.msg}
                msgBoxOnClose = {this._msgBoxOnClose}
                msgBoxOnYes = {this._msgBoxOnYes}
                msgBoxParam = {this.state.msgBox.param}
                loadingScreenShow = {this.state.loadingScreen.show}
                loadingScreenMsg = {this.state.loadingScreen.msg}
                status={[1,'Success']}
                title={TITLE}>

                <PaginatedListContainer 
                    containerStyle = {{backgroundColor: 'transparent'}}
                    title={TITLE}
                    hideHeader={true}
                    status={aStatus}
                    extraData={aList}
                    data={aList}
                    onMount={this._onMount}
                    onRefresh={this._initData}
                    renderItem={this._renderItem}
                    onReachEnd={this._handleLoadMore}
                    onSeeMore={this._onSeeMore}
                    keyExtractorName='empid'

                    page={page}
                    hasReachedEnd={hasReachedEnd}
                    refreshing={refreshing}
                    loadingMore={loadingMore}
                    loadMoreFailed={loadMoreFailed}
                    ref={(ref) => { this.oCompList = ref; }}

                    endOfResultsCustomStyle={{height: 20, backgroundColor: '#2e3030'}}
                />

            </GenericContainer>
        )
    }
}


function mapStateToProps (state) {
    return {
        logininfo: state.loginReducer.logininfo,
        activeProfile: state.employees.activeProfile,
        employeelist: state.employees.list,
        draftlist: state.employees.draft,
        activecompany: state.activeCompanyReducer.activecompany,
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            employeelist: bindActionCreators(employeeListActions, dispatch),
            activeProfile: bindActionCreators(activeProfileActions, dispatch),
            draftlist: bindActionCreators(draftlistActions, dispatch)
            
        }
    }
}
  
export default withNavigation(connect(
    mapStateToProps,
    mapDispatchToProps
)(EmployeeDraftList));
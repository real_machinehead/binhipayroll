import React, { Component } from 'react';
import { TabNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';

import {store} from '../../../../../store';
import * as activeProfile from './../../../data/employee/data/activeProfile/actions';

import EmployeeGenericList from './genericList';
import EmployeeDraftList from './draftList';

const EmployeeListTabs = TabNavigator({
    EmployeeGenericList: {
    screen: EmployeeGenericList,
    navigationOptions: {
        tabBarLabel: 'List',
        tabBarOnPress: (scene, jumpToIndex) => {
          store.dispatch(activeProfile.hideSummary());
          jumpToIndex(scene.index); 
        },
    }
},

    EmployeeDraftList: {
        screen: EmployeeDraftList,
        navigationOptions: {
          tabBarLabel: 'Drafts',
          tabBarOnPress: (scene, jumpToIndex) => {
            store.dispatch(activeProfile.hideSummary());
            jumpToIndex(scene.index); 
          },
        },
    },
},
  {
    animationEnabled: true,
    tabBarPosition: 'top',
    swipeEnabled: false,
    tabBarOptions: {
      showIcon: false,
      showLabel: true,
      scrollEnabled: false,
      activeTintColor: '#FFF',
      inactiveTintColor: '#202626',
      tabStyle: { height: 33},
      labelStyle: {
        fontSize: 11,
        /* fontWeight: '500' */
      },
      style: {
        backgroundColor: '#818489',
        zIndex: 999
      },
      indicatorStyle: {
        backgroundColor: '#EEB843',
        height: 5
      }
    },
    lazy: true
  }
);

export default EmployeeListTabs;
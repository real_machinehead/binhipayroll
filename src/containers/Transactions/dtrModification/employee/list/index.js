//Packages
import React, { Component, PureComponent } from 'react';
import { View, Text } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { withNavigation } from 'react-navigation';

import ActionButton from '../../../../../components/ActionButton';
import GenericContainer from '../../../../../components/GenericContainer';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as activeProfileActions from './../../../data/employee/data/activeProfile/actions';
import * as employeeApi from './../../../data/employee/data/activeProfile/api';

//helper
import * as oHelper from '../../../../../helper';

//Styles
import styles from './styles';

//tabs
import EmployeeListTabs from './tabs';

class EmployeeList extends Component {
    constructor(props){
        super(props);
        this.state={
            _status: [1, 'Success'],
            loadingScreen: {
                show: false,
                msg: 'test'
            },

            msgBox: {
                show: false,
                type: '',
                msg: '',
                param: ''
            },
        }
    }

    componentDidMount = () => {
        this.props.actions.activeProfile.hideSummary(false);
    }
    
    _addNewEmployee = async() => {
        this._setLoadingScreen(true, 'Initializing Employee Form, please wait...');
        await employeeApi.getDefaultData()
            .then((response) => response.json())
            .then(async(res) => {
                if(res.flagno == 1){
                    let employee = res.data;
                    employee.id = '';
                    await this.props.actions.activeProfile.updateAllInfo({employee});
                    this.props.navigation.navigate('AddEmployeeForm');
                }else{
                    this._setMessageBox(true, 'error-ok', res.message);
                }
            })
            .catch((exception) => {
                this._setMessageBox(true, 'error-ok', exception.message);
            });
        this._setLoadingScreen(false, 'Initializing Employee Form, please wait...');
    }

    _setMessageBox = (show, type, msg, param) => {
        this.setState({
            msgBox: oHelper.setMsgBox(
                this.state.msgBox,
                show, 
                type,
                msg,
                param
            )
        })
    }

    _setLoadingScreen = (show, msg) => {
        let oLoadingScreen = {...this.state.loadingScreen};
        oLoadingScreen.show = show;
        oLoadingScreen.msg = msg;
        this.setState({ loadingScreen: oLoadingScreen });
    }

    _msgBoxOnClose = (params) => {
        this.setState({
            msgBox: oHelper.clearMsgBox(this.state.msgBox)
        })
    }

    render(){
        //console.log('tesssssssssstttt1234',this.props.activecompany);
          return(
            <GenericContainer
                containerStyle={styles.leftCont}
                msgBoxShow = {this.state.msgBox.show}
                msgBoxType = {this.state.msgBox.type}
                msgBoxMsg = {this.state.msgBox.msg}
                msgBoxOnClose = {this._msgBoxOnClose}
                msgBoxOnYes = {this._msgBoxOnYes}
                msgBoxParam = {this.state.msgBox.param}
                loadingScreenShow = {this.state.loadingScreen.show}
                loadingScreenMsg = {this.state.loadingScreen.msg}
                status={[1,'Success']}
                title={'Employee List'}

                onRefresh={this._fetchDataFromDB}>
                <LinearGradient 
                    colors={['#818489', '#3f4144', '#202626']}
                    style={styles.leftCont}>
                    <LinearGradient 
                        colors={['#818489', '#3f4144', '#202626']}
                        style={styles.contTitle}>

                        
                        <View style={styles.contTitleName}>
                            <Text style={styles.txtTitle}>
                                {this.props.activecompany.name.toUpperCase()}
                            </Text>
                        </View>
                    
                    </LinearGradient>
                    <EmployeeListTabs />
                    {
                        this.props.viewOnly || false ?
                            null
                        :
                            <ActionButton onPress={this._addNewEmployee}/>
                    }
                </LinearGradient>
            </GenericContainer>
        )
    }
}

function mapStateToProps (state) {
   
    return {
        activecompany: state.activeCompanyReducer.activecompany,
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            activeProfile: bindActionCreators(activeProfileActions, dispatch)
        }
    }
}

export default withNavigation(connect(
    mapStateToProps,
    mapDispatchToProps
)(EmployeeList))
import React, { Component } from 'react';
import {
    View,
    Text,
    Button
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Header2 from '../../Headers/header2';

//Styles
import styles from '../styles';

//Children Components
import EmployeeDTR from './DTR';
import EmployeeList from './employee/list';

//Custom Components
import GenericContainer from '../../../components/GenericContainer';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
//import * as employeeListActions from '../../Employees/data/list/actions';
import * as employeeListActions from '../data/dtrModification/actions';

//Constants
const TITLE = 'Loading Employee List';

export class DTRModification extends Component {
    constructor(props){
        super(props);
        this.state={
            _summaryStatus: [2, 'Loading...'],
            _bHasMounted: false
        }
    }

    static navigationOptions = {
        headerTitle : 
            <Header2
                title= 'DTR MODIFICATION'
            />,
        headerLeft: null,
        headerStyle : 
            {backgroundColor: '#fff'},
    }

    componentDidMount = () => {
        if(this.props.employeelist.status[0] != 1){
            this._getEmployeeListFromDB();
        }
        this.setState({ _bHasMounted: true });
    }

    _generatePayload = (page) => ({
        compid: this.props.activecompany.id,
        page: page,
        //filter: 'COMPANY',
        filter: this.props.filter || 'COMPANY',
        empid: this.props.id || 0,
    })
    
    _getEmployeeListFromDB = () => {
        //this.props.actions.dtrModifcation.get(this._generatePayload(1));
        

        /*this.props.actions.notimeinReport.get(
            this._generatePayload(1)*/
    }
    
    _setActiveProfileStatus = (status) => {
        this.setState({ _summaryStatus: status });
    }

    render(){
        /*console.log('_summaryStatus: ' + this.state._summaryStatus);
        console.log('ENTERED LEAVE APPLICATION COMPONENT!');*/
        const status=[1,'success'];
        //console.log('aaaaaaaaaaaaaaaaaa',this.props.activeProfile)
        //console.log('Actions',this.props.actions);
        const {empdata,empstatus}=this.props.employeelist;
        const empProfile=this.props.activeProfile;
        return(
            this.state._bHasMounted ?
                <GenericContainer 
                    containerStyle={styles.containerFlexRow}
                    //status={this.props.employeelist.status}
                    status={status}
                    title={TITLE}
                    onRefresh={this._getEmployeeListFromDB}>

                    <EmployeeList 
                        activeProfileStatus={empstatus}
                        viewOnly={true}/>
                    <EmployeeDTR status={empstatus} activeprofile={empProfile}/>

                </GenericContainer>
            :
                <View style={styles.containerFlexRow}></View>
        );
    }
}

function mapStateToProps (state) {
   
    return {
        logininfo: state.loginReducer.logininfo,
        activecompany: state.activeCompanyReducer.activecompany,
        employeelist: state.employees.list,
        dtrModifcation:state.transactions.dtrModifcation,
        activeProfile: state.employees.activeProfile,
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            dtrModifcation: bindActionCreators(employeeListActions, dispatch)
        },
    }
}
  
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DTRModification)
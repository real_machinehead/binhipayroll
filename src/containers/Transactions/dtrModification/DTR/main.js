import React, { Component } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  ActivityIndicator
} from 'react-native';

//Children Components
import * as PromptScreen from '../../../../components/ScreenLoadStatus';

//Styles
import styles from './styles';
import DTRCalendar from './calendar';
import DTRHeader from './header';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as empDtrActions from '../../data/DTR/actions';

import { CONSTANTS } from '../../../../constants';

export class EmployeeDTRCalendar extends Component {
    constructor(props){
        super(props);
        this.state = {
            _bHasError: false,
            _bDidMount: false,
            payrollid:'',
            employeeid:'',
        }
    }

    componentDidMount(){
    
        this._getDataFromDB('');
        //console.log('logggsssssssssss234',this.props.activeprofile)
    }

   /* _getDataFromDB=(id)=>{
       
        console.log('loooooogggggggggggsss',id)
        //this.props.actions.empDtr.getpayroll({employeeid:this.props.employees.activeProfile.data.id});

    }*/
    componentWillReceiveProps(nextProps){
        //console.log('logggsssssssssss23455',nextProps.activeProfile.data.id);
        const empid=nextProps.activeProfile.data.id;
        //this._getDataFromDB(empid);
        if(empid !=this.state.employeeid){
            this.setState({
                employeeid:empid
            })
            this._getDataFromDB(empid);
        }
    }
    _getDataFromDB = (id) => {
     
        /*if(this.props.isCalledByActiveRule || false){
            console.log('XXXXXXXXXXXXXXXTRUE_isCalledByActiveRule');
            const oActiveRule = this.props.empDtr.activeRule;
            this.props.actions.empDtr.getpayroll({payrollid: oActiveRule.payrollid, employeeid: id})
        }else{
            console.log('XXXXXXXXXXXXXXFALSE_isCalledByActiveRule',this.props.employees.activeProfile.data);
            const activeProfile = this.props.employees.activeProfile.data;
            this.props.actions.empDtr.getpayroll({payrollid: id, employeeid: id});
        }*/
        this.props.actions.empDtr.getpayroll({payrollid: id, employeeid: id});
    }
    
    _updateDTRElement = (payload) => {
        console.log('loooooogggggggggggsss',payload);
        this.props.actions.empDtr.updateElement(payload)
    }
    

    _onRefresh = () => {
        this._getDataFromDB('');
    }
    
        render()
        {
            //console.log('logggggggggggssssssssssss',this.props.employees.activeProfile.data);
            //console.log('logggsssssssssss',this.props.activeProfile)
            console.log('renderlogggggggggggggg',this.props.empDtr);
            if(this.props.empDtr.status[0] == 1){
                /* alert('empDtr.data: ' + JSON.stringify(this.props.empDtr.data)); */
                return(
                    <View style={styles.container}>
                        <View style={styles.dividerHeader}>
                            <DTRHeader 
                                data={this.props.empDtr.data}
                                onPeriodSwitch={this._getDataFromDB}
                            />
                        </View>
                        <View style={styles.dividerBody}>
                            <DTRCalendar 
                                data={this.props.empDtr.data} 
                                activeEmployee={this.props.employees.activeProfile.data}
                                updateDTRElement={this._updateDTRElement}
                            />
                        </View>
                    </View>
                )
            }else if(this.props.empDtr.status[0] == 0){
                return (
                    <PromptScreen.PromptError title={'Daily Time Record. Check Employee Data.'} onRefresh={()=>this._getDataFromDB('')}/>
                );
            }

            else{
                return(
                    <View style={styles.emptyContainer}>
                        <ActivityIndicator size="small" color="#EEB843" />
                    </View>
                )
            }
        }
}


function mapStateToProps (state) {
    console.log('statessssssss',state.transactions.dtr);
    return {
        empDtr: state.transactions.dtr,
        //empDtr: state.dtr,
        employees: state.employees,
        activeProfile: state.employees.activeProfile,
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            empDtr: bindActionCreators(empDtrActions, dispatch),
        }
    }
}
  
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EmployeeDTRCalendar)



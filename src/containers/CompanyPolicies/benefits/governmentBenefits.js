import React, { Component } from 'react';
import {
    View,
    Text,
    Switch,
    Picker,
    TimePickerAndroid,
    ScrollView,
    TextInput,
    TouchableOpacity,
    RefreshControl,
    TouchableNativeFeedback,
    ToastAndroid,
    FlatList,
    CheckBox
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import CustomCard, 
{
    Description,
    PropTitle,
    PropLevel1, 
    PropLevel2
}
from '../../../components/CustomCards';

//Styles
import styles from '../styles'

const title_Gov = 'Government Benefits';
const description_Gov= 'Allow SSS, PAGIBIG, and PhilHealth';
const color_SwitchOn='#838383';
const color_SwitchOff='#505251';
const color_SwitchThumb='#EEB843';

class GovernmentBenefits extends Component{
    _toggleSwitch = (value) => {
        this.props.toggleSwitch('GOVERNMENT', value);
    }

    _toggleCheckbox = (id, value) => {
        this.props.toggleCheckbox(id, value)
    }

    _openForm = (oBenefit) => {
        this.props.openForm(oBenefit)
    }

    render(){
        const govCheckbox = (id, value) => (
            <CheckBox
                onValueChange={ (val) => {this._toggleCheckbox(id, val)}} 
                value={value}
            />
        );

        const govName = (value) => (
            <View>
            </View>
        );

        const govId = (value) => (
            <View>
            </View>
        );
        
        return(
            <CustomCard 
                title={title_Gov} 
                description={description_Gov} 
                oType='Switch'
                rightHeader={
                    <Switch
                        onValueChange={ (value) => this._toggleSwitch(value)} 
                        onTintColor={color_SwitchOn}
                        thumbTintColor={color_SwitchThumb}
                        tintColor={color_SwitchOff}
                        value={ this.props.data.enabled } 
                    />
                }
            >

            {
                this.props.data.enabled ?
                    <View style={{marginTop: -20}}>
                        <PropTitle name='Government Benefits Table'/>
                        {/* <View>
                            {govCheckbox()}
                            {govName()}
                            {govId()}
                        </View> */}
                        <View style={styles.benefitsCont}>
                            <View style={[styles.breakTimeDetailsCont, styles.breakHeaderBorder]}>
                                <View style={styles.benefitsNameCont}>
                                    <Text style={styles.txtBreakName}>NAME</Text>
                                </View>
                                <View style={styles.benefitsAmountCont}>
                                    <Text style={styles.txtDefault}>COMPANY ID</Text>
                                </View>
                                <View style={[styles.benefitsDetailsCont, {width: 50}]}>
                                    <Text style={styles.txtDefault}>ACTIVATE</Text>
                                </View>
                            </View>
                            
                            {
                                this.props.data.data.map((oBenefit, index) => (
                                    <TouchableNativeFeedback
                                        key={index}
                                        onPress={() => this._openForm(oBenefit)}
                                        background={TouchableNativeFeedback.SelectableBackground()}>
                                        <View style={styles.breakTimeDetailsCont}>
                                        

                                            <View style={styles.benefitsNameCont}>
                                                <Text style={[styles.txtDefault, {paddingLeft: 20}]}>
                                                    {oBenefit.name}
                                                </Text>
                                            </View>

                                            <View style={styles.benefitsAmountCont}>
                                                <Text style={styles.txtDefault}>
                                                    {oBenefit.compid}
                                                </Text>
                                            </View>

                                            <View style={[styles.benefitsDetailsCont, {width: 50}]}>
                                                {govCheckbox(oBenefit.id, oBenefit.enabled)}
                                            </View>
                                        </View>
                                    </TouchableNativeFeedback>
                                ))
                            }
                        </View>
                    </View>
                :
                    <Description 
                        enabled={this.props.data.description.enabled}
                        disabled={this.props.data.description.disabled}/>
            }

            </CustomCard>
        )
    }
}

export default GovernmentBenefits;
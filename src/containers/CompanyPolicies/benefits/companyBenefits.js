import React, { Component } from 'react';
import {
    View,
    Text,
    Switch,
    Picker,
    TimePickerAndroid,
    ScrollView,
    TextInput,
    TouchableOpacity,
    RefreshControl,
    TouchableNativeFeedback,
    ToastAndroid,
    FlatList,
    CheckBox
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import CustomCard, 
{
    Description,
    PropTitle,
    PropLevel1, 
    PropLevel2
}
from '../../../components/CustomCards';

//Styles
import styles from '../styles'

const title_Comp = 'Company Benefits';
const description_Comp= 'Add Company Allowances';
const color_SwitchOn='#838383';
const color_SwitchOff='#505251';
const color_SwitchThumb='#EEB843';

class CompanyBenefits extends Component{
    /* shouldComponentUpdate(nextProps, nextStates){
        return (JSON.stringify(this.props.data) !== JSON.stringify(nextProps.data));
    } */

    _toggleSwitch = (value) => {
        this.props.toggleSwitch('COMPANY', value);
    }

    _openForm = (oBenefit) => {
        let objBenefit = {...oBenefit};
        if (objBenefit.id == ''){
            objBenefit = {
                id: '',
                name: '',
                amountpermonth: '',
                scheme: {
                    value: "Last Pay of the Month",
                    options: ["First Pay of the Month", "Last Pay of the Month"]
                }
            }
        }
        this.props.openForm(objBenefit);   
    }

    _deleteCompBenefit = (oBenefit) => {
        this.props.removeItem(oBenefit);   
    }

    render(){
        return(
            <CustomCard 
                title={title_Comp} 
                description={description_Comp} 
                oType='Switch'
                rightHeader={
                    <Switch
                        onValueChange={ (value) => this._toggleSwitch(value)} 
                        onTintColor={color_SwitchOn}
                        thumbTintColor={color_SwitchThumb}
                        tintColor={color_SwitchOff}
                        value={ this.props.data.enabled } 
                    />
                }
            >
                {
                    this.props.data.enabled ?
                        <View>
                            <View style={{marginTop: -20}}>
                                <PropTitle name='Company Benefits Table'/>
                                <View style={styles.leaveCont}>
                                    <View style={[styles.breakTimeDetailsCont, styles.breakHeaderBorder]}>
                                        <View style={styles.benefitsNameCont}>
                                            <Text style={styles.txtBreakName}>NAME</Text>
                                        </View>
                                        <View style={styles.benefitsAmountCont}>
                                            <Text style={styles.txtDefault}>AMOUNT PER MONTH</Text>
                                        </View>
                                        <View style={styles.benefitsDetailsCont}>
                                            <Text style={styles.txtDefault}>SCHEME</Text>
                                        </View>
                                        <View style={styles.benefitsDeleteCont}>
                                            <Text style={styles.txtDefault}>REMOVE</Text>
                                        </View>
                                  
                                            
                                    </View>
                                    
                                    {
                                        this.props.data.data.map((oBenefit, index) => (
                                            <TouchableNativeFeedback
                                                key={index}
                                                onPress={() => this._openForm(oBenefit)}
                                                background={TouchableNativeFeedback.SelectableBackground()}>
                                                <View style={styles.breakTimeDetailsCont}>
                                                    <View style={styles.benefitsNameCont}>
                                                        <Text style={styles.txtBreakName}>{oBenefit.name}</Text>
                                                    </View>
                                                    <View style={styles.benefitsAmountCont}>
                                                        <Text style={styles.txtDefault}>{oBenefit.amountpermonth}</Text>
                                                    </View>
                                                    <View style={styles.benefitsDetailsCont}>
                                                        <Text style={styles.txtDefault}>{oBenefit.scheme.value}</Text>
                                                    </View>
                                                    <View style={styles.benefitsDeleteCont}>
                                                        <TouchableOpacity
                                                            activeOpacity={0.7}
                                                            onPress={() => {this._deleteCompBenefit(oBenefit)}}
                                                            >
                                                            <Icon size={30} name='md-close-circle' color='#EEB843' />
                                                        </TouchableOpacity>
                                                    </View>
                                                </View>
                                            </TouchableNativeFeedback>
                                        ))
                                    }
                                    
                                    <View style={styles.breakTimeDetailsCont}>
                                        <View style={styles.breakNameCont}>
                                            <TouchableOpacity
                                                style={{paddingLeft: 30}}
                                                activeOpacity={0.7}
                                                onPress={() => this._openForm({id:''})}
                                                >
                                                <Icon size={30} name='md-add' color='#EEB843' />
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </View>
                    :
                        <Description 
                            enabled={this.props.data.description.enabled}
                            disabled={this.props.data.description.disabled}/>
                }

            </CustomCard>
        )
    }
}

export default CompanyBenefits;

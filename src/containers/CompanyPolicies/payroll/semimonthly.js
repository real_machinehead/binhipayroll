import React, { Component } from 'react';
import {
    View,
    Text,
    Picker,
    Switch,
    DatePickerAndroid
} from 'react-native';

//Styles and Behavior
import styles from './styles'


//Custom Components
import CustomCard, 
    {
        PropTitle,
        PropLevel1, 
        PropLevel2
    } 
from '../../../components/CustomCards';

//Helper
import * as oHelper from '../../../helper/';
import { CONSTANTS } from '../../../constants/index';

class Semimonthly extends Component{
    _setPayDay = (value) => {
        this.props.onSetPayDay(value);
    }

    _setCutOff = (value) => {
        this.props.onSetCutOff(value);
    }

    _toggleIsMovedOnLastDay = (value) => {
        this.props.onSetIsMovedLastDay(value);
    }

    _showDatePicker = async() => {
        try {
            const {action, year, month, day} = await DatePickerAndroid.open({
              date: new Date()
            });
            if (action !== DatePickerAndroid.dismissedAction) {
                var oDate = new Date();
                oDate.setYear(year);
                oDate.setMonth(month);
                oDate.setDate(day);
                this.props.onSetFirstPayDate(oHelper.convertDateToString(oDate, 'YYYY-MM-DD'));
            }
        } catch ({code, message}) {
            console.warn('Cannot open date picker', message);
        }
    } 

    render(){
        const viewOnly = this.props.viewOnly || false;
        console.log('I WAS IN MONTHLY');
        const oSemimonthly = this.props.data.data.semimonthly;
        const strFirstPayDate = this.props.data.firstpayday;
        const bIsMovedOnLastDay =  this.props.data.ismovelastday;

        return(
            <View>
                <PropTitle name='Payroll Schedule'/>
                <PropLevel2 
                    name={oSemimonthly.label}
                    contentType={viewOnly ? 'TEXT' : null}
                    content={
                        viewOnly ? 
                            <Text style={styles.txtDisabledValue}>{oSemimonthly.value}</Text>
                        :
                            <Picker
                                mode='dropdown'
                                style={styles.pickerStyle}
                                selectedValue={oSemimonthly.value}
                                onValueChange={(itemValue, itemIndex) => {this._setPayDay(itemValue)}}>
                                {
                                    oSemimonthly.option.map((paytype, index) => (
                                        <Picker.Item key={index} label={paytype} value={paytype} />
                                    ))
                                }
                            </Picker>
                    }
                />

                <PropLevel2 
                    name={oSemimonthly.secondpayday.label}
                    content={oSemimonthly.secondpayday.value}
                    contentType='Text'
                />

                <PropLevel2 
                    name={oSemimonthly.cutoff.label}
                    contentType={viewOnly ? 'TEXT' : null}
                    content={
                        viewOnly ? 
                                <Text style={styles.txtDisabledValue}>{oSemimonthly.cutoff.value}</Text>
                            :
                            <Picker
                                mode='dropdown'
                                style={styles.pickerStyle}
                                selectedValue={oSemimonthly.cutoff.value}
                                onValueChange={(itemValue, itemIndex) => {this._setCutOff(itemValue)}}>
                                {
                                    oSemimonthly.cutoff.options.map((paytype, index) => (
                                        <Picker.Item key={index} label={paytype} value={paytype} />
                                    ))
                                }
                            </Picker>
                    }
                    placeHolderStyle={{
                        marginTop: 5,
                        marginBottom: 5
                    }}
                />

                <PropLevel2 
                    name={oSemimonthly.firstperiod.label}
                    content={oSemimonthly.firstperiod.value}
                    contentType='Text'
                />

                <PropLevel2 
                    name={oSemimonthly.secondperiod.label}
                    content={oSemimonthly.secondperiod.value}
                    contentType='Text'
                />

                <PropLevel2 
                    name={'First Payroll Date'}
                    content={
                        <Text 
                            disabled = {viewOnly}
                            onPress={this._showDatePicker}
                            style={{color: '#434646', 
                                height: '100%', 
                                textAlignVertical: 'center',
                            }}>
                            { 
                                strFirstPayDate ?
                                    oHelper.convertDateToString(strFirstPayDate, 'MMMM DD, YYYY') 
                                :
                                    ''    
                            }
                        </Text>
                    }
                    hideBorder={viewOnly}
                    contentStyle={{
                        paddingLeft: 15,
                        justifyContent: 'center',
                        width: 200
                    }}
                    placeHolderStyle={{
                        marginTop: 5,
                        marginBottom: 5
                    }}
                />


                <PropLevel2 
                    hideBorder
                    name={'When pay date falls on a 30th, always set payday on the Last Day of the month'}
                    content={
                        viewOnly ? 
                            <Text style={styles.txtDisabledValue}>{bIsMovedOnLastDay ? 'ON' : 'OFF'}</Text>
                        :
                            <Switch
                                onValueChange={this._toggleIsMovedOnLastDay} 
                                value={bIsMovedOnLastDay}
                            />
                    }
                    contentStyle={{
                        justifyContent: 'center',
                        alignItems: 'flex-start',
                        width: 200,
                    }}
                    placeHolderStyle={{
                       height: 100
                    }}
                    
                />
            </View>
        );
    }
}

export default Semimonthly;
import React, { Component } from 'react';
import {
    View,
    Text,
    ScrollView,
    Picker,
    RefreshControl,
    TouchableOpacity,
    DatePickerAndroid,
    Alert
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

//styles
import styles from './styles';

//Redux
import { connect } from 'react-redux';
import * as payrollSelector from '../data/payroll/selector';
import * as payrollActions from '../data/payroll/actions';
import { bindActionCreators } from 'redux';

//API
import * as payrollApi from '../data/payroll/api';

//Custom Components
import GenericContainer from '../../../components/GenericContainer';
import CustomCard, {PropLevel2, PropLevel1} from '../../../components/CustomCards';
import Monthly from './monthly';
import Weekly from './weekly';
import Semimonthly from './semimonthly'

//Helper
import * as oHelper from '../../../helper/';
import { CONSTANTS } from '../../../constants/index';

const TITLE = 'Set Payroll Rules and Schedule';

class Payroll extends Component{
    constructor(props){
        super(props);
        this.state = {
            _refreshing: false,
            _status: 2,
            _data: {},
            loadingScreen: {
                show: false,
                msg: 'test'
            },
            msgBox: {
                show: false,
                type: '',
                msg: '',
                param: ''
            },
        }
    }

    componentDidMount() {
        this._initData();
    }

    _initData = () => {
        const payroll = this.props.payroll;
        if(payroll.data == null){
            this._getDataFromDB();
        }
    }

    _getDataFromDB = () => {
        this.props.actions.payroll.get();
    }

    _setMessageBox = (show, type, msg, param) => {
        this.setState({
            msgBox: oHelper.setMsgBox(
                this.state.msgBox,
                show, 
                type,
                msg,
                param
            )
        })
    }

    _setLoadingScreen = (show, msg) => {
        let oLoadingScreen = {...this.state.loadingScreen};
        oLoadingScreen.show = show;
        oLoadingScreen.msg = msg;
        this.setState({ loadingScreen: oLoadingScreen });
    }

    _msgBoxOnClose = (params) => {
        this.setState({
            msgBox: oHelper.clearMsgBox(this.state.msgBox)
        })
    }

    _requestToChangePayType = (value) => {
        const oPaytype = this.props.payroll.data.paytype;

        Alert.alert(
            'Warning',
            'Switching Payroll Type will change your current payroll schedules.' +
            ' It is allowed only when there is no employee added yet.' + 
            '\n\nAre you sure you want to switch payroll type from ' + oPaytype.value + ' to ' + value + ' ?',
            [
                {text: 'NO', onPress: () => {}},
                {text: 'YES', onPress: () => this._switchPayType(value)}
            ],
            { cancelable: false }
        );
    }

    _switchPayType = async(value) => {
        this._setLoadingScreen(true, 'Saving Payroll Policy changes. Please wait...');

        let oPaytype = oHelper.copyObject(this.props.payroll.data.paytype);
        oPaytype.value = value;

        await payrollApi.switchType({paytype: oPaytype})
        .then((response) => response.json())
        .then((res) => {
            if(res.flagno == 1){
                this.props.actions.payroll.update(res);
                this._setMessageBox(true, 'success', res.message);
            }else{
                this._setMessageBox(true, 'error-ok', res.message);
            }
            console.log('RES_switchPayType: ' + JSON.stringify(res));
        })

        .catch((exception) => {
            this._setMessageBox(true, 'error-ok', exception.message);
        });

        this._setLoadingScreen(false);
    }

    _onSetPayDay = (value) => {
        let oData = oHelper.copyObject(this.props.payroll.data);
        let curValue = null;
        switch(oData.paytype.value.toUpperCase()){
            case 'MONTHLY':
                curValue = oData.data.monthly.payday.value;
                oData.data.monthly.payday.value = value;
                break;

            case 'SEMI-MONTHLY':
                curValue = oData.data.semimonthly.value
                oData.data.semimonthly.value = value;
                break;
                
            case 'WEEKLY':
                curValue = oData.data.weekly.payday.value;
                oData.data.weekly.payday.value = value;
                break;

            default: 
                break;
        }

        Alert.alert(
            'Warning',
            'Are you sure you want to change pay day from ' + curValue + ' to ' + value + ' ? ',
            [
                {text: 'NO', onPress: () => {}},
                {text: 'YES', onPress: () => this._setPayDay(oData)}
            ],
            { cancelable: false }
        );
    }

    _onSetCutOff = (value) => {
        let oData = oHelper.copyObject(this.props.payroll.data);
        let curValue = null;
        switch(oData.paytype.value.toUpperCase()){
            case 'MONTHLY':
                curValue = oData.data.monthly.cutoff.value;
                oData.data.monthly.cutoff.value = value;
                break;
            case 'SEMI-MONTHLY':
                curValue = oData.data.semimonthly.cutoff.value
                oData.data.semimonthly.cutoff.value = value;
                break;
            case 'WEEKLY':
                curValue = oData.data.weekly.cutoff.value;
                oData.data.weekly.cutoff.value = value;
                break;
            default: 
                break;
        }

        Alert.alert(
            'Warning',
            'Are you sure you want to change cutoff from ' + curValue + ' to ' + value + ' ? ',
            [
                {text: 'NO', onPress: () => {}},
                {text: 'YES', onPress: () => this._setCutOff(oData)}
            ],
            { cancelable: false }
        );
    }

    _onSetFirstPayDate = (value) => {
        let oData = oHelper.copyObject(this.props.payroll.data);
        let curValue = oData.firstpayday ? oHelper.convertDateToString(oData.firstpayday, 'MMMM DD, YYYY') : null;
        oData.firstpayday = value;
        const newValue = oHelper.convertDateToString(value, 'MMMM DD, YYYY');

        const strMsg = curValue ? 
            'Are you sure you want to change first pay date from ' + curValue + ' to ' + newValue + ' ? ' :
            'Are you sure you want to change first pay date to ' + newValue + ' ? ';

        Alert.alert(
            'Warning',
            strMsg,
            [
                {text: 'NO', onPress: () => {}},
                {text: 'YES', onPress: () => this._setFirstPayDate(oData)}
            ],
            { cancelable: false }
        );
    }

    _onSetIsMovedLastDay = (value) => {
        let oData = oHelper.copyObject(this.props.payroll.data);
        let curValue = oData.ismovelastday ? 'ON' : 'OFF';
        let newValue = value ? 'ON' : 'OFF';
        oData.ismovelastday = value;

        Alert.alert(
            'Warning',
            'Are you sure you want to change 30th day pay day Rule from ' + curValue + ' to ' + newValue + ' ? ',
            [
                {text: 'NO', onPress: () => {}},
                {text: 'YES', onPress: () => this._setIsMovedLastDay(oData)}
            ],
            { cancelable: false }
        );
    }

    _setPayDay = async(oData) => {
        this._setLoadingScreen(true, 'Updating pay day. Please wait...');
        console.log('INPUT_setPayDay: ' + JSON.stringify(oData));
        await payrollApi.setPayDay(oData)
            .then((response) => response.json())
            .then((res) => {
                if(res.flagno == 1){
                    this.props.actions.payroll.update(res);
                    this._setMessageBox(true, 'success', res.message);
                }else{
                    this._setMessageBox(true, 'error-ok', res.message);
                }
                console.log('OUTPUT_setPayDay: ' + JSON.stringify(oData));
            })
        .catch((exception) => {
            this._setMessageBox(true, 'error-ok', exception.message);
        });

        this._setLoadingScreen(false);
    }

    _setCutOff = async(oData) => {
        this._setLoadingScreen(true, 'Updating cutoff period. Please wait...');
        console.log('INPUT_setCutOff: ' + JSON.stringify(oData));
        await payrollApi.setCutOff(oData)
            .then((response) => response.json())
            .then((res) => {
                if(res.flagno == 1){
                    this.props.actions.payroll.update(res);
                    this._setMessageBox(true, 'success', res.message);
                }else{
                    this._setMessageBox(true, 'error-ok', res.message);
                }
                console.log('OUTPUT_setCutOff: ' + JSON.stringify(oData));
            })
        .catch((exception) => {
            this._setMessageBox(true, 'error-ok', exception.message);
        });

        this._setLoadingScreen(false);
    }

    _setFirstPayDate = async(oData) => {
        this._setLoadingScreen(true, 'Updating first pay date. Please wait...');
        console.log('INPUT_onSetFirstPayDate: ' + JSON.stringify(oData));
        await payrollApi.setFirstPayDate(oData)
            .then((response) => response.json())
            .then((res) => {
                if(res.flagno == 1){
                    this.props.actions.payroll.update(res);
                    this._setMessageBox(true, 'success', res.message);
                }else{
                    this._setMessageBox(true, 'error-ok', res.message);
                }
                console.log('OUTPUT_onSetFirstPayDate: ' + JSON.stringify(res));
            })
        .catch((exception) => {
            this._setMessageBox(true, 'error-ok', exception.message);
        });

        this._setLoadingScreen(false);
    }

    _setIsMovedLastDay = async(oData) => {
        this._setLoadingScreen(true, 'Updating 30th day pay day Rule. Please wait...');
        console.log('INPUT_setIsMovedLastDay: ' + JSON.stringify(oData));
        await payrollApi.setIsMovedLastDay(oData)
            .then((response) => response.json())
            .then((res) => {
                console.log('OUTPUT_setIsMovedLastDay: ' + JSON.stringify(oData));
                if(res.flagno == 1){
                    this.props.actions.payroll.update(res);
                    this._setMessageBox(true, 'success', res.message);
                }else{
                    this._setMessageBox(true, 'error-ok', res.message);
                }
            })
        .catch((exception) => {
            this._setMessageBox(true, 'error-ok', exception.message);
        });

        this._setLoadingScreen(false);
    }
    
    render() {
        const viewOnly = this.props.viewOnly || false;
        //const oData = this.props.payroll.data || {paytype:[{ label: 'payroll type',value: '',options: [ 'Semi-Monthly', 'Monthly', 'Weekly' ],}]}
        const oData = this.props.payroll.data; 
        /* const aStatus = oData ? [true, 'Has existing data.'] : this.props.payroll.status; */
        const aStatus = this.props.payroll.status;
        let oBody = null;
        console.log('datassssssss',oData);
        if(aStatus[0] == 1){
            if(oData){
                const strActiveType = oData ? oData.paytype.value : null;
                oBody = 
                    <View style={{padding: 25}}>
                        <PropLevel1 
                            name='Payroll Type' 
                            content={
                                this.state._disabledMode || viewOnly ? 
                                    <Text style={styles.txtDisabledValue}>{oData.paytype.value}</Text>
                                :
                                    <Picker
                                        mode='dropdown'
                                        selectedValue={oData.paytype.value}
                                        onValueChange={(itemValue, itemIndex) => {this._requestToChangePayType(itemValue)}}>
                                        {   
                                            
                                            oData.paytype.options.map((paytype, index) => (
                                                <Picker.Item key={index} label={paytype} value={paytype} />
                                            ))
                                        }
                                    </Picker> 
                            }
                            hideBorder={this.state._disabledMode || viewOnly}
                        />

                        {
                            strActiveType.toUpperCase() == "MONTHLY" ?
                                <Monthly 
                                    data={oData}
                                    onSetPayDay={this._onSetPayDay}
                                    onSetCutOff={this._onSetCutOff}
                                    onSetFirstPayDate={this._onSetFirstPayDate}
                                    onSetIsMovedLastDay={this._onSetIsMovedLastDay}
                                    viewOnly = {viewOnly}
                                />
                            :
                                strActiveType.toUpperCase() == "SEMI-MONTHLY" ?
                                    <Semimonthly
                                        data={oData}
                                        onSetPayDay={this._onSetPayDay}
                                        onSetCutOff={this._onSetCutOff}
                                        onSetFirstPayDate={this._onSetFirstPayDate}
                                        onSetIsMovedLastDay={this._onSetIsMovedLastDay}
                                        viewOnly = {viewOnly}
                                    />
                                :
                                    strActiveType.toUpperCase() == "WEEKLY" ?
                                        <Weekly 
                                            data={oData}
                                            onSetPayDay={this._onSetPayDay}
                                            onSetCutOff={this._onSetCutOff}
                                            onSetFirstPayDate={this._onSetFirstPayDate}
                                            onSetIsMovedLastDay={this._onSetIsMovedLastDay}
                                            viewOnly = {viewOnly}
                                        />
                                    :
                                        null
                        }
                    </View>
            }
        }


        return(
            <GenericContainer
                msgBoxShow = {this.state.msgBox.show}
                msgBoxType = {this.state.msgBox.type}
                msgBoxMsg = {this.state.msgBox.msg}
                msgBoxOnClose = {this._msgBoxOnClose}
                msgBoxOnYes = {this._msgBoxOnYes}
                msgBoxParam = {this.state.msgBox.param}
                loadingScreenShow = {this.state.loadingScreen.show}
                loadingScreenMsg = {this.state.loadingScreen.msg}
                status={aStatus}
                title={ TITLE }
                onRefresh={this._getDataFromDB}>
                    <CustomCard 
                        clearMargin
                        title={TITLE} 
                        oType='TEXT'
                    >
                        <ScrollView
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state._refreshing}
                                    onRefresh={this._getDataFromDB}
                                />
                            }
                        >
                            {oBody}
                        </ScrollView>
                    </CustomCard>
            </GenericContainer>
        );
    }
}

function mapStateToProps (state) {
    return {
        logininfo: state.loginReducer.logininfo,
        activecompany: state.activeCompanyReducer.activecompany,
        payroll: state.companyPoliciesReducer.payroll
        
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            payroll: bindActionCreators(payrollActions, dispatch),
        },
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Payroll)
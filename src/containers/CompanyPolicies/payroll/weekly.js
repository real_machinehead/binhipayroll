import React, { Component } from 'react';
import {
    View,
    Text,
    Picker,
    Switch,
    DatePickerAndroid
} from 'react-native';

//Styles and Behavior
import styles from './styles'


//Custom Components
import CustomCard, 
    {
        PropTitle,
        PropLevel1, 
        PropLevel2
    } 
from '../../../components/CustomCards';

//Helper
import * as oHelper from '../../../helper/';
import { CONSTANTS } from '../../../constants/index';

class Weekly extends Component{
    _setPayDay = (value) => {
        this.props.onSetPayDay(value);
    }

    _setCutOff = (value) => {
        this.props.onSetCutOff(value);
    }

    _toggleIsMovedOnLastDay = (value) => {
        this.props.onSetIsMovedLastDay(value);
    }

    _showDatePicker = async() => {
        try {
            const {action, year, month, day} = await DatePickerAndroid.open({
              date: new Date()
            });
            if (action !== DatePickerAndroid.dismissedAction) {
                var oDate = new Date();
                oDate.setYear(year);
                oDate.setMonth(month);
                oDate.setDate(day);
                this.props.onSetFirstPayDate(oHelper.convertDateToString(oDate, 'YYYY-MM-DD'));
            }
          } catch ({code, message}) {
            console.warn('Cannot open date picker', message);
          }
    } 

    render(){
        console.log('I WAS IN MONTHLY');
        const viewOnly = this.props.viewOnly || false;
        const oWeekly = this.props.data.data.weekly;
        const strFirstPayDate = this.props.data.firstpayday;
        const bIsMovedOnLastDay =  this.props.data.ismovelastday;

        return(
            <View>
                <PropTitle name='Payroll Schedule'/>
                <PropLevel2 
                    name={oWeekly.payday.label}
                    contentType={viewOnly ? 'TEXT' : null}
                    content={
                        viewOnly ? 
                            <Text style={styles.txtDisabledValue}>{oWeekly.payday.value}</Text>
                        :
                            <Picker
                                mode='dropdown'
                                style={styles.pickerStyle}
                                selectedValue={oWeekly.payday.value}
                                onValueChange={(itemValue, itemIndex) => {this._setPayDay(itemValue)}}>
                                {
                                    oWeekly.payday.options.map((paytype, index) => (
                                        <Picker.Item key={index} label={paytype} value={paytype} />
                                    ))
                                }
                            </Picker>
                        }
                    contentStyle={{
                        width: 200
                    }}
                />

                <PropLevel2 
                    name={oWeekly.cutoff.label}
                    contentType={viewOnly ? 'TEXT' : null}
                    content={
                        viewOnly ? 
                            <Text style={styles.txtDisabledValue}>{oWeekly.cutoff.value}</Text>
                        :
                            <Picker
                                mode='dropdown'
                                style={styles.pickerStyle}
                                selectedValue={oWeekly.cutoff.value}
                                onValueChange={(itemValue, itemIndex) => {this._setCutOff(itemValue)}}>
                                {
                                    oWeekly.cutoff.options.map((paytype, index) => (
                                        <Picker.Item key={index} label={paytype} value={paytype} />
                                    ))
                                }
                            </Picker>
                    }
                    contentStyle={{
                        width: 200
                    }}

                    placeHolderStyle={{
                        marginTop: 10,
                        marginBottom: 10
                    }}
                />

                <PropLevel2 
                    name={oWeekly.period.label}
                    content={oWeekly.period.value}
                    contentType='Text'
                    contentStyle={{
                        justifyContent: 'center',
                        width: 200
                    }}
                />

                <PropLevel2 
                    name={'First Payroll Date'}
                    content={
                        <Text 
                            disabled = {viewOnly}
                            onPress={this._showDatePicker}
                            style={{color: '#434646', 
                                height: '100%', 
                                textAlignVertical: 'center',
                            }}>
                            { 
                                strFirstPayDate ?
                                    oHelper.convertDateToString(strFirstPayDate, 'MMMM DD, YYYY') 
                                :
                                    ''    
                            }
                        </Text>
                    }
                    hideBorder={viewOnly}
                    contentStyle={{
                        paddingLeft: 15,
                        justifyContent: 'center',
                        width: 200
                    }}
                    placeHolderStyle={{
                        marginTop: 10,
                        marginBottom: 10
                    }}
                />


                {/* <PropLevel2 
                    hideBorder
                    name={'When pay date falls on a 30th, always set payday on the Last Day of the month'}
                    content={
                        <Switch
                            onValueChange={this._toggleIsMovedOnLastDay} 
                            value={bIsMovedOnLastDay}
                        />
                    }
                    contentStyle={{
                        justifyContent: 'center',
                        alignItems: 'flex-start',
                        width: 200,
                    }}
                    placeHolderStyle={{
                       height: 100
                    }}
                    
                /> */}
            </View>
        );
    }
}

export default Weekly;
import React, { Component } from 'react';
import {
    View,
    Text,
    Alert,
    Switch,
    ScrollView,
    RefreshControl,
    FlatList
} from 'react-native';
import CustomCard, {SimpleCard} from '../../../components/CustomCards';
import GenericContainer from '../../../components/GenericContainer';
import EmptyList from '../../../components/EmptyList';
import * as PromptScreen from '../../../components/ScreenLoadStatus';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Branchform from './form';

//Styles
import styles from './styles';

//helper
import * as oHelper from '../../../helper';

//Component Constants
const TITLE = 'COMPANY BRANCH';
const DESCRIPTION = 'Add, remove and update Branches'
const SWITCH_COLOR_ON = '#838383';
const SWITCH_COLOR_THUMB = '#EEB843';
const SWITCH_COLOR_TINT = '#505251';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as branchActions from '../data/branch/actions';

//API
import * as Branchapi from '../data/branch/api';

export class BranchPolicy extends Component {
    constructor(props){
        super(props);
        this.state = {
            refreshing: false,
            _showForm: false, 
            disabledMode: false,
            data: {
                isenabled: true,
                datedisplayformat: 'MMM DD YYYY',
                datedisplayformformat: 'MMMM DD, YYYY'
            },

            loadingScreen: {
                show: false,
                msg: 'test'
            },

            msgBox: {
                show: false,
                type: '',
                msg: '',
                param: ''
            },

            activeData: {
                id: '',
                branchname: '',
                address: '',
                contact: '',
                emailaddress: '',
                compid:'',
                status:'',
                transtype:'',
            }
            
        }
    }

    componentDidMount(){
        if(this.props.branch.status[0] != 1){
            this._fetchDataFromDB();
        }
    }

    _fetchDataFromDB= () => {
        this.props.actions.branch.get();
    }

    _setMessageBox = (show, type, msg, param) => {
        this.setState({
            msgBox: oHelper.setMsgBox(
                this.state.msgBox,
                show, 
                type,
                msg,
                param
            )
        })
    }

    _setLoadingScreen = (show, msg) => {
        let oLoadingScreen = {...this.state.loadingScreen};
        oLoadingScreen.show = show;
        oLoadingScreen.msg = msg;
        this.setState({ loadingScreen: oLoadingScreen });
    }

    _msgBoxOnClose = (params) => {
        this.setState({
            msgBox: oHelper.clearMsgBox(this.state.msgBox)
        })
    }

    _msgBoxOnYes = (param) => {
        console.log('param: ' + param );
        let oData = null;
        switch(param[0].toUpperCase()){
            case 'SWITCHOFFPOLICY':
                oData = {...this.props.branch.data};
                oData.validfrom = null;
                oData.validto = null;
                oData.amount = 0;
                oData.isenabled = param[1];
                this._updateEmpSavPol(oData);
                break;

            case 'CLOSEPOLICYFORM':
                this._setEmpSavPolForm(false);
                this._setMessageBox(false);
                break;

            case 'UPDATEPOLICY':
                this._setMessageBox(false);
                oData = {...param[1]};
                oData.isenabled = true;
                this._updateEmpSavPol(oData);
                break;

            default:
                this._setMessageBox(false);
                break;
        }
    }

    _onPress = () => {
        let oLoadingScreen = {...this.state.loadingScreen};
        oLoadingScreen.show = true;
        oLoadingScreen.msg = 'HAHAHHAhAHA';;
        
        this.setState({
            loadingScreen: oLoadingScreen
        });
    }

    _keyExtractor = (item, index) =>item.id;

    _renderItem = (oItem) => {
        console.log('oItems',oItem)
        oItem.status='ACTIVE';
        const menu = 
            oItem.status == 'ACTIVE' ? 
                [
                    { onSelect: () => this._updateBranch(oItem), label: 'Update' },
                    { onSelect: () => this._deleteBranch(oItem), label: 'Delete' }
                ]
            :
                []

        const backgroundColor =
            oItem.status == 'ACTIVE' ? 
                {backgroundColor: '#26A65B'}
            : 
                {backgroundColor: '#E74C3C'}
                
        const oTitle = 
            <View style={styles.title.contContent}>
                <View style={styles.title.contIcon}>
                    <View style={styles.title.iconPlaceholder}>
                        <Icon 
                            name='worker'
                            size={30} 
                            color='#f4f4f4'
                        />
                    </View>
                </View>
                <View style={styles.title.contLabel}>
                    <Text style={styles.title.txtLabel}>
                        { oItem.name }
                    </Text>
                </View>
            </View>

        return(
            <View>
                <View style={{
                    marginBottom: 10,
                    marginLeft: 30,
                    marginRight: 30,
                }}>
                    <SimpleCard 
                        data={[
                            ['Branch Name', oItem.name], 
                            ['Address',oItem.address],
                            ['Contact',oItem.contact],
                            ['Email',oItem.email],['Status',oItem.status],]
                        }
                        customTitleStyle={backgroundColor}
                        oTitle={oTitle}
                        menu={menu}
                    />
                </View>
                
            </View>
        );
    }

    _addNewBranch = () => {
        this.setState({
            _showForm: true,
            activeData: {id: '', name: '', address:'',contact:'',email:'',transtype:'insert',compid:this.props.companyid}
        })
    }

    _updateBranch = (oData) => {
        console.log('Datas',oData);
        this.setState({
            _showForm: true,
            activeData: oData
        })
    }

    _deleteBranch = (oData) => {
        Alert.alert(
            'Warning',
            'Are you sure you want to delete ' + oData.name + ' ?',
            [
                {text: 'No', onPress: () => {}},
                {text: 'Yes', onPress: () => this._deleteToDB(oData)},
            ],
            { cancelable: false }
        ) 
    }

    _closeForm = () => {
        this.setState({
            _showForm: false
        })
    }

    _onSubmitForm = (oData) => {
        if(oHelper.isStringEmptyOrSpace(oData.id)){
            this._addToDB(oData);
        }else{
            Alert.alert(
                'Confirm Action',
                'Are you sure you want to update ' + oData.name + ' ?',
                [
                    {text: 'No', onPress: () => {}},
                    {text: 'Yes', onPress: () => this._updateToDB(oData)},
                ],
                { cancelable: false }
            ) 
        }
    }

    _addToDB = async(oData) => {
        console.log('addData',oData);
        this._setLoadingScreen(true, 'Saving New Company Branch. Please wait...');
        await Branchapi.add(oData)
            .then((response) => response.json())
            .then(async(res) => {
                console.log('RES: ' + JSON.stringify(res));
                if(res.flagno == 1){
                    oData.id = res.id;

                    //console.log('RESSSS: ' + JSON.stringify(oData));
                    console.log('RESSSS: ' + oData);
                    //oData.dateadded = await oHelper.convertDateToString(new Date(), 'YYYY-MM-DD');
                    oData.status = 'ACTIVE';
                    await this.props.actions.branch.add(oData);
                    this._closeForm();
                }
                this._setMessageBox(true, res.flagno==1 ? 'success' : 'error-ok', res.message);
            })
            .catch((exception) => {
                this._setMessageBox(true, 'error-ok', exception.message);
            });
        this._setLoadingScreen(false);
    } 

    _updateToDB = async(oData) => {
        this._setLoadingScreen(true, 'Updating Company Branch. Please wait...');
        oData.transtype='update';
        oData.compid=this.props.companyid;
        await Branchapi.update(oData)
            .then((response) => response.json())
            .then((res) => {
                console.log('RES: ' + JSON.stringify(res));
                if(res.flagno == 1){
                    this.props.actions.branch.update(oData)
                    this._closeForm();
                }
                this._setMessageBox(true, res.flagno==1 ? 'success' : 'error-ok', res.message);
            })
            .catch((exception) => {
                /* this._setMessageBox(true, 'error-ok', 'Failed to update ' + TITLE); */
                this._setMessageBox(true, 'error-ok', exception.message);
            });
        this._setLoadingScreen(false);
    } 

    _deleteToDB = async(oData) => {
        this._setLoadingScreen(true, 'Deleting Company Branch. Please wait...');
        oData.transtype='delete';
        oData.compid=this.props.companyid;
        await Branchapi.remove(oData)
            .then((response) => response.json())
            .then((res) => {
                console.log('RES: ' + JSON.stringify(res));
                if(res.flagno == 1){
                    oData.status = 'DELETED';
                    this.props.actions.branch.update(oData);
                    this._closeForm();
                }
                this._setMessageBox(true, res.flagno==1 ? 'success' : 'error-ok', res.message);
            })
            .catch((exception) => {
                /* this._setMessageBox(true, 'error-ok', 'Failed to update ' + TITLE); */
                this._setMessageBox(true, 'error-ok', exception.message);
            });
        this._setLoadingScreen(false);
    } 

    render(){
        const oData = this.props.branch.data;
        const aStatus = this.props.branch.status;
        console.log('data length',oData);
        return(
            <GenericContainer
                msgBoxShow = {this.state.msgBox.show}
                msgBoxType = {this.state.msgBox.type}
                msgBoxMsg = {this.state.msgBox.msg}
                msgBoxOnClose = {this._msgBoxOnClose}
                msgBoxOnYes = {this._msgBoxOnYes}
                msgBoxParam = {this.state.msgBox.param}
                loadingScreenShow = {this.state.loadingScreen.show}
                loadingScreenMsg = {this.state.loadingScreen.msg}
                status={aStatus}
                title={TITLE}
                onRefresh={this._fetchDataFromDB}>
                {
                    oData ?
                        <CustomCard 
                            contentContainerStyle={styles.container}
                            clearMargin
                            title={TITLE} 
                            oType='text'
                            description = {DESCRIPTION}
                        >
    
                            {
                                oData.length < 1 ?
                                    <EmptyList message='No Branch Added' />
                                :
                                    <FlatList
                                        refreshing={this.state.refreshing}
                                        onRefresh={this._fetchDataFromDB}
                                        contentContainerStyle={styles.reportsFlatlist}
                                        ref={(ref) => { this.flatListRef = ref; }}
                                        extraData={oData}
                                        data={oData.data}
                                        keyExtractor={this._keyExtractor}
                                        renderItem={({item}) => this._renderItem(item) }
                                    />
                            }
                            
                        </CustomCard> 
                    :
                        null
                }
                {
                    this.state._showForm ?
                        <Branchform 
                            onCancel={this._closeForm}
                            onSubmit={this._onSubmitForm}
                            visible={this.state._showForm}
                            title='BRANCH'
                            data={this.state.activeData}
                        />
                    :
                        null
                }
                
                <ActionButton 
                    bgColor='rgba(0,0,0,0.8)'
                    buttonColor="#EEB843"
                    spacing={10}>
                    <ActionButton.Item buttonColor='#26A65B' title="ADD NEW HOLIDAY" onPress={this._addNewBranch}>
                        <Icon name="bell-plus" color='#fff' size={22} style={styles.actionButtonIcon} />
                    </ActionButton.Item>
                </ActionButton>
                
            </GenericContainer>
        );
    }
    
}

function mapStateToProps (state) {
    //console.log('statessss',state.companyPoliciesReducer);
    return {
        branch: state.companyPoliciesReducer.branch,
        companyid:state.activeCompanyReducer.activecompany[6]
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            branch: bindActionCreators(branchActions, dispatch),
        }
    }
}
  
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(BranchPolicy)
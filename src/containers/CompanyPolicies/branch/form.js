import React, { Component } from 'react';
import {
  View,
  ScrollView,
  Keyboard
} from 'react-native';
import t from 'tcomb-form-native'; // 0.6.9
import moment from "moment";

//Form Template 
import { customPickerTemplate } from '../../../global/tcomb-custom-select-android';
import { customDatePickerTemplate } from '../../../global/tcomb-custom-datepicker-android';

//Styles
import styles from './styles';
import stylesheet from '../../../global/globalFormStyle';
import * as CustomForm from '../../../components/CustomForm';

//Custom Components
import FormModal from '../../../components/FormModal';

//Helper
import * as oHelper from '../../../helper';

const Form = t.form.Form;

export default class Branchform extends Component {
    constructor(props){
        super(props);
        const dateHoliday=this.props.data.date ? 
        oHelper.convertStringToDate(this.props.data.date, this.props.data.displaydateformformat)
        :
        null;

        this.state={
            disabledSave: false,
            didMount: false,
            data: {
                id: this.props.data.id,
                name: this.props.data.name,
                address: this.props.data.address,
                contact: this.props.data.contact,
                email:this.props.data.email,
                transtype:this.props.data.transtype,
                compid:this.props.data.compid,
                status: this.props.data.status || '',
            }
        }
    }

    componentDidMount(){
        this.setState({didMount: true});
    }

    _onDataChange = (value) => {
       
        let oData = oHelper.copyObject(this.state.data);
        oData.name = value.name;
        oData.address = value.address;
        oData.contact = value.contact;
        oData.email = value.email

        if(value.name && value.address)
        this.setState({ data: oData });

    }

    _onSubmit = () => {
        Keyboard.dismiss();
        let oData = this.refs.form_branchform.getValue();
        if(oData){
            this.props.onSubmit(this.state.data)
        }
    }

    render() {
        console.log('branch form',this.props)
        const ENTITY = t.struct({
            name: t.String,
            address: t.String,
            contact: t.String,
            email:t.String
        })
        
        const OPTIONS = {
            fields: {
                name:{ 
                    label: 'BRANCH NAME' ,
                    returnKeyType: 'next',
                    error: '*Enter a company branch name'
                },
                address:{ 
                    label: 'ADDRESS' ,
                    returnKeyType: 'next',
                    error: '*Enter a company address information'
                },
                contact:{ 
                    label: 'CONTACT INFORMATION' ,
                    returnKeyType: 'next',
                    error: '*Enter a company contact number'
                },              
                email:{ 
                    label: 'EMAIL INFORMATION' ,
                    returnKeyType: 'done',
                    error: '*Enter a company email address'
                },               
            },
            stylesheet: stylesheet
        }

        return (
            <FormModal 
                containerStyle={styles.formStyles.container}
                visible={this.props.visible}
                onCancel={this.props.onCancel}
                onOK={this._onSubmit}
                disabledSave={this.state.disabledSave}
                title={this.props.title}
                submitLabel='SAVE'>
                
                <View style={styles.formStyles.contForm}>
                    {
                        this.state.didMount ?
                            <ScrollView>
                                <View style={styles.formStyles.formContent}>
                                    <Form 
                                        ref='form_branchform'
                                        type={ENTITY}
                                        value={this.state.data}
                                        options={OPTIONS}
                                        onChange={this._onDataChange}
                                    />
                                </View>
                            </ScrollView>
                        :
                            null
                    }
                    
                </View>
            </FormModal>
        );
    }
}

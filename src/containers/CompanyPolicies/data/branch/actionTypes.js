export const UPDATEALL = 'branch/UPDATEALL';
export const UPDATE = 'branch/UPDATE';
export const ADD = 'branch/ADD';
export const REMOVE = 'branch/REMOVE';
export const STATUS = 'branch/STATUS';
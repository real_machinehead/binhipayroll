import * as actionTypes from './actionTypes';
import { combineReducers } from 'redux';
import  { CONSTANTS } from '../../../../constants';
import * as oHelper from '../../../../helper';

const initialState = null;
const initialStatus = CONSTANTS.STATUS.LOADING;

const data = (state = initialState, action) => {
	let oState = {...state};
	//console.log('logs123677',oState,action);
	switch (action.type) {
		case actionTypes.UPDATEALL:
			console.log('logs123aaaaaaa',action.payload);

			return action.payload;
			break;

		case actionTypes.ADD:
			console.log('logs12365',oState);
			//oState.data.unshift(action.payload);
			oState.data.unshift(action.payload);
			return oState;
			break;
		
		case actionTypes.UPDATE:
			return { 
				oState, 
				data: oState.data.map(
					(data, i) => data.id == action.payload.id ? action.payload : data
				)
			}
			break;

		case actionTypes.REMOVE:
			return action.payload;
			break;

		default:
			return state;
	}
};

const status = (state = initialStatus, action) => {
	console.log('logsstatus1',action.type.status);
	switch (action.type) {
		case actionTypes.STATUS:
			return action.payload;
			break;
			
		default:
			return state;
	}
};

export const reducer = combineReducers({
	data: data,
	status: status
});
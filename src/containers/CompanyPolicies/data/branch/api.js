import { fetchApi, mockFetch } from '../../../../services/api';
import * as blackOps from '../../../../global/blackOps';
import * as endPoints from '../../../../global/endpoints';

export const get = payload => fetchApi(endPoints.policy.branch.get(payload), payload, 'get');
export const update = payload => fetchApi(endPoints.policy.branch.update(payload), payload, 'put');
export const remove = payload => fetchApi(endPoints.policy.branch.remove(payload), payload, 'delete');
export const add = payload => fetchApi(endPoints.policy.branch.add(payload), payload, 'post');
import * as actionTypes from './actionTypes';
import { combineReducers } from 'redux';
import  { CONSTANTS } from '../../../../constants';
import * as oHelper from '../../../../helper';

const initialState = null;
const initialStatus = CONSTANTS.STATUS.LOADING;
const initialActiveRule = null;

export const data = (state = initialState, action) => {
	let oState = oHelper.copyObject(state);
	switch (action.type) {
		case actionTypes.INITIALIZE:
			return action.payload;
			break;

		case actionTypes.TOGGLE:
			oState.enabled = action.payload
			return oState;
			break;

		case actionTypes.INSERTELEMENT:
			const iExist = oState.data.findIndex(x => x.id = action.payload.id);
			if(iExist > -1){
				oState.data[iExist] = action.payload;
			}else{
				oState.data.unshift(action.payload);
			}
			return oState;
			break;

		default:
			return state;
	}
};

const status = (state = initialStatus, action) => {
	switch (action.type) {
		case actionTypes.STATUS:
			return action.payload;
			break;

		default:
			return state;
	}
};

const activeRule = (state = initialActiveRule, action) => {
	
	switch (action.type) {
		case actionTypes.ACTIVERULE:
			return action.payload;
			break;

		default:
			return state;
	}
};

export const reducer = combineReducers({
	data: data,
	status: status,
	activeRule: activeRule,
});
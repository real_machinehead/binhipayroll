import * as api from './api';
import * as actionTypes from './actionTypes';
import  { CONSTANTS } from '../../../../constants';

export const init = payload => ({
	type: actionTypes.INITIALIZE,
	payload,
});

export const insertElement = payload => ({
	type: actionTypes.INSERTELEMENT,
	payload,
})

export const setActiveRule = payload => ({
	type: actionTypes.ACTIVERULE,
	payload,
})

export const reset = payload => 
	dispatch => {
		dispatch(update(null));
		dispatch(updateStatus(CONSTANTS.STATUS.LOADING));
	}

export const updateEnabledStatus = payload => ({
	type: actionTypes.TOGGLE,
	payload,
});

export const get = payload => 
	dispatch => {
		let objRes = {};
		dispatch(updateStatus(CONSTANTS.STATUS.LOADING));

		api.get(payload)
		.then((response) => response.json())
		.then((res) => {
			console.log('BONUS_RES: ' + JSON.stringify(res));
			if(res.data.length > 0){
				dispatch(setActiveRule(res.data[0]));
			}
			dispatch(init(res));
			objRes = {...res}
		})
		.then(() => {
			dispatch(updateStatus([
				objRes.flagno || 0, 
				objRes.message || CONSTANTS.ERROR.SERVER
			]));
		})
		.catch((exception) => {
			dispatch(updateStatus([
				0, 
				exception.message + '.'
			]));
			console.log('exception: ' + exception.message);
		});
	}

export const toggle = payload =>
	async dispatch => {
		let oRes = null;

		await api.toggle(payload)
		.then((response) => response.json())
		.then((res) => {
			//Action Here
			console.log('RES_toggle: ' + JSON.stringify(res));
			if(res.flagno == 1){
					dispatch(updateEnabledStatus(payload.enabled));
			}else{
				throw res;
			}
			
			oRes = {...res}
		})
		.catch((exception) => {
			oRes =  {flagno: 0, message: exception.message};
		});

		return oRes;
	}

export const create = payload =>
	async dispatch => {
		let oRes = null;

		await api.create(payload)
		.then((response) => response.json())
		.then((res) => {
			//Action Here
			console.log('RES_toggle: ' + JSON.stringify(res));
			if(res.flagno == 1){
				let oData = res.data;
				oData.id = res.id;
				dispatch(setActiveRule(oData));
				dispatch(insertElement(oData));
			}

			oRes = {...res}
		})
		.catch((exception) => {
			oRes =  {flagno: 0, message: exception.message};
		});

		return oRes;
	}

export const update = payload =>
	async dispatch => {
		let oRes = null;

		await api.create(payload)
		.then((response) => response.json())
		.then((res) => {
			//Action Here
			if(res.flagno == 1){
				console.log('RES_toggle: ' + JSON.stringify(res));
				dispatch(setActiveRule(res.data));
				dispatch(insertElement(res.data));
			}

			oRes = {...res}
		})
		.catch((exception) => {
			oRes =  {flagno: 0, message: exception.message};
		});

		return oRes;
	}
	
export const updateStatus = payload => ({
	type: actionTypes.STATUS,
	payload,
});
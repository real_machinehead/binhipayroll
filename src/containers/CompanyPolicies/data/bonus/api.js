import { fetchApi } from '../../../../services/api';

const endPoints = {
	create: 'forms/benefits/bonus.php',
	get: 'forms/benefits/bonus.php',
	toggle: 'forms/benefits/bonus.php',
	remove: 'forms/benefits/bonus.php',
	update: 'forms/benefits/bonus.php',
};

export const create = payload => fetchApi(endPoints.create, payload, 'post');
export const update = payload => fetchApi(endPoints.update, payload, 'post');
export const get = payload => fetchApi(endPoints.get, payload, 'post');
export const toggle = payload => fetchApi(endPoints.toggle, payload, 'post');
export const remove = payload => fetchApi(endPoints.remove, payload, 'post');
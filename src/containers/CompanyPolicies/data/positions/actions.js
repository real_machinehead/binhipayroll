import * as api from './api';
import * as actionTypes from './actionTypes';
import  { CONSTANTS } from '../../../../constants';

export const updateAll = payload => ({
	type: actionTypes.UPDATEALL,
	payload
});

export const reset = payload => 
	dispatch => {
		dispatch(updateAll(null));
		dispatch(updateStatus(CONSTANTS.STATUS.LOADING));
	}

export const add = payload => ({
	type: actionTypes.ADD,
	payload
});

export const update = payload => ({
	type: actionTypes.UPDATE,
	payload
});

export const remove = payload => ({
	type: actionTypes.REMOVE,
	payload
});

export const get = payload => 
	dispatch => {
		let objRes = {};
		dispatch(updateStatus(CONSTANTS.STATUS.LOADING));

		api.get(payload)
		.then((response) => response.json())
		.then((res) => {
			//console.log('Position: ' + JSON.stringify(res));
			console.log('Position: ' + JSON.stringify(res.position));
			dispatch(updateAll(res.position));
			objRes = {...res}
		})
		.then(() => {
			dispatch(updateStatus([
				objRes.flagno || 0, 
				objRes.message || CONSTANTS.ERROR.SERVER
			]));
		})
		.catch((exception) => {
			dispatch(updateStatus([
				0, 
				exception.message + '.'
			]));
			console.log('exception: ' + exception.message);
		});
	}

export const updateStatus = payload => ({
	type: actionTypes.STATUS,
	payload,
});
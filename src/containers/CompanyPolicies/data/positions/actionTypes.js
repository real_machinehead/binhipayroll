export const UPDATEALL = 'positions/UPDATEALL';
export const UPDATE = 'positions/UPDATE';
export const ADD = 'positions/ADD';
export const REMOVE = 'positions/REMOVE';
export const STATUS = 'positions/STATUS';
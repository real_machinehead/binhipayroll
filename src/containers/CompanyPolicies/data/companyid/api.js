import { fetchApi, mockFetch } from '../../../../services/api';
import * as endPoints from '../../../../global/endpoints';
import * as blackOps from '../../../../global/blackOps';

export let get = payload => fetchApi(endPoints.policy.companyid.get(payload), payload, 'post');
export let update = payload => fetchApi(endPoints.policy.companyid.update(payload), payload, 'put');
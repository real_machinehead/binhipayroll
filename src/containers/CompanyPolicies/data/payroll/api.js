import { fetchApi, mockFetch } from '../../../../services/api';
import * as blackOps from '../../../../global/blackOps';
import * as endPoints from '../../../../global/endpoints';

export const get = payload => fetchApi(endPoints.policy.payroll.get(payload), payload, 'get');
export const switchType = payload => fetchApi(endPoints.policy.payroll.switchType(payload), payload, 'put');
export const setPayDay = payload => fetchApi(endPoints.policy.payroll.setPayDay(payload), payload, 'put');
export const setCutOff = payload => fetchApi(endPoints.policy.payroll.setCutOff(payload), payload, 'put');
export const setFirstPayDate = payload => fetchApi(endPoints.policy.payroll.setFirstPayDate(payload), payload, 'put');
export const setIsMovedLastDay = payload => fetchApi(endPoints.policy.payroll.setIsMovedLastDay(payload), payload, 'put');
import React, { Component } from 'react';
import {
  View,
  ScrollView,
  Keyboard
} from 'react-native';
import t from 'tcomb-form-native'; // 0.6.9
import moment from "moment";

//Form Template 
import { customPickerTemplate } from '../../../global/tcomb-custom-select-android';
import { customDatePickerTemplate } from '../../../global/tcomb-custom-datepicker-android';

//Styles
import styles from './styles';
import stylesheet from '../../../global/globalFormStyle';
import * as CustomForm from '../../../components/CustomForm';

//Custom Components
import FormModal from '../../../components/FormModal';
import BonusInstallmentForm from './installments';

//Helper
import * as oHelper from '../../../helper';

const Form = t.form.Form;

export default class BonusForm extends Component {
    constructor(props){
        super(props);
        this.state={
            disabledSave: false,
            didMount: false,
            data: {
                id: "",
                name: "",
                default: false,
                installments: 0,
                cutoff: 0,
                date: null
            },
            cutOffRange: {},
            hiddenDate: true
        }
    }

    componentDidMount(){
        this.setState({didMount: true});
        this.initCutOffRange();
        this.initForm();
    }

    initForm = () => {
        const {data} = this.props;
        const activeData = {...this.state.data};
        let bHideDate = false;

        if(data.schedule.length === 0){
            activeData.date = null;
            bHideDate = true;
        }else{
            bHideDate = false;
            var curDate = data.schedule[0].date.value ? 
                Date.parse(oHelper.convertStringToDate(data.schedule[0].date.value))
            : 
                null;
            console.log('YYYYYYYYYYYYYY: ' + curDate);
            activeData.date = curDate;
        }

        activeData.id = data.id;
        activeData.name = data.name;
        activeData.default = data.default;
        activeData.installments = data.installments;
        activeData.cutoff = data.cutoff;

        this.setState({
            data: oHelper.copyObject(activeData),
            hiddenDate: bHideDate,
        })
    }
    
    initCutOffRange = async() => {
        /* alert(JSON.stringify(this.generateNumber(0, 25))); */
        /* this.setState({
            cutOffRange: await this.generateNumber(0, 25)
        }); */
    }

    _onDataChange = (value) => {
        console.log('XXXXXXXXX_onDataChange: _onDataChange')
        const oData = {...this.state.data};
        let bHideDate = false;
        if(value.installments == 0){
            oData.date = null;
            bHideDate = true;
        }else{
            bHideDate = false;
            oData.date = value.date;
        }

        oData.name = value.name;
        oData.installments = value.installments,
        oData.cutoff = value.cutoff;

        this.setState({
            data: oData,
            hiddenDate: bHideDate
        })
    }

    _onSubmit = () => {
        Keyboard.dismiss();
        let oData = this.refs.form_bonusSchedule.getValue();
        if(oData){
            this.props.onSubmit(this.state.data)
        }
    }

    generateNumber = (min, max) => {
        let oEnums = {};
        for (i = min; i <= max; i++) { 
            oEnums[i] = i;
        }
        return oEnums;
    }

    render() {
        const {data, cutOffRange, hiddenDate} = this.state;
        console.log('XXXXXTEST_VALUE: ' + data.date);
        console.log('XXXXXTEST_TYPE: ' + typeof data.date);
        const INSTALLMENTS_OPTIONS = t.enums({
            0: "0",
            1: "1"
        });

        const CUTOFF_OPTIONS = t.enums({
            0: "0",
            1: "1",
            2: "2",
            3: "3",
            4: "4",
            5: "5",
            6: "6",
            7: "7",
            8: "8",
            9: "9",
            10: "10",
            11: "11",
            12: "12",
            13: "13",
            14: "14",
            15: "15",
            16: "16",
            17: "17",
            18: "18",
            19: "19",
            20: "20",
            21: "21",
            22: "22",
            23: "23",
            24: "24",
            25: "25",
        });

        const ENTITY = t.struct({
            name: t.String,
            installments: INSTALLMENTS_OPTIONS,
            cutoff: CUTOFF_OPTIONS,
            date: hiddenDate ? t.maybe(t.Date) : t.Date,
        })
        
        const OPTIONS = {
            fields: {
                name:{ 
                    editable: false,
                    label: 'YEAR',
                    error: '*Required field'
                },

                installments: {
                    template: customPickerTemplate,
                    label: 'NUMBER OF INSTALLMENTS',
                    error: '*Required field'
                },

                cutoff: {
                    template: customPickerTemplate,
                    label: 'CUTOFF',
                    error: '*Required field'
                },

                date:{ 
                    template: customDatePickerTemplate,
                    hidden: hiddenDate,
                    label: 'PAY DATE',
                    mode:'date',
                    config:{
                        format:  (strDate) => oHelper.convertDateToString(strDate, 'MMMM DD, YYYY')
                    },
                    error: '*Required field'
                },
            },
            stylesheet: stylesheet
        }

        return (
            <FormModal 
                containerStyle={styles.formStyles.container}
                visible={this.props.visible}
                onCancel={() => this.props.onCancel(false)}
                onOK={this._onSubmit}
                disabledSave={this.state.disabledSave}
                title={this.props.title}
                submitLabel='SAVE'>
                
                <View style={styles.formStyles.contForm}>
                    {
                        this.state.didMount ?
                            <ScrollView>
                                <View style={styles.formStyles.formContent}>
                                    <Form 
                                        ref='form_bonusSchedule'
                                        type={ENTITY}
                                        value={data}
                                        options={OPTIONS}
                                        onChange={this._onDataChange}
                                    />
                                </View>
                            </ScrollView>
                        :
                            null
                    }
                    
                </View>
            </FormModal>
        );
    }
}

import React, { Component } from 'react';
import {
    View,
    Text,
    Alert,
    Switch,
    ScrollView,
    RefreshControl,
    Picker
} from 'react-native';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import GenericContainer from '../../../components/GenericContainer';
import CustomCard, 
{
    Description,
    PropTitle,
    PropLevel1, 
    PropLevel2
}
from '../../../components/CustomCards';
import ScreenButton from '../../../components/ScreenButton';
import BonusForm from './form'

//Component Constants
const TITLE = '13th Month Pay Policy';
const DESCRIPTION = 'Set 13th Month Pay Rule';
const SWITCH_COLOR_ON = '#838383';
const SWITCH_COLOR_THUMB = '#EEB843';
const SWITCH_COLOR_TINT = '#505251';

//Redux
import { connect } from 'react-redux';
import * as bonusSelector from '../data/bonus/selector';
import * as bonusActions from '../data/bonus/actions';
import { bindActionCreators } from 'redux';

//API
import * as bonusApi from '../data/bonus/api';

//Helper
import * as oHelper from '../../../helper';

//Styles
import styles from './styles';

export class Bonus extends Component {
    constructor(props){
        super(props);
        this.state = {
            refreshing: false,
            disabledMode: false,
            showForm: false,
            loadingScreen: {
                show: false,
                msg: 'test'
            },

            msgBox: {
                show: false,
                type: '',
                msg: '',
                param: ''
            },
            activeData: null,
            defaultData: {
                id: "",
                name: "2018",
                default: false,
                installments: 0,
                cutoff: 0,
                schedule: [],
            }
        }
    }

    componentDidMount(){
        this._fetchDataFromDB();
    }

    _fetchDataFromDB = async() => {
        await this.props.actions.bonus.get({...this._requiredInputs(), transtype:'get'});
    }

    _requiredInputs = () => {
        return({
            companyid: this.props.activecompany.id,
            username: this.props.logininfo.username,
            accesstoken: '',
            clientid: ''
        })
    }

    _onTogglePolicy = (value) => {
        const strReqStatus = value ? 'ON' : 'OFF';
        this._setMessageBox(
            true,
            'warning',
            'Are you sure you want to turn ' + strReqStatus + ' 13th Month Policy ? Press Continue to proceed.',
            'TOGGLEPOLICY'
        )
    }

    _toggleToDB = async() => {
        this._setMessageBox(false);
        const bStatus = !this.props.bonus.data.enabled;
        const strReqStatus = bStatus ? 'ON' : 'OFF';

        let oInput = this._requiredInputs();
        oInput.enabled = bStatus;
        oInput.transtype = 'request';

        this._setLoadingScreen(true, 'Turning ' + strReqStatus + ' 13th Month Policy. Please wait...');
        const oRes = await this.props.actions.bonus.toggle(oInput);
        this._evaluateResponse(oRes);
        this._setLoadingScreen(false);
    }

    _onSubmit = (value) => {
        let oInput = oHelper.copyObject(this.state.defaultData);
        oInput.id = value.id;
        oInput.name = value.name;
        oInput.default = value.default;
        oInput.installments = value.installments;
        oInput.cutoff = value.cutoff;
        
        if(value.date){
            oInput.schedule = [{
                index:"",
                date:{
                    label: oHelper.convertDateToString(value.date, 'MMMM DD, YYYY'),
                    value: oHelper.convertDateToString(value.date, 'MM/DD/YY')
                },
                editable: true
            }]
        }else{
            oInput.schedule = [];
        }

        if(value.id == ""){
            this._addToDB(oInput);
        }else{
            this._updateToDB(oInput);
        }
    }

    _addToDB = async(value) => {
        this._setLoadingScreen(true, 'Adding New 13th Month Schedule. Please wait...');
        this._setMessageBox(false);

        let oInput = this._requiredInputs();
        oInput.data = value;
        oInput.transtype = 'add';  
        const oRes = await this.props.actions.bonus.create(oInput);
        this._evaluateResponse(oRes);
        this._setLoadingScreen(false);
    }

    _updateToDB = async(value) => {
        this._setLoadingScreen(true, 'Updating 13th Month Pay Schedule for ' + value + '. Please wait...');
        this._setMessageBox(false);

        let oInput = this._requiredInputs();
        oInput.data = value;
        oInput.transtype = 'update';  
        const oRes = await this.props.actions.bonus.update(oInput);
        this._evaluateResponse(oRes);
        this._setLoadingScreen(false);
    }

    _addNewSchedule = () => {
        let oNewData = {...this.state.defaultData};

        const data = this.props.bonus.data;
        let iLatestYear = 0;

        if(data.data.length > 0){
            data.data.map((data, index) => {
                if(!isNaN(data.name)){
                    const iCurYear = parseInt(data.name);
                    if(iCurYear > iLatestYear){
                        iLatestYear = iCurYear;
                    }
                }
            })

            const iName = iLatestYear;
            oNewData.name = (iName + 1) + "";
        }else{
            oNewData.name = data.initialyear || '2018';
        }

        this.setState({
            activeData: oNewData
        },
            () => {
                this._setForm(true);
            }
        )
    }

    _updateActiveRule = async(value) => {
        const oData = await bonusSelector.getRuleFromID(value);
        this.props.actions.bonus.setActiveRule(oData);
    }

    _modifySchedule = () => {
        const activeRule = oHelper.copyObject(this.props.bonus.activeRule);
        this.setState({
            activeData: activeRule
        },
            () => {
                this._setForm(true);
            }
        )
    }

    _setForm = (value) => {
        this.setState({showForm: value})
    }

    _evaluateResponse = (res) => {
        switch (res.flagno + ''){
            case '0':
                this._setMessageBox(true, 'error-ok', res.message);
                return false
                break;
            case '1':
                this._setMessageBox(true, 'success', res.message);
                return true;
                break;
            default:
                this._setMessageBox(true, 'error-ok', CONSTANTS.ERROR.UNKNOWN);
                return false
                break;
        }
    }

    _setMessageBox = (show, type, msg, param) => {
        this.setState({
            msgBox: oHelper.setMsgBox(
                this.state.msgBox,
                show, 
                type,
                msg,
                param
            )
        })
    }

    _setLoadingScreen = (show, msg) => {
        let oLoadingScreen = {...this.state.loadingScreen};
        oLoadingScreen.show = show;
        oLoadingScreen.msg = msg;
        this.setState({ loadingScreen: oLoadingScreen });
    }

    _msgBoxOnClose = (params) => {
        this.setState({
            msgBox: oHelper.clearMsgBox(this.state.msgBox)
        })
    }

    _msgBoxOnYes = (param) => {

    }

    _msgBoxOnContinue = (param) => {
        switch(param.toUpperCase()){
            case 'TOGGLEPOLICY':
                this._toggleToDB();
                break;

            default:
                alert('UFO');
                break;
        }
    }

    render(){
        let viewOnly = this.props.viewOnly || false;

        const noteStyles = styles.noteStyles;
        const {data, status, activeRule} = this.props.bonus;

        /* console.log('XXXXXXXXXXXXXXXDATA: ' + JSON.stringify(data));
        console.log('XXXXXXXXXXXXXXXactiveRule: ' + JSON.stringify(activeRule));
        console.log('XXXXXXXXXXXXXXXstatus: ' + JSON.stringify(status)); */
        const {showForm, activeData} = this.state;
        let switchValue = false;
        let isEmpty = true;
        let oDisabledContent = null;
        let dPayDay = null;
        if(data){
            isEmpty = data.data.length === 0;
            switchValue = data.enabled;
            if(!isEmpty){
                /* console.log('XXXXXXXXXXisEmpty')
                console.log('XXXXXXXXXXactiveRule.schedule.length: ' + activeRule.schedule.length) */
                if(activeRule.schedule.length > 0){
                    /* console.log('XXXXXXXXXXlength') */
                    dPayDay = activeRule.schedule[0].date.label;
                }
            }
            oDisabledContent = 
                <View style={noteStyles.container}>
                    <View style={noteStyles.content}>
                        <Text style={noteStyles.txtTitle}>Policy is currently turned OFF.</Text>
                    </View>
                        
                    <View style={noteStyles.content}>
                        <Text style={noteStyles.txtDescription}>{data.description.enabled}</Text>
                    </View>
                    <View style={noteStyles.content}>
                        <Text style={noteStyles.txtDescription}>{data.description.disabled}</Text>
                    </View>
                </View>
        }
        
        
        return(
            <GenericContainer
                msgBoxShow = {this.state.msgBox.show}
                msgBoxType = {this.state.msgBox.type}
                msgBoxMsg = {this.state.msgBox.msg}
                msgBoxOnClose = {this._msgBoxOnClose}
                msgBoxOnYes = {this._msgBoxOnYes}
                msgBoxOnContinue = {this._msgBoxOnContinue}
                msgBoxParam = {this.state.msgBox.param}
                loadingScreenShow = {this.state.loadingScreen.show}
                loadingScreenMsg = {this.state.loadingScreen.msg}
                status={status}
                title={TITLE}
                onRefresh={this._fetchDataFromDB}>

                <CustomCard 
                    title={TITLE} 
                    description={DESCRIPTION} 
                    oType="Switch"
                    isScrollable={false}
                    rightHeader={
                        viewOnly ? 
                            <Text style={styles.txtSwitchViewOnly}>
                                {switchValue ? 'ON' : 'OFF'}
                            </Text>
                        :
                            <Switch
                                disabled={false}
                                onValueChange={this._onTogglePolicy} 
                                onTintColor={SWITCH_COLOR_ON}
                                thumbTintColor={SWITCH_COLOR_THUMB}
                                tintColor={SWITCH_COLOR_TINT}
                                value={switchValue}
                            />
                    }
                >
                    {
                        isEmpty ?
                            <ScreenButton
                                onPress={this._addNewSchedule}
                                title="No Added Schedule. Tap Here to Add."
                            />
                        :
                            <ScrollView
                                refreshControl={
                                    <RefreshControl
                                        refreshing={this.state.refreshing}
                                        onRefresh={this._fetchDataFromDB}
                                    />
                                }
                            >
                                {
                                    !switchValue ?
                                        oDisabledContent
                                    :
                                    <View>
                                        <PropLevel1 
                                            name='Year'
                                            content={
                                                <Picker
                                                    mode='dropdown'
                                                    style={styles.pickerStyle}
                                                    selectedValue={activeRule.id}
                                                    onValueChange={(itemValue, itemIndex) => {this._updateActiveRule(itemValue)}}>
                                                    {
                                                        data.data.map((data, index) => (
                                                            <Picker.Item key={index} label={data.name} value={data.id} />
                                                        ))
                                                    }
                                                </Picker>
                                            }
                                            
                                            hideBorder={false}
                                            contentStyle={{
                                                width: 130
                                            }}
                                        />
            
                                        <PropTitle name='Installments'/>
                                            
                                        <PropLevel2 
                                            name='Number of Installments'
                                            content={
                                                <Text>
                                                    {activeRule.installments} 
                                                </Text>
                                            }
                                            hideBorder={true}
                                            contentStyle={{
                                                width: 75
                                            }}
                                        />
            
                                        <PropLevel2 
                                            name='Notification Cutoff'
                                            content={
                                                <Text>
                                                     {activeRule.cutoff} 
                                                </Text>
                                            }
                                            hideBorder={true}
                                            contentStyle={{
                                                width: 75
                                            }}
                                        />
            
                                        <PropTitle name='Payment Schedule'/>

                                        <PropLevel2 
                                            name={dPayDay ? 'Pay Day' : 'No Schedule Set'}
                                            content={
                                                <Text>
                                                    {dPayDay} 
                                                </Text>
                                            }
                                            hideBorder={true}
                                            contentStyle={{
                                                width: 200
                                            }}
                                        />  
                                    </View>
                                }
                            </ScrollView>
                    }

                </CustomCard>

                {
                    isEmpty || !switchValue || viewOnly ? 
                        null
                    :
                        <ActionButton 
                            bgColor='rgba(0,0,0,0.8)'
                            buttonColor="#EEB843"
                            spacing={10}
                            icon={<Icon name="clock" color='#fff' size={25} />}>
                            
                            <ActionButton.Item size={45} buttonColor='#26A65B' title="ADD NEW SCHEDULE" onPress={this._addNewSchedule}>
                                <Icon name="plus" color='#fff' size={22} />
                            </ActionButton.Item>
                            {
                                activeRule.editable ? 
                                    <ActionButton.Item size={45} buttonColor='#4183D7' title="MODIFY SELECTED YEAR'S SCHEDULE" onPress={this._modifySchedule}>
                                        <Icon name="table-edit" color='#fff' size={22} />
                                    </ActionButton.Item>
                                :
                                    null
                            }
                            

                        </ActionButton>
                }

                {
                    showForm ? 
                        <BonusForm
                            data={activeData}
                            visible={showForm}
                            title={'13TH MONTH PAY SCHEUDULE'}
                            onSubmit={this._onSubmit}
                            onCancel={()=> this._setForm(false)}
                        />
                    :
                        null
                }
                
            </GenericContainer>
        );
    }
}
    

function mapStateToProps (state) {
    return {
        logininfo: state.loginReducer.logininfo,
        activecompany: state.activeCompanyReducer.activecompany,
        bonus: state.companyPoliciesReducer.bonus
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            bonus: bindActionCreators(bonusActions, dispatch),
        },
    }
}
  
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Bonus)
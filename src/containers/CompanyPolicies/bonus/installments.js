import React, { Component } from 'react';
import {
  View,
  ScrollView,
  Keyboard
} from 'react-native';
import t from 'tcomb-form-native'; // 0.6.9
import moment from "moment";

//Form Template 
import { customPickerTemplate } from '../../../global/tcomb-custom-select-android';
import { customDatePickerTemplate } from '../../../global/tcomb-custom-datepicker-android';

//Styles
import styles from './styles';
import stylesheet from '../../../global/globalFormStyle';
import * as CustomForm from '../../../components/CustomForm';

//Custom Components
import FormModal from '../../../components/FormModal';

//Helper
import * as oHelper from '../../../helper';

const Form = t.form.Form;

export default class BonusInstallmentForm extends Component {
    constructor(props){
        super(props);
        this.state={
            data: {
                index: "",
                date: null,
                editable: true
            },   
        }
    }

    componentDidMount(){
        this.setState({didMount: true});
    }

    _onSubmit = () => {
        Keyboard.dismiss();
        let oData = this.refs.form_installments.getValue();
        if(oData){
            this.props.onSubmit(this.state.data)
        }
    }

    render() {
        const {data} = this.state;

        const ENTITY = t.struct({
            date: t.Date,
        })
        
        const OPTIONS = {
            fields: {
                date:{ 
                    template: customDatePickerTemplate,
                    label: 'INSTALLMENT #',
                    mode:'date',
                    config:{
                        format:  (strDate) => oHelper.convertDateToString(strDate, 'MMMM DD, YYYY')
                    },
                    error: '*Required field'
                },
            },
            stylesheet: stylesheet
        }

        return (

            <Form 
                ref='form_installments'
                type={ENTITY}
                value={data}
                options={OPTIONS}
                onChange={this._onDataChange}
            />

        );
    }
}

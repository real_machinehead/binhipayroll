import React, { Component } from 'react';
import {
    View,
    Text,
    Alert,
    Switch,
    ScrollView,
    RefreshControl,
    FlatList
} from 'react-native';
import CustomCard, {SimpleCard} from '../../../components/CustomCards';
import GenericContainer from '../../../components/GenericContainer';
import EmptyList from '../../../components/EmptyList';
import * as PromptScreen from '../../../components/ScreenLoadStatus';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Calendarholiday from './form';

//Styles
import styles from './styles';

//helper
import * as oHelper from '../../../helper';

//Component Constants
const TITLE = 'List of Holidays';
const DESCRIPTION = 'Add, remove and update Holidays'
const SWITCH_COLOR_ON = '#838383';
const SWITCH_COLOR_THUMB = '#EEB843';
const SWITCH_COLOR_TINT = '#505251';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as holidayActions from '../data/holidays/actions';

//API
import * as calendarApi from '../data/holidays/api';

export class HolidayPolicy extends Component {
    constructor(props){
        super(props);
        this.state = {
            refreshing: false,
            _showForm: false, 
            disabledMode: false,
            data: {
                isenabled: true,
                datedisplayformat: 'MMM DD YYYY',
                datedisplayformformat: 'MMMM DD, YYYY'
            },

            loadingScreen: {
                show: false,
                msg: 'test'
            },

            msgBox: {
                show: false,
                type: '',
                msg: '',
                param: ''
            },

            activeData: {
                id: '',
                name: '',
                date: '',
                type:''
            }
            
        }
    }

    componentDidMount(){
        if(this.props.holidays.status[0] != 1){
            this._fetchDataFromDB();
        }
    }

    _fetchDataFromDB= () => {
        this.props.actions.holidays.getList();
    }

    _setMessageBox = (show, type, msg, param) => {
        this.setState({
            msgBox: oHelper.setMsgBox(
                this.state.msgBox,
                show, 
                type,
                msg,
                param
            )
        })
    }

    _setLoadingScreen = (show, msg) => {
        let oLoadingScreen = {...this.state.loadingScreen};
        oLoadingScreen.show = show;
        oLoadingScreen.msg = msg;
        this.setState({ loadingScreen: oLoadingScreen });
    }

    _msgBoxOnClose = (params) => {
        this.setState({
            msgBox: oHelper.clearMsgBox(this.state.msgBox)
        })
    }

    _msgBoxOnYes = (param) => {
        console.log('param: ' + param );
        let oData = null;
        switch(param[0].toUpperCase()){
            case 'SWITCHOFFPOLICY':
                oData = {...this.props.calendar.data};
                oData.validfrom = null;
                oData.validto = null;
                oData.amount = 0;
                oData.isenabled = param[1];
                this._updateEmpSavPol(oData);
                break;

            case 'CLOSEPOLICYFORM':
                this._setEmpSavPolForm(false);
                this._setMessageBox(false);
                break;

            case 'UPDATEPOLICY':
                this._setMessageBox(false);
                oData = {...param[1]};
                oData.isenabled = true;
                this._updateEmpSavPol(oData);
                break;

            default:
                this._setMessageBox(false);
                break;
        }
    }

    _onPress = () => {
        let oLoadingScreen = {...this.state.loadingScreen};
        oLoadingScreen.show = true;
        oLoadingScreen.msg = 'HAHAHHAhAHA';;
        
        this.setState({
            loadingScreen: oLoadingScreen
        });
    }

    _keyExtractor = (item, index) => item.id;

    _renderItem = (oItem) => {
        console.log('oItems',oItem)
        const menu = 
            oItem.status == 'ACTIVE' ? 
                [
                    { onSelect: () => this._updatePosition(oItem), label: 'Update' },
                    { onSelect: () => this._deletePosition(oItem), label: 'Delete' }
                ]
            :
                []

        const backgroundColor =
            oItem.status == 'ACTIVE' ? 
                {backgroundColor: '#26A65B'}
            : 
                {backgroundColor: '#E74C3C'}
                
        const oTitle = 
            <View style={styles.title.contContent}>
                <View style={styles.title.contIcon}>
                    <View style={styles.title.iconPlaceholder}>
                        <Icon 
                            name='worker'
                            size={30} 
                            color='#f4f4f4'
                        />
                    </View>
                </View>
                <View style={styles.title.contLabel}>
                    <Text style={styles.title.txtLabel}>
                        { oItem.code }
                    </Text>
                </View>
            </View>

        return(
            <View>
                <View style={{
                    marginBottom: 10,
                    marginLeft: 30,
                    marginRight: 30,
                }}>
                    <SimpleCard 
                        data={[
                            ['Holiday Name', oItem.name], 
                            ['Date', oHelper.convertDateToString(oItem.date, 'MMMM DD, YYYY')],
                            ['type',oItem.type]]
                        }
                        customTitleStyle={backgroundColor}
                        oTitle={oTitle}
                        menu={menu}
                    />
                </View>
                
            </View>
        );
    }

    _addNewCalendar = () => {
        this.setState({
            _showForm: true,
            activeData: {id: '', name: '', name:'',date:'',type:''}
        })
    }

    _updateCalendar = (oData) => {
        this.setState({
            _showForm: true,
            activeData: oData
        })
    }

    _deleteCalendar = (oData) => {
        Alert.alert(
            'Warning',
            'Are you sure you want to delete ' + oData.name + ' ?',
            [
                {text: 'No', onPress: () => {}},
                {text: 'Yes', onPress: () => this._deleteToDB(oData)},
            ],
            { cancelable: false }
        ) 
    }

    _closeForm = () => {
        this.setState({
            _showForm: false
        })
    }

    _onSubmitForm = (oData) => {
        if(oHelper.isStringEmptyOrSpace(oData.id)){
            this._addToDB(oData);
        }else{
            Alert.alert(
                'Confirm Action',
                'Are you sure you want to update ' + oData.name + ' ?',
                [
                    {text: 'No', onPress: () => {}},
                    {text: 'Yes', onPress: () => this._updateToDB(oData)},
                ],
                { cancelable: false }
            ) 
        }
    }

    _addToDB = async(oData) => {
        this._setLoadingScreen(true, 'Saving New Position. Please wait...');
        await calendarApi.add(oData)
            .then((response) => response.json())
            .then(async(res) => {
                console.log('RES: ' + JSON.stringify(res));
                if(res.flagno == 1){
                    oData.id = res.id;
                    oData.dateadded = await oHelper.convertDateToString(new Date(), 'YYYY-MM-DD');
                    oData.status = 'ACTIVE';
                    await this.props.actions.calendar.add(oData);
                    this._closeForm();
                }
                this._setMessageBox(true, res.flagno==1 ? 'success' : 'error-ok', res.message);
            })
            .catch((exception) => {
                this._setMessageBox(true, 'error-ok', exception.message);
            });
        this._setLoadingScreen(false);
    } 

    _updateToDB = async(oData) => {
        this._setLoadingScreen(true, 'Updating Position. Please wait...');
        await calendarApi.update(oData)
            .then((response) => response.json())
            .then((res) => {
                console.log('RES: ' + JSON.stringify(res));
                if(res.flagno == 1){
                    this.props.actions.calendar.update(oData)
                    this._closeForm();
                }
                this._setMessageBox(true, res.flagno==1 ? 'success' : 'error-ok', res.message);
            })
            .catch((exception) => {
                /* this._setMessageBox(true, 'error-ok', 'Failed to update ' + TITLE); */
                this._setMessageBox(true, 'error-ok', exception.message);
            });
        this._setLoadingScreen(false);
    } 

    _deleteToDB = async(oData) => {
        this._setLoadingScreen(true, 'Deleting Position. Please wait...');
        await calendarApi.remove(oData)
            .then((response) => response.json())
            .then((res) => {
                console.log('RES: ' + JSON.stringify(res));
                if(res.flagno == 1){
                    oData.status = 'DELETED';
                    this.props.actions.calendar.update(oData);
                    this._closeForm();
                }
                this._setMessageBox(true, res.flagno==1 ? 'success' : 'error-ok', res.message);
            })
            .catch((exception) => {
                /* this._setMessageBox(true, 'error-ok', 'Failed to update ' + TITLE); */
                this._setMessageBox(true, 'error-ok', exception.message);
            });
        this._setLoadingScreen(false);
    } 

    render(){
        const oData = this.props.holidays.data;
        const aStatus = this.props.holidays.status;
        console.log('data length',aStatus);
        return(
            <GenericContainer
                msgBoxShow = {this.state.msgBox.show}
                msgBoxType = {this.state.msgBox.type}
                msgBoxMsg = {this.state.msgBox.msg}
                msgBoxOnClose = {this._msgBoxOnClose}
                msgBoxOnYes = {this._msgBoxOnYes}
                msgBoxParam = {this.state.msgBox.param}
                loadingScreenShow = {this.state.loadingScreen.show}
                loadingScreenMsg = {this.state.loadingScreen.msg}
                status={this.props.holidays.status}
                title={TITLE}
                onRefresh={this._fetchDataFromDB}>
                {
                    oData ?
                        <CustomCard 
                            contentContainerStyle={styles.container}
                            clearMargin
                            title={TITLE} 
                            oType='text'
                            description = {DESCRIPTION}
                        >
    
                            {
                                oData.data.length < 1 ?
                                    <EmptyList message='No Calendar Added' />
                                :
                                    <FlatList
                                        refreshing={this.state.refreshing}
                                        onRefresh={this._fetchDataFromDB}
                                        contentContainerStyle={styles.reportsFlatlist}
                                        ref={(ref) => { this.flatListRef = ref; }}
                                        extraData={oData}
                                        data={oData.data}
                                        keyExtractor={this._keyExtractor}
                                        renderItem={({item}) => this._renderItem(item) }
                                    />
                            }
                            
                        </CustomCard> 
                    :
                        null
                }
                {
                    this.state._showForm ?
                        <Calendarholiday 
                            onCancel={this._closeForm}
                            onSubmit={this._onSubmitForm}
                            visible={this.state._showForm}
                            title='HOLIDAYS'
                            data={this.state.activeData}
                        />
                    :
                        null
                }
                <ActionButton 
                    bgColor='rgba(0,0,0,0.8)'
                    buttonColor="#EEB843"
                    spacing={10}>
                    <ActionButton.Item buttonColor='#26A65B' title="ADD NEW HOLIDAY" onPress={this._addNewCalendar}>
                        <Icon name="bell-plus" color='#fff' size={22} style={styles.actionButtonIcon} />
                    </ActionButton.Item>
                </ActionButton>
                
            </GenericContainer>
        );
    }
    
}

function mapStateToProps (state) {
    return {
        holidays: state.companyPoliciesReducer.holidays
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            holidays: bindActionCreators(holidayActions, dispatch),
        }
    }
}
  
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HolidayPolicy)
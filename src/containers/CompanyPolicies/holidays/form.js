import React, { Component } from 'react';
import {
  View,
  ScrollView,
  Keyboard
} from 'react-native';
import t from 'tcomb-form-native'; // 0.6.9
import moment from "moment";

//Form Template 
import { customPickerTemplate } from '../../../global/tcomb-custom-select-android';
import { customDatePickerTemplate } from '../../../global/tcomb-custom-datepicker-android';

//Styles
import styles from './styles';
import stylesheet from '../../../global/globalFormStyle';
import * as CustomForm from '../../../components/CustomForm';

//Custom Components
import FormModal from '../../../components/FormModal';

//Helper
import * as oHelper from '../../../helper';

const Form = t.form.Form;

export default class Calendarholiday extends Component {
    constructor(props){
        super(props);
        const dateHoliday=this.props.data.date ? 
        oHelper.convertStringToDate(this.props.data.date, this.props.data.displaydateformformat)
        :
        null;

        this.state={
            disabledSave: false,
            didMount: false,
            data: {
                id: this.props.data.id,
                name: this.props.data.name,
                date: dateHoliday || '',
                type: this.props.data.status || '',
            }
        }
    }

    componentDidMount(){
        this.setState({didMount: true});
    }

    _onDataChange = (value) => {
        let oData = oHelper.copyObject(this.state.data);
        oData.code = value.code;
        oData.name = value.name;
        if(value.code && value.name)
        this.setState({ data: oData });
    }

    _onSubmit = () => {
        Keyboard.dismiss();
        let oData = this.refs.form_calendarform.getValue();
        if(oData){
            this.props.onSubmit(this.state.data)
        }
    }

    render() {

        const ENTITY = t.struct({
            name: t.String,
            type: t.Integer,
            date: t.Date
        })
        
        const OPTIONS = {
            fields: {
                name:{ 
                    label: 'HOLIDAY NAME' ,
                    returnKeyType: 'done',
                    error: '*Enter a Holiday name'
                },
                date: {
                    template: customDatePickerTemplate,
                    label: this.state.data.isEndSet ? 'VALID FROM' : 'EFFECTIVE DATE',
                    mode:'date',
                    config:{
                        format:  (strDate) => oHelper.convertDateToString(strDate, this.props.data.displaydateformformat)
                    },
                    error: '*Required field'
                },
            },
            stylesheet: stylesheet
        }

        return (
            <FormModal 
                containerStyle={styles.formStyles.container}
                visible={this.props.visible}
                onCancel={this.props.onCancel}
                onOK={this._onSubmit}
                disabledSave={this.state.disabledSave}
                title={this.props.title}
                submitLabel='SAVE'>
                
                <View style={styles.formStyles.contForm}>
                    {
                        this.state.didMount ?
                            <ScrollView>
                                <View style={styles.formStyles.formContent}>
                                    <Form 
                                        ref='form_calendarform'
                                        type={ENTITY}
                                        value={this.state.data}
                                        options={OPTIONS}
                                        onChange={this._onDataChange}
                                    />
                                </View>
                            </ScrollView>
                        :
                            null
                    }
                    
                </View>
            </FormModal>
        );
    }
}

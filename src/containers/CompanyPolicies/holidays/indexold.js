import React, { Component } from 'react';
import {
    View,
    Text,
    Alert,
    Switch,
    ScrollView,
    RefreshControl,
    FlatList
} from 'react-native';
import CustomCard, {SimpleCard} from '../../../components/CustomCards';
import GenericContainer from '../../../components/GenericContainer';
import EmptyList from '../../../components/EmptyList';
import * as PromptScreen from '../../../components/ScreenLoadStatus';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Calendarholiday from './form';

//Styles
import styles from './styles';

//helper
import * as oHelper from '../../../helper';

//Component Constants
const TITLE = 'List of Employee Positions';
const DESCRIPTION = 'Add, remove and update company positions'
const SWITCH_COLOR_ON = '#838383';
const SWITCH_COLOR_THUMB = '#EEB843';
const SWITCH_COLOR_TINT = '#505251';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as calendarActions from '../data/holidays/actions';

//API
import * as calendarApi from '../data/holidays/api';

export class CalendarPolicy extends Component {
    constructor(props){
        super(props);
        this.state = {
            refreshing: false,
            _showForm: false, 
            disabledMode: false,
            data: {
                
                isenabled: true,
                datedisplayformat: 'MMM DD YYYY',
                datedisplayformformat: 'MMMM DD, YYYY'
            },

            loadingScreen: {
                show: false,
                msg: 'test'
            },

            msgBox: {
                show: false,
                type: '',
                msg: '',
                param: ''
            },

            activeData: {
                id: '',
                name: '',
                code: ''
            }
            
        }
    }
    render(){
        console.log('asdfasdfssss');
        return(
            <View>
                <Text>adfadsfadsf</Text>
            </View>
        );
    }
}

function mapStateToProps (state) {
    return {
        calendar: state.companyPoliciesReducer.calendar
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            calendar: bindActionCreators(calendarActions, dispatch),
        }
    }
}
  
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CalendarPolicy)
import React, { Component } from 'react';
import {
    View,
    Text,
    Alert,
    Switch,
    ScrollView,
    RefreshControl,
    FlatList
} from 'react-native';
import CustomCard, {SimpleCard} from '../../../components/CustomCards';
import GenericContainer from '../../../components/GenericContainer';
import EmptyList from '../../../components/EmptyList';
import * as PromptScreen from '../../../components/ScreenLoadStatus';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Companyidform from './form';

//Styles
import styles from './styles';

//helper
import * as oHelper from '../../../helper';

//Component Constants
const TITLE = 'COMPANY ID';
const DESCRIPTION = 'Update Company Id'
const SWITCH_COLOR_ON = '#838383';
const SWITCH_COLOR_THUMB = '#EEB843';
const SWITCH_COLOR_TINT = '#505251';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as companyidAction from '../data/companyid/actions';

//API
import * as CompanyidApi from '../data/companyid/api';

export class CompanyidPolicy extends Component {
    constructor(props){
        super(props);
        this.state = {
            refreshing: false,
            _showForm: false, 
            disabledMode: false,
            data: {
                isenabled: true,
                datedisplayformat: 'MMM DD YYYY',
                datedisplayformformat: 'MMMM DD, YYYY'
            },

            loadingScreen: {
                show: false,
                msg: 'test'
            },

            msgBox: {
                show: false,
                type: '',
                msg: '',
                param: ''
            },

            activeData: {
                id: '',
                name: '',
                number: '',
            }
            
        }
    }

    _generatePayload = (page) => ({
        compid: this.props.compid,
        infotype: "companyid",
        companyname:""
    });

    _onMount = () => 
        this.props.actions.pagibig.get(
            this._generatePayload(1)
        );


    componentDidMount(){
        if(this.props.companyid.status[0] != 1){
            this._fetchDataFromDB();
        }
    }

    _fetchDataFromDB= () => {
        this.props.actions.companyid.get(this._generatePayload());
    }

    _setMessageBox = (show, type, msg, param) => {
        this.setState({
            msgBox: oHelper.setMsgBox(
                this.state.msgBox,
                show, 
                type,
                msg,
                param
            )
        })
    }

    _setLoadingScreen = (show, msg) => {
        let oLoadingScreen = {...this.state.loadingScreen};
        oLoadingScreen.show = show;
        oLoadingScreen.msg = msg;
        this.setState({ loadingScreen: oLoadingScreen });
    }

    _msgBoxOnClose = (params) => {
        this.setState({
            msgBox: oHelper.clearMsgBox(this.state.msgBox)
        })
    }

    _msgBoxOnYes = (param) => {
        console.log('param: ' + param );
        let oData = null;
        switch(param[0].toUpperCase()){
            case 'SWITCHOFFPOLICY':
                oData = {...this.props.companyid.data};
                oData.validfrom = null;
                oData.validto = null;
                oData.amount = 0;
                oData.isenabled = param[1];
                this._updateEmpSavPol(oData);
                break;

            case 'CLOSEPOLICYFORM':
                this._setEmpSavPolForm(false);
                this._setMessageBox(false);
                break;

            case 'UPDATEPOLICY':
                this._setMessageBox(false);
                oData = {...param[1]};
                oData.isenabled = true;
                this._updateEmpSavPol(oData);
                break;

            default:
                this._setMessageBox(false);
                break;
        }
    }

    _onPress = () => {
        let oLoadingScreen = {...this.state.loadingScreen};
        oLoadingScreen.show = true;
        oLoadingScreen.msg = 'HAHAHHAhAHA';;
        
        this.setState({
            loadingScreen: oLoadingScreen
        });
    }

    _keyExtractor = (item, index) =>item.name;

    _renderItem = (oItem) => {
        console.log('oItems',oItem)
        oItem.status='ACTIVE';
        const menu = 
            oItem.status == 'ACTIVE' ? 
                [
                    { onSelect: () => this._updateCompanyid(oItem), label: 'Update' }
                ]
            :
                []

        const backgroundColor =
            oItem.status == 'ACTIVE' ? 
                {backgroundColor: '#26A65B'}
            : 
                {backgroundColor: '#E74C3C'}
                
        const oTitle = 
            <View style={styles.title.contContent}>
                <View style={styles.title.contIcon}>
                    <View style={styles.title.iconPlaceholder}>
                        <Icon 
                            name='worker'
                            size={30} 
                            color='#f4f4f4'
                        />
                    </View>
                </View>
                <View style={styles.title.contLabel}>
                    <Text style={styles.title.txtLabel}>
                        { oItem.name }
                    </Text>
                </View>
            </View>

        return(
            <View>
                <View style={{
                    marginBottom: 10,
                    marginLeft: 30,
                    marginRight: 30,
                }}>
                    <SimpleCard 
                        data={[
                            ['Name', oItem.name], 
                            ['number',oItem.number],['Status',oItem.status],]
                        }
                        customTitleStyle={backgroundColor}
                        oTitle={oTitle}
                        menu={menu}
                    />
                </View>
                
            </View>
        );
    }

    _addNewCompanyid = () => {
        this.setState({
            _showForm: true,
            activeData: {id: '', name: '', number:'',transtype:'insert',compid:this.props.companyid}
        })
    }

    _updateCompanyid = (oData) => {
        console.log('Datas',oData);
        this.setState({
            _showForm: true,
            activeData: oData
        })
    }

    _deleteCompanyid = (oData) => {
        Alert.alert(
            'Warning',
            'Are you sure you want to delete ' + oData.name + ' ?',
            [
                {text: 'No', onPress: () => {}},
                {text: 'Yes', onPress: () => this._deleteToDB(oData)},
            ],
            { cancelable: false }
        ) 
    }

    _closeForm = () => {
        this.setState({
            _showForm: false
        })
    }

    _onSubmitForm = (oData) => {
        if(oHelper.isStringEmptyOrSpace(oData.id)){
            this._addToDB(oData);
        }else{
            Alert.alert(
                'Confirm Action',
                'Are you sure you want to update ' + oData.name + ' ?',
                [
                    {text: 'No', onPress: () => {}},
                    {text: 'Yes', onPress: () => this._updateToDB(oData)},
                ],
                { cancelable: false }
            ) 
        }
    }

    _addToDB = async(oData) => {
        console.log('addData',oData);
        this._setLoadingScreen(true, 'Saving New Company Id Please wait...');
        await CompanyidApi.add(oData)
            .then((response) => response.json())
            .then(async(res) => {
                console.log('RES: ' + JSON.stringify(res));
                if(res.flagno == 1){
                    oData.id = res.id_;

                    //console.log('RESSSS: ' + JSON.stringify(oData));
                    console.log('RESSSS: ' + oData);
                    //oData.dateadded = await oHelper.convertDateToString(new Date(), 'YYYY-MM-DD');
                    oData.status = 'ACTIVE';
                    await this.props.actions.companyid.add(oData);
                    this._closeForm();
                }
                this._setMessageBox(true, res.flagno==1 ? 'success' : 'error-ok', res.message);
            })
            .catch((exception) => {
                this._setMessageBox(true, 'error-ok', exception.message);
            });
        this._setLoadingScreen(false);
    } 

    _updateToDB = async(oData) => {
        this._setLoadingScreen(true, 'Updating Company Id. Please wait...');
        oData.transtype='update';
        oData.compid=this.props.compid;
        await CompanyidApi.update(oData)
            .then((response) => response.json())
            .then((res) => {
                console.log('RES: ' + JSON.stringify(res));
                if(res.flagno == 1){
                    this.props.actions.companyid.update(oData)
                    this._closeForm();
                }
                this._setMessageBox(true, res.flagno==1 ? 'success' : 'error-ok', res.message);
            })
            .catch((exception) => {
                /* this._setMessageBox(true, 'error-ok', 'Failed to update ' + TITLE); */
                this._setMessageBox(true, 'error-ok', exception.message);
            });
        this._setLoadingScreen(false);
    } 

    _deleteToDB = async(oData) => {
        this._setLoadingScreen(true, 'Deleting Position. Please wait...');
        await CompanyidApi.remove(oData)
            .then((response) => response.json())
            .then((res) => {
                console.log('RES: ' + JSON.stringify(res));
                if(res.flagno == 1){
                    oData.status = 'DELETED';
                    this.props.actions.companyid.update(oData);
                    this._closeForm();
                }
                this._setMessageBox(true, res.flagno==1 ? 'success' : 'error-ok', res.message);
            })
            .catch((exception) => {
                /* this._setMessageBox(true, 'error-ok', 'Failed to update ' + TITLE); */
                this._setMessageBox(true, 'error-ok', exception.message);
            });
        this._setLoadingScreen(false);
    } 

    render(){
        const oData = this.props.companyid.data;
        const aStatus = this.props.companyid.status;
        console.log('data length',oData);
        return(
            <GenericContainer
                msgBoxShow = {this.state.msgBox.show}
                msgBoxType = {this.state.msgBox.type}
                msgBoxMsg = {this.state.msgBox.msg}
                msgBoxOnClose = {this._msgBoxOnClose}
                msgBoxOnYes = {this._msgBoxOnYes}
                msgBoxParam = {this.state.msgBox.param}
                loadingScreenShow = {this.state.loadingScreen.show}
                loadingScreenMsg = {this.state.loadingScreen.msg}
                status={aStatus}
                title={TITLE}
                onRefresh={this._fetchDataFromDB}>
                {
                    oData ?
                        <CustomCard 
                            contentContainerStyle={styles.container}
                            clearMargin
                            title={TITLE} 
                            oType='text'
                            description = {DESCRIPTION}
                        >
    
                            {
                                oData.length < 1 ?
                                    <EmptyList message='No Company Added' />
                                :
                                    <FlatList
                                        refreshing={this.state.refreshing}
                                        onRefresh={this._fetchDataFromDB}
                                        contentContainerStyle={styles.reportsFlatlist}
                                        ref={(ref) => { this.flatListRef = ref; }}
                                        extraData={oData}
                                        data={oData}
                                        keyExtractor={this._keyExtractor}
                                        renderItem={({item}) => this._renderItem(item) }
                                    />
                            }
                            
                        </CustomCard> 
                    :
                        null
                }
                {
                    this.state._showForm ?
                        <Companyidform 
                            onCancel={this._closeForm}
                            onSubmit={this._onSubmitForm}
                            visible={this.state._showForm}
                            title='COMPANY ID'
                            data={this.state.activeData}
                        />
                    :
                        null
                }
                
                
            </GenericContainer>
        );
    }
    
}

function mapStateToProps (state) {
    //console.log('statessss',state.companyPoliciesReducer.companyid);
    return {
        companyid: state.companyPoliciesReducer.companyid,
        compid: state.activeCompanyReducer.activecompany[6]
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: {
            companyid: bindActionCreators(companyidAction, dispatch),
        }
    }
}
  
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CompanyidPolicy)
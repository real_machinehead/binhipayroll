import React, { Component } from 'react';
import {
  View,
  ScrollView,
  Keyboard
} from 'react-native';
import t from 'tcomb-form-native'; // 0.6.9
import moment from "moment";

//Form Template 
import { customPickerTemplate } from '../../../global/tcomb-custom-select-android';
import { customDatePickerTemplate } from '../../../global/tcomb-custom-datepicker-android';

//Styles
import styles from './styles';
import stylesheet from '../../../global/globalFormStyle';
import * as CustomForm from '../../../components/CustomForm';

//Custom Components
import FormModal from '../../../components/FormModal';

//Helper
import * as oHelper from '../../../helper';

const Form = t.form.Form;

export default class Companyidform extends Component {
    constructor(props){
        super(props);
        const dateHoliday=this.props.data.date ? 
        oHelper.convertStringToDate(this.props.data.date, this.props.data.displaydateformformat)
        :
        null;

        this.state={
            disabledSave: false,
            didMount: false,
            data: {
                id: this.props.data.id,
                name: this.props.data.name,
                number: this.props.data.number,
                transtype:this.props.data.transtype,
                compid:this.props.data.compid,
                status: this.props.data.status || '',
            }
        }
    }

    componentDidMount(){
        this.setState({didMount: true});
    }

    _onDataChange = (value) => {
       
        let oData = oHelper.copyObject(this.state.data);
        oData.name = value.name;
        oData.number = value.number;
 
        if(value.name && value.number)
        this.setState({ data: oData });

    }

    _onSubmit = () => {
        Keyboard.dismiss();
        let oData = this.refs.form_companyid.getValue();
        if(oData){
            this.props.onSubmit(this.state.data)
        }
    }

    render() {
        console.log('branch form',this.props)
        const ENTITY = t.struct({
            name: t.String,
            number: t.String
        })
        
        const OPTIONS = {
            fields: {
                name:{ 
                    label: 'NAME' ,
                    returnKeyType: 'next',
                    error: '*Enter a company branch name'
                },
                number:{ 
                    label: 'NUMBER' ,
                    returnKeyType: 'next',
                    error: '*Enter a govt id information'
                },              
            },
            stylesheet: stylesheet
        }

        return (
            <FormModal 
                containerStyle={styles.formStyles.container}
                visible={this.props.visible}
                onCancel={this.props.onCancel}
                onOK={this._onSubmit}
                disabledSave={this.state.disabledSave}
                title={this.props.title}
                submitLabel='SAVE'>
                
                <View style={styles.formStyles.contForm}>
                    {
                        this.state.didMount ?
                            <ScrollView>
                                <View style={styles.formStyles.formContent}>
                                    <Form 
                                        ref='form_companyid'
                                        type={ENTITY}
                                        value={this.state.data}
                                        options={OPTIONS}
                                        onChange={this._onDataChange}
                                    />
                                </View>
                            </ScrollView>
                        :
                            null
                    }
                    
                </View>
            </FormModal>
        );
    }
}

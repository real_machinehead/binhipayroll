import { combineReducers } from 'redux';
import { reducer as employmenttypeReducer } from './employmenttype/reducer';
import { reducer as paytypeReducer } from './paytype/reducer';
import { reducer as defaultgovbenefitsReducer } from './defaultgovbenifits/reducer';

export const reducer = combineReducers({
	employmenttype: employmenttypeReducer,
	paytype: paytypeReducer,
	defaultgovbenefits: defaultgovbenefitsReducer,
});
import * as api from './api';
import * as actionTypes from './actionTypes';
import { CONSTANTS } from '../../../../constants';
import * as activeProfileActions from '../../../Employees/data/activeProfile/actions';

export const update = payload =>({
	type: actionTypes.UPDATE,
	payload
})

export const updateStatus = payload =>({
	type: actionTypes.STATUS,
	payload
})

export const get = payload =>
	dispatch => {
		let objRes = {};
		dispatch(updateStatus(CONSTANTS.STATUS.LOADING));
		api.get(payload)
		.then((response) => response.json())
		.then((res) => {
			console.log('RES: ' + JSON.stringify(res));
			if(res.flagno == 1){
				dispatch(update(res.data.government.data));
			}
			objRes = {...res}
		})
		.then(() => {
			dispatch(updateStatus([
				objRes.flagno || 0, 
				objRes.message || CONSTANTS.ERROR.SERVER
			]));
		})
		.catch((exception) => {
			dispatch(updateStatus([
				0, 
				exception.message + '.'
			]));
			console.log('exception: ' + exception.message);
		});
	}

export const getAndUpdateActiveEmployee = payload =>
	dispatch => {
		let objRes = {};
		dispatch(updateStatus(CONSTANTS.STATUS.LOADING));
		api.get(payload)
		.then((response) => response.json())
		.then((res) => {
			console.log('RES: ' + JSON.stringify(res));
			if(res.flagno == 1){
				dispatch(activeProfileActions.updateGovernmentBenefits(res.data.government.data));
			}
			
			objRes = {...res}
		})
		.then(() => {
			dispatch(updateStatus([
				objRes.flagno || 0, 
				objRes.message || CONSTANTS.ERROR.SERVER
			]));
		})
		.catch((exception) => {
			dispatch(updateStatus([
				0, 
				exception.message + '.'
			]));
			console.log('exception: ' + exception.message);
		});
	}

import React, { Component } from 'react';
import {
    View,
    Text,
    TextInput,
    ScrollView,
    TouchableNativeFeedback
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Header3 from './header3';
import styles from './styles';
import { connect } from 'react-redux';
export class CompanyIdForm extends Component {
    constructor(props){
        super(props);
        this.state = {
            _branchName: '',
            _address: '',
            _contact: [],
            _emailAddress: ''
        }
    }

    static navigationOptions = {
        header:
            <Header3
                title= 'EMPLOYER PROFILE'
            />
    }

    render(){
        console.log('logsssssssss123',this.props);
        return(
            <View style={styles.container}>
                <ScrollView>
                    <View style={styles.formCont}>
                        <View style={[styles.nameCont, styles.defaultProp]}>
                            <Text style={styles.txtLabel}>LAST NAME</Text>
                            <Text style={styles.txtLabel}>{this.props.activecompany.lastname}</Text>
                        </View>
                        <View style={[styles.nameCont, styles.defaultProp]}>
                            <Text style={styles.txtLabel}>FIRST NAME</Text>
                            <Text style={styles.txtLabel}>{this.props.activecompany.firstname}</Text>
                        </View>
                        <View style={[styles.nameCont, styles.defaultProp]}>
                            <Text style={styles.txtLabel}>MIDDLE NAME</Text>
                            <Text style={styles.txtLabel}>{this.props.activecompany.middlename}</Text>
                        </View>
                        <View style={[styles.nameCont, styles.defaultProp]}>
                            <Text style={styles.txtLabel}>COMPANY NAME</Text>
                            <Text style={styles.txtLabel}>{this.props.activecompany.name}</Text>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

function mapStateToProps (state) {
    return {
        activecompany: state.activeCompanyReducer.activecompany,
    }
}

export default connect(
    mapStateToProps
)(CompanyIdForm)